# SPDX-FileCopyrightText: (C) 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
import multiprocessing as mp
from queue import Queue, Empty
import time
from typing import override

import paho.mqtt.client as mqtt

import hunter.events as ev
import hunter.logger_config as lc

logger = lc.get_logger()


TOPIC_ROOT = 'hunter/'


HEARTBEAT_TOPIC = TOPIC_ROOT + ev.TYPE_HEARTBEAT
HEARTBEAT_SENDER_FREQ = 30  # 20 seconds between each heartbeat (not Controller)
HEARTBEAT_SENDER_QOS = 0
HEARTBEAT_RECEIVER_QOS = 0

WORKER_EXIT_TOPIC = TOPIC_ROOT + ev.TYPE_WORKER_EXIT
WORKER_EXIT_SENDER_QOS = 1
WORKER_EXIT_RECEIVER_QOS = 1

TARGET_KILLED_TOPIC = TOPIC_ROOT + ev.TYPE_TARGET_KILLED
TARGET_KILLED_SENDER_QOS = 1
TARGET_KILLED_RECEIVER_QOS = 1

# We want the message transfer of FGMS data to be as fast as possible
# and not pile up if the Controller has a short glitch -> QoS 0
RECEIVED_FGMS_DATA_TOPIC = TOPIC_ROOT + ev.TYPE_RECEIVED_FGMS_DATA
RECEIVED_FGMS_DATA_SENDER_QOS = 0
RECEIVED_FGMS_DATA_RECEIVER_QOS = 0

CHAT_SEND_TOPIC = TOPIC_ROOT + ev.TYPE_CHAT_SEND
CHAT_SEND_SENDER_QOS = 2
CHAT_SEND_RECEIVER_QOS = 2

# Missile events are mostly for debugging - and therefore we can
# live with some of them missing
MISSILE_EVENT_TOPIC = TOPIC_ROOT + ev.TYPE_MISSILE_EVENT
MISSILE_EVENT_SENDER_QOS = 0
MISSILE_EVENT_RECEIVER_QOS = 0

DAMAGE_RESULT_TOPIC = TOPIC_ROOT + ev.TYPE_DAMAGE_RESULT
DAMAGE_RESULT_SENDER_QOS = 1
DAMAGE_RESULT_RECEIVER_QOS = 1

ATTACKER_EVENT_TOPIC = TOPIC_ROOT + ev.TYPE_ATTACKER_EVENT
ATTACKER_EVENT_SENDER_QOS = 1
ATTACKER_EVENT_RECEIVER_QOS = 1

POSITION_UPDATES_BATCH_TOPIC = TOPIC_ROOT + ev.TYPE_POSITION_UPDATES_BATCH
POSITION_UPDATES_BATCH_SENDER_QOS = 1
POSITION_UPDATES_BATCH_RECEIVER_QOS = 1

MESSAGE_ENCODING = 'utf-8'


def encode_message_payload(payload: str) -> bytes:
    return payload.encode(MESSAGE_ENCODING)


def decode_message_payload(payload: bytes) -> str:
    return payload.decode(MESSAGE_ENCODING)


class BaseMQTTClient(mqtt.Client):

    __slots__ = ('client_id', '_incoming_event_buffer')

    def __init__(self, client_id: str):
        self.client_id = client_id
        self._incoming_event_buffer = Queue()
        super().__init__(callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
                         client_id=self.client_id,
                         protocol=mqtt.MQTTv5)
        self.on_connect = self.callback_on_connect
        self.on_disconnect = self.callback_on_disconnect
        self.on_subscribe = self.callback_on_subscribe
        self.connect('localhost', port=1883)
        self.loop_start()

    def _put_on_incoming_event_buffer(self, message: mqtt.MQTTMessage) -> None:
        self._incoming_event_buffer.put(message)

    def get_next_from_incoming_event_buffer(self) -> mqtt.MQTTMessage | None:
        try:
            return self._incoming_event_buffer.get(block=False)
        except Empty:
            return None

    def _callback_on_received_custom_event(self, client, userdata, message: mqtt.MQTTMessage) -> None:
        self._put_on_incoming_event_buffer(message)

    def callback_on_connect(self, client, userdata, flags, reason_code, properties):
        logger.debug('%s: Successfully connected with result code %s: ', self.client_id, str(reason_code))
        self._subscribe_to_topics_on_connect()

    def callback_on_disconnect(self, client, userdata, disconnect_flags, reason_code, properties):
        if reason_code != 0:
            logger.info('%s: Unexpected disconnection from MQTT', self.client_id)
        else:
            logger.debug('%s: Disconnected from MQTT', self.client_id)

    def callback_on_subscribe(self, client, userdata, mid, reason_code, properties):
        logger.debug('%s: Subscribed to topic', self.client_id)

    def _subscribe_to_topics_on_connect(self) -> None:
        """Needs to be overridden by subclasses.
        subscribing in on_connect means we re-subscribe if connection is lost
        """
        raise NotImplementedError('Needs to be overridden in sub-class')

    def stop_and_disconnect(self) -> None:
        self.loop_stop()
        self.disconnect()


class WorkerExitHandler:
    slots = '_exit_requested'

    def __init__(self) -> None:
        self._exit_requested = False

    @property
    def exit_requested(self) -> bool:
        return self._exit_requested

    def set_exit_requested(self) -> None:
        logger.info('worker exit requested')
        self._exit_requested = True


class TargetMQTTClient(BaseMQTTClient):
    __slots__ = ('_worker_exit_handler_', '_callsign', '_is_fg_instance')

    def __init__(self, worker_exit_handler: WorkerExitHandler, callsign: str, is_fg_instance: bool) -> None:
        self._worker_exit_handler = worker_exit_handler
        self._callsign = callsign
        self._is_fg_instance = is_fg_instance
        super().__init__(self._callsign)
        self.message_callback_add(WORKER_EXIT_TOPIC, self._callback_on_worker_exit)

    def send_heartbeat(self, session_id: str) -> None:
        json_payload = ev.create_heartbeat_json(session_id, self.client_id)
        _ = self.publish(topic=HEARTBEAT_TOPIC, payload=encode_message_payload(json_payload),
                         qos=HEARTBEAT_SENDER_QOS)

    def send_target_killed(self, session_id: str, killed_type: ev.KilledType, notes: str | None = None) -> None:
        json_payload = ev.create_target_killed_json(session_id, self._callsign, killed_type, notes, self._is_fg_instance)
        _ = self.publish(topic=TARGET_KILLED_TOPIC, payload=encode_message_payload(json_payload),
                         qos=TARGET_KILLED_SENDER_QOS)

    def _callback_on_worker_exit(self, client, userdata, message: mqtt.MQTTMessage):
        callsign = ev.extract_callsign_from_worker_exit_json(decode_message_payload(message.payload))
        if callsign == ev.CALLSIGN_ALL or callsign == self._callsign:
            self._worker_exit_handler.set_exit_requested()

    @override
    def _subscribe_to_topics_on_connect(self) -> None:
        self.subscribe(WORKER_EXIT_TOPIC, options=mqtt.SubscribeOptions(qos=WORKER_EXIT_RECEIVER_QOS))


class LoggerMQTTClient(BaseMQTTClient):
    __slots__ = '_worker_exit_handler'

    def __init__(self, worker_exit_handler: WorkerExitHandler) -> None:
        self._worker_exit_handler = worker_exit_handler
        super().__init__('EventLoggerClient')
        self.message_callback_add(HEARTBEAT_TOPIC, self._log_event)
        self.message_callback_add(WORKER_EXIT_TOPIC, self._callback_on_worker_exit)
        self.message_callback_add(TARGET_KILLED_TOPIC, self._log_event)
        self.message_callback_add(ATTACKER_EVENT_TOPIC, self._log_event)
        self.message_callback_add(MISSILE_EVENT_TOPIC, self._log_event)
        self.message_callback_add(DAMAGE_RESULT_TOPIC, self._log_event)
        self.message_callback_add(CHAT_SEND_TOPIC, self._log_event)

    @staticmethod
    def _log_event(client, userdata, message: mqtt.MQTTMessage) -> None:
        logger.info(decode_message_payload(message.payload))

    def _callback_on_worker_exit(self, client, userdata, message: mqtt.MQTTMessage) -> None:
        self._log_event(client, userdata, message)
        callsign = decode_message_payload(message.payload)
        if callsign == ev.CALLSIGN_ALL:
            self._worker_exit_handler.set_exit_requested()

    @override
    def _subscribe_to_topics_on_connect(self) -> None:
        self.subscribe(HEARTBEAT_TOPIC, options=mqtt.SubscribeOptions(qos=HEARTBEAT_RECEIVER_QOS))
        self.subscribe(WORKER_EXIT_TOPIC, options=mqtt.SubscribeOptions(qos=WORKER_EXIT_RECEIVER_QOS))
        self.subscribe(TARGET_KILLED_TOPIC, options=mqtt.SubscribeOptions(qos=TARGET_KILLED_RECEIVER_QOS))
        self.subscribe(ATTACKER_EVENT_TOPIC, options=mqtt.SubscribeOptions(qos=ATTACKER_EVENT_RECEIVER_QOS))
        self.subscribe(MISSILE_EVENT_TOPIC, options=mqtt.SubscribeOptions(qos=MISSILE_EVENT_RECEIVER_QOS))
        self.subscribe(DAMAGE_RESULT_TOPIC, options=mqtt.SubscribeOptions(qos=DAMAGE_RESULT_RECEIVER_QOS))
        self.subscribe(CHAT_SEND_TOPIC, options=mqtt.SubscribeOptions(qos=CHAT_SEND_RECEIVER_QOS))


class EventLogger(mp.Process, WorkerExitHandler):
    """Logs a sub-set of events (e.g. Heartbeat)."""
    __slots__ = ('mqtt_client', 'log_config')

    CLIENT_NAME = 'EventLogger'

    def __init__(self, log_config: lc.LogConfig) -> None:
        mp.Process.__init__(self, name='EventLogger')
        WorkerExitHandler.__init__(self)
        self.mqtt_client: LoggerMQTTClient | None = None
        self.log_config = log_config

    @override
    def run(self) -> None:
        lc.configure_logging(self.log_config)
        global logger
        logger = lc.get_logger()

        self.mqtt_client = LoggerMQTTClient(self)
        # FIXME os.setsid()
        logger.info('%s started', self.CLIENT_NAME)
        while True:
            if self.exit_requested:
                logger.info('worker exiting: %s', self.CLIENT_NAME)
                self.mqtt_client.stop_and_disconnect()
                return
            time.sleep(0.5)
