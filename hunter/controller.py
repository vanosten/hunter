# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""
Controls all aspects of a scenario and runs the mp_targets. fg_targets (instances of FlightGear) run in workers,
but are also controlled by the Controller.

https://docs.python.org/3/library/subprocess.html
https://docs.python.org/3.7/library/multiprocessing.html

"""

import logging
import multiprocessing as mp
import random
import sys
import time
from typing import Any, Optional, override

import paho.mqtt.client as mqtt

import networkx as nx

import hunter.azure_io as aio
import hunter.damage as d
import hunter.emesary_notifications as en
import hunter.emesary_producer as ep
import hunter.events as ev
import hunter.fg_targets as fgt
import hunter.fgms_io as fio
import hunter.geometry as g
import hunter.iff_datalink as ifd
import hunter.logger_config as lc
import hunter.messages as m
import hunter.mp_carrier as mpc
import hunter.mp_dispensers as mpd
import hunter.mp_shooters as mps
import hunter.mp_targets as mpt
import hunter.mqtt_io as mqi
import hunter.scenario_tools.dem_grid as dg
import hunter.scenarios as sc
import hunter.utils as u

logger = lc.get_logger()

# http://flightgear.sourceforge.net/getstart-en/getstart-enpa2.html

GCI_TIME_LIMIT = 10  # if request is newer than this, then it is supposed to be a resend and therefore ignored
GCI_SAME_TARGET_LIMIT = 120  # if a request is newer than this, then info for the same target might be displayed
GCI_SIMPLIFIED_INTERCEPT = 5000  # if below value, then intercept vector will be calculated in a simplified way

FGMS_CLIENT_PORT = 5010

MIN_TIME_BETWEEN_INSTANCES = 60  # minimum time passed between creating FGFS instances (very CPU intensive)


class ObjectContainer:
    __slots__ = ('_last_position_update', '_last_position_processed', 'iff_hash', 'callsign')

    def __init__(self, callsign: str) -> None:
        # need to have a fake default position so properties can be accessed immediately
        self._last_position_update = m.PositionUpdate('', '', m.HealthType.fit_for_fight, g.Position(0., 0., 0.))
        self._last_position_processed = time.time()
        self.iff_hash = None
        self.callsign = callsign

    @property
    def health(self) -> m.HealthType:
        return self._last_position_update.health

    @health.setter
    def health(self, new_health: m.HealthType) -> None:
        self._last_position_update.health = new_health

    @property
    def position(self) -> g.Position:
        return self._last_position_update.position

    @property
    def last_position_update(self) -> m.PositionUpdate:
        return self._last_position_update

    @last_position_update.setter
    def last_position_update(self, position_update: m.PositionUpdate) -> None:
        self._last_position_update = position_update

    @property
    def heading(self) -> float:
        return self._last_position_update.heading

    @property
    def speed(self) -> float:
        return self._last_position_update.speed

    def process_position(self, now: float, current_pos: g.Position, callsign: str, kind: str) -> None:
        """Position object is from ATCpie and is tuple of EarthCoord and alt_ft"""
        if self._last_position_update is not None:
            # now we can also get speed and bearing
            bearing = g.calc_bearing_pos(self._last_position_update.position, current_pos)
            distance = g.calc_distance_pos(self._last_position_update.position, current_pos)
            delta_time = now - self._last_position_processed
            speed = distance / delta_time if delta_time != 0 else 0.
            pitch = g.calc_altitude_angle(self._last_position_update.position, current_pos)
            self._last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos,
                                                          bearing, pitch, speed)
        else:
            self._last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos)
        self._last_position_processed = now

    def is_zombie(self) -> bool:
        zombie = True if time.time() - self._last_position_processed > u.POS_UPDATE_FREQ_ZOMBIE_TIME else False
        if zombie:
            logger.info('%s is zombie at time %i and last_position_processed %i', self.callsign,
                        time.time(), self._last_position_processed)
        return zombie


class TargetContainer(ObjectContainer):
    __slots__ = 'emesary_callsign'

    def __init__(self, callsign: str):
        super().__init__(callsign)
        self.emesary_callsign = d.transform_to_emesary_callsign(callsign)  # cached for faster processing in loops


class MPTargetContainer(TargetContainer):
    """Container for targets represented by simulated MP instances served by the controller"""
    __slots__ = ('broker', '_mp_target')

    def __init__(self, broker: u.ParticipantMessageInterface, mp_target: mpt.MPTarget) -> None:
        super().__init__(mp_target.callsign)
        self.broker = broker  # e.g. fio.FGMSThreadedSender
        self._mp_target = mp_target

    @property
    def mp_target(self) -> Optional[mpt.MPTarget]:
        """Returns None if this is not a simulated target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget):
            return self._mp_target
        return None

    @property
    def mp_target_shooter(self) -> Optional[mps.Shooter]:  # can return a ShooterImpl
        """Returns None if this is not a simulated shooting target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget) and self._mp_target.is_shooting:
            return self._mp_target.shooter
        return None

    @property
    def mp_target_dispenser(self) -> Optional[mpd.Dispenser]:  # can return a DispenserImpl
        """Returns None if this is not a simulated dispensing target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget) and self._mp_target.is_dispensing:
            return self._mp_target.dispenser
        return None

    @property
    def mp_moving_target(self) -> Optional[mpt.MPTargetMoving]:
        """Returns None if this is not a simulated moving target, else a pointer to the object."""
        target = self._mp_target
        if target and isinstance(target, mpt.MPTargetMoving):
            return target
        return None


class FGTargetContainer(TargetContainer):
    """Container for targets represented by FGFS instances"""
    __slots__ = ('target_type', 'name', 'last_damage_result', 'shooting')

    def __init__(self, callsign: str) -> None:
        super().__init__(callsign)
        self.target_type: Optional[mpt.MPTargetType] = None  # if it is None, then this container is just a placeholder
        self.name: Optional[str] = None
        self.last_damage_result: Optional[m.DamageResult] = None  # used when FG instance is destroyed -> result resent
        self.shooting = False

    def last_damage_result_as_destroyed(self) -> Optional[m.DamageResult]:
        """If there has been a damage result registered at all, then resend the last to give the last
        pilot hitting the reward of having destroyed the target."""
        if self.last_damage_result:
            res = self.last_damage_result.copy_with_health(m.HealthType.destroyed)
            self.last_damage_result = None  # in case proxy is reused - e.g. for automat
            return res
        return None

    @property
    def placeholder(self) -> bool:
        return self.target_type is None

    def reset(self) -> None:
        """Set back to being a placeholder and not an FG target."""
        self.target_type = None
        self.name = None
        self.shooting = False

    def make_sam(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.vehicle
        self.name = name
        self.shooting = True

    def make_automat(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.plane
        self.name = name
        self.shooting = True


class AttackerContainer(ObjectContainer):
    __slots__ = ('mp_time_stamp', 'assigned_tgt', '_processed_emesary_indices',
                 'fgfs_model', 'newest_position')

    def __init__(self, fgfs_model: str, callsign: str, iff_hash: Optional[str]) -> None:
        super().__init__(callsign)
        self.iff_hash = iff_hash
        self.mp_time_stamp = 0  # MP time is not the same as local computer time!
        self.assigned_tgt = None  # callsign of target from Hunter GCI
        self._processed_emesary_indices = list()
        self.fgfs_model = fgfs_model

        # this position is constantly updated, last_position is not -> datastore
        self.newest_position = None  # g.Position

    def check_existing_emesary_index(self, msg_idx_type_id: str) -> bool:
        """Checks whether an emesary index (msg_idx plus type_id) has been seen already.
        Returns True if it has been seen already"""
        if msg_idx_type_id in self._processed_emesary_indices:
            return True
        if len(self._processed_emesary_indices) > 20:  # we do not need extensive history
            self._processed_emesary_indices.pop(0)
        self._processed_emesary_indices.append(msg_idx_type_id)
        return False


class ControllerMQTTClient(mqi.BaseMQTTClient):
    __slots__ = '_controller'

    def __init__(self, controller: 'Controller') -> None:
        self._controller = controller
        super().__init__('Controller')
        self.message_callback_add(mqi.TARGET_KILLED_TOPIC, self._callback_on_received_custom_event)
        self.message_callback_add(mqi.RECEIVED_FGMS_DATA_TOPIC, self._callback_on_received_custom_event)
        self.message_callback_add(mqi.DAMAGE_RESULT_TOPIC, self._callback_on_received_custom_event)

    @property
    def _session_id(self) -> str:
        return self._controller.session_id

    @property
    def _callsign(self) -> str:
        return self._controller.callsign  # receiver's callsign

    def send_heartbeat(self) -> None:
        json_payload = ev.create_heartbeat_json(self._session_id, self.client_id)
        _ = self.publish(topic=mqi.HEARTBEAT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.HEARTBEAT_SENDER_QOS)

    def send_target_exit(self, callsign: str) -> None:
        json_payload = ev.create_worker_exit_json(self._session_id, callsign)
        _ = self.publish(topic=mqi.WORKER_EXIT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.WORKER_EXIT_SENDER_QOS)

    def send_chat_send(self, chat_message: str, sender_callsign: Optional[str] = None) -> None:
        the_callsign = sender_callsign if sender_callsign is not None else self._callsign
        json_payload = ev.create_chat_send_json(self._session_id, the_callsign, chat_message)
        _ = self.publish(topic=mqi.CHAT_SEND_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.CHAT_SEND_SENDER_QOS)

    def send_damage_result(self, damage_result: m.DamageResult) -> None:
        json_payload = ev.create_damage_result_json(self._session_id, damage_result)
        _ = self.publish(topic=mqi.DAMAGE_RESULT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.DAMAGE_RESULT_SENDER_QOS)

    def send_attacker_event(self, attacker_event: m.AttackerEvent) -> None:
        json_payload = ev.create_attacker_event_json(self._session_id, attacker_event)
        _ = self.publish(topic=mqi.ATTACKER_EVENT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.ATTACKER_EVENT_SENDER_QOS)

    def send_position_updates_batch(self, position_updates_batch: m.PositionUpdatesBatch) -> None:
        json_payload = ev.create_position_updates_batch_json(self._session_id, position_updates_batch)
        _ = self.publish(topic=mqi.POSITION_UPDATES_BATCH_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.POSITION_UPDATES_BATCH_SENDER_QOS)

    @override
    def _subscribe_to_topics_on_connect(self) -> None:
        self.subscribe(mqi.TARGET_KILLED_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.TARGET_KILLED_RECEIVER_QOS))
        self.subscribe(mqi.RECEIVED_FGMS_DATA_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.RECEIVED_FGMS_DATA_RECEIVER_QOS))


class Controller(mp.Process):
    """The heart of Hunter controlling all targets and having much of the main logic"""
    def __init__(self, parent_conn: mp.Pipe,
                 ctrl_callsign: str, identifier: str, gci_responses: bool,
                 use_cloud: bool, max_fg_instances: int,
                 scenario_path: str, scenario_name: str, hostility: u.Hostility,
                 defender_callsigns: list[str], admin_callsigns: list[str],
                 throttle_static: int, no_list: bool,
                 log_config: lc.LogConfig) -> None:
        super().__init__(name='Controller')
        self.log_config = log_config

        if ctrl_callsign is None or len(ctrl_callsign) > 7:
            raise ValueError('The callsign for the controller must at least have one character and not more than 7')
        if identifier is None or len(identifier) > 5:
            raise ValueError('The identifier must have between 1 and 5 letters')
        self.callsign = ctrl_callsign

        self.mqtt_client: Optional[ControllerMQTTClient] = None  # initialized in run()

        self.scenario = sc.load_scenario(scenario_name, scenario_path)  # raises ValueError if it does not exist
        self.identifier = identifier
        self.mp_server_host = u.get_mp_server_address()
        if self.mp_server_host is None:
            logger.error('mp_server may not be one of the FlightGear default MP servers')
            logging.shutdown()
            sys.exit(1)
        logger.info('mp_server is set to value %s', self.mp_server_host)

        self.hostility = hostility
        if self.hostility.is_hostile:
            self.scenario.adapt_scenario_to_hostile_environment()
        # there is no network port parameter because there are many assumptions on port ranges -> u.PORT_MP_BASIS

        self.parent_conn = parent_conn  # The Pipe to the parent process (for command line CTRL-C only)
        self.attackers = dict()  # key = callsign, value = AttackerContainer (actually attackers and defenders)

        self.defender_callsigns = defender_callsigns
        self.admin_callsigns = admin_callsigns
        self.last_chat_command = dict()  # key = callsign, value = last submitted chat message
        self.mp_targets: dict[str, MPTargetContainer] = dict()  # key = target_callsign
        self.awacs_callsign: Optional[str] = None
        self.last_datalink_processed = 0
        self.tanker_callsign: Optional[str] = None
        self.carrier_callsign: Optional[str] = None

        self.fg_targets: dict[str, FGTargetContainer] = dict()  # key = callsign
        self.max_fg_instances = max_fg_instances

        # following handled during set_up() and tear_down()
        self.fgms_receiver = None
        self.number_of_mp_messages_received = 0  # used to do a bit of heart beat control
        self.gci_responses = gci_responses

        self.use_cloud = use_cloud

        self.session_id: str = '__0__'  # if run in cloud, this will be a guid set by Cosmos DB
        if self.use_cloud:
            proxy = aio.initialize_container_proxy()
            try:
                self.session_id = aio.register_new_session(proxy, self.scenario, self.mp_server_host,
                                                           self.gci_responses,
                                                           self.hostility.heli_shoot,
                                                           self.hostility.drone_shoot,
                                                           self.hostility.ship_shooting_type,
                                                           self.hostility.shilka_shoot,
                                                           self.hostility.sam_shooting_type,
                                                           self.scenario.awacs_definition is not None,
                                                           self.scenario.tanker_definition is not None,
                                                           self.scenario.carrier_definition is not None,
                                                           self.callsign, self.identifier, no_list)
            except ValueError:
                logger.exception('Cannot start controller because interaction with Cloud is not working as expected')
                logging.shutdown()
                sys.exit(1)

        # Populate assets
        random.shuffle(self.scenario.static_targets)  # make sure that there is randomness in callsigns etc

        # static targets are added one after the other with a bit of lag
        self.static_targets_to_add: list[sc.StaticTarget] = list()
        self.throttle_static = throttle_static
        self.last_next_static_poll = 0  # when has there last time been checked to add a new static target?
        self.last_next_moving_poll = 0  # when has there last time been checked to add a new moving target?
        self.last_position_processed = 0  # based on u.POS_UPDATE_FREQ_MOVING_TARGET
        self.last_send_position_update_ds = 0  # based on u.SEND_POSITION_UPDATES_TO_DS
        self.last_one_second_poll = 0  # reset every second to be able to make checks every second (run cycle is faster)

        self.run_cycles = 0  # Used for heartbeat

        self.local_damage_results: list[m.DamageResult] = list()  # local list of DamageResult messages

    # noinspection PyBroadException
    def run(self) -> None:
        """Overrides the method in Process and runs the endless loop"""
        lc.configure_logging(self.log_config)
        global logger
        logger = lc.get_logger()

        try:
            logger.info('Starting Controller processing')
            event_logger = mqi.EventLogger(self.log_config)
            event_logger.start()

            self.mqtt_client = ControllerMQTTClient(self)

            if self.use_cloud:
                db_runner = aio.AzureDBRunner(self.session_id, self.log_config)
                db_runner.start()

            # We add fg_instances as soon as possible, because (A) it will take a while due to high CPU,
            # (B) they take time to reload etc. and (C) SAMs are static and need to be before other static SAMs.
            self._populate_with_fg_instances()

            # We want the fg_instances up and running - otherwise the CPU might be starved and the
            # receiver not getting connections in time
            logger.info('Setting up receiver and start accepting messages')
            self._set_up_receiver()

            # add static and towed mp_targets - moving will be added dynamically
            # need to be added in run-methods, such that daemon for FGMSRunner is not stopped
            for static_target in self.scenario.static_targets:
                if not static_target.is_fg_instance():
                    self.static_targets_to_add.append(static_target)
            self._add_towed_targets()
            self._add_awacs()
            self._add_tanker()
            self._add_carrier()

            self.scenario.stamp_scenario_as_started()
        except:
            logger.exception('Something bad happened during start of Controller - most probably programming error')
            self._exit_session()

        try:
            while True:
                now = time.time()
                self.run_cycles += 1
                if self.run_cycles % 1000 == 0:
                    self.mqtt_client.send_heartbeat()
                    logger.debug('Cycle %i in Controller', self.run_cycles)
                if self.parent_conn.poll():
                    message = self.parent_conn.recv()
                    logger.info('Controller got message from parent (start_controller): %s', message)
                    if isinstance(message, m.ExitSession):
                        self._exit_session()
                        return
                while True:
                    mqtt_message = self.mqtt_client.get_next_from_incoming_event_buffer()
                    if mqtt_message is not None:
                        if mqtt_message.topic == mqi.TARGET_KILLED_TOPIC:
                            self.on_target_killed_event(mqtt_message)
                        elif mqtt_message.topic == mqi.RECEIVED_FGMS_DATA_TOPIC:
                            self.on_received_fgms_data_event(mqtt_message)
                        elif mqtt_message.topic == mqi.DAMAGE_RESULT_TOPIC:
                            self.on_damage_result(mqtt_message)
                        else:
                            raise ValueError('Programming logic problem: no handling of topic: ' + mqtt_message.topic)
                    else:
                        break

        # messaging from targets' FGMSSenders through Pipes
                for target_container in self.mp_targets.values():
                    while True:
                        message = target_container.broker.receive_message()
                        if message is None:
                            break  # we are done polling data from cloud right now
                        if isinstance(message, m.PositionUpdate):
                            target_container.last_position_update = message
                        else:
                            raise ValueError('Controller received message from mp_target without handler: ' + str(message))

                if self.use_cloud:
                    if (now - self.last_send_position_update_ds) > u.SEND_POSITION_UPDATES_TO_DS:
                        self.last_send_position_update_ds = now
                        position_updates = m.PositionUpdatesBatch()
                        for attacker in self.attackers.values():
                            if attacker and attacker.is_zombie() is False:
                                position_updates.add_position_update(attacker.last_position_update)
                        for target in self.mp_targets.values():
                            if target:  # not checking for zombie
                                position_updates.add_position_update(target.last_position_update)
                        for target in self.fg_targets.values():
                            if target and target.placeholder is False and target.is_zombie() is False:
                                position_updates.add_position_update(target.last_position_update)
                        self.mqtt_client.send_position_updates_batch(position_updates)

                # check whether we should dynamically add another moving target
                if (now - self.last_next_moving_poll) > self.scenario.polling_freq:
                    self.last_next_moving_poll = now
                    self._poll_next_moving_targets()

                # check whether we should dynamically add another static target
                if self.static_targets_to_add and (now - self.last_next_static_poll) > self.throttle_static:
                    self.last_next_static_poll = now
                    static_target = self.static_targets_to_add.pop()
                    self._add_static_target(static_target.target_type, static_target.position, static_target.heading)

                # datalink stuff
                self._process_datalink_data()
                # just shortly pause the endless loop; must be a low number due to maybe many MP items
                time.sleep(0.01)
                # now do stuff only needed every second or so
                if (now - self.last_one_second_poll) > 1:
                    self.last_one_second_poll = now
                    self._poll_next_trip_targets()
                    # check whether an attacker has not been seen for a while
                    to_remove_attacker_callsigns = list()
                    for callsign, container in self.attackers.items():
                        if container.is_zombie():
                            to_remove_attacker_callsigns.append(callsign)
                    for callsign in to_remove_attacker_callsigns:
                        self._process_unregister_attacker(callsign)

        except Exception:
            logger.exception('Some exception during Controller.run() occurred. Exiting the session.')
            self._exit_session()

    def _exit_session(self):
        """This should be the last thing happening. We close down stuff - even logger."""
        logger.info("Controller got exit command: shutting down dependencies and itself (takes at least 20 secs)")
        if self.fgms_receiver:
            self.mqtt_client.send_chat_send('Hunter is shutting down')
        time.sleep(2)
        self.mqtt_client.send_target_exit(ev.CALLSIGN_ALL)

        # finish up
        self._print_total_stats()
        self.mp_targets.clear()
        self.fg_targets.clear()
        self.attackers.clear()

        logger.info('Waiting 20 seconds to let all asynchronous stuff execute exit commands')
        time.sleep(20)

        logger.info('FGMS interaction stopped')
        logging.shutdown()

        self.mqtt_client.stop_and_disconnect()

    def _print_total_stats(self) -> None:
        """Prints the damage results to sys.out"""
        print('######## DAMAGE STATS ########')
        if len(self.local_damage_results) == 0:
            print('No damage registered')
        for damage_result in self.local_damage_results:
            print(damage_result)

        print('\n\n######## REMAINING STATS ########')
        remaining_ships = 0
        remaining_coasters = 0
        remaining_helis = 0
        remaining_drones = 0
        remaining_planes = 0
        remaining_towed = 0
        remaining_static = 0
        remaining_vehicles = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_target
            if tgt and tgt.target_type is mpt.MPTargetType.ship:
                remaining_ships += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.coaster:
                remaining_coasters += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.helicopter:
                remaining_helis += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.drone:
                remaining_drones += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.plane:
                remaining_planes += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.towed:
                remaining_towed += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.static:
                remaining_static += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.vehicle:
                remaining_vehicles += 1
        print('Remaining ships: {}'.format(remaining_ships))
        print('Remaining coasters: {}'.format(remaining_coasters))
        print('Remaining helicopters: {}'.format(remaining_helis))
        print('Remaining drones: {}'.format(remaining_drones))
        print('Remaining planes: {}'.format(remaining_planes))
        print('Remaining towed: {}'.format(remaining_towed))
        print('Remaining static: {}'.format(remaining_static))
        print('Remaining vehicles: {}'.format(remaining_vehicles))

    def _poll_next_moving_targets(self) -> None:
        """Checks in the scenario whether there are more targets to add dynamically.
        We return immediately if there is one to add in order not to have too many at once
        and thereby slowing the loop in Controller.
        """
        remaining_gas = 0
        remaining_ships = 0
        remaining_coasters = 0
        remaining_helis = 0
        remaining_drones = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_moving_target
            if tgt:
                if tgt.civilian:  # needs to be checked first because target_type can be many things
                    remaining_gas += 1
                elif tgt.target_type is mpt.MPTargetType.ship:
                    remaining_ships += 1
                elif tgt.target_type is mpt.MPTargetType.coaster:
                    remaining_coasters += 1
                elif tgt.target_type is mpt.MPTargetType.helicopter:
                    remaining_helis += 1
                elif tgt.target_type is mpt.MPTargetType.drone:
                    remaining_drones += 1

        add_message = self.scenario.next_moving_ship(remaining_ships)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
            return
        add_message = self.scenario.next_moving_coaster(remaining_coasters)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
            return
        add_message = self.scenario.next_moving_heli(remaining_helis)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
            return
        add_message = self.scenario.next_moving_drone(remaining_drones)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
            return
        add_message = self.scenario.next_moving_ga(remaining_gas)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)

    def _poll_next_trip_targets(self) -> None:
        activated_trip_targets = self.scenario.next_trip_targets()
        if activated_trip_targets:
            for trip_target in activated_trip_targets:
                self._add_trip_target(trip_target)

    def _add_static_target(self, name: str, pos: g.Position, hdg: float) -> None:
        # because SAMs can change characteristic, we need to check first
        if mpt.MPTarget.is_sam(name):
            if self.hostility.sam_shooting_type is u.SAMShootingType.passive:
                mp_target = mpt.MPTarget.create_target(name, pos, hdg)
                mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                mp_target.iff_code = self.scenario.iff_opfor
                mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                mp_target.shooter = mps.Shooter.create_missile_fake_shooter(mp_target)
            elif self.hostility.sam_shooting_type is u.SAMShootingType.replace_all_sams:
                mp_target = mpt.MPTarget.create_target(mpt.MPTarget.SHILKA, pos, hdg)
                mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                mp_target.iff_code = self.scenario.iff_opfor
                mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                mp_target.shooter = mps.Shooter.create_shilka_shooter(mp_target, True)
            elif name in [mpt.MPTarget.SHILKA_IR, mpt.MPTarget.BUK_M2, mpt.MPTarget.SA_3, mpt.MPTarget.S_300]:
                mp_target = mpt.MPTarget.create_target(name, pos, hdg)
                mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                mp_target.iff_code = self.scenario.iff_opfor
                mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                if mp_target.name == mpt.MPTarget.SHILKA_IR:
                    mp_target.shooter = mps.Shooter.create_missile_ir_sa_shooter(mp_target, True)
                elif mp_target.name in [mpt.MPTarget.BUK_M2, mpt.MPTarget.SA_3]:
                    mp_target.shooter = mps.Shooter.create_sam_shooter(mp_target, True)
                elif mp_target.name == mpt.MPTarget.S_300:
                    mp_target.shooter = mps.Shooter.create_s_300_shooter(mp_target, True)
            else:
                if self.hostility.sam_shooting_type is u.SAMShootingType.replace_not_simulated_sams_shilka:
                    mp_target = mpt.MPTarget.create_target(mpt.MPTarget.SHILKA, pos, hdg)
                    mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                    mp_target.iff_code = self.scenario.iff_opfor
                    mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                    mp_target.shooter = mps.Shooter.create_shilka_shooter(mp_target, True)
                elif self.hostility.sam_shooting_type is u.SAMShootingType.replace_not_simulated_sams_sa_3:
                    mp_target = mpt.MPTarget.create_target(mpt.MPTarget.SA_3, pos, hdg)
                    mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                    mp_target.iff_code = self.scenario.iff_opfor
                    mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                    mp_target.shooter = mps.Shooter.create_sam_shooter(mp_target, True)
                else:
                    mp_target = mpt.MPTarget.create_target(name, pos, hdg)
                    mp_target.callsign = self._create_callsign(mp_target.target_type.name)
                    mp_target.iff_code = self.scenario.iff_opfor
                    mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                    mp_target.shooter = mps.Shooter.create_missile_fake_shooter(mp_target)

        else:
            mp_target = mpt.MPTarget.create_target(name, pos, hdg)
            mp_target.callsign = self._create_callsign(mp_target.target_type.name)
            mp_target.iff_code = self.scenario.iff_opfor
            if mp_target.name == mpt.MPTarget.SHILKA:
                mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
                mp_target.shooter = mps.Shooter.create_shilka_shooter(mp_target, self.hostility.shilka_shoot)
        return self._add_target(mp_target)

    def _add_moving_target(self, asset: mpt.MPTargetAsset, path: nx.Graph, start_node: g.WayPoint,
                           override_speed: int = 0) -> None:
        mp_target = mpt.MPTargetMoving.create_moving_target(asset, path, start_node,
                                                            towing_speed=override_speed,
                                                            iff_code=self.scenario.iff_opfor)
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        if asset.ttype is mpt.MPTargetType.helicopter:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.dispenser = mpd.Dispenser(mp_target)
            if mp_target.is_missile_heli:
                mp_target.shooter = mps.Shooter.create_missile_ir_aa_shooter(mp_target, self.hostility.heli_shoot)
        elif asset.ttype == mpt.MPTargetType.drone:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.dispenser = mpd.Dispenser(mp_target)
            mp_target.shooter = mps.Shooter.create_missile_ir_aa_shooter(mp_target, self.hostility.drone_shoot)
        elif asset.ttype == mpt.MPTargetType.ship:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            if self.hostility.ship_shooting_type is u.ShipShootingType.passive:
                mp_target.shooter = mps.Shooter.create_frigate_shooter(mp_target, False)
            elif self.hostility.ship_shooting_type is u.ShipShootingType.only_ciws:
                mp_target.shooter = mps.Shooter.create_kashtan_shooter(mp_target, True)
            elif self.hostility.ship_shooting_type is u.ShipShootingType.ciws_or_missiles:
                if random.random() > 0.5:
                    mp_target.shooter = mps.Shooter.create_kashtan_shooter(mp_target, True)
                else:
                    mp_target.shooter = mps.Shooter.create_frigate_shooter(mp_target, True)
            else:  # only missiles
                mp_target.shooter = mps.Shooter.create_frigate_shooter(mp_target, True)
        elif asset.ttype == mpt.MPTargetType.coaster:
            pass  # nothing for now - see https://gitlab.com/vanosten/hunter/-/issues/171
        self._add_target(mp_target)

    def _add_awacs(self) -> None:
        if self.scenario.awacs_definition:
            start_node = random.choice(list(self.scenario.awacs_definition.awacs_network.nodes))
            asset = mpt.awacs_ec_137r
            mp_target = mpt.MPTargetMoving.create_moving_target(asset, self.scenario.awacs_definition.awacs_network,
                                                                start_node, iff_code=self.scenario.iff_attackers)
            mp_target.callsign = self.scenario.awacs_definition.callsign
            self.awacs_callsign = mp_target.callsign
            mp_target.dispenser = mpd.Dispenser(mp_target)
            self._add_target(mp_target, u.INDEX_AWACS)

    def _add_tanker(self) -> None:
        if self.scenario.tanker_definition:
            start_node = random.choice(list(self.scenario.tanker_definition.tanker_network.nodes))
            asset = mpt.tanker_kc_137r
            mp_target = mpt.MPTargetMoving.create_moving_target(asset, self.scenario.tanker_definition.tanker_network,
                                                                start_node, iff_code=self.scenario.iff_attackers)
            mp_target.callsign = self.scenario.tanker_definition.callsign
            self.tanker_callsign = mp_target.callsign
            mp_target.dispenser = mpd.Dispenser(mp_target)
            self._add_target(mp_target, u.INDEX_TANKER)

    def _add_carrier(self) -> None:
        if self.scenario.carrier_definition:
            mp_target = mpc.MPCarrier(self.scenario.carrier_definition.carrier_type,
                                      self.scenario.carrier_definition.sail_area,
                                      self.scenario.carrier_definition.loiter_centre)
            mp_target.iff_code = self.scenario.iff_attackers
            self.carrier_callsign = mp_target.callsign
            self._add_target(mp_target, u.INDEX_CARRIER)

    def _add_trip_target(self, mp_target: mpt.MPTargetTrips) -> None:
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        self._add_target(mp_target)
        logger.info('Added new trip target with activation delay = %i', mp_target.activation_delay)

    def _add_towed_target(self, asset: mpt.MPTargetAsset, path: nx.DiGraph, start_node: g.WayPoint,
                          override_speed: int = 0) -> None:
        towed_target = mpt.MPTargetMoving.create_moving_towed_target(asset, path, start_node, override_speed)
        towed_target.callsign = self._create_callsign(mpt.MPTargetType.towed.name)
        self._add_target(towed_target)

    def _add_towed_targets(self) -> None:
        """Add a series of towed targets.
        Towed targets are just moving targets with a lag, common speed, common start_node and common network.
        The network is directed and simple, such that the tractor and towed target always choose the same.
        There can be more than one tractor/towed combination - they just do not start at the same node.
        """
        if self.scenario.towed_number > 0:
            interval = int(len(self.scenario.towed_network.nodes)/self.scenario.towed_number)
            towed_lag = int(self.scenario.towed_dist / g.knots_to_ms(self.scenario.towed_speed))
            # add tractor towing the asset
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_moving_target(self.scenario.towing_asset, self.scenario.towed_network,
                                        start_node, self.scenario.towed_speed)
            # add distance between tractor and towed by using lag * constant speed
            time.sleep(towed_lag)
            # add the towed target
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_towed_target(self.scenario.towing_asset, self.scenario.towed_network,
                                       start_node, self.scenario.towed_speed)

    def _format_callsign(self, type_string: str, index: int) -> str:
        """Target callsign is always 7 letters and either hiding or revealing the type of the target plus 2 digits.
        If revealing: a combination of identifier (min 1 letter) + type (max 4 letters) + index (2 digits)
        If hiding: a 5-letter code + index (2 digits)
        Because the number of targets is limited, but targets are respawned, index is re-used."""
        callsign = self.identifier
        max_type = 5 - len(self.identifier)
        if type_string:
            if len(type_string) <= max_type:
                callsign += type_string
            else:
                callsign += type_string[:max_type]
        else:
            callsign += mpt.MPTargetType.unknown.name[:max_type]
        return callsign + '{:02d}'.format(index)

    def _create_callsign(self, type_string: str) -> str:
        """Finds and allocates the index number of the callsign and then formats it to a valid callsign."""
        next_index = -1
        for digit in range(u.INDEX_DYNAMIC_START, 100):
            no_existing_found = True
            # search in targets' callsigns
            for key in self.fg_targets.keys():
                try:
                    my_digits = u.number_part_of_callsign(key)
                    if digit == my_digits:
                        no_existing_found = False
                        break
                except ValueError:
                    pass  # happens if there is a carrier or tanker, which does not have 2 digits at the end of callsign
            for key in self.mp_targets.keys():
                if self.tanker_callsign and key == self.tanker_callsign:
                    continue
                if self.awacs_callsign and key == self.awacs_callsign:
                    continue
                if self.carrier_callsign and key == self.carrier_callsign:
                    continue
                my_digits = u.number_part_of_callsign(key)
                if digit == my_digits:
                    no_existing_found = False
                    break

            if no_existing_found:
                # search in defender's callsign
                for callsign in self.defender_callsigns:
                    my_digits = u.number_part_of_callsign(callsign)
                    if digit == my_digits:
                        no_existing_found = False
                        break

            if no_existing_found:
                next_index = digit
                break

        if next_index < 0:
            raise ValueError('No valid index position found - too many targets')

        return self._format_callsign(type_string, next_index)

    def _add_target(self, mp_target: mpt.MPTarget, special_index: int = 0) -> None:
        index = special_index if special_index > 0 else u.number_part_of_callsign(mp_target.callsign)
        participant = fio.FGMSParticipant(mp_target, self.mp_server_host, u.PORT_MP_BASIS,
                                          u.PORT_MP_BASIS_CLIENT_CTRL + index, self.session_id)
        logger.info('%s has send_freq %04.2f with name %s', mp_target.callsign, mp_target.send_freq, mp_target.name)
        fgms_sender = fio.FGMSMultiProcessingBroker(participant, self.log_config)
        self.mp_targets[mp_target.callsign] = MPTargetContainer(fgms_sender, mp_target)

    def _set_up_receiver(self) -> None:
        logger.info('Starting Receiver')
        receiver = mpt.MPTarget.create_receiver(self.callsign, self.scenario.centre_position,
                                                self.scenario.mp_visibility_range)
        participant = fio.FGMSParticipant(receiver, self.mp_server_host, u.PORT_MP_BASIS,
                                          u.PORT_MP_BASIS_CLIENT_CTRL + u.INDEX_CONTROLLER, self.session_id)
        self.fgms_receiver = fio.FGMSMultiProcessingBroker(participant, self.log_config)

    def on_damage_result(self, mqtt_message: mqtt.MQTTMessage) -> None:
        damage_dict = ev.extract_from_damage_result_json(mqi.decode_message_payload(mqtt_message.payload))
        self.local_damage_results.append(ev.damage_result_object_from_dict(damage_dict))

    def on_received_fgms_data_event(self, mqtt_message: mqtt.MQTTMessage) -> None:
        """Interpret data from FGMS. We are only interested in attackers, because they might shoot at targets.
        """
        fgms_data = ev.extract_from_received_fgms_avro(mqtt_message.payload)

        self.number_of_mp_messages_received += 1
        if self.number_of_mp_messages_received % 2000 == 0:
            logger.info('Received %i MP messages until now', self.number_of_mp_messages_received)
        try:
            fgms_unpacked_data = fio.decode_fgms_data_packet(fgms_data, self.mp_targets)
        except ValueError as err:
            logger.warning('Ignoring packet: %s' % err)
            return

        if fgms_unpacked_data is None:
            return

        if fgms_unpacked_data.callsign in self.fg_targets:
            attacker_and_found = False
        elif fgms_unpacked_data.callsign in self.attackers:
            attacker_and_found = True
            trigger = None
            has_opfor_iff_now = False
            result = ifd.interrogate(fgms_unpacked_data.callsign, fgms_unpacked_data.iff_hash,
                                     self.scenario.iff_opfor)
            if result is ifd.IFFType.iff_friendly:
                has_opfor_iff_now = True
            has_opfor_iff_prev = False
            result = ifd.interrogate(fgms_unpacked_data.callsign, self.attackers[fgms_unpacked_data.callsign].iff_hash,
                                     self.scenario.iff_opfor)
            if result is ifd.IFFType.iff_friendly:
                has_opfor_iff_prev = True
            if has_opfor_iff_prev is not has_opfor_iff_now:
                # NB: cannot just compare .iff_hash, because it automatically gets changed ca. every 2 minutes
                trigger = m.AttackerEventTrigger.iff_changed
            elif self.attackers[fgms_unpacked_data.callsign].fgfs_model != fgms_unpacked_data.fgfs_model:
                trigger = m.AttackerEventTrigger.plane_model_changed
            if trigger is not None:
                attacker_event = m.AttackerEvent(fgms_unpacked_data.callsign,
                                                 fgms_unpacked_data.fgfs_model,
                                                 has_opfor_iff_now, trigger)
                self.mqtt_client.send_attacker_event(attacker_event)
        else:
            self._process_register_attacker(fgms_unpacked_data.callsign, fgms_unpacked_data.fgfs_model,
                                            fgms_unpacked_data.iff_hash)
            attacker_and_found = True

        now = time.time()

        # do attacker specific stuff
        if attacker_and_found:  # process attackers all the time
            if fgms_unpacked_data.chat:
                self._process_chat_command(fgms_unpacked_data.callsign, fgms_unpacked_data.position,
                                           fgms_unpacked_data.chat)

            container = self.attackers[fgms_unpacked_data.callsign]
            if container.mp_time_stamp > 0 and (
                    fgms_unpacked_data.time_stamp < container.mp_time_stamp):
                logger.info('Dropping unordered package for %s due to negative time difference %f',
                            fgms_unpacked_data.callsign,
                            fgms_unpacked_data.time_stamp - container.mp_time_stamp)
                return  # Drop unordered UDP packet
            # make updates
            container.mp_time_stamp = fgms_unpacked_data.time_stamp
            container.fgfs_model = fgms_unpacked_data.fgfs_model  # pilot could have changed the plane
            self._process_emesary_notifications(fgms_unpacked_data.callsign, fgms_unpacked_data.encoded_notifications)
            container.newest_position = fgms_unpacked_data.position
            container.iff_hash = fgms_unpacked_data.iff_hash

            if (now - self.last_position_processed) > u.POS_UPDATE_FREQ_ATTACKER:
                self.last_position_processed = now
                container.process_position(now, fgms_unpacked_data.position, fgms_unpacked_data.callsign,
                                           container.fgfs_model)

            for mp_target_container in self.mp_targets.values():
                shooter = mp_target_container.mp_target_shooter
                dispenser = mp_target_container.mp_target_dispenser
                if shooter or dispenser:  # dispenser could also be a shooter - we do not want to send stuff twice
                    lpd = container.last_position_update
                    mp_target_container.broker.post_message(m.AttackerPosition(fgms_unpacked_data.callsign,
                                                                               fgms_unpacked_data.position,
                                                                               lpd.heading, lpd.pitch,
                                                                               fgms_unpacked_data.radar_lock,
                                                                               fgms_unpacked_data.radar_on,
                                                                               fgms_unpacked_data.chaff_on,
                                                                               fgms_unpacked_data.flares_on,
                                                                               fgms_unpacked_data.time_stamp,
                                                                               fgms_unpacked_data.iff_hash))

        # do the position updates (the ones used for data store etc.)
        if (now - self.last_position_processed) > u.POS_UPDATE_FREQ_FG_TARGET:
            self.last_position_processed = now
            if fgms_unpacked_data.callsign in self.fg_targets:
                container = self.fg_targets[fgms_unpacked_data.callsign]
                kind = container.name
                container.process_position(now, fgms_unpacked_data.position, fgms_unpacked_data.callsign, kind)

    def _process_register_attacker(self, callsign: str, fgfs_model: str, iff_hash: Optional[str]) -> None:
        self.attackers[callsign] = AttackerContainer(fgfs_model, callsign, iff_hash)
        logger.info('Registered attacker %s flying %s', callsign, fgfs_model)
        has_opfor_iff = False
        if iff_hash:
            result = ifd.interrogate(callsign, iff_hash, self.scenario.iff_opfor)
            if result is ifd.IFFType.iff_friendly:
                has_opfor_iff = True
        attacker_event = m.AttackerEvent(callsign, fgfs_model, has_opfor_iff, m.AttackerEventTrigger.register)
        self.mqtt_client.send_attacker_event(attacker_event)

    def _process_unregister_attacker(self, callsign: str) -> None:
        logger.info('Unregistering %s flying %s: not seen for at least %i seconds', callsign,
                    self.attackers[callsign].fgfs_model, u.POS_UPDATE_FREQ_ZOMBIE_TIME)
        container = self.attackers[callsign]
        has_opfor_iff = False
        if container.iff_hash:
            result = ifd.interrogate(callsign, container.iff_hash, self.scenario.iff_opfor)
            if result is ifd.IFFType.iff_friendly:
                has_opfor_iff = True
        attacker_event = m.AttackerEvent(callsign, container.fgfs_model, has_opfor_iff, m.AttackerEventTrigger.zombie)
        self.mqtt_client.send_attacker_event(attacker_event)
        del self.attackers[callsign]

    def _process_chat_command(self, caller_callsign: str, caller_pos: g.Position,
                              chat_command: Optional[str]) -> None:
        """Process the content of chat messages from registered admin callsigns."""
        if not chat_command:
            return
        executed = False
        looks_like_command = False
        try:
            if caller_callsign in self.last_chat_command and chat_command == self.last_chat_command[caller_callsign]:
                return  # chat message will be sent several times by MP protocol or by mistake from pilot
            else:
                self.last_chat_command[caller_callsign] = chat_command
            split = chat_command.strip().split()
            if split and len(split) >= 2 and split[0].strip() == '$' and caller_callsign in self.admin_callsigns:
                looks_like_command = True
                command_str = split[1].strip().lower()
                if command_str == 'td':
                    callsign = split[2].strip()
                    for key, mp_container in self.mp_targets.items():
                        if key == callsign:
                            self.mqtt_client.send_target_exit(callsign)
                            executed = True
                            break
                    for key, fg_target_container in self.fg_targets.items():
                        if key == callsign:
                            self.mqtt_client.send_target_exit(callsign)
                            executed = True
                            break
                elif command_str == 'an':
                    callsign = split[2].strip()
                    number_of_bullets = int(split[3].strip())
                    eban = ep.process_outgoing_armament_hit_notification(time.time(), 11111, 4, number_of_bullets,
                                                                         callsign, True)
                    encoded_notifications = {0: eban.encoded_message}
                    self._process_emesary_notifications(self.admin_callsigns[0], encoded_notifications)

            elif split and len(split) >= 2 and split[0].strip() == '@':
                looks_like_command = True
                command_str = split[1].strip().lower()
                if command_str.startswith('c_'):
                    carrier_msg = None
                    max_dist = g.nm_to_metres(10000)
                    if command_str == 'c_ctlo':
                        carrier_msg = m.CarrierCommand.command_loiter()
                    elif command_str == 'c_ctre':
                        speed = int(split[2].strip())
                        carrier_msg = m.CarrierCommand.command_recovery(speed)
                    elif command_str == 'c_ctla':
                        speed = int(split[2].strip())
                        carrier_msg = m.CarrierCommand.command_launch(speed)
                    elif command_str == 'c_ctna':
                        course = int(split[2].strip())
                        carrier_msg = m.CarrierCommand.command_navigate(course)
                    elif command_str == 'c_dlon':
                        carrier_msg = m.CarrierCommand.command_deck_lights(True)
                    elif command_str == 'c_dloff':
                        carrier_msg = m.CarrierCommand.command_deck_lights(False)
                    elif command_str == 'c_flon':
                        carrier_msg = m.CarrierCommand.command_flood_lights(True)
                    elif command_str == 'c_floff':
                        carrier_msg = m.CarrierCommand.command_flood_lights(False)
                    elif command_str == 'c_wset':
                        hdg = int(split[2])
                        speed = int(split[3])
                        carrier_msg = m.CarrierCommand.command_wind(hdg, speed)
                    elif command_str == 'c_in':
                        carrier_msg = m.CarrierCall(caller_callsign, m.CarrierCallType.inbound)
                        max_dist = g.nm_to_metres(50)
                    elif command_str == 'c_sy':
                        carrier_msg = m.CarrierCall(caller_callsign, m.CarrierCallType.see_you_at_10)
                        max_dist = g.nm_to_metres(10)
                    elif command_str == 'c_pos':
                        carrier_msg = m.CarrierPositionRequest(caller_callsign, caller_pos)
                        max_dist = g.nm_to_metres(1000)
                    if carrier_msg and self.carrier_callsign:
                        container = self.mp_targets[self.carrier_callsign]
                        dist = g.calc_distance_pos(container.position, caller_pos)
                        if dist <= max_dist:
                            container.broker.post_message(carrier_msg)
                            executed = True
                elif command_str.startswith('t_'):
                    if self.tanker_callsign:
                        container = self.mp_targets[self.tanker_callsign]
                        if command_str == 't_pos':
                            msg = "{}, {}, bearing to my position: Fly ca. {} deg true for ca. {} nm. Angels {}."
                            bearing = int(g.calc_bearing(caller_pos.lon, caller_pos.lat,
                                                         container.position.lon, container.position.lat))
                            dist = int(g.metres_to_nm(g.calc_distance(caller_pos.lon, caller_pos.lat,
                                                      container.position.lon, container.position.lat)))
                            # adding a bit to make sure that rounding does not change angels downwards
                            angels = int(g.metres_to_feet(container.position.alt_m + 100)/1000)
                            msg = msg.format(caller_callsign, container.callsign, bearing, dist, angels)
                            self.mqtt_client.send_chat_send(msg, self.tanker_callsign)
                            executed = True
                elif command_str.startswith('g_'):
                    executed = self._process_attacker_gci_requests(caller_callsign, command_str)

        except Exception:  # you never know what happens if string are processed
            logger.exception('Something went wrong in processing a chat command')
        if executed:
            logger.info('Command "%s" from %s successfully executed', chat_command, caller_callsign)
        elif looks_like_command:
            msg = 'Not understood command from {}: {}. Does the command and the target exist? All parameters submitted?'
            self.mqtt_client.send_chat_send(msg.format(caller_callsign, chat_command))

    def _process_emesary_notifications(self, req_callsign: str, encoded_notifications: dict[int, Any]) -> None:
        """Process incoming Emesary notifications - mostly hit/collision messages.
        """
        if not encoded_notifications:
            return
        attacker_container = self.attackers[req_callsign]
        decoded_notifications = dict()
        for value in encoded_notifications.values():
            if value != '':
                # noinspection PyBroadException
                try:
                    decoded_notifications.update(en.process_incoming_message(value))
                except Exception:
                    logger.exception('Cannot process emesary notification for %s', req_callsign)
        for idx, notification in decoded_notifications.items():
            if attacker_container.check_existing_emesary_index(idx):
                continue
            logger.info('New msg_idx_type_id=%s -> msg=%s', idx, notification)
            if notification.type_id == en.ArmamentNotification_Id and notification.kind == en.KIND_COLLISION:
                target_found = False
                # first test the MP targets
                for key, mp_container in self.mp_targets.items():
                    if mp_container.emesary_callsign == notification.remote_callsign:
                        logger.info('Processing armament notification from %s for MP target %s: %s',
                                    req_callsign, key, notification)
                        mp_container.broker.post_message(m.HitNotification(notification, req_callsign,
                                                                           attacker_container.fgfs_model, key))
                        target_found = True
                        break
                # next test the FG targets
                if not target_found:
                    for key, fg_container in self.fg_targets.items():
                        if fg_container.emesary_callsign == notification.remote_callsign:
                            logger.info('Processing armament notification for FG target %s: %s', key, notification)
                            weapon_type, name = d.weapon_type_from_notification(notification, fg_container.target_type)
                            # hit -> we do not know better - and send new damage if FGTargetDestroyed received
                            health = m.HealthType.hit
                            fg_container.health = health
                            damage = m.DamageResult(health, weapon_type, name,
                                                    req_callsign, attacker_container.fgfs_model,
                                                    key, fg_container.name, False, True, fg_container.shooting)
                            fg_container.last_damage_result = damage
                            self.mqtt_client.send_damage_result(damage)
                            break
            elif notification.type_id == en.ArmamentInFlightNotification_Id:
                for key, mp_container in self.mp_targets.items():
                    if mp_container.emesary_callsign == notification.remote_callsign:
                        dispenser = mp_container.mp_target_dispenser
                        if dispenser:
                            mp_container.broker.post_message(notification)
                            logger.info('Processing armament in flight notification from %s for MP target %s: %s',
                                        req_callsign, key, notification)
                        break
            elif notification.type_id == en.ObjectInFlightNotification_Id:
                pass  # currently we do nothing - Chaff/Flare is only visual info - shooters process another property

    def _process_attacker_gci_requests(self, req_callsign: str, chat_command_str: str) -> bool:
        """Based on chat command GCI request find a suitable target and assign to pilot."""
        type_ = m.map_command_to_type(chat_command_str)
        if type_ is m.GCIRequestType.unknown:
            return False
        if not self.gci_responses:
            self.mqtt_client.send_chat_send('Not serving GCI requests')
            return True

        attacker_container = self.attackers[req_callsign]
        if attacker_container is None:
            logger.warning('Programming error somewhere or just bad luck in timing')
            return True
        if type_ is m.GCIRequestType.clear:
            attacker_container.assigned_tgt = None
            self.mqtt_client.send_chat_send(f'{req_callsign}, target assignment is cleared.')
            return True

        tgt_container = None
        do_update_existing = False
        if attacker_container.assigned_tgt:
            if attacker_container.assigned_tgt in self.mp_targets:
                tgt_container = self.mp_targets[attacker_container.assigned_tgt]
                do_update_existing = True
                health = tgt_container.health
            else:
                health = m.HealthType.dead  # should only happen if removed in the meantime
            if health in [m.HealthType.destroyed, m.HealthType.dead]:
                if attacker_container.assigned_tgt is None:
                    msg = '{}, target assignment {} is probably dead - cleared - you can request a new assignment.'
                    self.mqtt_client.send_chat_send(msg.format(req_callsign, attacker_container.assigned_tgt))
                return True

        if tgt_container is None:  # find a candidate
            assigned_to_others = list()  # these are blocked and should not be used for this attacker
            for attacker_container in self.attackers.values():
                if attacker_container.assigned_tgt:
                    assigned_to_others.append(attacker_container.assigned_tgt)

            closest_distance = 9999999
            # mp targets
            for callsign, mpt_container in self.mp_targets.items():
                tgt = mpt_container.mp_target
                if tgt is not None:  # static target is None
                    if mpt_container.health in [m.HealthType.destroyed, m.HealthType.dead]:
                        continue
                    if tgt.civilian:
                        continue
                    if self.carrier_callsign and self.carrier_callsign == callsign:
                        continue
                    if self.tanker_callsign and self.tanker_callsign == callsign:
                        continue
                    if self.awacs_callsign and self.awacs_callsign == callsign:
                        continue
                    if type_ is m.GCIRequestType.plane and tgt.target_type is not mpt.MPTargetType.plane:
                        continue
                    elif type_ is m.GCIRequestType.helicopter and tgt.target_type is not mpt.MPTargetType.helicopter:
                        continue
                    elif type_ is m.GCIRequestType.drone and tgt.target_type is not mpt.MPTargetType.drone:
                        continue
                    elif type_ is m.GCIRequestType.offshore_ship and tgt.target_type is not mpt.MPTargetType.ship:
                        continue
                    elif type_ is m.GCIRequestType.coastal_ship and tgt.target_type is not mpt.MPTargetType.coaster:
                        continue
                    # sam must be checked before vehicle and static
                    elif type_ is m.GCIRequestType.sam and mpt.MPTarget.is_sam(tgt.name) is False:
                        continue
                    elif type_ is m.GCIRequestType.vehicle:
                        if mpt.MPTarget.is_sam(tgt.name) or tgt.target_type is not mpt.MPTargetType.vehicle:
                            continue
                    elif type_ is m.GCIRequestType.static:
                        if mpt.MPTarget.is_sam(tgt.name) or tgt.target_type is not mpt.MPTargetType.static:
                            continue
                    # now we know it is a candidate - but is it closer than the existing candidate?
                    if type_ in [m.GCIRequestType.plane, m.GCIRequestType.helicopter, m.GCIRequestType.drone]:
                        _, eta = g.calc_intercept(attacker_container.position, attacker_container.speed,
                                                  mpt_container.position, mpt_container.speed, mpt_container.heading,
                                                  GCI_SIMPLIFIED_INTERCEPT)
                        if 0 < eta < closest_distance:  # if eta = 0 then cannot be reached by interceptor
                            closest_distance = eta
                            tgt_container = mpt_container
                    else:
                        distance = g.calc_distance_pos(attacker_container.position, mpt_container.position)
                        if distance < closest_distance:
                            closest_distance = distance
                            tgt_container = mpt_container

        if tgt_container is None:
            msg = '{}, no suitable candidate found - consider requesting a different type.'
            self.mqtt_client.send_chat_send(msg.format(req_callsign))
            return True
        # we have a new valid candidate or can update for the existing assignment
        attacker_container.assigned_tgt = tgt_container.callsign
        gci_resp = m.GCIResponse(type_, req_callsign, tgt_container.callsign, do_update_existing)
        dist = g.calc_distance_pos(attacker_container.position, tgt_container.position)
        gci_resp.set_range(dist)
        altitude = tgt_container.position.alt_m
        gci_resp.set_altitude_m(altitude)
        bearing = g.calc_bearing_pos(attacker_container.position, tgt_container.position)
        gci_resp.set_bearing(bearing)
        aspect = g.calc_aspect(bearing, tgt_container.heading)
        gci_resp.set_aspect(aspect)
        if type_ in [m.GCIRequestType.plane, m.GCIRequestType.helicopter, m.GCIRequestType.drone]:
            vector, eta = g.calc_intercept(attacker_container.position,
                                           attacker_container.speed,
                                           tgt_container.position, tgt_container.speed, tgt_container.heading,
                                           GCI_SIMPLIFIED_INTERCEPT)
            gci_resp.set_vector(vector)
            gci_resp.set_eta_s(eta)
        if type_ is m.GCIRequestType.static:
            gci_resp.set_lon_lat(tgt_container.position.lon, tgt_container.position.lat)
        self.mqtt_client.send_chat_send(str(gci_resp))
        return True

    def _process_datalink_data(self) -> None:
        """Prepare the datalink data for AWACS - but only if there is an AWACS and not all the time."""
        now = time.time()
        if self.awacs_callsign and (now - self.last_datalink_processed) > 10:
            self.last_datalink_processed = now
            data = dict()
            for callsign, container in self.attackers.items():
                data[callsign] = ifd.interrogate(callsign, container.iff_hash, self.scenario.iff_attackers)
            for callsign, container in self.mp_targets.items():
                if container.mp_target and container.mp_target.iff_code:
                    # we only show flying stuff, e.g. no shooting ships or shooting SAMs
                    if container.mp_target.target_type in [mpt.MPTargetType.helicopter,
                                                           mpt.MPTargetType.drone,
                                                           mpt.MPTargetType.plane]:
                        if container.mp_target.iff_code == self.scenario.iff_attackers:
                            data[callsign] = ifd.IFFType.iff_friendly
                        else:
                            data[callsign] = ifd.IFFType.iff_hostile
            # no processing of fg_targets because SAMs/ships do not have IFF and
            # automats are not yet implemented
            encoded_data = ifd.encode_datalink_data(self.awacs_callsign, self.scenario.datalink_attackers, data)

            for callsign, container in self.mp_targets.items():
                if callsign == self.awacs_callsign:
                    container.broker.post_message(m.DatalinkData(encoded_data))

    def _populate_with_fg_instances(self) -> None:
        if self.max_fg_instances <= 0:
            return
        index = 0

        # preallocate the callsigns
        pre_allocated_indexes = set()
        for pos in range(self.max_fg_instances):
            continue_search = True
            while continue_search:
                callsign_index = random.randint(u.INDEX_DYNAMIC_START, 99)
                if callsign_index not in pre_allocated_indexes:
                    pre_allocated_indexes.add(callsign_index)
                    continue_search = False

        # automats get first priority
        if self.scenario.automats:
            for automat_target in self.scenario.automats:  # List[AutomatTarget]
                if self.max_fg_instances <= 0:
                    break
                logger.info('Controller requested an automat: %s', automat_target.craft.name)
                index += 1
                self.max_fg_instances -= 1

                callsign = self._format_callsign('fgfs', pre_allocated_indexes.pop())
                props_string = automat_target.create_properties_string(callsign, self.scenario.iff_opfor,
                                                                       self.scenario.datalink_opfor)
                automat = fgt.AutomatAsset(index, self.mp_server_host, self.session_id, callsign,
                                           self.scenario.icao, automat_target.number_of_lives,
                                           props_string, False)
                container = FGTargetContainer(callsign)
                container.make_automat(automat_target.craft.name)
                automat.start()
                self.fg_targets[callsign] = container
                logger.info('Adding and starting new "%s" with callsign %s', automat_target.craft.name, callsign)
                time.sleep(MIN_TIME_BETWEEN_INSTANCES)  # because it is very CPU intensive

        # Now populating the world with missile SAMs running in a worker.
        # The SAMs in a scenario have a priority assigned: the higher the priority (1 is highest) the more likely
        # a static asset will be transformed into a shooting SAM - as long as there is capacity in fg_targets.
        if self.scenario.static_targets:
            prio_1 = list()
            prio_2 = list()
            prio_3 = list()
            for static_target in self.scenario.static_targets:
                if mpt.MPTarget.static_has_fg_instance(static_target.target_type):
                    if static_target.missile_priority == 0:
                        continue
                    elif static_target.missile_priority == 1:
                        prio_1.append(static_target)
                    elif static_target.missile_priority == 2:
                        prio_2.append(static_target)
                    else:
                        prio_3.append(static_target)

            while self.max_fg_instances > 0:
                index += 1
                self.max_fg_instances -= 1
                if prio_1:
                    chosen_static_target = prio_1.pop()
                elif prio_2:
                    chosen_static_target = prio_2.pop()
                elif prio_3:
                    chosen_static_target = prio_3.pop()
                else:
                    break  # no point in looping if no priorities left
                chosen_static_target.mark_as_fg_instance()
                logger.info('Controller requested a SAM: %s', chosen_static_target.target_type)
                callsign = self._format_callsign('fgfs', pre_allocated_indexes.pop())
                sam = fgt.SurfaceAirMissile(index, self.mp_server_host, self.session_id,
                                            callsign, chosen_static_target.target_type,
                                            chosen_static_target.position, chosen_static_target.heading, False)
                container = FGTargetContainer(callsign)
                container.make_sam(sam.aero)
                sam.start()
                self.fg_targets[callsign] = container
                logger.info('Adding and starting new "%s" with callsign %s', sam.aero, callsign)
                time.sleep(MIN_TIME_BETWEEN_INSTANCES)  # because it is very CPU intensive

    def on_target_killed_event(self, mqtt_message: mqtt.MQTTMessage) -> None:
        callsign, killed_type, notes, is_fg_instance = ev.extraxt_from_target_killed_json(mqi.decode_message_payload(mqtt_message.payload))

        if killed_type is ev.KilledType.damaged_destroyed and is_fg_instance and callsign in self.fg_targets:
            fg_target_container = self.fg_targets[callsign]
            fg_target_container.health = m.HealthType.destroyed
            damage = fg_target_container.last_damage_result_as_destroyed()
            if damage:
                self.mqtt_client.send_damage_result(damage)
        else:
            if self.tanker_callsign and self.tanker_callsign == callsign:
                self.tanker_callsign = None
            elif self.awacs_callsign and self.awacs_callsign == callsign:
                self.awacs_callsign = None
            elif self.carrier_callsign and self.carrier_callsign == callsign:
                self.carrier_callsign = None
            if callsign in self.mp_targets:
                del self.mp_targets[callsign]
            elif callsign in self.fg_targets:
                del self.fg_targets[callsign]
