# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""
Common target attributes including damage/health.

MP assets without FGFS instance / pure emulation over MP.
"""
from collections import deque
from dataclasses import dataclass
import time
from enum import IntEnum, unique
from math import atan, cos, degrees, fabs, pow, sin
from os import path
import random
from typing import Any, Optional, override

import networkx as nx
from networkx.algorithms.shortest_paths.weighted import dijkstra_path
from networkx.exception import NetworkXNoPath, NodeNotFound

import hunter.iff_datalink as ifd
import hunter.geometry as g
from hunter.damage import BROKEN_RATE, TargetWithDamage
import hunter.logger_config as lc
from hunter.messages import HealthType, is_capable_of_fighting_health
import hunter.network_stuff as ns
import hunter.utils as u

logger = lc.get_logger()


MP_RECEIVER_TIMEOUT = 5  # quite a large number, normally lag should not be that of a problem
MP_RECEIVER_MAX_BACKOFF_CYCLES = 6  # 1+2+4+8+16+32+64=127 seconds
MP_RECEIVER_MAX_HEARTBEAT_WAIT = 180  # must be larger than the time backoff takes


def extract_fgfs_model_base_name(model: str) -> str:
    fgfs_model = path.basename(model)
    if fgfs_model.endswith('.xml'):
        fgfs_model = fgfs_model[:-4]
    return fgfs_model


@unique
class MPTargetType(IntEnum):
    """The target type determines whether and how a target can move."""
    receiver = 0  # handled as building in most situations
    static = 10
    vehicle = 20
    ship = 30  # an offshore ship using an offshore_network
    coaster = 35  # a smaller ship using a coastal network
    helicopter = 40
    drone = 50
    plane = 60
    towed = 70
    unknown = 80


# for moving targets
TURN_MAX_SPEED_REDUCTION_VEHICLE = 0.2  # slow down to this fraction of the max_speed
TURN_MAX_SPEED_REDUCTION_SHIP = 0.3
TURN_MAX_SPEED_REDUCTION_COASTER = 0.3
TURN_MAX_SPEED_REDUCTION_HELICOPTER = 0.5
TURN_MAX_SPEED_REDUCTION_DRONE = 0.7
TURN_MAX_SPEED_REDUCTION_PLANE = 0.9


DEATH_TIME_UNKNOWN = 120  # seconds - when it is not known or maybe not even used
DEATH_TIME_VEHICLE = 180  # seconds
DEATH_TIME_FIGHTER_PLANE = 180  # seconds
# OPRF admin missile-frigate has a sink rate 0.025 feet every 1/20 seconds = ca. 0.15 m/s
# If a frigate is 20 m high, then 20/0.15 = ca. 140 seconds
# We show the MP stuff a bit longer
DEATH_TIME_SHIP = 600  # seconds - also used in FG ships before removed
DEATH_TIME_COASTER = 300  # seconds
SINK_DEPTH = 20  # metres
PITCH_RATE = 0.1  # results in max 60 degrees during DEATH_TIME_SHIP
ROLL_RATE = 0.3  # results in max 180 degrees during DEATH_TIME_SHIP
SPEED_SHIP = 11  # m/s, ca. 21 knots cruise speed
SPEED_COASTER = 7  # m/s

# Below values are not used in FG instances directly, only in proxies and in mp_targets
MAX_HITPOINTS_NOT_VULNERABLE = 2000000  # practicably invulnerable

MAX_HITPOINTS_VEHICLE = 10
MAX_HITPOINTS_LIGHT_ARMOURED = 20
MAX_HITPOINTS_FIGHTER_PLANE = 100
MAX_HITPOINTS_LARGE_SAM = 60
MAX_HITPOINTS_TANK = 80
MAX_HITPOINTS_S_300 = 80
MAX_HITPOINTS_FRIGATE = 300  # an Exocet (364 HP) can kill it when hit directly, it takes 2 direct hits by Maverick
MAX_HITPOINTS_CARRIER = MAX_HITPOINTS_NOT_VULNERABLE
MAX_HITPOINTS_AUTOMAT = 6  # corresponds to R-60
MAX_HITPOINTS_HEAVY_PLANE = 40
MAX_HITPOINTS_TANKER = MAX_HITPOINTS_HEAVY_PLANE
MAX_HITPOINTS_AWACS = MAX_HITPOINTS_HEAVY_PLANE
FULL_DAMAGE_DIST_VEHICLE = 4
FULL_DAMAGE_DIST_FIGHTER_PLANE = 8
FULL_DAMAGE_DIST_LARGE_VEHICLE = 8
FULL_DAMAGE_DIST_LARGE_SAM = 8
FULL_DAMAGE_DIST_FRIGATE = 40
FULL_DAMAGE_DIST_AMPHIBIOUS_ASSAULT = 15
FULL_DAMAGE_DIST_CARRIER = 50
FULL_DAMAGE_DIST_AUTOMAT = 3
FULL_DAMAGE_DIST_TANKER = 20


class MPTarget(TargetWithDamage):
    """A target you can shoot at emulated through MP."""
    RECEIVER = 'receiver'

    DEPOT = 'depot'  # depot-2, 0 -- small warehouse with fence around it
    COMPOUND = 'compound'  # depot-2, 1
    GASOMETER = 'gasometer'  # depot-2, 2 -- "Fuel" in menu
    HANGAR = 'hangar'  # depot-1, 3 aka hg25
    WAREHOUSE = 'warehouse'  # depot-2, 4 -- aka. "industry" in xml
    POWER_PLANT = 'power_plant'  # depot-2, 5, aka "pool" in xml
    BRIDGE = 'bridge'  # depot-2, 6
    CONTAINERS = 'containers'  # depot-2, 7
    LIGHT_HANGAR = 'light_hangar'  # depot-2, 9 aka "lh"
    HQ = 'hq'  # depot-2, 11, aka "flat" in xml
    FACTORY = 'factory'  # depot-2, 15, aka "fact2" in xml
    FUEL_FARM = 'fuel_farm'  # depot-2, 16, aka "farm" in xml

    TOWER = 'tower'  # depot-1, 8, aka "tour" in xml
    BUNKER = 'bunker'  # depot-1, 10
    CONTARGET = 'contarget'  # depot-1, 12, aka "tgt" or ContainerTarget. A bit like CONTAINERS
    DOUBLE_SHELTER = 'double_shelter'  # depot-1, 14, aka "double"
    HARD_SHELTER = 'hard_shelter'  # depot-1, 17
    RADAR_STATION = 'radar_station'  # depot-1, 20
    COM_TOWER = 'com_tower'  # depot-1, 21

    OILRIG = 'oilrig'  # depot-3, 19
    CHECKPOINT = 'checkpoint'  # depot-4, 18

    HILL_TARGET = 'hill_target'
    CLIFF_TARGET = 'cliff_target'
    WATER_TARGET = 'water_target'
    # GCI radar cannot be set because the gci_listener in planes gets confused which one to pick
    TRUCK = 'truck'  # Humvee / hummer, groundtarget=0 - in FG UI: Humvee
    M1_TANK = 'm1_tank'  # groundtarget=1 - in FG UI: Tank
    MLRS_ROCKET_LAUNCHER = 'mlrs'  # groundtarget=2 - in FG UI: Rocket launcher
    BRADLEY_TANK = 'bradley_tank'  # groundtarget=3 - in FG UI: Tracked fighting vehicle -> (small tank)
    BUFFALO_MINE = 'buffalo_mine'  # groundtarget=4 - in FG UI: Mine protected vehicle -> like a truck
    STRYKER_TANK = 'stryker_tank'  # groundtarget=5 - in FG UI: Armoured personnel carrier

    VIGGEN_STATIC = 'jsbsim-aj37'

    # potentially shooting SAMS - if this is extended, then the SAM_VARIANTS dict at the end of this module
    # needs to be extended as well
    BUK_M2 = 'BUK-M2'
    S_200 = 'S-200'
    S_300 = 'S-300'
    PATRIOT = 'MIM-104D'
    DVINA = 'S-75'
    SA_6 = 'SA-6'
    SA_3 = 'SA-3'
    # other shooting
    SHILKA = 'zsu-23'
    SHILKA_IR = 'ZSU-IR'  # a fake Shilka shooting short range IR missiles

    SPEEDBOAT = 'speedboat'

    position: g.Position
    orientation = g.Orientation
    z_offset: float  # offset in metres for the z-position.
    by_cannon: bool  # whether the target was destroyed by cannon
    # The z_offset is applied by FGMSSender, such that the position can be kept static or based on routes.

    def __init__(self, target_type: MPTargetType, name: str, aircraft_model: str,
                 max_hit_points: int, full_damage_dist_m: float, radar_range: int = 1) -> None:
        super().__init__(max_hit_points, full_damage_dist_m)
        self.target_type = target_type
        self.name = name
        self.aircraft_model = aircraft_model
        self.radar_range = radar_range  # default, so FGMS sends as little network traffic as possible

        # other props set outside of init
        self.radar_enabled = False
        self.radar_lock = ''  # a hashed string if the radar locks on something
        self.civilian = False
        self.extra_prop_id = None
        self.extra_prop_value = None
        self.livery = None

        # damage
        self.death_time = 0
        self.death_sink = 0
        self.death_roll_rate = 0  # only used if target also sinks
        self.death_pitch_rate = 0  # ditto
        self.broken_timestamp = 0  # set when broken to have a basis for slowing down moving targets
        self.destroyed_timestamp = 0  # set when destroyed to have a basis for eventually removing this target
        self.hp = self.max_hp

        self.z_offset = 0.

        self.shooter = None  # associated object which handles the shooting
        self.dispenser = None  # associated object which handles the dispensing of defensive countermeasures

        self.cruise_speed = 0.  # the current speed - only > 0 if MPMovingTarget

        self.emesary_msg_idx = 1

        self._send_freq = None  # gets calculated once -> see property send_freq

        self.dem = None  # a dem_grid.DigitalElevationModel

        self.chat_message_handler = u.ChatMessageHandler()

        self.iff_code = None
        self.datalink_data = None  # only used by AWACS

    def next_emesary_msg_idx(self) -> int:
        self.emesary_msg_idx += 1
        return self.emesary_msg_idx

    @property
    def is_tanker(self) -> bool:
        return self.name == tanker_kc_137r.name

    @property
    def is_awacs(self) -> bool:
        return self.name == awacs_ec_137r.name

    @property
    def is_carrier(self) -> bool:
        return self.name == 'org.flightgear.fgaddon.trunk.vinson'

    @property
    def is_shooting(self) -> bool:
        """If true then this MP target is shooting at attackers - sending data by emesary or at least pretending"""
        return self.shooter is not None

    @property
    def is_really_shooting(self) -> bool:
        """If true then this MP target is really shooting at attackers - not only simulating."""
        if self.shooter and self.shooter.releases_ammunition:
            return True
        return False

    def is_shooting_missiles(self) -> bool:
        if self.shooter is None:
            return False
        return self.shooter.shoots_missiles

    @staticmethod
    def is_sam(name: str) -> bool:
        return name in [MPTarget.BUK_M2, MPTarget.S_200, MPTarget.S_300, MPTarget.PATRIOT,
                        MPTarget.DVINA, MPTarget.SA_6, MPTarget.SA_3, MPTarget.SHILKA_IR]

    @staticmethod
    def static_has_fg_instance(name: str) -> bool:
        return name in [MPTarget.BUK_M2, MPTarget.S_200, MPTarget.S_300, MPTarget.PATRIOT,
                        MPTarget.DVINA, MPTarget.SA_6, MPTarget.SA_3,
                        MPTarget.SHILKA]

    @staticmethod
    def is_sam_or_shilka(name: str) -> bool:
        return MPTarget.is_sam(name) or name in [MPTarget.SHILKA]

    def is_shilka(self) -> bool:
        return self.name == MPTarget.SHILKA

    def is_sam_s_300(self) -> bool:
        return self.name == MPTarget.S_300

    def is_sam_sa_3(self) -> bool:
        return self.name == MPTarget.SA_3

    def is_sam_sa_6(self) -> bool:
        return self.name == MPTarget.SA_6

    def is_sam_patriot(self) -> bool:
        return self.name == MPTarget.PATRIOT

    @property
    def is_dispensing(self) -> bool:
        """If true then this MP target is dispensing chaff/flare as a reaction to attackers - sending data by emesary"""
        return self.dispenser is not None

    @property
    def is_moving(self) -> bool:
        """Needs to be overwritten by subclasses, which are moving"""
        return False

    @property
    def send_freq(self) -> float:
        """How long it takes between sending data to MP.
        0.5 means twice per second, 2.0 means every other second.
        NB: all movement calculations are based on movement per second. Therefore, if the send_freq is different
        from 1.0, then the calculations of movement have to be adapted.
        The send_freq value is calculated only once to save some CPU cycles, as it is used often.
        """
        if self._send_freq is None:
            if self.is_shooting_missiles():
                self._send_freq = 0.2
            elif self.is_shooting:
                self._send_freq = 0.5
            elif self.is_dispensing:
                self._send_freq = 0.5
            elif self.is_awacs:
                self._send_freq = 0.5
            elif self.is_tanker:
                self._send_freq = 0.2  # to make the tanker smoother and easier to follow
            elif self.is_carrier:
                self._send_freq = 0.2  # need high frequency due to submit of mp_properties and minimize other stuff
            elif self.is_moving:
                if isinstance(self, MPTargetTrips):
                    self._send_freq = 1.0  # currently set to this value and prohibits shooting
                else:  # MPTargetMoving can be anything, as method update_position_variables has been adapted
                    self._send_freq = 1.0
            elif self.target_type is MPTargetType.receiver:
                self._send_freq = 0.5
            else:  # it is a static target just sitting around
                # just to send position to MP and make MP know that still alive (should never be >= 10)
                self._send_freq = 3.0
        return self._send_freq

    def enable_radar(self) -> None:
        self.radar_enabled = True

    def disable_radar(self) -> None:
        self.release_radar_lock()
        self.radar_enabled = False

    def release_radar_lock(self) -> None:
        self.radar_lock = ''

    def lock_radar_on_callsign(self, callsign: str) -> None:
        hashed = u.create_radar_hash_for_callsign(callsign)
        self.radar_lock = hashed

    @classmethod
    def create_receiver(cls, callsign: str, pos: g.Position, mp_visibility_range: int) -> 'MPTarget':
        model = 'Aircraft/depot/Models/depot.xml'  # not using GCI model to not confuse real OPRF GCI assets

        instance = cls(MPTargetType.receiver, cls.RECEIVER, model, cls.INVULNERABLE_HP, 0, mp_visibility_range)
        instance.callsign = callsign
        instance.position = pos
        instance.orientation = g.Orientation(0)
        instance.z_offset = -1000.  # we do not want the controller to be shown
        instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
        instance.extra_prop_value = 0
        return instance

    @classmethod
    def _create_vehicle(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        hit_points = MAX_HITPOINTS_LIGHT_ARMOURED
        damage_dist = FULL_DAMAGE_DIST_VEHICLE
        prop_val = None
        if name == cls.TRUCK:
            prop_val = 0
        elif name == cls.M1_TANK:
            prop_val = 1
            hit_points = MAX_HITPOINTS_TANK
        elif name == cls.MLRS_ROCKET_LAUNCHER:
            prop_val = 2
        elif name == cls.BRADLEY_TANK:
            prop_val = 3
            hit_points = MAX_HITPOINTS_TANK
        elif name == cls.BUFFALO_MINE:
            prop_val = 4
            damage_dist = FULL_DAMAGE_DIST_LARGE_VEHICLE
        elif name == cls.STRYKER_TANK:
            prop_val = 5
            hit_points = MAX_HITPOINTS_TANK
        if prop_val is None:
            raise NotImplementedError('{} is not yet available as a static vehicle target.'.format(name))
        instance = cls(MPTargetType.vehicle, name, 'Aircraft/groundtarget/Models/truck.xml', hit_points, damage_dist)
        instance.position = pos
        instance.orientation = g.Orientation(hdg)
        instance.death_time = DEATH_TIME_VEHICLE
        instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
        instance.extra_prop_value = prop_val
        return instance

    @classmethod
    def _create_static_plane(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        aircraft_model = ''
        livery = None
        if name == cls.VIGGEN_STATIC:
            aircraft_model = 'Aircraft/JA37/Models/AJS37-Viggen.xml'
            livery = 'ferris'
        instance = cls(MPTargetType.plane, name, aircraft_model,
                       MAX_HITPOINTS_FIGHTER_PLANE, FULL_DAMAGE_DIST_FIGHTER_PLANE)
        instance.position = pos
        instance.position.alt_m += 1.8
        instance.orientation = g.Orientation(hdg)
        instance.death_time = DEATH_TIME_FIGHTER_PLANE
        if livery:
            instance.livery = livery
        return instance

    @classmethod
    def _create_sam_or_shilka(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        if name == cls.SHILKA:
            instance = cls(MPTargetType.vehicle, cls.SHILKA, 'Aircraft/ZSU-23/Models/ZSU-23-4M.xml',
                           MAX_HITPOINTS_TANK, FULL_DAMAGE_DIST_VEHICLE)
            instance.position = pos
        elif name == cls.SHILKA_IR:
            instance = cls(MPTargetType.vehicle, cls.SHILKA_IR, 'Aircraft/ZSU-23/Models/ZSU-23-4M.xml',
                           MAX_HITPOINTS_TANK, FULL_DAMAGE_DIST_VEHICLE)
            instance.position = pos
        elif name == cls.BUK_M2:
            # https://github.com/l0k1/oprf_assets/blob/master/BUK-M2/BUK-M2-set.xml
            instance = cls(MPTargetType.vehicle, cls.BUK_M2, 'Aircraft/BUK-M2/Models/buk-m2.xml',
                           80,  # hp_max
                           2.25)  # full_damage_dist_m
        elif name == cls.S_200:
            instance = cls(MPTargetType.vehicle, cls.S_200, 'Aircraft/S-200/Models/s-200.xml',
                           MAX_HITPOINTS_LARGE_SAM, FULL_DAMAGE_DIST_LARGE_SAM)
        elif name == cls.S_300:
            instance = cls(MPTargetType.vehicle, cls.S_300, 'Aircraft/S-300/Models/s-300.xml',
                           MAX_HITPOINTS_LARGE_SAM, FULL_DAMAGE_DIST_LARGE_SAM)
        elif name == cls.PATRIOT:
            instance = cls(MPTargetType.vehicle, cls.PATRIOT, 'Aircraft/MIM-104D/Models/MIM104D.xml',
                           MAX_HITPOINTS_LARGE_SAM, FULL_DAMAGE_DIST_LARGE_SAM)
        elif name == cls.DVINA:
            instance = cls(MPTargetType.vehicle, cls.DVINA, 'Aircraft/S-75/Models/S-75.xml',
                           MAX_HITPOINTS_LIGHT_ARMOURED, FULL_DAMAGE_DIST_VEHICLE)
        elif name == cls.SA_6:
            instance = cls(MPTargetType.vehicle, cls.SA_6, 'Aircraft/SA-6/Models/SA-6.xml',
                           MAX_HITPOINTS_LIGHT_ARMOURED, FULL_DAMAGE_DIST_VEHICLE)
        elif name == cls.SA_3:
            instance = cls(MPTargetType.vehicle, cls.SA_3, 'Aircraft/SA-3/Models/SA-3.xml',
                           MAX_HITPOINTS_LIGHT_ARMOURED, FULL_DAMAGE_DIST_VEHICLE)
        else:
            raise NotImplementedError('{} is not yet available as a static vehicle target.'.format(name))
        instance.position = pos
        instance.orientation = g.Orientation(hdg)
        instance.death_time = DEATH_TIME_VEHICLE
        instance.enable_radar()
        return instance

    @classmethod
    def create_target(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        if name in [cls.TRUCK, cls.M1_TANK, cls.MLRS_ROCKET_LAUNCHER, cls.BRADLEY_TANK, cls.BUFFALO_MINE,
                    cls.STRYKER_TANK]:
            return cls._create_vehicle(name, pos, hdg)
        elif cls.is_sam_or_shilka(name):
            return cls._create_sam_or_shilka(name, pos, hdg)
        elif name in [cls.VIGGEN_STATIC]:
            return cls._create_static_plane(name, pos, hdg)
        elif name in [cls.HILL_TARGET, cls.CLIFF_TARGET, cls.WATER_TARGET]:
            prop_val = 0
            full_damage_dist_m = 5
            if name == cls.CLIFF_TARGET:
                prop_val = 1
                full_damage_dist_m = 4
            elif name == cls.WATER_TARGET:
                prop_val = 3
                full_damage_dist_m = 4
            instance = cls(MPTargetType.static, name, 'Aircraft/hunter/Models/hunter.xml', cls.STATIC_HP,
                           full_damage_dist_m)
            instance.position = pos
            instance.orientation = g.Orientation(hdg)
            instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
            instance.extra_prop_value = prop_val
            return instance

        prop_val = None
        max_hit_points = 0
        full_damage_dist_m = 0

        model_name = 'Aircraft/depot/Models/depot.xml'  # depot-2
        model_depot_1 = 'Aircraft/depot/Models/struct.xml'
        if name == cls.DEPOT:
            prop_val = 0
            max_hit_points = 40
            full_damage_dist_m = 12
        elif name == cls.COMPOUND:
            prop_val = 1
            max_hit_points = 35
            full_damage_dist_m = 10
        elif name == cls.GASOMETER:
            prop_val = 2
            max_hit_points = 80
            full_damage_dist_m = 20
            # there is no prop_val = 3
        elif name == cls.HANGAR:
            prop_val = 3
            max_hit_points = 60
            full_damage_dist_m = 12
            model_name = model_depot_1
        elif name == cls.WAREHOUSE:
            prop_val = 4
            max_hit_points = 100
            full_damage_dist_m = 32
        elif name == cls.POWER_PLANT:
            prop_val = 5
            max_hit_points = 600
            full_damage_dist_m = 40
        elif name == cls.BRIDGE:
            prop_val = 6
            max_hit_points = 150
            full_damage_dist_m = 30
        elif name == cls.CONTAINERS:
            prop_val = 7
            max_hit_points = 25
            full_damage_dist_m = 6
        elif name == cls.TOWER:
            prop_val = 8
            max_hit_points = 60
            full_damage_dist_m = 12
            model_name = model_depot_1
        elif name == cls.LIGHT_HANGAR:
            prop_val = 9
            max_hit_points = 80
            full_damage_dist_m = 20
            model_name = model_depot_1
        elif name == cls.BUNKER:
            prop_val = 10
            max_hit_points = 800
            full_damage_dist_m = 15
            model_name = model_depot_1
        elif name == cls.HQ:
            prop_val = 11
            max_hit_points = 250
            full_damage_dist_m = 20
        elif name == cls.CONTARGET:
            prop_val = 12
            max_hit_points = cls.STATIC_HP
            full_damage_dist_m = 6
            model_name = model_depot_1
        # there is no prop_val = 13
        elif name == cls.DOUBLE_SHELTER:
            prop_val = 14
            max_hit_points = 700
            full_damage_dist_m = 22
            model_name = model_depot_1
        elif name == cls.FACTORY:
            prop_val = 15
            max_hit_points = 300
            full_damage_dist_m = 30
        elif name == cls.FUEL_FARM:
            prop_val = 16
            max_hit_points = 20
            full_damage_dist_m = 12
        elif name == cls.HARD_SHELTER:
            prop_val = 17
            max_hit_points = 725
            full_damage_dist_m = 16
            model_name = model_depot_1
        elif name == cls.CHECKPOINT:
            prop_val = 18
            max_hit_points = 35
            full_damage_dist_m = 12
            model_name = 'Aircraft/depot/Models/point.xml'
        elif name == cls.OILRIG:
            prop_val = 19
            max_hit_points = 300
            full_damage_dist_m = 30
            model_name = 'Aircraft/depot/Models/rig.xml'
        elif name == cls.RADAR_STATION:
            prop_val = 20
            max_hit_points = 50
            full_damage_dist_m = 8
            model_name = model_depot_1
        elif name == cls.COM_TOWER:
            prop_val = 21
            max_hit_points = 50
            full_damage_dist_m = 8
        if prop_val is None:
            raise NotImplementedError('{} is not yet available as a static target.'.format(name))

        instance = cls(MPTargetType.static, name, model_name, max_hit_points, full_damage_dist_m)
        instance.death_time = 300
        instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
        instance.extra_prop_value = prop_val
        instance.position = pos
        instance.position.alt_m += 1.  # make sure that Mirage sees it, because very picky on x/y/z above ground
        instance.orientation = g.Orientation(hdg)
        if instance.name == cls.RADAR_STATION:
            instance.enable_radar()
        return instance

    @property
    def fgfs_model(self) -> str:
        return extract_fgfs_model_base_name(self.aircraft_model)

    def is_alive(self) -> bool:
        """Checks the health status and controls heart-beat.

        And updates the z_offset if the target is destroyed, but not dead."""
        if self.target_type is MPTargetType.receiver:
            return True
        if self.health is HealthType.dead:
            return False
        if self.health is HealthType.destroyed:
            time_passed = time.time() - self.destroyed_timestamp
            if time_passed > self.death_time:
                self.health = HealthType.dead
                return False
            if self.death_sink > 0:
                self.z_offset = -1 * self.death_sink * time_passed / self.death_time
                self.orientation.pitch = self.death_pitch_rate * time_passed
                self.orientation.roll = self.death_roll_rate * time_passed
            return True
        else:  # all other states like fit_for_fight, hit, broken
            # make sure that if assets are not visible, then they should not exist
            # especially broken helicopters/planes, because they fall out of the sky (and not yet destroyed)
            if self.position.alt_m < -200:  # -200 to account for places on Earth below MSL
                if is_capable_of_fighting_health(self.health):
                    logger.warning('Set fit or hit target to dead because below surface')
                self.health = HealthType.dead  # because under surface we do not want any effects
                return False
            return True

    def update_position_variables(self) -> None:
        """Updates the instance variables, which provide input to the first part of the position message.

        https://wiki.flightgear.org/Multiplayer_protocol#First_part
        """
        pass

    def prepare_mp_properties(self) -> dict[int, Any]:
        """Provides asset and instance specific properties and triggers updates of the first part of the position msg.

        Position etc. will be read by FGMS sender from instance variables. But properties not.
        For SAMs randomly toggle radar on and off (only if not broken)."""
        self.update_position_variables()

        smoke = 0
        if self.target_type is not MPTargetType.receiver and self.health is HealthType.destroyed:
            smoke = 1
        # radar logic for those which are not shooters
        if self.target_type is not MPTargetType.receiver and self.shooter is None:
            if self.health in [HealthType.broken, HealthType.destroyed]:
                self.disable_radar()

        props: dict[int, Any] = dict()
        if self.name == self.VIGGEN_STATIC:
            if smoke > 0:
                props[u.MP_PROP_GENERIC_BOOL_BASE + 3] = True  # smoke
            props[u.MP_PROP_GENERIC_BOOL_BASE + 12] = False if smoke > 0 else True  # Wings
            props[u.MP_PROP_GEAR_0_POSITION_NORM] = 1
            props[u.MP_PROP_GEAR_1_POSITION_NORM] = 1
            props[u.MP_PROP_GEAR_2_POSITION_NORM] = 1
        else:
            props[u.MP_PROP_OPRF_SMOKE] = smoke

        if self.radar_enabled:
            props[u.MP_PROP_OPRF_RADAR] = 0  # counterintuitive, but that is the OPRF way
        else:
            props[u.MP_PROP_OPRF_RADAR] = 1
        props[u.MP_PROP_OPRF_RADAR_LOCK] = self.radar_lock
        if self.extra_prop_id is not None:
            props[self.extra_prop_id] = self.extra_prop_value
        if self.livery:
            props[u.MP_PROP_LIVERY] = self.livery

        self.chat_message_handler.get_chat_message(props)

        if self.iff_code:
            props[u.MP_PROP_OPFOR_IFF_HASH] = ifd.calculate_iff_hash(self.callsign, self.iff_code)
        if self.datalink_data:
            props[u.MP_PROP_OPFOR_DATALINK] = self.datalink_data

        return props

    def fail_systems(self, probability: float, factor: float = 100, by_cannon: bool = False) -> HealthType:
        """Depending on hit_impact fail systems in target.

        Based on fail_systems in damage.nas.
        """
        now = time.time()
        if self.target_type is MPTargetType.receiver:
            return HealthType.missed
        if self.health in [HealthType.destroyed, HealthType.dead]:  # cannot get points on what is already dead
            return HealthType.missed

        final_factor = 50 if by_cannon else factor  # reduce damage for rockets and cannon
        reduction = final_factor * probability * (0.75 + random.random() * 0.25)
        self.hp -= reduction
        if self.hp <= 0:
            self.health = HealthType.destroyed
            self.destroyed_timestamp = now
            if self.broken_timestamp == 0:  # because health can go directly from fit/hit to destroyed
                self.broken_timestamp = now
        else:
            if self.hp > BROKEN_RATE * self.max_hp:
                self.health = HealthType.hit
            else:
                self.health = HealthType.broken
                self.broken_timestamp = now
        return self.health

    def attach_dem(self, dem) -> None:  # : Optional[dg.DigitalElevationModel]
        self.dem = dem


MISSILE_FRIGATE_NAME = 'missile-frigate'

HELI_MIL_MI_24 = 'mil_mi_24'
HELI_KA_50 = 'ka50'


@dataclass(frozen=True)
class MPTargetAsset:
    ttype: MPTargetType
    name: str  # /sim/aero
    model: str  # the path to the model file
    max_hit_points: int  # given a direct hit, how much pounds of warhead is needed to kill (see description damage.nas)
    full_damage_dist_m: int  # For assets this should be average radius of the asset (see description damage.nsa)
    radar: bool  # whether the target is using an active radar (which can be seen by an attacker)
    cruise_speed: float  # m/s. 1 knot ca. 0.51 m/s. To be able to shoot as flying target at least 60 kt / 31 m/s needed
    acceleration: float  # m/s2
    turn_rate: float  # degrees per second
    death_time: int  # how many sec the thing burns until it is gone. After that time the asset is also removed from MP
    death_sink: int  # how many metres the asset sinks during the death time - 0 if no sinking
    extra_prop_id: Optional[int]
    extra_prop_value: Optional[Any]
    livery: Optional[str]  # can be None, else file name of livery
    civilian: bool  # whether this is a civil target. Default = False -> military.


oprf_frigate = MPTargetAsset(MPTargetType.ship, MISSILE_FRIGATE_NAME,
                             'Aircraft/missile-frigate/Models/missile_frigate.xml',
                             MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_FRIGATE,
                             True, SPEED_SHIP, .1, 2.,
                             DEATH_TIME_SHIP, SINK_DEPTH, None, None, None, False)
us_navy_san_antonio = MPTargetAsset(MPTargetType.ship, 'san_antonio', 'Aircraft/u.s.navy/Models/USS-SanAntonio.xml',
                                    MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_FRIGATE,
                                    True, SPEED_SHIP, .1, 2.,
                                    DEATH_TIME_SHIP, SINK_DEPTH, None, None, None, False)
us_navy_oliver_perry = MPTargetAsset(MPTargetType.ship, 'oliver_perry', 'Aircraft/u.s.navy/Models/USS-OliverPerry.xml',
                                     MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_FRIGATE,
                                     True, SPEED_SHIP, .1, 2.,
                                     DEATH_TIME_SHIP, SINK_DEPTH, None, None, None, False)
us_navy_normandy = MPTargetAsset(MPTargetType.ship, 'normandy', 'Aircraft/u.s.navy/Models/USS-NORMANDY.xml',
                                 MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_FRIGATE,
                                 True, SPEED_SHIP, .1, 2.,
                                 DEATH_TIME_SHIP, SINK_DEPTH, None, None, None, False)
us_navy_lake_champlain = MPTargetAsset(MPTargetType.ship, 'lake_champlain',
                                       'Aircraft/u.s.navy/Models/USS-LakeChamplain.xml',
                                       MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_FRIGATE,
                                       True, SPEED_SHIP, .1, 2.,
                                       DEATH_TIME_SHIP, SINK_DEPTH, None, None, None, False)
# amphibious_assault = MPTargetAsset(MPTargetType.coaster, 'amphibious_assault',
#                                   'Models/Maritime/Military/Amphibious_assault_ship.xml',
#                                   MAX_HITPOINTS_FRIGATE, FULL_DAMAGE_DIST_AMPHIBIOUS_ASSAULT,
#                                   True, SPEED_COASTER, .1, 2.,
#                                   DEATH_TIME_COASTER, SINK_DEPTH, None, None, None, False)
inflatable_speedboat = MPTargetAsset(MPTargetType.coaster, 'inflatable_speedboat',
                                     'Aircraft/hunter/Models/hunter.xml',
                                     MAX_HITPOINTS_LIGHT_ARMOURED, FULL_DAMAGE_DIST_VEHICLE,
                                     False, 15, .5, 10., DEATH_TIME_COASTER, SINK_DEPTH,
                                     u.MP_PROP_OPRF_SELECT, 10, None, False)

blue_mq_9 = MPTargetAsset(MPTargetType.drone, 'MQ-9', 'Aircraft/MQ-9/Models/MQ-9.xml', 20, 10,
                          False, 100., 2., 5.,
                          20, 0, None, None, None, False)
red_mi_8 = MPTargetAsset(MPTargetType.helicopter, 'Mi-8', 'Aircraft/Mil-Mi-8/Models/Mil-Mi-8.xml', 20, 8,
                         False, 60., 2., 7.,
                         20, 0, None, None, None, False)
red_mi_24 = MPTargetAsset(MPTargetType.helicopter, HELI_MIL_MI_24, 'Aircraft/Mil-Mi-24/Models/mi24.xml', 10, 3,
                          False, 70., 2., 7.,
                          20, 0, None, None, None, False)
red_ka_50 = MPTargetAsset(MPTargetType.helicopter, HELI_KA_50, 'Aircraft/Ka-50/Models/ka50.xml', 10, 3,
                          False, 70., 2., 7.,
                          20, 0, None, None, None, False)
pc9 = MPTargetAsset(MPTargetType.plane, 'pc9', 'Aircraft/PC-9M/Models/PC-9M.xml', 20, 3,
                    False, 120., 4., 15.,
                    20, 0, None, None, 'SwissAF1', False)
f20 = MPTargetAsset(MPTargetType.plane, 'f20', 'Aircraft/F-20/Models/f-20C.xml', 30, 4,
                    False, 150., 4., 15.,
                    20, 0, None, None, 'Suisse', False)
# TODO: f-20 has an extra property: /sim/model/livery/F-20-texture = "LiveriesF20C/Suisse.png" -> but which MP ID?
ga_c172p = MPTargetAsset(MPTargetType.plane, 'c172p', 'Aircraft/c172p/Models/c172-common.xml', 10, 3,
                         False, 62, 2., 10.,
                         20, 0, None, None, None, True)
ga_dhc6 = MPTargetAsset(MPTargetType.plane, 'dhc6', 'Aircraft/dhc6/Models/DHC6.xml', 10, 5,
                        False, 80, 2., 10.,
                        20, 0, None, None, None, True)
ga_ec130b4 = MPTargetAsset(MPTargetType.helicopter, 'ec130b4', 'Aircraft/ec130/Models/ec130b4.xml', 10, 3,
                           False, 60, 2., 10.,
                           30, 0, None, None, None, True)
ga_a320neo = MPTargetAsset(MPTargetType.plane, 'A320neo-PW', 'Aircraft/A320-family/Models/A320neo-PW.xml', 40, 20,
                           True, 200., 4., 5.,
                           30, 0, None, None, None, True)  # A320neo-PW
ga_b737ng = MPTargetAsset(MPTargetType.plane, '738', 'Aircraft/737-800/Models/738.xml', 40, 20,
                          True, 200., 4., 5.,
                          30, 0, None, None, None, True)
tanker_kc_137r = MPTargetAsset(MPTargetType.plane, 'KC-137R', 'Aircraft/KC-137R/Models/KC-137R.xml',
                               MAX_HITPOINTS_TANKER, 20,
                               True, g.knots_to_ms(280), 4., 2.0,
                               30, 0, None, None, None, False)
awacs_ec_137r = MPTargetAsset(MPTargetType.plane, 'EC-137R', 'Aircraft/KC-137R/Models/EC-137R.xml',
                              MAX_HITPOINTS_AWACS, 20,
                              True, g.knots_to_ms(280), 4., 2.0,
                              30, 0, None, None, None, False)

TOWED_TURN_RATE = 1


class MPTargetMoving(MPTarget):
    """A moving target, which moves from way-point to way-point, but does not follow the path's geometry strictly.

    Suitable for targets in air or water.
    Two types of using the network:
    [A] Towed: Follows a simple DiGraph from one waypoint to another. Both tractor and towed do this.
    [B] Not towed: Travels on a network from start to destination on shortest path, such that
    there are not too many random changes of direction.

    # Values checked against https://github.com/l0k1/oprf_assets 2019-04-21 version commit 2fa118c
    """
    def __init__(self, target_type: MPTargetType, name: str, aircraft_model: str,
                 max_hit_points: int, full_damage_dist_m: int) -> None:
        super().__init__(target_type, name, aircraft_model, max_hit_points, full_damage_dist_m)
        self.network = None  # nx.Graph
        self.shortest_path = deque()  # the currently used shortest path
        self.shortest_path_initialized = False  # whether a first shortest path has been found at all
        self.next_path_waypoints = None  # the list of waypoints for the next shortest path
        self.current_wp = None  # g.WayPoint - the way-point we are flying towards
        self.next_wp = None  # g.WayPoint - the way-point after the current one
        self.next_position = None  # g.Position - the position we will be at in 1 second
        self.max_cruise_speed = 0.  # depends on the actual asset's official cruise_speed
        self.acceleration = 0.
        self.current_acceleration = 0.  # is always 0. for towed, but can change otherwise
        self.turn_rate = 0.
        self.towed = False
        self.fixed_altitude = None  # if != None, then this moves constantly at delta_altitude

    @property
    def is_moving(self) -> bool:  # overwrites property in MPTarget
        return True

    @property
    def is_missile_heli(self) -> bool:
        return self.name in [HELI_MIL_MI_24, HELI_KA_50]

    @classmethod
    def create_target(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        raise NotImplementedError('Cannot be called on moving target.')

    def _initialise_properties_from_asset(self, asset: MPTargetAsset, towing_speed: int) -> None:
        """Sets this instance's properties based on the asset.

        Should only be called from a class method creating a new instance.
        """
        self.cruise_speed = asset.cruise_speed
        self.max_cruise_speed = asset.cruise_speed
        self.acceleration = asset.acceleration
        self.turn_rate = asset.turn_rate
        if towing_speed > 0:
            self.cruise_speed = towing_speed
            self.max_cruise_speed = towing_speed
            self.turn_rate = TOWED_TURN_RATE
            self.towed = True

        if asset.radar:
            self.enable_radar()
        self.death_time = asset.death_time
        self.death_sink = asset.death_sink
        if self.death_sink > 0:  # calculate once so it is available when we need it
            plus_min = random.randint(0, 1)
            plus_min = 1 if plus_min == 1 else -1
            self.death_roll_rate = random.random() * ROLL_RATE * plus_min
            self.death_pitch_rate = random.random() * PITCH_RATE * plus_min
        self.livery = asset.livery

        if asset.extra_prop_id:
            self.extra_prop_id = asset.extra_prop_id
            self.extra_prop_value = asset.extra_prop_value

        # calculate a delta altitude for general aviation as the network will be based on ground elevation
        if asset in [ga_a320neo, ga_b737ng]:  # airliners
            self.fixed_altitude = 4000 + random.randint(-3, 3) * 200
        elif asset in [ga_c172p, ga_dhc6, ga_ec130b4]:
            self.fixed_altitude = 500 + random.randint(-3, 3) * 30

    def _initialise_location(self, graph: nx.Graph, start_node: g.WayPoint) -> None:
        self.network = graph
        if self.is_awacs or self.is_tanker:
            my_speed = tanker_kc_137r.cruise_speed if self.is_tanker else awacs_ec_137r.cruise_speed
            wp = list(self.network.nodes)[0]
            gs_ms = g.kias_to_gs(wp.alt_ft, my_speed)
            self.cruise_speed = gs_ms
            self.max_cruise_speed = gs_ms

        self.position = g.Position(start_node.lon, start_node.lat, start_node.alt_m)
        # for the first second we will just stay static
        self.next_position = self.position
        if self.fixed_altitude is not None:
            self.position.alt_m = self.fixed_altitude

        self.current_wp = start_node
        bearing = g.calc_bearing(start_node.lon, start_node.lat, self.current_wp.lon, self.current_wp.lat)
        self.orientation = g.Orientation(bearing)

    def _find_next_shortest_path(self, start_wp: g.WayPoint):
        way_points = ns.find_next_shortest_path(self.network, start_wp, self.callsign)
        for wp in way_points:
            self.shortest_path.append(wp)

    @classmethod
    def create_moving_target(cls, asset: MPTargetAsset, graph: nx.Graph, start_node: g.WayPoint,
                             towing_speed: int = 0, iff_code: Optional[int] = None) -> 'MPTargetMoving':
        if asset.ttype in [MPTargetType.helicopter, MPTargetType.drone, MPTargetType.plane,
                           MPTargetType.coaster, MPTargetType.ship]:
            instance = cls(asset.ttype, asset.name, asset.model,
                           asset.max_hit_points, asset.full_damage_dist_m)
        else:
            raise NotImplementedError('{} is not yet available as a moving target.'.format(asset.name))

        if asset.civilian:
            instance.civilian = True

        instance.iff_code = iff_code
        instance._initialise_properties_from_asset(asset, towing_speed)
        instance._initialise_location(graph, start_node)
        return instance

    @classmethod
    def create_moving_towed_target(cls, asset: MPTargetAsset, graph: nx.DiGraph, start_node: g.WayPoint,
                                   towing_speed: int = 0) -> 'MPTargetMoving':
        instance = cls(MPTargetType.towed, 'sack', 'Aircraft/hunter/Models/hunter.xml', cls.STATIC_HP, 3)

        instance._initialise_properties_from_asset(asset, towing_speed)
        instance._initialise_location(graph, start_node)

        instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
        instance.extra_prop_value = 2
        return instance

    def prepare_mp_properties(self) -> dict[int, Any]:
        props = super().prepare_mp_properties()
        if self.target_type is MPTargetType.helicopter:
            rpm = 0.
            if is_capable_of_fighting_health(self.health):
                rpm = 265.
            elif self.health in [HealthType.broken, HealthType.destroyed]:
                rpm = 20.
            props[u.MP_PROP_ROTOR_MAIN] = rpm
            props[u.MP_PROP_ROTOR_TAIL] = rpm
            if self.name == ga_ec130b4:
                props[u.MP_PROP_GENERIC_INT_BASE + 2] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 3] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 5] = 1

        elif self.target_type is MPTargetType.drone:
            n2 = 0.
            if is_capable_of_fighting_health(self.health):
                n2 = 600
            elif self.health in [HealthType.broken, HealthType.destroyed]:
                n2 = 60
            props[u.MP_PROP_ENGINE_0_N2] = n2
        elif self.target_type is MPTargetType.plane:
            props[u.MP_PROP_GEAR_0_POSITION_NORM] = 0
            props[u.MP_PROP_GEAR_1_POSITION_NORM] = 0
            props[u.MP_PROP_GEAR_2_POSITION_NORM] = 0
            if self.name == pc9.name:
                props[u.MP_PROP_ENGINE_0_RPM] = 1000
            elif self.name == ga_dhc6.name:
                props[u.MP_PROP_ENGINE_0_N2] = 1000
                props[u.MP_PROP_ENGINE_1_N2] = 1000
                # lights
                props[u.MP_PROP_GENERIC_INT_BASE + 2] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 6] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 7] = 1
            elif self.name == ga_c172p.name:
                props[u.MP_PROP_ENGINE_0_N1] = 1000
                # lights
                props[u.MP_PROP_GENERIC_INT_BASE + 12] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 13] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 14] = 1
            elif self.name == ga_a320neo.name:
                # lights
                props[u.MP_PROP_GENERIC_INT_BASE + 5] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 7] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 9] = 1
            elif self.name == tanker_kc_137r.name:  # based on common.xml ca. line 240
                props[u.MP_PROP_TANKER] = 1
                # lights
                props[u.MP_PROP_GENERIC_INT_BASE + 3] = 1
                props[u.MP_PROP_GENERIC_INT_BASE + 4] = 1
                # probes/booms
                props[u.MP_PROP_GENERIC_INT_BASE + 10] = 1  # right probe
                props[u.MP_PROP_GENERIC_INT_BASE + 11] = 1  # left probe
                props[u.MP_PROP_GENERIC_FLOAT_BASE + 18] = 1  # boom
            elif self.name == awacs_ec_137r.name:  # based on common.xml ca. line 240
                props[u.MP_PROP_GENERIC_INT_BASE + 17] = 1  # rotate radar

        return props

    def _min_rate_of_max_speed_in_turn(self) -> float:
        if self.target_type is MPTargetType.vehicle:
            return TURN_MAX_SPEED_REDUCTION_VEHICLE
        elif self.target_type is MPTargetType.ship:
            return TURN_MAX_SPEED_REDUCTION_SHIP
        elif self.target_type is MPTargetType.coaster:
            return TURN_MAX_SPEED_REDUCTION_COASTER
        elif self.target_type is MPTargetType.helicopter:
            return TURN_MAX_SPEED_REDUCTION_HELICOPTER
        elif self.target_type is MPTargetType.drone:
            return TURN_MAX_SPEED_REDUCTION_DRONE
        elif self.target_type is MPTargetType.plane:
            return TURN_MAX_SPEED_REDUCTION_PLANE
        else:
            raise ValueError('Programming error - there is no TURN_MAX_* for %s', self.target_type.name)

    def _announce_change_of_wp(self) -> None:
        if self.is_tanker:
            if self.current_wp.is_point_of_interest:
                self.chat_message_handler.add_message('About to turn ({})'.format(self.current_wp.index))

    def _calculate_current_roll_flying_target(self, delta_bearing: float) -> float:
        """Calculates the roll of a plane based on previous roll, current change of direction and the
        flying asset's max turn rate"""
        asked_roll = delta_bearing / self.turn_rate * 25  # if fully turning, then tilt the thing 20 degs
        diff_from_prev = asked_roll - self.orientation.roll
        diff_negative = diff_from_prev < 0.
        if fabs(diff_from_prev) > (5 * self.send_freq):  # limit the max roll change per update
            diff_from_prev = 5 * self.send_freq
            if diff_negative:
                diff_from_prev *= -1
        return self.orientation.roll + diff_from_prev

    @override
    def update_position_variables(self) -> None:
        """Overrides method from MPTarget. Here we move the position of the target based on way-points.

        The decision to turn into the next way-point is based on the angle, turn rate and distance. That can only be
        an approximation, as a turn will lead to a curved distance, which might or might not suit the case.

        The reason to calculate the next position in advance is to be able to calculate the velocity vector
        based on the next position and the current position.
        FIXME: acceleration vectors for changing direction

        The speed is based on the current difference of heading vs. bearing of current_wp. The more pointed the
        angle is, the slower moving through the turn, which also makes the turn tighter.
        """
        # ---- [-1] Do we already have a first short path? If not, then initialize one
        if self.shortest_path_initialized is False:
            # start_wp is stored as self.current_wp in _initialise_location
            # - cannot use position as it is not a WP object
            self._find_next_shortest_path(self.current_wp)
            self.current_wp = self.shortest_path.popleft()
            self.next_wp = self.shortest_path.popleft()
            self.shortest_path_initialized = True

        # ---- [A] Check the health and its influence on cruise_speed, turn_rate as well as z_offset
        if self.health in [HealthType.broken, HealthType.destroyed, HealthType.dead]:
            self.turn_rate = 0.
            time_since_broken = time.time() - self.broken_timestamp
            if time_since_broken >= 20:
                self.cruise_speed = 0.1
            else:  # the longer it goes, the slower it moves
                self.cruise_speed = pow(self.cruise_speed, 1. - 0.05 * time_since_broken)
            if self.target_type in [MPTargetType.helicopter, MPTargetType.drone, MPTargetType.plane]:
                # Using 5 m/s2 instead of ca. 9.8 m/s2 because still some lift
                self.z_offset -= 0.5 * 5 * time_since_broken * time_since_broken

        # ---- [B] take over the previous position / orientation, which is what will be used by MP
        self.position = self.next_position

        # ---- [C] check whether we should change to the next way-point
        delta_horizontal = g.calc_distance_pos_wp(self.position, self.current_wp)
        next_bearing = g.calc_bearing_pos_wp(self.position, self.next_wp)
        delta_bearing = g.calc_delta_bearing(self.orientation.hdg, next_bearing)
        if self.turn_rate == 0. or self.cruise_speed == 0.:
            pass  # nothing to do - we are broken
        else:
            # given that we might de-accelerate a bit during the turn, the distance calc is conservative
            distance_to_turn = fabs(delta_bearing) / self.turn_rate * self.cruise_speed
            distance_to_turn += 3 * self.cruise_speed  # a bit of "thinking time" to start slowing down

            if distance_to_turn > delta_horizontal:
                if self.towed:
                    prev_wp = self.current_wp
                    self.current_wp = self.next_wp
                    self.next_wp = g.random_next_way_point(self.network, self.current_wp, prev_wp)
                else:
                    self.current_wp = self.next_wp
                    self._announce_change_of_wp()
                    if len(self.shortest_path) == 0:
                        self._find_next_shortest_path(self.current_wp)
                    self.next_wp = self.shortest_path.popleft()

        # ---- [D] We might have a new current WP to move towards -> re-calculate our heading until next update,
        current_bearing = g.calc_bearing_pos_wp(self.position, self.current_wp)
        delta_bearing_calc = g.calc_delta_bearing(self.orientation.hdg, current_bearing)

        # the delta_bearing is how much the asset can turn within this update -> depends on turn rate
        turn_rate_upd = self.turn_rate * self.send_freq  # the turn rate in this update
        if fabs(delta_bearing_calc) > turn_rate_upd:
            if delta_bearing_calc < 0:
                delta_bearing = - turn_rate_upd
                current_bearing = self.orientation.hdg - turn_rate_upd
            else:
                current_bearing = self.orientation.hdg + turn_rate_upd
                delta_bearing = turn_rate_upd
        else:
            delta_bearing = delta_bearing_calc

        # ---- [E] change the speed based on the delta bearing
        if self.towed or self.is_tanker:
            pass  # we do not change the speed - towed targets should have constant speed
        else:
            # For every 10 degrees we decrease the allowed speed by 7%, but not more than x
            allowed_speed = self.max_cruise_speed * max(1 - (int(fabs(delta_bearing_calc) / 10) * 0.07),
                                                        self._min_rate_of_max_speed_in_turn())
            if self.cruise_speed > allowed_speed:
                self.cruise_speed -= self.acceleration
                self.current_acceleration = -self.acceleration
            elif self.cruise_speed < allowed_speed:
                new_speed = min(self.max_cruise_speed, self.cruise_speed + self.acceleration)
                self.current_acceleration = new_speed - self.cruise_speed
                self.cruise_speed = new_speed
            else:
                self.current_acceleration = 0.

        # ---- [F] now we know the target -> recalculate some values
        delta_horizontal = g.calc_distance_pos_wp(self.position, self.current_wp)
        if self.fixed_altitude is None:
            delta_vertical = self.current_wp.alt_m - self.position.alt_m
            pitch = atan(delta_vertical / delta_horizontal)
        else:
            pitch = 0.
        orientation_pitch = degrees(pitch)
        if self.target_type in [MPTargetType.helicopter, MPTargetType.drone, MPTargetType.plane]:
            if is_capable_of_fighting_health(self.health):
                orientation_pitch -= 0  # forward moving helicopters have a down pitch
                roll = self._calculate_current_roll_flying_target(delta_bearing)
            else:
                orientation_pitch += random.randint(-10, 10)
                roll = random.randint(-10, 10)
            self.orientation = g.Orientation(current_bearing, pitch=orientation_pitch, roll=roll)
        elif self.target_type in [MPTargetType.ship, MPTargetType.coaster]:
            if self.health not in [HealthType.destroyed, HealthType.dead]:
                self.orientation.hdg = current_bearing
                self.orientation.roll = 0.
                self.orientation.pitch = 0.
            else:
                pass  # we keep heading and rest is set with self.death_rate_pitch/roll
        else:
            self.orientation = g.Orientation(current_bearing, pitch=orientation_pitch)
        cruise_speed_upd = self.cruise_speed * self.send_freq
        delta_vertical = sin(pitch) * cruise_speed_upd
        delta_horizontal = cos(pitch) * cruise_speed_upd

        lon_lat = g.calc_destination(self.position.lon, self.position.lat, delta_horizontal,
                                     current_bearing)
        self.next_position = g.Position(lon_lat[0], lon_lat[1], self.position.alt_m + delta_vertical)


class MPTargetTrips(MPTarget):
    """A variable speed target, which moves from way-point to way-point along a pre-planned route for a set of trips.

    Suitable for targets on roads or rails. To make speed/acceleration predictable,
    a start->end trip is calculated in advance within the path network.

    Speed is always measured horizontally (i.e. along a hill the vehicle moves actually faster).

    Speed is increased/decreased in steps, such that the acceleration is 1 speed_step per second and the
    speed is always a multiple of speed_step.
    """

    SPEED_PER_STEP = 2.5  # metres per second, 9 km/h. Same for all target types

    # if an angle at waypoint is larger than key, then the speed is value*speed_step.
    # e.g. for 50 degrees it would be 4*2.5 = 10 m/s. Below 15 there are no speed restrictions
    MAX_ANGLE_SPEED_STEPS = {15: 6, 30: 5, 45: 4, 60: 3, 75: 2, 90: 1}

    __slots__ = ('path', 'start_wp', 'trip', 'bearings', 'distances',
                 'next_wp_idx', 'next_position', 'current_speed_step', 'next_speed_step', 'current_acceleration',
                 'time_of_arrival', 'pause_between_trips', 'activation_delay')

    def __init__(self, target_type: MPTargetType, name: str, aircraft_model: str,
                 max_hit_points: int, full_damage_dist_m: int, max_speed: int,
                 pause_between_trips: int, activation_delay: int,
                 graph: nx.Graph, start_wp: g.WayPoint) -> None:
        super().__init__(target_type, name, aircraft_model, max_hit_points, full_damage_dist_m)

        self.max_speed_step = int(round(max_speed / self.SPEED_PER_STEP))

        self.min_trip_length = 0.
        self.destinations = None  # list of lon/lat. If None, then random, else routes
        self.destinations_repeat = False  # Instead of stopping at last destination, go back to first and repeat
        self.destination_index = 0  # the index of the current dest. If larger than len(destinations), then stop moving
        self.path = graph  # nx.Graph
        self.start_wp = start_wp  # the start way point of the current trip within the graph.
        # current trip pre-calculated values. The list of WP (trip) is 1 longer than bearings and distances
        self.trip = None  # List[g.WayPoint]  # the current pre-calculated trip from start to end.
        self.bearings = None  # List[float]  # the bearing (without mag dev) from the wp at index to the next wp
        self.distances = None  # List[float]  # the distance from the wp at index to the next wp

        self.next_wp_idx = 0  # the index in the trip of the WP we are moving towards (but will not reach within 1 sec)
        self.next_position = None  # g.Position  # the position we will be at in 1 second (might not be a WP)
        self.current_speed_step = 0  # the speed we have for the next second
        self.next_speed_step = 0  # The speed we will have in 1 second
        self.current_acceleration = 0.

        self.time_of_arrival = 0  # time in seconds when arrived at a stop waypoint. 0 if we just can move
        self.pause_between_trips = pause_between_trips  # time in seconds taking a break between trips
        self.activation_delay = activation_delay  # delay in seconds between scenario started and activation

    @property
    def is_moving(self) -> bool:  # overwrites property in MPTarget
        return True

    @property
    def is_routed(self) -> bool:
        return self.destinations is not None

    @classmethod
    def create_target(cls, name: str, pos: g.Position, hdg: float) -> 'MPTarget':
        raise NotImplementedError('Cannot be called on moving target.')

    @classmethod
    def create_receiver(cls, callsign: str, pos: g.Position, mp_visibility_range: int) -> 'MPTarget':
        raise NotImplementedError('Cannot be called on moving target.')

    @classmethod
    def create_random_target(cls, name: str, graph: nx.Graph, origin: Optional[g.Position],
                             max_hit_points: int, pause_between_trips: int,
                             min_trip_length: int, activation_delay: int) -> 'MPTargetTrips':
        """Creates a target, which uses random trips.
        Currently only truck, speedboat and buk_m2 are supported.
        """
        if origin is None:
            origin = g.random_way_point(graph)
        instance = cls._create_instance(name, graph, origin, max_hit_points, pause_between_trips, activation_delay)

        if min_trip_length <= 0.1:
            raise ValueError('Minimum trip length must be positive and should most often be >> hundreds of metres')
        instance.min_trip_length = min_trip_length
        instance._create_random_trip()
        return instance

    @classmethod
    def create_routed_target(cls, name: str, graph: nx.Graph, origin: g.Position,
                             max_hit_points: int, pause_between_trips: int,
                             destinations: list[g.Position], destinations_repeat: bool,
                             activation_delay: int) -> 'MPTargetTrips':
        """Creates a target, which uses shortest-path routes between destinations.
        Currently only truck, speedboat and buk_m2 are supported.
        """
        instance = cls._create_instance(name, graph, origin, max_hit_points, pause_between_trips, activation_delay)

        if destinations:
            if destinations_repeat and len(destinations) < 2:
                raise ValueError('There must be at least 2 destinations if repeating')
        else:
            raise ValueError('The list of destinations may not be empty')
        instance.destinations = destinations
        instance.destinations_repeat = destinations_repeat
        instance._create_routed_trip()
        return instance

    @staticmethod
    def create_routed_convoy_targets(name: str, graph: nx.Graph, origin: g.Position,
                                     max_hit_points: int, pause_between_trips: int,
                                     destinations: list[g.Position], destinations_repeat: bool,
                                     activation_delay: int,
                                     number_of_targets: int, time_between_targets: int) -> list['MPTargetTrips']:
        convoy_targets = list()
        for i in range(number_of_targets):
            convoy_targets.append(MPTargetTrips.create_routed_target(name, graph, origin,
                                                                     max_hit_points, pause_between_trips,
                                                                     destinations, destinations_repeat,
                                                                     activation_delay + i * time_between_targets))
        return convoy_targets

    @classmethod
    def _create_instance(cls, name: str, graph: nx.Graph, origin: g.Position,
                         max_hit_points: int, pause_between_trips: int, activation_delay: int) -> 'MPTargetTrips':
        start_wp = g.closest_way_point(graph, origin)
        if name == cls.TRUCK:
            instance = cls(MPTargetType.vehicle, cls.TRUCK, 'Aircraft/groundtarget/Models/truck.xml',
                           max_hit_points, FULL_DAMAGE_DIST_VEHICLE,
                           17, pause_between_trips, activation_delay, graph, start_wp)
            instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
            instance.extra_prop_value = 0
        elif name == cls.BUK_M2:
            instance = cls(MPTargetType.vehicle, cls.BUK_M2, 'Aircraft/BUK-M2/Models/buk-m2.xml',
                           max_hit_points, FULL_DAMAGE_DIST_VEHICLE,
                           14, pause_between_trips, activation_delay, graph, start_wp)
        elif name == cls.SPEEDBOAT:
            instance = cls(MPTargetType.ship, cls.SPEEDBOAT, 'Aircraft/hunter/Models/hunter.xml',
                           max_hit_points, 3, 12, pause_between_trips, activation_delay, graph, start_wp)
            instance.extra_prop_id = u.MP_PROP_OPRF_SELECT
            instance.extra_prop_value = 10
        else:
            raise NotImplementedError('{} is not yet available as a rails target.'.format(name))
        instance.death_time = DEATH_TIME_VEHICLE
        if name == cls.BUK_M2:
            instance.enable_radar()
        return instance

    def _create_random_trip(self) -> None:
        """Create a trip as a sequence of way-points until a minimal distance has been passed.

        Populates variables trip, distances and bearings. Resets next_wp_idx
        """
        self._create_trip_prepare()
        min_distance = random.uniform(self.min_trip_length, self.min_trip_length * 1.5)
        total_dist = 0.
        prev_wp = None
        this_wp = self.start_wp
        loops = 0
        while total_dist < min_distance:
            next_wp = g.random_next_way_point(self.path, this_wp, prev_wp)
            self.trip.append(next_wp)
            distance = g.calc_distance_wp_wp(this_wp, next_wp)
            self.distances.append(distance)
            bearing = g.calc_bearing_wp(this_wp, next_wp)
            self.bearings.append(bearing)
            total_dist += distance
            prev_wp = this_wp
            this_wp = next_wp
            loops += 1
            if loops > min_distance:
                logger.warning('%s looping endlessly to find random trip', self.callsign)
        self._create_trip_finalise()

    def _create_trip_prepare(self) -> None:
        self.trip = []
        self.distances = []
        self.bearings = []

    def _create_trip_finalise(self) -> None:
        # Finally reset the index to make sure we start at the right spot
        self.next_wp_idx = 0
        self.position = self.start_wp.make_position()
        self.next_position = self.position  # just to start out. First second no movement
        self.orientation = g.calc_orientation(self.position, self.trip[self.next_wp_idx].make_position())
        trip_type = 'routed' if self.is_routed else 'random'
        logger.info('Created new %s trip for %s with %i legs', trip_type, self.name, len(self.trip))

    def _create_routed_trip(self) -> None:
        try:
            self._create_trip_prepare()
            destination_wp = g.closest_way_point(self.path, self.destinations[self.destination_index])
            node_sequence = dijkstra_path(self.path, self.start_wp, destination_wp, ns.EDGE_ATTR_COST)

            # fill the trip/distances/bearings
            for i in range(1, len(node_sequence)):
                self.trip.append(node_sequence[i])
                self.distances.append(g.calc_distance_wp_wp(node_sequence[i - 1], node_sequence[i]))
                self.bearings.append(g.calc_bearing_wp(node_sequence[i - 1], node_sequence[i]))
        except (NodeNotFound, NetworkXNoPath):
            logger.exception('Path could not be found for routed trip for %s - target set to not move', self.name)
            self.destination_index = len(self.destinations)  # make sure it does not move

        self._create_trip_finalise()

    def _change_trip(self, distance_to_wp: float) -> None:
        """When we are so close to the last waypoint that we can switch to the next trip"""
        self.next_speed_step = 0
        self.current_acceleration = distance_to_wp * -1
        self.next_position = self.trip[-1].make_position()

        # Prepare next trip
        self.start_wp = self.trip[-1]
        if self.is_routed:
            if self.destinations_repeat:
                if self.destination_index + 1 == len(self.destinations):
                    self.destination_index = 0
                else:
                    self.destination_index += 1
                self._create_routed_trip()
            else:
                self.destination_index += 1
                if self.destination_index < len(self.destinations):
                    self._create_routed_trip()
                # else do nothing - we are done
        else:
            self._create_random_trip()
        self.time_of_arrival = time.time()
        logger.info('%s end of trip and waiting %i seconds before next trip', self.callsign, self.pause_between_trips)

    @override
    def update_position_variables(self) -> None:
        """Overrides method from MPTarget. Here we move the position of the target along a strict path.

        The movement is from way-point to way-point. The speed is adapted depending on the tightness of turns.
        In order to ease computation, the movement steps are based on 1 second, only few pre-defined speeds
        with pre-defined accelerations.
        Contrary to MovingTarget the next position is not only calculated based on the next WayPoint, but
        also on the WayPoint getting passed within the next second (if the WPs are very close to each other
        and despite reduced speed the distance covered is passed just the next WP), because that way the
        next position is still on the road and not just on a vector from the current position to the next WP.
        In many situations, this will not be very noticeable - but in some situations it will, and therefore we
        take the pain.
        """
        next_wp_is_final = self.next_wp_idx == len(self.trip) - 1
        try:
            if self.is_routed and self.destination_index == len(self.destinations):
                return  # we are at the final destination and just sit idle
            if self.time_of_arrival > 0:  # handle pause between trips
                self.current_speed_step = 0
                self.current_acceleration = 0
                now = time.time()
                if now - self.time_of_arrival > self.pause_between_trips:
                    self.time_of_arrival = 0
                    logger.info('%s done with pause and ready for next trip', self.callsign)
                else:
                    return
            # at the moment we take over the previous position / orientation, which is what will be used by MP
            self.position = self.next_position
            self.current_speed_step = self.next_speed_step
            delta_speed_step = 0  # whether to speed up, keep speed or brake
            distance_to_wp = g.calc_distance_pos_wp(self.position, self.trip[self.next_wp_idx])

            # Handle case that within the next second we get at a stop waypoint and therefore acceleration etc. must
            # be handled a bit special (no matter the health)
            # Reducing the distance with 0.999*SPEED_PER_STEP due to rounding/residuals
            if next_wp_is_final:
                if self.current_speed_step * self.SPEED_PER_STEP >= (distance_to_wp - self.SPEED_PER_STEP * 0.999):
                    self._change_trip(distance_to_wp)
                    return

            # check whether we can accelerate or need to brake
            if self.health in [HealthType.broken, HealthType.destroyed, HealthType.dead]:
                if self.current_speed_step > 0:
                    delta_speed_step = -1
                else:  # we are already stopped, just make sure that acceleration is correct
                    self.current_acceleration = 0
                    # no need to change self.orientation
                    return
            else:  # acceleration or braking depends on angle at next waypoint
                if next_wp_is_final:
                    max_angle_speed = 0
                else:
                    corner_deg = g.calc_delta_bearing(self.bearings[self.next_wp_idx],
                                                      self.bearings[self.next_wp_idx + 1])
                    max_angle_speed = 99
                    for angle, value in self.MAX_ANGLE_SPEED_STEPS.items():
                        if fabs(corner_deg) >= angle:
                            max_angle_speed = min(value, max_angle_speed)
                if max_angle_speed == 99:  # no restrictions
                    if self.current_speed_step < self.max_speed_step:
                        delta_speed_step = 1
                else:
                    # if max_angle_speed is smaller than current speed: do we need to start breaking already now?
                    if self.current_speed_step > max_angle_speed:
                        min_distance = self.current_speed_step
                        for i in range(0, self.current_speed_step - max_angle_speed):
                            min_distance += self.current_speed_step - i
                        min_distance += max_angle_speed  # rather brake a bit too early
                        if min_distance * self.SPEED_PER_STEP >= distance_to_wp:
                            delta_speed_step = -1  # we need to start braking
                    # if we did not need to brake - could we accelerate if we are not yet at max speed?
                    if delta_speed_step == 0 and self.current_speed_step < self.max_speed_step:
                        maybe_speed_step = self.current_speed_step + 1
                        min_distance = maybe_speed_step
                        for i in range(0, maybe_speed_step - max_angle_speed):
                            min_distance += maybe_speed_step - i
                        min_distance += max_angle_speed  # rather brake a bit too early
                        if min_distance * self.SPEED_PER_STEP < distance_to_wp:
                            delta_speed_step = 1  # we can accelerate

            # now we know the delta_speed_step: calculate speed and next position
            self.next_speed_step = self.current_speed_step + delta_speed_step
            if not next_wp_is_final:  # make sure we keep moving - can happen in roundabouts with close nodes
                if self.next_speed_step == 0:
                    self.next_speed_step = 1
            self.current_acceleration = delta_speed_step * self.SPEED_PER_STEP

            remaining_distance = self.current_speed_step * self.SPEED_PER_STEP
            temp_position = self.position
            while True:
                if distance_to_wp >= remaining_distance:
                    lon_lat = g.calc_destination_pos(temp_position, remaining_distance,
                                                     self.bearings[self.next_wp_idx])
                    self.next_position = g.Position(lon_lat[0], lon_lat[1], 0)
                    delta_alt_m = self.trip[self.next_wp_idx].alt_m - self.trip[self.next_wp_idx - 1].alt_m
                    dist_to_next = g.calc_distance_pos_wp(self.next_position, self.trip[self.next_wp_idx])
                    share_of_delta = (self.distances[self.next_wp_idx] - dist_to_next) / self.distances[
                        self.next_wp_idx]
                    self.next_position.alt_m = self.trip[self.next_wp_idx - 1].alt_m + delta_alt_m * share_of_delta
                    break
                else:
                    remaining_distance -= distance_to_wp
                    self.next_wp_idx += 1
                    temp_position = self.trip[self.next_wp_idx - 1]
                    distance_to_wp = self.distances[self.next_wp_idx]
            self.orientation = g.calc_orientation(self.position, self.next_position)
        except IndexError:
            # A bit of a hack, because have not found out when it happens (found during trips based on OSM data).
            logger.warning('Trip target got out of sync with number of edges. Changing trip.')
            self._change_trip(1.)


@dataclass(frozen=True)
class SAMVariant:
    """Holds configuration for specific SAM variants according to the values specified in file
    Nasal/fire-control-custom.nas in the respective folders for SAMs in OPRF Admin Assets."""
    reload_time: int  # time to reload ammunition in seconds - and startup time
    radar_off_time_min: int
    radar_off_time_max: int
    radar_on_time: int
    radar_on_after_detect_time: int
    radar_elevation_above_terrain_m: int  # FCS-z
    num_missiles: int
    missile_brevity: str
    missile_max_distance_nm: float  # *-set.xml - max-fire-range
    missile_min_distance_nm: float  # *-set.xml - min-fire-range
    fire_minimum_interval: int
    rail_pitch_deg: int  # rail-pitch-deg


# up to date with commit 0c412c5462a925ab81d90f1f8c749ea093e6361e 2023-05-26
DEFAULT_FIRE_MINIMUM_INTERVAL_SHORT_RANGE = 10  # Guess
DEFAULT_FIRE_MINIMUM_INTERVAL_MID_RANGE = 15  # Guess
DEFAULT_FIRE_MINIMUM_INTERVAL_LONG_RANGE = 30  # Guess

DEFAULT_RELOAD_TIME_SAM = 600  # e.g. BUK-M2 has a reload time of 15 minutes for 4 missiles IRL

RAIL_PITCH_VERTICAL = 89  # close to vertical, but not totally

SAM_VARIANTS = {MPTarget.BUK_M2: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                            30, 90, 45, 180,
                                            6,  # FCS-z is 3 but on pictures it looks taller
                                            4, '9M317',
                                            31, 1.6, DEFAULT_FIRE_MINIMUM_INTERVAL_MID_RANGE,
                                            24),
                MPTarget.S_200: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                           30, 150, 45, 240, 25,
                                           6, '5V28V',
                                           162, 32, DEFAULT_FIRE_MINIMUM_INTERVAL_LONG_RANGE,
                                           45),
                MPTarget.S_300: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                           30, 120, 30, 180, 35,
                                           4, 'KN-06',
                                           65, 2, DEFAULT_FIRE_MINIMUM_INTERVAL_LONG_RANGE,
                                           RAIL_PITCH_VERTICAL),
                MPTarget.PATRIOT: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                             30, 90, 45, 180, 25,
                                             4, 'GEM',
                                             43, 1, DEFAULT_FIRE_MINIMUM_INTERVAL_LONG_RANGE,
                                             45),
                MPTarget.DVINA: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                           15, 45, 90, 240, 25,
                                           6, '5Ya23',
                                           36, 4, DEFAULT_FIRE_MINIMUM_INTERVAL_MID_RANGE,
                                           45),
                MPTarget.SA_6: SAMVariant(DEFAULT_RELOAD_TIME_SAM,
                                          30, 90, 45, 180, 10,
                                          3, '3M9',
                                          18, 0.4, DEFAULT_FIRE_MINIMUM_INTERVAL_SHORT_RANGE,
                                          33),
                MPTarget.SA_3: SAMVariant(DEFAULT_RELOAD_TIME_SAM, 30, 90, 45, 180, 7,
                                          4, '5V27',
                                          18, 1.8, DEFAULT_FIRE_MINIMUM_INTERVAL_SHORT_RANGE,
                                          33)
                }
