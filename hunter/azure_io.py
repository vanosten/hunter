# SPDX-FileCopyrightText: (C) 2024 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from datetime import datetime as dt
import logging
import multiprocessing as mp
import os
import os.path as osp
import pickle
import sys
import time
from typing import Any, Optional, override

import azure.cosmos as azc
from azure.identity import ClientSecretCredential

import paho.mqtt.client as mqtt

import hunter.geometry as g
import hunter.events as ev
import hunter.logger_config as lc
import hunter.messages as m
import hunter.mqtt_io as mqi
import hunter.scenarios as s
import hunter.utils as u

logger = lc.get_logger()

HUNTER_CORE_DB = 'hunter-core'
CONTAINER_SESSIONS = 'sessions'


ENV_VARIABLE_AZ_COSMOS_ACCOUNT_URI = 'AZ_COSMOS_ACCOUNT_URI'
ENV_VARIABLE_AZ_COSMOS_TENANT_ID = 'AZ_COSMOS_TENANT_ID'
ENV_VARIABLE_AZ_COSMOS_CLIENT_ID = 'AZ_COSMOS_CLIENT_ID'
ENV_VARIABLE_AZ_COSMOS_CLIENT_SECRET = 'AZ_COSMOS_CLIENT_SECRET'


def check_minimal_azure_env_variables() -> None:
    """Checks the availability of the minimal set of environment variables and exits if not fulfilled.
    """
    u.read_dotenv()

    missing = list()
    if os.getenv(ENV_VARIABLE_AZ_COSMOS_ACCOUNT_URI, None) is None:
        missing.append(ENV_VARIABLE_AZ_COSMOS_ACCOUNT_URI)
    if os.getenv(ENV_VARIABLE_AZ_COSMOS_TENANT_ID, None) is None:
        missing.append(ENV_VARIABLE_AZ_COSMOS_TENANT_ID)
    if os.getenv(ENV_VARIABLE_AZ_COSMOS_CLIENT_ID, None) is None:
        missing.append(ENV_VARIABLE_AZ_COSMOS_CLIENT_ID)
    if os.getenv(ENV_VARIABLE_AZ_COSMOS_CLIENT_SECRET, None) is None:
        missing.append(ENV_VARIABLE_AZ_COSMOS_CLIENT_SECRET)

    if missing:
        print('The following environment variables are missing -> aborting: {}'.format(', '.join(missing)))
        logging.shutdown()
        sys.exit(1)


def initialize_container_proxy() -> azc.ContainerProxy:
    u.read_dotenv()

    # Create a client object using the service principal credentials
    credential = ClientSecretCredential(
        tenant_id=os.getenv(ENV_VARIABLE_AZ_COSMOS_TENANT_ID),
        client_id=os.getenv(ENV_VARIABLE_AZ_COSMOS_CLIENT_ID),
        client_secret=os.getenv(ENV_VARIABLE_AZ_COSMOS_CLIENT_SECRET)
    )
    cosmos_client = azc.CosmosClient(os.getenv(ENV_VARIABLE_AZ_COSMOS_ACCOUNT_URI), credential)

    cosmos_database = cosmos_client.get_database_client(database=HUNTER_CORE_DB)
    return cosmos_database.get_container_client(CONTAINER_SESSIONS)


# ===================== Hunter Session and Stuff =======================================================================


def _query_session_not_terminated(proxy: azc.ContainerProxy, override_session_id: str) -> list[str]:
    """Check whether we can find not terminated session(s) based on a session id."""
    # query = ds_client.query(kind=KIND_HUNTER_SESSION)
    # query.add_filter(filter=PropertyFilter(PROP_STOPPED_DT, '=', None))
    # query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', override_session_id))
    query = 'SELECT * FROM c WHERE c.{} = "{}" and c.{} = "{}" and c.{} = null'.format(ev.PROP_TYPE,
                                                                                       ev.TYPE_HUNTER_SESSION,
                                                                                       ev.PROP_SESSION_ID,
                                                                                       override_session_id,
                                                                                       ev.PROP_STOPPED_DT)
    existing = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):
        existing.append(item[ev.PROP_SESSION_ID])
    return existing


def register_new_session(proxy: azc.ContainerProxy, scenario: s.ScenarioContainer, mp_server_host: str, gci: bool,
                         heli_shoot: bool, drone_shoot: bool, ship_shoot: u.ShipShootingType, shilka_shoot: bool,
                         sam_shoot: u.SAMShootingType,
                         has_awacs: bool, has_tanker: bool, has_carrier: bool,
                         ctrl_callsign: str, identifier: str, no_list: bool) -> str:
    logger.info('Registering new session for scenario %s on %s', scenario.ident, mp_server_host)
    session_id = u.create_session_id()

    session_item_dict = {
        ev.PROP_ID: session_id,
        ev.PROP_SESSION_ID: session_id,
        ev.PROP_TYPE: ev.TYPE_HUNTER_SESSION,
        ev.PROP_SCENARIO_IDENT: scenario.ident,
        ev.PROP_STARTED_BY: os.getenv(ENV_VARIABLE_AZ_COSMOS_CLIENT_ID),
        ev.PROP_STARTED_DT: u.create_utf_isostring_timestamp(),
        ev.PROP_STOPPED_DT: None,  # must be set explicitly
        ev.PROP_SCENARIO_NAME: scenario.name,
        ev.PROP_SCENARIO_DESCRIPTION: scenario.description,
        ev.PROP_SCENARIO_ICAO: scenario.icao,
        ev.PROP_SCENARIO_WEST_LON: scenario.south_west[0],
        ev.PROP_SCENARIO_SOUTH_LAT: scenario.south_west[1],
        ev.PROP_SCENARIO_EAST_LON: scenario.north_east[0],
        ev.PROP_SCENARIO_NORTH_LAT: scenario.north_east[1],
        ev.PROP_MP_SERVER_HOST: mp_server_host,
        ev.PROP_GCI: gci,
        ev.PROP_HELI_SHOOT: heli_shoot,
        ev.PROP_DRONE_SHOOT: drone_shoot,
        ev.PROP_SHIP_SHOOT: ship_shoot.value,
        ev.PROP_SHILKA_SHOOT: shilka_shoot,
        ev.PROP_SAM_SHOOT: sam_shoot.value,
        ev.PROP_AWACS: has_awacs,
        ev.PROP_TANKER: has_tanker,
        ev.PROP_CARRIER: has_carrier,
        ev.PROP_CALLSIGN: ctrl_callsign,
        ev.PROP_IDENTIFIER: identifier,
        ev.PROP_OVERRIDDEN: False,
        ev.PROP_NO_LIST: no_list
    }

    proxy.create_item(session_item_dict)
    logger.info('Registered new session with id %s', session_id)
    return session_id


class HunterSession:
    def __init__(self, session_id: str, started_by: str, started_dt: dt, stopped_dt: dt,
                 name: str, description: str, icao: str,
                 south_west: tuple[float, float], north_east: tuple[float, float],
                 mp_server_host: str, gci: bool,
                 heli_shoot: bool, drone_shoot: bool, ship_shoot: bool, shilka_shoot: bool, sam_shoot: bool,
                 awacs: bool, tanker: bool, carrier: bool,
                 ctrl_callsign: str, identifier: str,
                 overridden: bool, no_list: bool) -> None:
        self.session_id = session_id
        self.url_param = '?{}={}'.format(ev.PROP_SESSION_ID, self.session_id)
        self.started_by = started_by
        self.started_dt = started_dt
        self.stopped_dt = stopped_dt
        self.name = name
        self.description = description
        self.icao = icao
        self.south_west = south_west
        self.north_east = north_east
        self.mp_server_host = mp_server_host
        self.gci = gci
        self.heli_shoot = heli_shoot
        self.drone_shoot = drone_shoot
        self.ship_shoot = ship_shoot
        self.shilka_shoot = shilka_shoot
        self.sam_shoot = sam_shoot
        self.awacs = awacs
        self.tanker = tanker
        self.carrier = carrier
        self.ctrl_callsign = ctrl_callsign
        self.identifier = identifier
        self.overridden = overridden
        self.no_list = no_list
        self.airports = list()

    @property
    def ship_shooting_display(self) -> str:
        return u.map_ship_shooting_display(self.ship_shoot)

    @property
    def sam_shooting_display(self) -> str:
        return u.map_sam_shooting_display(self.sam_shoot)

    def load_apt_dat_airports(self) -> None:
        # noinspection PyBroadException
        try:
            full_path = osp.join(u.get_scenario_resources_dir(), s.APT_DAT_AIRPORT_FILE)
            with open(full_path, 'rb') as file_pickle:
                all_airports = pickle.load(file_pickle)

            airports_in_boundary = list()
            for apt in all_airports:
                if self.south_west[0] <= apt.lon <= self.north_east[0] and \
                        self.south_west[1] <= apt.lat <= self.north_east[1]:
                    airports_in_boundary.append(apt)
            self.airports = airports_in_boundary
        except Exception:  # Pickle can throw pretty much any exception
            logger.exception('Could not read airports information from file')

    @classmethod
    def map_session_from_entity(cls, entity: dict[str, Any]) -> 'HunterSession':
        my_session = HunterSession(entity[ev.PROP_SESSION_ID],
                                   entity[ev.PROP_STARTED_BY],
                                   u.parse_isostring_timestamp(entity[ev.PROP_STARTED_DT]),
                                   u.parse_isostring_timestamp(entity[ev.PROP_STOPPED_DT]),
                                   entity[ev.PROP_SCENARIO_NAME],
                                   entity[ev.PROP_SCENARIO_DESCRIPTION],
                                   entity[ev.PROP_SCENARIO_ICAO],
                                   (entity[ev.PROP_SCENARIO_WEST_LON], entity[ev.PROP_SCENARIO_SOUTH_LAT]),
                                   (entity[ev.PROP_SCENARIO_EAST_LON], entity[ev.PROP_SCENARIO_NORTH_LAT]),
                                   entity[ev.PROP_MP_SERVER_HOST],
                                   entity[ev.PROP_GCI],
                                   entity[ev.PROP_HELI_SHOOT], entity[ev.PROP_DRONE_SHOOT], entity[ev.PROP_SHIP_SHOOT],
                                   entity[ev.PROP_SHILKA_SHOOT], entity[ev.PROP_SAM_SHOOT],
                                   entity[ev.PROP_AWACS], entity[ev.PROP_TANKER], entity[ev.PROP_CARRIER],
                                   entity[ev.PROP_CALLSIGN], entity[ev.PROP_IDENTIFIER],
                                   entity[ev.PROP_OVERRIDDEN],
                                   entity[ev.PROP_NO_LIST])
        return my_session


def query_session(proxy: azc.ContainerProxy, session_id: Optional[str],
                  load_airports: bool = False) -> Optional[HunterSession]:
    if session_id is None:
        return None
    query = 'SELECT * FROM c WHERE c.{} = "{}" AND c.{} = "{}"'.format(ev.PROP_TYPE,
                                                                       ev.TYPE_HUNTER_SESSION,
                                                                       ev.PROP_SESSION_ID,
                                                                       session_id)
    sessions = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):
        sessions.append(HunterSession.map_session_from_entity(item))
    if len(sessions) == 1:
        my_session = sessions[0]
        if load_airports:
            my_session.load_apt_dat_airports()
        return my_session
    elif len(sessions) > 1:
        logger.warning('There is more than one session with the same ID. Should never happen ?!')
    return None


def query_available_sessions(proxy: azc.ContainerProxy) -> list[HunterSession]:
    query = 'SELECT TOP 50 * FROM c WHERE c.{} = "{}" ORDER BY c.{} DESC'.format(ev.PROP_TYPE,
                                                                                 ev.TYPE_HUNTER_SESSION,
                                                                                 ev.PROP_SESSION_ID,
                                                                                 ev.PROP_STARTED_DT)
    sessions = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):
        hunter_session = HunterSession.map_session_from_entity(item)
        if hunter_session.no_list is False:
            sessions.append(hunter_session)
    return sessions


def _terminate_session(proxy: azc.ContainerProxy, session_id: str) -> None:
    logger.info('Terminating session')
    results = _query_session_not_terminated(proxy, session_id)
    if len(results) == 0:
        raise ValueError(f'Current session ID {session_id} cannot be found as ongoing.')

    item = proxy.read_item(results[0], results[0])
    item[ev.PROP_STOPPED_DT] = u.create_utf_isostring_timestamp()
    proxy.upsert_item(item)


# ===================== Damage =========================================================================================

def _register_damage_result(proxy: azc.ContainerProxy, mqtt_message: mqtt.MQTTMessage) -> None:
    damage_result_dict = ev.extract_from_damage_result_json(mqi.decode_message_payload(mqtt_message.payload))
    proxy.create_item(damage_result_dict, enable_automatic_id_generation=True)
    logger.info('Registered a damage result for attacker %s on %s',
                damage_result_dict[ev.PROP_ATTACKER], damage_result_dict[ev.PROP_TARGET])


def query_damage_results(proxy: azc.ContainerProxy, session_id: str) -> list[m.DamageResult]:
    query = 'SELECT TOP 100 * FROM c WHERE c.{} = "{}" and c.{} = "{}" ORDER BY c.{} DESC'.format(ev.PROP_TYPE,
                                                                                                  ev.TYPE_DAMAGE_RESULT,
                                                                                                  ev.PROP_SESSION_ID,
                                                                                                  session_id,
                                                                                                  ev.PROP_DAMAGE_TIME)
    damages = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):
        damages.append(ev.damage_result_object_from_dict(item))
    return damages


# ===================== AttackerEvent ==================================================================================
def _register_attacker_event(proxy: azc.ContainerProxy, mqtt_message: mqtt.MQTTMessage) -> None:
    attacker_event_dict = ev.extract_from_attacker_event_json(mqi.decode_message_payload(mqtt_message.payload))
    proxy.create_item(attacker_event_dict, enable_automatic_id_generation=True)
    logger.info('Registered an attacker event for callsign %s', attacker_event_dict[ev.PROP_CALLSIGN])


def query_attacker_event_results(proxy: azc.ContainerProxy, session_id: str) -> list[m.AttackerEvent]:
    query = 'SELECT TOP 100 * FROM c WHERE c.{} = "{}" and c.{} = "{}" ORDER BY c.{} DESC'.format(ev.PROP_TYPE,
                                                                                                  ev.TYPE_ATTACKER_EVENT,
                                                                                                  ev.PROP_SESSION_ID,
                                                                                                  session_id,
                                                                                                  ev.PROP_EVENT_TIME)
    events = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):
        events.append(ev.attacker_event_object_from_dict(item))
    return events


# ===================== MissileEvent ===================================================================================
def _register_missile_event(proxy: azc.ContainerProxy, mqtt_message: mqtt.MQTTMessage) -> None:
    missile_event_dict = ev.extract_from_missile_event_json(mqi.decode_message_payload(mqtt_message.payload))
    proxy.create_item(missile_event_dict, enable_automatic_id_generation=True)
    logger.info('Registered a missile event for callsign %s', missile_event_dict[ev.PROP_ATTACKER])


# ===================== PositionUpdates ================================================================================

def _compose_query_string_for_position_updates(session_id: str) -> str:
    return 'SELECT * FROM c WHERE c.{} = "{}" and c.{} = "{}" ORDER BY c.{} ASC'.format(ev.PROP_TYPE,
                                                                                        ev.TYPE_POSITION_UPDATES_BATCH,
                                                                                        ev.PROP_SESSION_ID,
                                                                                        session_id,
                                                                                        ev.PROP_CALLSIGN)


def _upsert_position_updates(proxy: azc.ContainerProxy, mqtt_message: mqtt.MQTTMessage) -> None:
    """Inserts or updates positions based on the newest values. We are just adding all in one go instead of
    position by position. That saves DB operations and no deletes needed.
    """
    item = ev.extract_from_damage_result_json(mqi.decode_message_payload(mqtt_message.payload))
    proxy.upsert_item(item)
    logger.info('Upserted position updates')


def query_current_session_positions(proxy: azc.ContainerProxy, session_id: str) -> list[m.PositionUpdate]:
    """The actual positions for the current session - can be empty."""
    query = _compose_query_string_for_position_updates(session_id)
    positions: list[m.PositionUpdate] = list()
    for item in proxy.query_items(query, enable_cross_partition_query=True):  # there will only be one
        for record in item[ev.PROP_POSITIONS]:
            health_type = m.map_health_type(record[ev.PROP_HEALTH], False)
            position = g.Position(record[ev.PROP_POSITION_LON], record[ev.PROP_POSITION_LAT],
                                  record[ev.PROP_POSITION_ALT_M])
            position_update = m.PositionUpdate(record[ev.PROP_CALLSIGN], record[ev.PROP_KIND], health_type, position,
                                               record[ev.PROP_HEADING], record[ev.PROP_PITCH], record[ev.PROP_SPEED])
            positions.append(position_update)
    return positions


# ===================== Other Stuff ====================================================================================


class AzureDBRunnerMQTTClient(mqi.BaseMQTTClient):
    __slots__ = '_worker_exit_handler'

    def __init__(self, worker_exit_handler: mqi.WorkerExitHandler) -> None:
        self._worker_exit_handler = worker_exit_handler
        super().__init__('AzureDBRunnerClient')
        self.message_callback_add(mqi.WORKER_EXIT_TOPIC, self._callback_on_worker_exit)
        self.message_callback_add(mqi.MISSILE_EVENT_TOPIC, self._callback_on_received_custom_event)
        self.message_callback_add(mqi.DAMAGE_RESULT_TOPIC, self._callback_on_received_custom_event)
        self.message_callback_add(mqi.ATTACKER_EVENT_TOPIC, self._callback_on_received_custom_event)
        self.message_callback_add(mqi.POSITION_UPDATES_BATCH_TOPIC, self._callback_on_received_custom_event)

    def send_heartbeat(self, session_id: str) -> None:
        json_payload = ev.create_heartbeat_json(session_id, self.client_id)
        logger.debug('Sent heartbeat for client %s', self.client_id)
        _ = self.publish(topic=mqi.HEARTBEAT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.HEARTBEAT_SENDER_QOS)

    def _callback_on_worker_exit(self, client, userdata, message: mqtt.MQTTMessage):
        callsign = ev.extract_callsign_from_worker_exit_json(mqi.decode_message_payload(message.payload))
        if callsign == ev.CALLSIGN_ALL:
            self._worker_exit_handler.set_exit_requested()

    @override
    def _subscribe_to_topics_on_connect(self) -> None:
        self.subscribe(mqi.WORKER_EXIT_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.WORKER_EXIT_RECEIVER_QOS))
        self.subscribe(mqi.MISSILE_EVENT_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.MISSILE_EVENT_RECEIVER_QOS))
        self.subscribe(mqi.DAMAGE_RESULT_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.DAMAGE_RESULT_RECEIVER_QOS))
        self.subscribe(mqi.ATTACKER_EVENT_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.ATTACKER_EVENT_RECEIVER_QOS))
        self.subscribe(mqi.POSITION_UPDATES_BATCH_TOPIC,
                       options=mqtt.SubscribeOptions(qos=mqi.POSITION_UPDATES_BATCH_RECEIVER_QOS))


class AzureDBRunner(mp.Process, mqi.WorkerExitHandler):
    """A thread interacting with Azure Cosmos DB asynchronously, so the main process' run() method does not get halted.
    """
    CLIENT_NAME = 'AzureDBRunner'

    __slots__ = ('mqtt_client', 'session_id', 'last_heartbeat_sent', 'log_config')

    def __init__(self, session_id: str, log_config: lc.LogConfig) -> None:
        mp.Process.__init__(self, name='AzureDBRunner')

        mqi.WorkerExitHandler.__init__(self)
        self.mqtt_client: Optional[AzureDBRunnerMQTTClient] = None
        self.session_id = session_id
        self.last_heartbeat_sent = 0
        self.log_config = log_config

    def run(self) -> None:
        lc.configure_logging(self.log_config)
        global logger
        logger = lc.get_logger()

        self.mqtt_client = AzureDBRunnerMQTTClient(self)
        proxy = None
        logger.info('%s started', self.CLIENT_NAME)
        while True:
            # noinspection PyBroadException
            try:
                if not proxy:
                    proxy = initialize_container_proxy()
                while True:
                    mqtt_message = self.mqtt_client.get_next_from_incoming_event_buffer()
                    if mqtt_message is not None:
                        if mqtt_message.topic == mqi.MISSILE_EVENT_TOPIC:
                            _register_missile_event(proxy, mqtt_message)
                        elif mqtt_message.topic == mqi.DAMAGE_RESULT_TOPIC:
                            _register_damage_result(proxy, mqtt_message)
                        elif mqtt_message.topic == mqi.ATTACKER_EVENT_TOPIC:
                            _register_attacker_event(proxy, mqtt_message)
                        elif mqtt_message.topic == mqi.POSITION_UPDATES_BATCH_TOPIC:
                            _upsert_position_updates(proxy, mqtt_message)
                        else:
                            raise ValueError('Programming logic problem: no handling of topic: ' + mqtt_message.topic)
                    else:
                        break

            except Exception:
                logger.exception('Some exception during %s.run() occurred. Continuing and see.', self.CLIENT_NAME)
            if self.exit_requested:
                logger.info('worker exiting: %s', self.CLIENT_NAME)
                try:
                    _terminate_session(proxy, self.session_id)
                except ValueError as e:
                    logger.warning('Unable to update the session as stopped.', e)
                logger.info('%s got exit command - exiting', self.CLIENT_NAME)
                self.mqtt_client.stop_and_disconnect()
                return
            now = time.time()
            if now - self.last_heartbeat_sent > mqi.HEARTBEAT_SENDER_FREQ:
                self.mqtt_client.send_heartbeat(self.session_id)
                self.last_heartbeat_sent = now
            time.sleep(0.5)
