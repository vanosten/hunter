# SPDX-FileCopyrightText: (C) 2022 - 2024, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Provides common code for mp_shooters and mp_dispensers for assessing attackers."""

from abc import ABC, abstractmethod
import time

import hunter.geometry as g
import hunter.emesary_notifications as en
import hunter.iff_datalink as ifd
import hunter.messages as m
import hunter.utils as u


# In alignment with OPRF Nasal (60 kt)
# slower than this and we must assume a parked or taxiing attacker -> not processed for counter-measures and shooting
TOO_SLOW_ATTACKER = 30  # m/s


class AttackerPositionHistory:
    """Stores positions for an attacker aircraft over time in LIFO principle."""
    __slots__ = ('max_length', 'historic_positions')

    def __init__(self, max_length: int) -> None:
        self.max_length = max_length  # how much history is kept
        self.historic_positions = list()

    def add_position(self, newest_position: m.AttackerPosition, now: float):
        if len(self.historic_positions) > self.max_length:
            del self.historic_positions[0]
        newest_position.timestamp = now
        self.historic_positions.append(newest_position)

    @property
    def latest_position(self) -> g.Position:
        return self.historic_positions[-1].position

    @property
    def latest_attacker_position(self) -> m.AttackerPosition:
        return self.historic_positions[-1]

    def calc_time_since_latest_position(self, now: float) -> float:
        return now - self.historic_positions[-1].timestamp

    def guess_next_position(self, now: float, delta_time: float) -> g.Position:
        """Very primitive guess of future position based on last 2 known positions.

        E.g. we do not try to guess whether the attacker is turning.

        The future time is now + delta_time (when we guess we have a new iteration).
        The last historic_position can be a while back."""
        if self.estimate_speed() is None:
            return self.historic_positions[-1].position
        else:
            delta_historic = now + delta_time - self.historic_positions[-1].timestamp
            orientation = g.calc_orientation(self.historic_positions[-2].position, self.historic_positions[-1].position)
            dist = self.estimate_speed() * delta_historic
            return g.calc_destination_pos_3d(self.historic_positions[-1].position, dist,
                                             orientation.hdg, orientation.pitch)

    def estimate_speed(self) -> float | None:
        """The estimated speed based on the last 2 positions and a time_diff of the updates.
        NB: the time_diff is not the same as send_freq, because it depends on the rate of MP packages
        received from the attacker.

        If the speed cannot be calculated, then None is returned
        """
        if len(self.historic_positions) <= 1:
            return None
        pos_prev = self.historic_positions[-2].position
        pos_last = self.historic_positions[-1].position
        distance = g.calc_distance_pos_3d(pos_prev, pos_last)
        return distance / (self.historic_positions[-1].timestamp - self.historic_positions[-2].timestamp)

    def assumed_on_ground(self) -> bool:
        attacker_speed = self.estimate_speed()
        if attacker_speed is None or attacker_speed < TOO_SLOW_ATTACKER:
            return True
        return False


class AttackerTracker(ABC):
    __slots__ = ('parent', 'attackers_hist_positions',)

    def __init__(self, parent) -> None:  # mpt.MPTarget not imported due to potential circular reference
        self.parent = parent
        self.attackers_hist_positions = dict()  # key = callsign, value = g.AttackerPositionHistory

    def is_latest_position_up_to_date(self, now: float, position_history: AttackerPositionHistory) -> bool:
        return position_history.calc_time_since_latest_position(now) < self.parent.send_freq * 0.5

    def process_attacker_info(self, attacker_positions: dict[str, m.AttackerPosition],
                              armament_in_flight: list[en.ArmamentInFlightNotification]) -> list[m.MissileEvent]:
        """Updates the internal structure of attacker position history and then processes the attackers.
        Even if an attacker might not have been there for a long time, we never remove it: there are not
        so many attackers and therefore memory consumption is low; handling hiccups is difficult
        """
        now = time.time()
        for attacker_callsign, attacker_position in attacker_positions.items():
            iff_type = ifd.interrogate(attacker_callsign, attacker_position.iff_hash, self.parent.iff_code)
            if iff_type is ifd.IFFType.iff_friendly:
                continue
            if attacker_callsign in self.attackers_hist_positions:
                position_history = self.attackers_hist_positions[attacker_callsign]
            else:
                position_history = AttackerPositionHistory(u.MAX_DELTA_SHOOTABLE / self.parent.send_freq + 1)
                self.attackers_hist_positions[attacker_callsign] = position_history
            position_history.add_position(attacker_position, now)

        return self._process_attackers(now, armament_in_flight)

    @abstractmethod
    def _process_attackers(self, now: float,
                           missiles_in_flight: list[en.ArmamentInFlightNotification]) -> list[m.MissileEvent]:
        pass
