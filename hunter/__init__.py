# SPDX-FileCopyrightText: (C) 2020 - 2020, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""The top package for the part of Hunter getting executed as server processes."""
