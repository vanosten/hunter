# SPDX-FileCopyrightText: (C) 2021 - 2023, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Creates a JSON file with all airports referenced in Terrasync.
Preferably information is
"""
import os
import pickle

import osm2city.utils.aptdat_io as aio

import hunter.scenarios as s
import hunter.utils as u


if __name__ == '__main__':
    airports = aio.read_apt_dat_gz_file(-180, -90, 180, 90, True)
    simplified_list = list()
    for airport in airports:
        lon, lat = airport.calculate_centre()
        apt_dat_apt = s.AptDatAirport(airport.code, airport.name, lon, lat, airport.kind)
        simplified_list.append(apt_dat_apt)

    output_file = os.path.join(u.get_scenario_resources_dir(), s.APT_DAT_AIRPORT_FILE)
    with open(output_file, 'wb') as file_pickle:
        pickle.dump(simplified_list, file_pickle)
    print(len(simplified_list))
