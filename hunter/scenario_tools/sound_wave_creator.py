"""Simple script to create a .wav file - not really belonging to Hunter ..."""
import numpy as np
import wave


if __name__ == '__main__':
    sample_rate: int = 44100  # sample rate in Hz
    sound_duration: float = .5  # duration of the sound
    silence_duration: float = .5 # duration of no sound added to the end (e.g. useful when looping the sound to create a pattern)
    total_duration: float = 10.  # only used if silence_duration > 0
    frequency: int = 1000
    chop_frequency: int = 25  # chopping frequency in Hz - i.e. how often the sound is on/off per second
    amplitude: int = 32767  # max amplitude for 16-bit audio

    # generate the sound wave
    t = np.linspace(0, sound_duration, int(sound_duration * sample_rate), endpoint=False)
    wave_sound = amplitude * np.sin(2 * np.pi * frequency * t)

    if chop_frequency > 0:
        chop = 0.5 * (1 + np.sign(np.sin(2 * np.pi * chop_frequency * t)))
        wave_sound = wave_sound * chop
    if silence_duration > 0:
        t_silence = np.zeros(int(silence_duration * sample_rate))
        num_cyles = int(total_duration / (sound_duration + silence_duration))
        wave_data = np.tile(np.concatenate((wave_sound, t_silence)), num_cyles)
    else:
        wave_data = wave_sound
    samples = wave_data.astype(np.int16)
    with wave.open('sound.wav', 'wb') as wf:
        wf.setnchannels(1)  # mono
        wf.setsampwidth(2)  # 16 bit
        wf.setframerate(sample_rate)
        wf.writeframes(samples.tobytes())
