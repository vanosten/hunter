# SPDX-FileCopyrightText: (C) 2023 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
""" Creates plausible networks for ships based on land vs. sea.

The goal is to generate a network for ships automatically based on mapping data (from OSM) instead of creating
a network manually. Also, the network should be suitable for larger ships (e.g. frigates) vs. smaller coastal
ships. The former stay away from the coast due to steering a threat - while the others stay close to the shore due
to visual navigation etc. For path planning a global view of a static environment is assumed.

References:
A] Yiquan Du, 2021: An Optimized Path Planning Method for Coastal Ships Based on Improved DDPG and DP
B] S. Russel, P. Norvig: Artificial Intelligence; A modern approach; Third edition, chapter 24.4
C] Yuanyuan Qiao et al., 2023: Survey of Deep Learning for Autonomous Surface Vehicles in Marine Environments
D] Junfeng Yuan et al., 2022: A second‐order dynamic and static ship path planning model based on
   reinforcement learning and heuristic search algorithms
E] Siyu Guo, 2021: Path Planning of Coastal Ships Based on Optimized DQN Reward Function
F] Yuanyuan Qiao, 2023: Survey of Deep Learning for Autonomous Surface Vehicles in Marine Environments
G] https://blog.habrador.com/2015/11/explaining-hybrid-star-pathfinding.html
H] Dmitri Dolgov et al., ????: Practical Search Techniques in Path Planning for Autonomous Driving.
   https://ai.stanford.edu/~ddolgov/papers/dolgov_gpp_stair08.pdf

A roadmap approach for static global path planning has been used, because:
* It is easy to understand and implement.
* Traditional shortest path can then be used (e.g. Dijkstra or A*) - maybe with some smoothing e.g. based on a
  Douglas-Peucher algorithm.
* Computationally OK - plus network generation can be done once and saved for each scenario.
* This is a simulation for fun - it does not have to be an optimal solution and the solution does not have to be
  smooth. Just when a pilot looks at it, then the navigation should look probable.
* While it does not allow for dynamic path planning, local obstacle avoidance e.g. for other ships can still be
  done by temporarily hop out of the current path. That might give collisions sometimes - but that is acceptable.

Alternatives would have been (see e.g. ref[A], which has good comparison of advantages/disadvantages incl. direct
tabular and visual comparison of path length, geometry as well as computation time)
* Artificial potential fields
* Different approaches using reinforcement learning with different types of neural networks

A custom network generation has been used based on grid. Alternatives would have been:
* Roadmap based path planning: visibility or Voronoi graphs (e.g. https://www.youtube.com/watch?v=Y5_aHsqX22s)
* Probabilistic roadmap (PRM, PRM* - e.g. https://theclassytim.medium.com/robotic-path-planning-prm-prm-b4c64b1f5acb)
* Rapidly-exploring random trees (RRT, RRT* - e.g.
                                  https://theclassytim.medium.com/robotic-path-planning-rrt-and-rrt-212319121378)

-------------
A quadtree is used with grid-sizes as follows:
    Level 0 is the most detailed quadtree - and an edge has the same length as the scenario's dem_grid_size.
    Level is the hierarchy in the quadtree.
    The size of a level is based on the scenario's dem_grid_size -> dem_grid_size * 2^level
    Level 2 (typically smallest) is ca. 50m * 2^2 = 200m, level 5 (largest) ca. 50m * 2^5 = 1600m


The chosen design is based on the fact that for a typical scenario with 200 km * 200 km and the smallest grid
size of 200 m the resulting network could have 1 mio nodes, which makes it very computationally intensive to
find a path. The other reason is that the "blockiness" of the grid along the coast for the near-shore buffer gives
[A] some randomness to where the coastal ships are moving (not only close to the shore), [B] reduces the necessary
network size, and [C] increases the chance that e.g. 2 islands or an island and the shore get into one connected
network instead of isolated. ("Blockiness" here means that the buffer around land mass is first tested against
the largest grid size and then smaller. Sometimes an edge of a rather large grid cell just touches the buffer and
thereby a large grid cell is added to nearshore.

The network is created as follows:
* Vector data is based on land polygons from https://osmdata.openstreetmap.de/data/land-polygons.html, which again is
derived from OSM based on natural=coastline.
* A simplistic artificial potential field is added along the coast - one for close shore (to push the sips a bit
  away where possible) as well as a belt of a few 100 metres to have a preferred sailing area for small coastal
  ships respectively an area close to the shore which large ships avoid.
* Cell decomposition from vector data to raster data. Using approximate quadtree composition - see e.g.
  https://cs.stanford.edu/people/eroberts/courses/soco/projects/1998-99/robotics/basicmotion.html.
  Using composite / button-up clustering.
* To reduce the first call decomposition calculation time, clusters of cells will be combined
  (e.g. from 50m raster to 100m->200m->400m).

For the final network of the large ships the following is done:
* Frigates / large ships: Only grid cells on level 5 (not broken down) are taken into consideration - but no difference
  is made whether the cell is nearshore or offshore (that will be used for weight on edges).
  This makes sure that the ships are only in fairly open water or between islands still take routes that are
  wide enough.
* Speedboats: only grid cells for nearshore are taken into account and broken down to the chosen lowest level.

Based on this a network is drawn between all nodes.This will most probably result in several networks.
Smaller networks are discarded based on parameters.

There is an optimization possibility to reduce the number of nodes and thereby the network size by using hierarchical
cells in the quadtree, i.e. not braking them down to an equal grid size - however, that has not been implemented.

"""
from enum import IntEnum, unique
import logging
import os.path as osp
import pickle
from typing import Optional

import geopandas as gpd
import networkx as nx
from shapely.geometry import box, Polygon, LineString

import hunter.geometry as g
import hunter.logger_config as lc
import hunter.scenarios as sc
import hunter.network_stuff as ns


GEOMETRY_FEATURE = 'geometry'

LEVEL_CELLS_PKL = 'level_cells_dict.pkl'


@unique
class ShoreType(IntEnum):
    """The type of content in the cell. All but 'coast' are navigable.
        Cf. https://en.wikipedia.org/wiki/Littoral_zone for terms.
    """
    land = 0
    # used to keep a safety distance from the beach, but can be penetrated to reach a port
    # however is not used, because the blockyness of the coast vs. nearshore plus using the middle of the cell as
    # the waypoint already gives a safety margin
    foreshore = 1
    nearshore = 2  # in this zone most small/coastal vessels are - and large vessels keep away
    offshore = 3  # good for frigates etc.
    outside_sail_area = 5


class ShipNetwork:
    __slots__ = ('offshore_network', 'offshore_poi', 'nearshore_network', 'nearshore_poi')

    def __init__(self):
        self.offshore_network = None
        self.offshore_poi = None
        self.nearshore_network = None
        self.nearshore_poi = None


class GridCell:
    """The raster underlying the network.

    lon_idx is the index for longitude and starts in West -> towards East
    lat_idx is the index for latitude and starts in South -> towards North
    """
    __slots__ = ('lon', 'lat', 'lon_idx', 'lat_idx', 'level', 'shore_type', 'final_level',
                 'parent', 'children')

    def __init__(self, lon: float, lat: float, lon_idx: int, lat_idx: int, level: int,
                 parent: Optional['GridCell']):
        self.lon = lon
        self.lat = lat
        self.lon_idx = lon_idx
        self.lat_idx = lat_idx
        self.level = level
        self.shore_type = ShoreType.offshore
        self.final_level: bool = False
        self.parent = parent
        self.children = list()

    @property
    def grid_id(self) -> tuple[int, int]:
        return self.lon_idx, self.lat_idx

    def node_id(self) -> int:
        """A unique id for the node to be used in networkx.
        We need a large number to cover e.g. on level 0 a 50 metres grid for 200 km.
        """
        return self.level*10000*10000 + self.lon_idx*10000 + self.lat_idx

    def add_child(self, child: 'GridCell') -> None:
        self.children.append(child)

    def has_children(self) -> bool:
        return len(self.children) > 0

    def has_final_parent_in_hierarchy(self) -> bool:
        cell = self.find_final_parent_in_hierarchy()
        return False if cell is None else True

    def find_final_parent_in_hierarchy(self) -> Optional['GridCell']:
        current = self
        while True:
            if current.parent:
                if current.parent.final_level:
                    return current.parent
                else:
                    current = current.parent
            else:
                break
        return None


def _create_geo_data_frame_box(boundary_tuple: tuple[float, float, float, float],
                               buffer_deg: float = 0.) -> gpd.GeoDataFrame:
    boundary_box = box(boundary_tuple[0] - buffer_deg, boundary_tuple[1] - buffer_deg,
                       boundary_tuple[2] + buffer_deg, boundary_tuple[3] + buffer_deg)
    d = {GEOMETRY_FEATURE: [boundary_box]}
    return gpd.GeoDataFrame(d, crs='EPSG:4326')


def _read_land_data(boundary_tuple: tuple[float, float, float, float], land_poly_file: str) -> gpd.GeoDataFrame:
    geo_box = _create_geo_data_frame_box(boundary_tuple, 0.1)  # make sure no residuals at borders
    land_polygons = gpd.read_file(land_poly_file)
    # Clip the data using GeoPandas clip
    land_clip = gpd.clip(land_polygons, geo_box, keep_geom_type=False)
    return land_clip


def my_colourmap(value):  # scalar value defined in 'column'
    if value == ShoreType.land:
        return "grey"
    elif value == ShoreType.offshore:
        return "blue"
    elif value == ShoreType.outside_sail_area:
        return "green"
    return "yellow"


def _calculate_grid_cells(land_data: gpd.GeoDataFrame, lon_steps: int, lat_steps: int,
                          scenario: sc.ScenarioContainer,
                          levels: list[int], nearshore_factor: int,
                          is_offshore: bool) -> dict[int, dict[tuple[int, int], GridCell]]:
    """Generates GridCells at different levels depending on need for break-down into smaller cells.
    Test first intersect - and then within, because if within land, then no further breakdown needed.
    => only those which intersect need to be broken down to find out what is what (again first intersect, then within)
    """
    level_cells_dict = dict()
    this_level_cells = dict()
    for idx_lon in range(round(lon_steps / pow(2, levels[0]))):
        for idx_lat in range(round(lat_steps / pow(2, levels[0]))):
            lon = scenario.south_west[0] + (idx_lon + 0.5) * scenario.lon_dem_grid_size * pow(2, levels[0])
            lat = scenario.south_west[1] + (idx_lat + 0.5) * scenario.lat_dem_grid_size * pow(2, levels[0])
            cell = GridCell(lon, lat, idx_lon, idx_lat, levels[0], None)
            this_level_cells[cell.grid_id] = cell
    for level in levels:
        logging.info('Processing quadtree decomposition at level %i', level)
        if level != levels[0]:
            this_level_cells = dict()
            parent_level_cells = level_cells_dict[level + 1]
            for parent_cell in parent_level_cells.values():
                start_lon_idx = parent_cell.lon_idx * 2
                start_lat_idx = parent_cell.lat_idx * 2
                for idx_lon in range(2):
                    for idx_lat in range(2):
                        lon = scenario.south_west[0] + (
                                start_lon_idx + idx_lon + 0.5) * scenario.lon_dem_grid_size * pow(2, level)
                        lat = scenario.south_west[1] + (
                                start_lat_idx + idx_lat + 0.5) * scenario.lat_dem_grid_size * pow(2, level)
                        cell = GridCell(lon, lat, start_lon_idx + idx_lon, start_lat_idx + idx_lat, level,
                                        parent_cell)
                        this_level_cells[cell.grid_id] = cell
                        parent_cell.add_child(cell)

        count_land_cells = 0
        count_land_cells_final = 0
        count_outside_sail_area_cells_final = 0
        count_offshore_cells_final = 0
        total_level_cells = len(this_level_cells)
        counter = 0
        sail_area_nodes = scenario.ships_definition.ship_network_parameters.sail_area_offshore if is_offshore else \
            scenario.ships_definition.ship_network_parameters.sail_area_coastal
        if sail_area_nodes:
            sail_area = Polygon(sail_area_nodes)
            logging.info('Sail area is applied')
        else:
            sail_area = None
            logging.info('No sail area applied')
        for cell in this_level_cells.values():
            if counter % 1000 == 0:
                logging.info('%i out of %i cells tested at level %i', counter, total_level_cells, level)
            counter += 1
            if cell.has_final_parent_in_hierarchy():
                cell.shore_type = cell.parent.shore_type
            else:
                cell_box = box(scenario.south_west[0] + cell.lon_idx * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + cell.lat_idx * scenario.lat_dem_grid_size * pow(2, level),
                               scenario.south_west[0] + (cell.lon_idx + 1) * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + (cell.lat_idx + 1) * scenario.lat_dem_grid_size * pow(2, level)
                               )
                disjoint_or_intersected = False

                if sail_area and cell_box.disjoint(sail_area):  # first test whether within sail area
                    cell.shore_type = ShoreType.outside_sail_area
                    count_outside_sail_area_cells_final += 1
                    disjoint_or_intersected = True
                    cell.final_level = True
                if not disjoint_or_intersected:  # test against land
                    for index, row in land_data.iterrows():  # Looping over all polygons
                        if cell_box.intersects(row[GEOMETRY_FEATURE]):
                            cell.shore_type = ShoreType.land
                            count_land_cells += 1
                            disjoint_or_intersected = True
                            if level != levels[-1] and cell_box.within(row[GEOMETRY_FEATURE]):
                                cell.final_level = True
                                count_land_cells_final += 1
                            break
                if not disjoint_or_intersected:
                    # cell is by default .shore_type = ShoreType.offshore
                    cell.final_level = True
                    count_offshore_cells_final += 1
                if level == levels[-1]:
                    cell.final_level = True

        level_cells_dict[level] = this_level_cells
        logging.info('Out of %i grid cells %i are land', total_level_cells, count_land_cells)
        logging.info('Out of %i grid cells %i are land final', total_level_cells, count_land_cells_final)
        logging.info('Out of %i grid cells %i are outside sail area final',
                     total_level_cells, count_outside_sail_area_cells_final)
        logging.info('Out of %i grid cells %i are offshore final', total_level_cells, count_offshore_cells_final)

    # For all those cells, which still are .offshore: test against nearshore buffer, but do not break down
    # into smaller cells -> just one pass
    nearshore_geoms = list()
    for index, row in land_data.iterrows():  # Looping over all polygons
        nearshore_geoms.append(row[GEOMETRY_FEATURE].buffer(scenario.lon_dem_grid_size * nearshore_factor))

    count_land_cells = 0
    count_offshore_cells = 0
    count_nearshore_cells = 0
    geometry_col = list()
    shore_type_col = list()
    for level in levels:
        for cell in level_cells_dict[level].values():
            if cell.final_level:
                intersection_found = False
                cell_box = box(scenario.south_west[0] + cell.lon_idx * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + cell.lat_idx * scenario.lat_dem_grid_size * pow(2, level),
                               scenario.south_west[0] + (cell.lon_idx + 1) * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + (cell.lat_idx + 1) * scenario.lat_dem_grid_size * pow(2, level)
                               )
                if cell.shore_type is ShoreType.offshore:
                    for nearshore in nearshore_geoms:
                        if cell_box.intersects(nearshore):
                            cell.shore_type = ShoreType.nearshore
                            count_nearshore_cells += 1
                            intersection_found = True
                            break
                    if intersection_found is False:
                        count_offshore_cells += 1
                else:
                    count_land_cells += 1
                geometry_col.append(cell_box)
                shore_type_col.append(cell.shore_type.value)

    logging.info('There are %i coast, %i nearshore and %i offshore grid cells', count_land_cells,
                 count_nearshore_cells, count_offshore_cells)
    if not is_offshore:
        # now make sure that we also have nearshore set at lowest level
        added_nearshore_lowest_level = 0
        for cell in level_cells_dict[levels[-1]].values():
            final_parent = cell.find_final_parent_in_hierarchy()
            if final_parent and final_parent.shore_type is ShoreType.nearshore:
                cell.shore_type = ShoreType.nearshore
                added_nearshore_lowest_level += 1
        logging.info('Added %i nearshore cells on lowest level', added_nearshore_lowest_level)

    d = {GEOMETRY_FEATURE: geometry_col, 'ShoreType': shore_type_col}
    map_gdf = gpd.GeoDataFrame(d, crs='EPSG:4326')
    m = map_gdf.explore(column='ShoreType',
                        tooltip="ShoreType",
                        popup=True,
                        cmap='Set1',  # use "Set1" matplotlib colormap
                        # cmap=lambda value: my_colourmap(value),
                        # legend=False,  # needed -> https://github.com/geopandas/geopandas/pull/2590
                        style_kwds=dict(color="black"),
                        )
    map_name = 'final_data_offshore.html' if is_offshore else 'final_data_coastal.html'
    m.save(scenario.ident + '_' + map_name)

    return level_cells_dict


def _calc_grid_cells_from_landuse(scenario: sc.ScenarioContainer, land_poly_file: str, levels: list[int],
                                  is_offshore: bool) -> None:
    """Read a scenario file and use the contained information to generate a DEM.
    The DEM is written to the scenario files with a file name matching a convention."""

    lon_steps = int((scenario.north_east[0] - scenario.south_west[0]) / scenario.lon_dem_grid_size)
    lat_steps = int((scenario.north_east[1] - scenario.south_west[1]) / scenario.lat_dem_grid_size)

    logging.info('Starting to read land data from Shapefile')
    land_data = _read_land_data((scenario.south_west[0], scenario.south_west[1],
                                 scenario.north_east[0], scenario.north_east[1]),
                                land_poly_file)
    logging.info('Done reading land data from Shapefile')

    m = land_data.explore()
    m.save(scenario.ident + '_land_data.html')

    level_cells_dict = _calculate_grid_cells(land_data, lon_steps, lat_steps, scenario, levels,
                                             scenario.ships_definition.ship_network_parameters.nearshore_factor,
                                             is_offshore)

    with open(LEVEL_CELLS_PKL, 'wb') as file_pickle:
        pickle.dump(level_cells_dict, file_pickle)


def _generate_offshore_network(level_cells_dict: dict[int, dict[tuple[int, int], GridCell]],
                               levels: list[int]) -> nx.Graph:
    """Generate the offshore network - which is only based on highest level cells."""
    offshore_graph = nx.Graph()

    top_level_cells_dict = level_cells_dict[levels[0]]
    max_lon_idx = 0
    max_lat_idx = 0
    for indices in top_level_cells_dict.keys():
        max_lon_idx = max(max_lon_idx, indices[0])
        max_lat_idx = max(max_lat_idx, indices[1])

    all_wps: dict[int, g.WayPoint] = dict()
    for lon_idx in range(0, max_lon_idx + 1):
        for lat_idx in range(0, max_lat_idx + 1):
            grid_cell = top_level_cells_dict[(lon_idx, lat_idx)]
            if grid_cell.shore_type is ShoreType.offshore:
                if grid_cell.node_id() not in all_wps:
                    grid_wp = g.WayPoint(grid_cell.node_id(), grid_cell.lon, grid_cell.lat, is_poi=True)
                    offshore_graph.add_node(grid_wp)
                    all_wps[grid_cell.node_id()] = grid_wp
                else:
                    grid_wp = all_wps[grid_cell.node_id()]
                # cell to East
                if lon_idx + 1 <= max_lon_idx:
                    east_cell = top_level_cells_dict.get((lon_idx + 1, lat_idx))
                    if east_cell.shore_type is ShoreType.offshore:
                        if east_cell.node_id() not in all_wps:
                            east_wp = g.WayPoint(east_cell.node_id(), east_cell.lon, east_cell.lat, is_poi=True)
                            offshore_graph.add_node(east_wp)
                            all_wps[east_cell.node_id()] = east_wp
                        else:
                            east_wp = all_wps[east_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, east_wp)
                        offshore_graph.add_edge(grid_wp, east_wp, weight=dist)
                # cell to North
                if lat_idx + 1 <= max_lat_idx:
                    north_cell = top_level_cells_dict.get((lon_idx, lat_idx + 1))
                    if north_cell.shore_type is ShoreType.offshore:
                        if north_cell.node_id() not in all_wps:
                            north_wp = g.WayPoint(north_cell.node_id(), north_cell.lon, north_cell.lat, is_poi=True)
                            offshore_graph.add_node(north_wp)
                            all_wps[north_cell.node_id()] = north_wp
                        else:
                            north_wp = all_wps[north_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, north_wp)
                        offshore_graph.add_edge(grid_wp, north_wp, weight=dist)
                # cell to North-East
                if lon_idx + 1 <= max_lon_idx and lat_idx + 1 <= max_lat_idx:
                    ne_cell = top_level_cells_dict.get((lon_idx + 1, lat_idx + 1))
                    if ne_cell.shore_type is ShoreType.offshore:
                        if ne_cell.node_id() not in all_wps:
                            ne_wp = g.WayPoint(ne_cell.node_id(), ne_cell.lon, ne_cell.lat, is_poi=True)
                            offshore_graph.add_node(ne_wp)
                            all_wps[ne_cell.node_id()] = ne_wp
                        else:
                            ne_wp = all_wps[ne_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, ne_wp)
                        offshore_graph.add_edge(grid_wp, ne_wp, weight=dist)
                # cell to North-West
                if lon_idx - 1 >= 0 and lat_idx + 1 <= max_lat_idx:
                    nw_cell = top_level_cells_dict.get((lon_idx - 1, lat_idx + 1))
                    if nw_cell.shore_type is ShoreType.offshore:
                        if nw_cell.node_id() not in all_wps:
                            nw_wp = g.WayPoint(nw_cell.node_id(), nw_cell.lon, nw_cell.lat, is_poi=True)
                            offshore_graph.add_node(nw_wp)
                            all_wps[nw_cell.node_id()] = nw_wp
                        else:
                            nw_wp = all_wps[nw_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, nw_wp)
                        offshore_graph.add_edge(grid_wp, nw_wp, weight=dist)

    logging.info("Created offshore network graph with %i nodes and %i edges",
                 nx.number_of_nodes(offshore_graph), nx.number_of_edges(offshore_graph))
    return offshore_graph


def _generate_coastal_network(level_cells_dict: dict[int, dict[tuple[int, int], GridCell]],
                              levels: list[int]) -> nx.Graph:
    """Generate the coastal network - which is based on a hierarchy of cells starting at the lowest level.
    In contrast to the offshore network cells are only connected vertical and horizontal - not diagonal.
    """
    coastal_graph = nx.Graph()

    lowest_level_cells_dict = level_cells_dict[levels[-1]]
    max_lon_idx = 0
    max_lat_idx = 0
    for indices in lowest_level_cells_dict.keys():
        max_lon_idx = max(max_lon_idx, indices[0])
        max_lat_idx = max(max_lat_idx, indices[1])

    all_wps: dict[int, g.WayPoint] = dict()
    all_lowest_used_grid_cells: dict[int, GridCell] = dict()
    for lon_idx in range(0, max_lon_idx + 1):
        for lat_idx in range(0, max_lat_idx + 1):
            grid_cell = lowest_level_cells_dict[(lon_idx, lat_idx)]
            if grid_cell.shore_type is ShoreType.nearshore:
                all_lowest_used_grid_cells[grid_cell.node_id()] = grid_cell
                if grid_cell.node_id() not in all_wps:
                    grid_wp = g.WayPoint(grid_cell.node_id(), grid_cell.lon, grid_cell.lat, is_poi=True)
                    coastal_graph.add_node(grid_wp)
                    all_wps[grid_cell.node_id()] = grid_wp
                else:
                    grid_wp = all_wps[grid_cell.node_id()]
                # cell to East
                if lon_idx + 1 <= max_lon_idx:
                    east_cell = lowest_level_cells_dict.get((lon_idx + 1, lat_idx))
                    if east_cell.shore_type is ShoreType.nearshore:
                        if east_cell.node_id() not in all_wps:
                            east_wp = g.WayPoint(east_cell.node_id(), east_cell.lon, east_cell.lat, is_poi=True)
                            coastal_graph.add_node(east_wp)
                            all_wps[east_cell.node_id()] = east_wp
                        else:
                            east_wp = all_wps[east_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, east_wp)
                        coastal_graph.add_edge(grid_wp, east_wp, weight=dist)
                # cell to North
                if lat_idx + 1 <= max_lat_idx:
                    north_cell = lowest_level_cells_dict.get((lon_idx, lat_idx + 1))
                    if north_cell.shore_type is ShoreType.nearshore:
                        if north_cell.node_id() not in all_wps:
                            north_wp = g.WayPoint(north_cell.node_id(), north_cell.lon, north_cell.lat, is_poi=True)
                            coastal_graph.add_node(north_wp)
                            all_wps[north_cell.node_id()] = north_wp
                        else:
                            north_wp = all_wps[north_cell.node_id()]
                        dist = g.calc_distance_wp_wp(grid_wp, north_wp)
                        coastal_graph.add_edge(grid_wp, north_wp, weight=dist)

    logging.info("Created coastal network graph with %i nodes and %i edges on level %i",
                 nx.number_of_nodes(coastal_graph), nx.number_of_edges(coastal_graph), levels[-1])

    # Now we try to reduce the network by connecting the final parents in the hierarchy
    final_parents_found = 0
    final_parents_unique = 0
    lowest_level_to_remove: set[g.WayPoint] = set()
    for lowest_grid_cell in all_lowest_used_grid_cells.values():
        final_parent = lowest_grid_cell.find_final_parent_in_hierarchy()
        if final_parent is None:
            continue  # nothing to do - will not have a final parent
        final_parents_found += 1
        # find all current neighbours of the lowest_grid_cell and make connections from the final_parent
        # once we will be done with all nodes and remove them, then all related edges will also be removed.
        if final_parent.node_id() not in all_wps:
            final_parents_unique += 1
            final_parent_wp = g.WayPoint(final_parent.node_id(), final_parent.lon, final_parent.lat, is_poi=True)
            coastal_graph.add_node(final_parent_wp)
            all_wps[final_parent.node_id()] = final_parent_wp
        else:
            final_parent_wp = all_wps[final_parent.node_id()]
        lowest_wp = all_wps[lowest_grid_cell.node_id()]
        neighbours_lowest = list(coastal_graph.neighbors(lowest_wp))
        for neighbour_wp in neighbours_lowest:
            coastal_graph.add_edge(final_parent_wp, neighbour_wp)
        lowest_level_to_remove.add(lowest_wp)

    logging.info('Coastal network has now %i nodes and %i edges after finding %i final parents (%i unique).',
                 nx.number_of_nodes(coastal_graph), nx.number_of_edges(coastal_graph),
                 final_parents_found, final_parents_unique)

    # finally remove the nodes on the lowest level
    coastal_graph.remove_nodes_from(lowest_level_to_remove)
    logging.info('Coastal network has now %i nodes and %i edges after removing the lowest nodes (%i to replace)',
                 nx.number_of_nodes(coastal_graph), nx.number_of_edges(coastal_graph),
                 len(lowest_level_to_remove))
    return coastal_graph


def _curate_network(scenario: sc.ScenarioContainer, dirty_graph: nx.Graph, minimal_circumference: int,
                    is_offshore: bool) -> list[nx.Graph]:
    """Find the large enough subgraphs and clean for dangling nodes"""
    offshore_subgraphs = [dirty_graph.subgraph(c).copy() for c in nx.connected_components(dirty_graph)]
    final_offshore_networks = list()
    removed = 0
    for network in offshore_subgraphs:
        min_lon = 999
        min_lat = 999
        max_lon = -999
        max_lat = -99
        nodes_list = list(network.nodes())
        if network.number_of_edges() < ns.MIN_NUMBER_NODES_NETWORKS:
            removed += 1
            continue
        for wp in nodes_list:
            min_lon = min(min_lon, wp.lon)
            min_lat = min(min_lat, wp.lat)
            max_lon = max(max_lon, wp.lon)
            max_lat = max(max_lat, wp.lat)
        lon_dist = g.calc_distance(min_lon, min_lat, max_lon, min_lat)
        lat_dist = g.calc_distance(min_lon, min_lat, min_lon, max_lat)
        if (lon_dist * 2 + lat_dist * 2) >= minimal_circumference:
            ns.remove_dangling_nodes(network)
            final_offshore_networks.append(network)
            logging.info("Created sub-network with %i nodes and %i edges",
                         nx.number_of_nodes(network), nx.number_of_edges(network))
        else:
            removed += 1
    logging.info("Original graph has %i sub-networks of sufficient size, %i were removed.",
                 len(final_offshore_networks), removed)

    # now do the drawing stuff
    line_strings: list[LineString] = list()
    for network in final_offshore_networks:
        edges = list(network.edges())
        for edge in edges:
            node1, node2 = edge
            line_strings.append(LineString([(node1.lon, node1.lat), (node2.lon, node2.lat)]))
    gdf = gpd.GeoDataFrame(
        {"geometry": line_strings},
        crs="EPSG:4326",
    )
    m = gdf.explore(tooltip=False, popup=False, legend=False)
    map_name = 'final_network_offshore.html' if is_offshore else 'final_network_coastal.html'
    m.save(scenario.ident + '_' + map_name)

    return final_offshore_networks


if __name__ == '__main__':
    parser = ns.create_argument_parser_default('Create a ship network for Hunter using OSM data \
    based on a lon/lat defined area', False, False, False, True)
    parser.add_argument('-l', dest='land_poly', type=str,
                        help='the full path for the land polygons shapefile -'
                             'see https://osmdata.openstreetmap.de/data/land-polygons.html',
                        required=True)

    lc.configure_logging(lc.LogConfig('INFO', True), 'ship_network')
    args = parser.parse_args()

    the_scenario = sc.load_scenario(args.scenario, args.scenario_path)  # raises ValueError if it does not exist
    ship_networks = sc.ShipNetworks()
    the_ship_networks_file_path = osp.join(args.scenario_path, args.scenario + sc.FILE_SHIP_NETWORKS)

    # do the offshore
    is_it_offshore = True
    the_levels = [the_scenario.ships_definition.ship_network_parameters.max_level]
    _calc_grid_cells_from_landuse(the_scenario, args.land_poly, the_levels, is_it_offshore)
    with open(LEVEL_CELLS_PKL, 'rb') as my_file_pickle:
        my_level_cells_dict = pickle.load(my_file_pickle)

    min_circumference = the_scenario.ships_definition.ship_network_parameters.min_offshore_circumference
    my_dirty_graph = _generate_offshore_network(my_level_cells_dict, the_levels)
    curated_graphs = _curate_network(the_scenario, my_dirty_graph, min_circumference, is_it_offshore)
    if curated_graphs:
        ship_networks.offshore_networks = curated_graphs

    # do the coastal stuff
    is_it_offshore = False
    the_levels: list[int] = list()
    for a_level in range(the_scenario.ships_definition.ship_network_parameters.max_level,
                         the_scenario.ships_definition.ship_network_parameters.min_level - 1, -1):
        the_levels.append(a_level)

    _calc_grid_cells_from_landuse(the_scenario, args.land_poly, the_levels, is_it_offshore)
    with open(LEVEL_CELLS_PKL, 'rb') as my_file_pickle:
        my_level_cells_dict = pickle.load(my_file_pickle)

    min_circumference = the_scenario.ships_definition.ship_network_parameters.min_coastal_circumference
    my_dirty_graph = _generate_coastal_network(my_level_cells_dict, the_levels)
    curated_graphs = _curate_network(the_scenario, my_dirty_graph, min_circumference, is_it_offshore)
    if curated_graphs:
        ship_networks.coastal_networks = curated_graphs

    # finally save the new ShipNetworks object
    ship_networks.save_ship_networks_to_file(the_ship_networks_file_path)
