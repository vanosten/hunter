# SPDX-FileCopyrightText: (C) 2020 - 2023, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Contains scripts to generate data resources for Hunter based on OpenStreetMap.

Often these scripts are based on osm2city (explicit dependency) and are only run once to prepare scenarios;
i.e. they are not run as part of the multiplayer processing of a scenario.
"""
