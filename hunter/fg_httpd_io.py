# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from typing import Any, Tuple

import requests
from requests.adapters import HTTPAdapter
from urllib3.util import Retry

import hunter.logger_config as lc

logger = lc.get_logger()


def convert_to_fg_bool_str(value: bool) -> str:
    if value:
        return 'true'
    else:
        return 'false'


def convert_to_fg_bool_int(value: bool) -> int:
    if value:
        return 1
    else:
        return 0


def convert_from_fg_int_bool(value: int) -> bool:
    if value == 1:
        return True
    else:
        return False


class FGHttpd:

    __slots__ = ('port', 'host', 'session')

    # values are set quite high, but we want to be sure they succeed also on a smaller computer which might
    # get overwhelmed
    RESPONSE_TIMEOUT_GET = 10.  # seconds to wait for response when getting data
    RESPONSE_TIMEOUT_SET = 10.  # seconds to wait for response when setting data
    FG_COMMAND_TIMEOUT = 15.  # seconds to wait for response - waiting a bit longer as it is a command
    """An interface to a running FlightGear instance using the FlightGear built-in httpd-server.
    See http://wiki.flightgear.org/Property_Tree/Web_Server and http://wiki.flightgear.org/Json_Properties.

    Many concepts can also be found in the JavaScript source code of Phi (http://wiki.flightgear.org/Phi),
    available in $FG_DATA/Phi.
    
    Typing of values is explained in http://wiki.flightgear.org/Property_Tree/Explained#Typing."""
    def __init__(self, port: int, host: str = 'localhost') -> None:
        self.port = port
        self.host = host
        self.session = requests.Session()
        retry = Retry(total=5, backoff_factor=0.1, status_forcelist=(500, 502, 504))
        adapter = HTTPAdapter(max_retries=retry)
        self.session.mount('http://', adapter)
        logger.info('Initialized FGHttp on %s:%i', host, port)

    def _get_single_property(self, path: str) -> Tuple[Any, str]:
        r = self.session.get('http://{}:{}/json{}'.format(self.host, self.port, path),
                             timeout=self.RESPONSE_TIMEOUT_GET)
        r.raise_for_status()
        json_content = r.json()
        value = json_content['value']
        value_type = json_content['type']
        return value, value_type

    def get_str_property(self, path: str) -> str:
        return self._get_single_property(path)[0]

    def get_float_property(self, path: str) -> float:
        """Use the same for FG properties of type double or float."""
        return float(self._get_single_property(path)[0])

    def get_bool_property(self, path: str, int_value: bool) -> bool:
        value = self._get_single_property(path)[0]
        if int_value:
            return convert_from_fg_int_bool(value)
        else:
            return value

    def set_single_property(self, path: str, value) -> None:
        logger.debug('Sending set_single_property() with path "%s" and value %s', path, str(value))
        payload = {'value': value}
        r = self.session.post('http://{}:{}/json{}'.format(self.host, self.port, path), json=payload,
                              timeout=self.RESPONSE_TIMEOUT_SET)
        logger.debug('Response status code: %i - with text: %s', r.status_code, r.text)
        r.raise_for_status()

    def set_bool_property(self, path: str, value: bool, int_value: bool = False) -> None:
        if int_value:
            self.set_single_property(path, convert_to_fg_bool_int(value))
        else:
            self.set_single_property(path, convert_to_fg_bool_str(value))

    def _run_fgcommand(self, fg_command: str) -> None:
        payload = {'value': fg_command}
        r = self.session.get('http://{}:{}/run.cgi'.format(self.host, self.port), params=payload,
                             timeout=self.FG_COMMAND_TIMEOUT)
        status_code = r.status_code
        logger.info('HTTP return code %i for fg_command "%s"', status_code, fg_command)

    def simulator_reset(self) -> None:
        self._run_fgcommand('reset')

    def simulator_exit(self) -> None:
        self._run_fgcommand('exit')

    def crashed(self, prop: str = '/sim/crashed') -> bool:
        """Checks whether the simulated aircraft has crashed.
        Parameter prop can be set if the property to check is not the default one."""
        is_crashed = self._get_single_property(prop)[0]
        if isinstance(is_crashed, bool):
            return is_crashed
        if isinstance(is_crashed, int):
            return is_crashed > 0
        if isinstance(is_crashed, str):
            return is_crashed == 'true'
        return False

    def sunk(self) -> bool:
        """Carriers, missile frigates and missile SAMs use this instead of /sim/crashed"""
        return self.crashed('/carrier/sunk')

    def send_chat(self, message: str) -> None:
        self.set_single_property('/sim/multiplay/chat', message)


if __name__ == '__main__':
    fg = FGHttpd(8000)
    # my_value = fg.get_float_property('/orientation/heading-deg')
    # print(my_value)
    my_value = 200.2
    fg.set_single_property('/orientation/heading-deg', str(my_value))
