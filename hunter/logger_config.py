# SPDX-FileCopyrightText: (C) 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from dataclasses import dataclass
import datetime
import logging
import multiprocessing as mp
import os
import time

from azure.monitor.opentelemetry import configure_azure_monitor



_my_logger = None

def get_logger() -> logging.Logger:
    global _my_logger
    if _my_logger is None:
        _configure_for_missing_configuration()
    return _my_logger


@dataclass(frozen=True)
class LogConfig:
    log_level: str
    log_to_file: bool

class RuntimeFormatter(logging.Formatter):
    """A logging formatter which includes the delta time since start.

    Cf. https://stackoverflow.com/questions/25194864/python-logging-time-since-start-of-program
    """
    def __init__(self, *the_args, **kwargs) -> None:
        super().__init__(*the_args, **kwargs)
        self.start_time = time.time()

    def formatTime(self, record, datefmt=None):
        duration = datetime.datetime.fromtimestamp(record.created - self.start_time, datetime.UTC)
        elapsed = duration.strftime('%H:%M:%S')
        return "{}".format(elapsed)


def _configure_for_missing_configuration() -> None:
    configure_logging(LogConfig('INFO', False), 'FooProcess')
    global _my_logger
    _my_logger.warning('The logger should be properly configured before using it')

def configure_logging(log_config: LogConfig, main_process_name: str | None = None) -> None:
    """Set the logging level and maybe write to file.

    See also accepted answer to https://stackoverflow.com/questions/29015958/how-can-i-prevent-the-inheritance-
    of-python-loggers-and-handlers-during-multipro?noredirect=1&lq=1.
    And: https://docs.python.org/3.5/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
    """
    if main_process_name is None:
        the_process_name = mp.current_process().name
    else:
        the_process_name = main_process_name
    print('Logging configuration:', the_process_name, log_config.log_level, log_config.log_to_file)
    global _my_logger
    azure_insights_connection_string = os.getenv('APPLICATIONINSIGHTS_CONNECTION_STRING')
    if azure_insights_connection_string:
        configure_azure_monitor(
            logger_name=the_process_name
        )

    log_format = '%(processName)-13s - %(asctime)s - %(levelname)-9s: %(message)s'
    console_handler = logging.StreamHandler()
    fmt = RuntimeFormatter(log_format)
    console_handler.setFormatter(fmt)
    _my_logger = logging.getLogger(the_process_name)

    # first remove handlers which might be inherited from mp forking etc.
    for handler in _my_logger.handlers[:]:  # Iterate through a copy of the list
        _my_logger.removeHandler(handler)

    _my_logger.addHandler(console_handler)
    _my_logger.setLevel(log_config.log_level)
    _my_logger.propagate = False

    if log_config.log_to_file:
        now = datetime.datetime.now()
        now_str = now.strftime("%Y-%m-%d_%H%M%S")
        file_name = '{}_{}.log'.format(the_process_name, now_str)
        file_handler = logging.FileHandler(filename=file_name)
        file_handler.setFormatter(fmt)
        _my_logger.addHandler(file_handler)
