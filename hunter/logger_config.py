# SPDX-FileCopyrightText: (C) 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later

import datetime
import logging
import multiprocessing as mp
import os
import time

from azure.monitor.opentelemetry import configure_azure_monitor


_my_logger = None

def get_logger():
    global _my_logger
    if _my_logger is None:
        raise Exception('Logger needs to be configured in program entry first - delay import of modules!')
    return _my_logger


class RuntimeFormatter(logging.Formatter):
    """A logging formatter which includes the delta time since start.

    Cf. https://stackoverflow.com/questions/25194864/python-logging-time-since-start-of-program
    """
    def __init__(self, *the_args, **kwargs) -> None:
        super().__init__(*the_args, **kwargs)
        self.start_time = time.time()

    def formatTime(self, record, datefmt=None):
        duration = datetime.datetime.fromtimestamp(record.created - self.start_time, datetime.UTC)
        elapsed = duration.strftime('%H:%M:%S')
        return "{}".format(elapsed)


def configure_logging(log_level: str, log_to_file: bool, main_process_name: str) -> None:
    """Set the logging level and maybe write to file.

    See also accepted answer to https://stackoverflow.com/questions/29015958/how-can-i-prevent-the-inheritance-
    of-python-loggers-and-handlers-during-multipro?noredirect=1&lq=1.
    And: https://docs.python.org/3.5/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
    """
    global _my_logger
    azure_insights_connection_string = os.getenv('APPLICATIONINSIGHTS_CONNECTION_STRING')
    if azure_insights_connection_string:
        configure_azure_monitor(
            logger_name=main_process_name
        )

    log_format = '%(processName)-10s %(threadName)s -- %(asctime)s - %(levelname)-9s: %(message)s'
    console_handler = logging.StreamHandler()
    fmt = RuntimeFormatter(log_format)
    console_handler.setFormatter(fmt)
    _my_logger = logging.getLogger(main_process_name)
    _my_logger.addHandler(console_handler)
    _my_logger.setLevel(log_level)

    if log_to_file:
        now = datetime.datetime.now()
        now_str = now.strftime("%Y-%m-%d_%H%M%S")

        process_name = mp.current_process().name
        if process_name == 'MainProcess':
            file_name = '{}_main_{}.log'.format(main_process_name, now_str)
        else:
            file_name = '{}_process_{}_{}.log'.format(main_process_name, process_name, now_str)
        file_handler = logging.FileHandler(filename=file_name)
        file_handler.setFormatter(fmt)
        _my_logger.addHandler(file_handler)
