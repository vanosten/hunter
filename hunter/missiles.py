# SPDX-FileCopyrightText: (C) 2023 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Code to simulate the flight path of guided missiles in Hunter.
This is a primitive good enough simulation for SAM and Air-to-air
launched missiles, so it gives some realism - but not more.
E.g. wind is not simulated, the coordinate system is primitive.
Aerodynamic coefficients are approximated by experiments and used
in simplified formulas etc.

Inspiration:
* Original code contributed by Nikolai V. Chr. (https://gitlab.com/emptyyetfull) from
  a private repo. See at the git-history to see the first version.
  Simulates the way missiles and other weapons are guided and handled in OPRF
  Nasal code (see below) divided into horizontal and vertical 2-D space.
* Missile code in Nasal code cf. https://github.com/NikolaiVChr/OpRedFlag.
  When referring to missile-code.nas then it is the file in the OPRF F-16.
  Many variable / function names are kept as is from Nasal to ease comparison
  despite the fact that they might not comply to Python naming.
* OpenRocket: https://openrocket.info/

According to https://en.wikipedia.org/wiki/Missile a missile has five system
components: targeting, guidance system, flight system, engine, and warhead.

This module calculates the flight path and some steering limitations given different
forces (e.g. thrust and drag) as well as outside input of guidance.
Targeting and guidance is done in module mp_shooters.py.

There are a set of phases in flight:
* Launch
* Powered flight in stages
* Flight without power
The flight is finished either at crash into target or when it does
not matter any more (too slow, fall en earth).

FPS: feet per second (not frames per second)
SLUG: British gravitational system (equivalent of 24.59 kg / 32.17 lbs)
"""

from dataclasses import dataclass
from enum import Enum, IntEnum, unique
import math
import random
import time
import unittest

import hunter.emesary_producer as ep
import hunter.geometry as g
import hunter.logger_config as lc
import hunter.messages as m
import hunter.mp_attacker_tracker as mat

logger = lc.get_logger()

G_FPS = g.metres_to_feet(9.80665)
SLUG2LB = 32.1740485564  # https://en.wikipedia.org/wiki/Slug_(unit)


def clamp(value, min_value, max_value):
    return min_value if value < min_value else max_value if value > max_value else value


def _calc_gravity_compensation(delta_time: float, speed_down_fps: float, speed_horizontal_fps) -> float:
    """Calculates the compensation degrees for the predicted gravity drop of attitude of the missile."""
    attitude_pn = math.degrees(math.atan2(-1 * (speed_down_fps + G_FPS * delta_time), speed_horizontal_fps) -
                               math.atan2(-1 * speed_down_fps, speed_horizontal_fps))
    return -1 * attitude_pn


def _calc_rho_speed_of_sound(altitude: float) -> tuple[float, float]:
    if altitude < 36152:
        # curve fits for the troposphere
        temperature = 59 - 0.00356 * altitude
        p = 2116 * math.pow(((temperature + 459.7) / 518.6), 5.256)
    elif 36152 < altitude < 82345:
        # lower stratosphere
        temperature = -70
        p = 473.1 * math.pow(math.e, 1.73 - (0.000048 * altitude))
    else:
        # upper stratosphere
        temperature = -205.05 + (0.00164 * altitude)
        p = 51.97 * math.pow(((temperature + 459.7) / 389.98), -11.388)

    rho = p / (1718 * (temperature + 459.7))
    snd_speed = math.sqrt(1.4 * 1716 * (temperature + 459.7))
    return rho, snd_speed


def _calc_max_g_at_rho(rho: float, max_g_sea_level: float,
                       has_vector_thrust: bool, current_thrust_lbf: float) -> float:
    if has_vector_thrust and current_thrust_lbf == 0:
        max_g_sea_level = max_g_sea_level * 0.666
    return clamp(max_g_sea_level + ((rho - 0.0023769) / (0.00036159 - 0.0023769)) * (
                max_g_sea_level * 0.5909 - max_g_sea_level), 0.25, 100)


def _calc_drag_coefficient(mach: float, current_thrust_lbf: float,
                           cd_base: float, cd_plume: float) -> float:
    if mach < 0.7:
        cd_0 = (0.0125 * mach + 0.20) * 5 * cd_base
    elif mach < 1.2:
        cd_0 = (0.3742 * math.pow(mach, 2) - 0.252 * mach + 0.0021 + 0.2) * 5 * cd_base
    else:
        cd_0 = (0.2965 * math.pow(mach, -1.1506) + 0.2) * 5 * cd_base
    # TODO: implement advanced from original: https://gitlab.com/vanosten/hunter/-/issues/141
    cd_i = 0.
    cd_0 = cd_0 * cd_plume if current_thrust_lbf > 0 else cd_0
    return cd_0 + cd_i


def _calc_speed_loss_from_energy_bleed(delta_time: float, vector_thrust: bool,
                                       current_g: float, current_thrust_lbf,
                                       altitude_ft: float) -> float:
    """Bleed energy and thereby loose speed from pulling Gs.
    Is a negative number.
    """
    # if self.advanced:
    #    return 0  https://gitlab.com/vanosten/hunter/-/issues/141

    # First we get the speed loss due to normal drag:
    b300 = _bleed32800at0g(delta_time)
    b000 = _bleed0at0g(delta_time)

    # We then subtract the normal drag from the loss due to G and normal drag.
    b325 = _bleed32800at25g(delta_time) - b300
    b025 = _bleed0at25g(delta_time) - b000
    b300 = 0
    b000 = 0

    # We now find what the speed loss will be at sea level respectively at 32800 ft.
    speed_loss_32800 = b300 + ((current_g - 0) / (25 - 0)) * (b325 - b300)
    speed_loss_0 = b000 + ((current_g - 0) / (25 - 0)) * (b025 - b000)

    # Now extrapolate that to the current density-altitude.
    speed_loss = speed_loss_0 + ((altitude_ft - 0) / (32800 - 0)) * (speed_loss_32800 - speed_loss_0)
    #
    # For good measure the result is clamped to below zero.
    speed_loss = clamp(speed_loss, -100000, 0)
    if vector_thrust:
        speed_loss = speed_loss - speed_loss * 0.66 * (
           0 if current_thrust_lbf == 0 else 1)  # vector thrust will only bleed 1/3 of the calculated loss.
    return speed_loss


def _calc_generic_bleed_for_altitude_g(factor_1: float, factor_2: int, delta_time: float) -> float:
    loss_fps = 0 + ((delta_time - 0) / (factor_1 - 0)) * (factor_2 - 0)
    return g.metres_to_feet(loss_fps)


def _bleed32800at0g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(15., -330, delta_time)


def _bleed32800at25g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(3.5, -240, delta_time)


def _bleed0at0g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(22., -950, delta_time)


def _bleed0at25g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(7., -750, delta_time)


def _calc_angular_speed_g(delta_orientation_deg: float,
                          current_speed_fps: float,
                          delta_time: float) -> float:
    """The g-force due to angular acceleration.
    https://en.wikipedia.org/wiki/Angular_acceleration
    https://en.wikipedia.org/wiki/Centrifugal_force
    """
    w = math.radians(delta_orientation_deg) / 2 / math.pi  # angular velocity
    w = w / delta_time  # need to correct with time passed (e.g. 0.5 secs)
    return current_speed_fps * w / G_FPS


def _approximate_max_delta_orientation(delta_orientation_deg: float,
                                       current_speed_fps: float,
                                       delta_time: float,
                                       max_allowed_g: float) -> float:
    """Calculates a factor to apply to the current delta_orientation to keep below max allowed g."""
    guess = 1.0
    coef = 1.0
    best_guess = 1

    for i in range(25):
        g_force = _calc_angular_speed_g(delta_orientation_deg * guess, current_speed_fps, delta_time)
        coef = coef / 2
        if g_force < max_allowed_g:
            best_guess = guess
            guess = guess + coef
        else:
            guess = guess - coef
    return best_guess


class MissileState(Enum):
    active = 1
    overshot = 2  # just a special case of lost_tracking
    tracking_lost = 3  # for now - might be able to reacquire
    tracking_given_up = 4  # too much time passed - tracking cannot be reacquired
    bit_on_counter_measures = 5
    hit_target = 6
    deleted = 7


@dataclass(frozen=True)
class MissileTelemetry:
    """Stores telemetry data for a missile - for later visualization."""
    life_time: float
    speed_fps: float
    mach: float
    max_mach: float
    thrust_lbf: float
    weight_lbf: float
    g_force: float
    max_g_force: float
    orientation: g.Orientation
    position: g.Position
    state: MissileState
    total_distance: float
    deletion_reason: str

    def __str__(self):
        my_telemetry_string = 'life_time: {:.2f}; '.format(self.life_time)
        my_telemetry_string += 'speed_fps: {:.2f}; '.format(self.speed_fps)
        my_telemetry_string += 'max_mach: {:.2f}; '.format(self.max_mach)
        my_telemetry_string += 'max_g_force: {:.2f}; '.format(self.max_g_force)
        my_telemetry_string += str(self.orientation) + '; '
        my_telemetry_string += str(self.position) + '; '
        my_telemetry_string += 'missile state: {}; '.format(self.state.name)
        my_telemetry_string += 'total_distance (metres): {:.1f}; '.format(self.total_distance)
        if self.deletion_reason:
            my_telemetry_string += 'deletion_reason: ' + self.deletion_reason
        return my_telemetry_string


MISSILE_HIT_CONE = 20

CD_PLUME_DEFAULT = 1.  # exhaust-plume-parasitic-drag-factor


@unique
class GuidanceLawType(IntEnum):
    apn = 3
    opn = 4


class Guidance:
    """See explanation in missile-code.nas.
    APNxxyy: xx is degrees to aim above target, yy is seconds it will do that
    e.g. APN0506, vertical aim 5 degrees above for 6 seconds
    https://en.wikipedia.org/wiki/Missile_guidance
    https://en.wikipedia.org/wiki/Proportional_navigation

    Compared to Nasal code in guided_missiles.nas there are some simplifications.
    * NB: OPN is me.APN = -1 and APN is me.APN = 1
    * me.cruise_or_loft is only relevant for cruise missiles etc. -> False / not taken into account
    """
    __slot__ = ('law', '_i_fixed_aim_angle', '_i_fixed_aim_time', '_i_proportionality_constant', '_i_seeker_filter',
                '_noise', '_i_seed', '_last', '_next',
                '_last_position', '_last_orientation', '_last_attacker_position')

    def __init__(self, guidance_law_str: str, proportionality_constant: float | None,
                 seeker_filter: float = 0.) -> None:
        self._initialize_guidance(guidance_law_str)
        if proportionality_constant is None:
            self._i_proportionality_constant = 3.
        else:
            self._i_proportionality_constant = proportionality_constant
        self._i_seeker_filter = seeker_filter

        self._noise = 1.
        self._last = 0.
        self._next = 0.1  # # duration for this noise factor, till a new is computed.
        self._i_seed = random.random() * self._i_seeker_filter

        # get set in run_increment, so we have the most recent historic values
        self._last_position = None  # g.Position
        self._last_orientation = None  # g.Orientation
        self._last_attacker_position = None  # m.AttackerPosition

    def run_guidance(self, life_time: float, current_position: g.Position, current_orientation: g.Orientation,
                     current_attacker_position: m.AttackerPosition,
                     speed_down_fps: float, speed_horizontal_fps: float) -> tuple[float, float, bool]:
        self._adjust_pn_noise(life_time)
        if self._last_position is None:
            self._last_position = current_position
            self._last_orientation = current_orientation
            self._last_attacker_position = current_attacker_position
            return 0., 0., False

        delta_time = current_attacker_position.timestamp - self._last_attacker_position.timestamp
        if delta_time == 0:  # most probably lost LOS tracking or hiccup
            return 0., 0., False

        # Setting gravity compensation to 0 because other code in HunterMissile does not pull the missile down
        # Nasal does it somewhere
        gravity_compensation = 0.  # _calc_gravity_compensation(delta_time, speed_down_fps, speed_horizontal_fps)

        last_elev_course = g.calc_altitude_angle(self._last_position, self._last_attacker_position.position)
        current_elev_course = g.calc_altitude_angle(current_position, current_attacker_position.position)
        curr_deviation_e = current_elev_course - current_orientation.pitch
        # print('position alt:', current_position.alt_m)
        # print('elevation course:', current_elev_course)
        # print('pitch', current_orientation.pitch)

        last_horizontal_course = g.calc_bearing_pos(self._last_position, self._last_attacker_position.position)
        current_horizontal_course = g.calc_bearing_pos(current_position, current_attacker_position.position)
        course_deviation = g.norm_deg_180(current_horizontal_course - last_horizontal_course)

        line_of_sight_rate_rps = math.radians(course_deviation) / delta_time
        line_of_sight_rate_up_rps = math.radians(current_elev_course - last_elev_course) / delta_time
        # print('line of sight rate up:', line_of_sight_rate_up_rps)
        # print('noise:', self._noise)

        # horizontal homing
        curr_deviation_h = current_horizontal_course - current_orientation.hdg
        if math.fabs(curr_deviation_h) < 15:  # me.guidanceLawHorizInit
            raw_steer_signal_hdg = curr_deviation_h  # use pure pursuit
        else:
            radians_lateral_per_sec = self._i_proportionality_constant * line_of_sight_rate_rps * self._noise
            raw_steer_signal_hdg = delta_time * math.degrees(radians_lateral_per_sec)

        # vertical homing
        if self._i_fixed_aim_angle is not None and life_time < self._i_fixed_aim_time:
            raw_steer_signal_elev = curr_deviation_e + self._i_fixed_aim_angle
            raw_steer_signal_elev += gravity_compensation

        else:
            # if math.fabs(curr_deviation_e) < 10:  # FIXME https://gitlab.com/vanosten/hunter/-/issues/149
            #    raw_steer_signal_elev = curr_deviation_e
            # else:
            radians_up_per_sec = self._i_proportionality_constant * line_of_sight_rate_up_rps * self._noise
            raw_steer_signal_elev = delta_time * math.degrees(radians_up_per_sec)
            raw_steer_signal_elev += gravity_compensation

        # print('raw_steer_signal_elev:', raw_steer_signal_elev)
        self._last_position = current_position
        self._last_orientation = current_orientation
        self._last_attacker_position = current_attacker_position

        return raw_steer_signal_hdg, raw_steer_signal_elev, True

    def _adjust_pn_noise(self, life_time: float):
        if life_time > 6.:
            self._noise = 1.
        elif self._i_seeker_filter > 0 and life_time - self._last > self._next:
            sign = (-1 if random.random() > 0.75 else 1) if self._i_seed > 0.85 else 1.
            opn_noise_reduction = 0.5 if self.law is GuidanceLawType.opn else 1.
            self._noise = (1 + self._i_seeker_filter * opn_noise_reduction) * random.random()
            self._noise = sign * (self._noise + 1)
            self._last = life_time
            self._next = self._i_seeker_filter * 0.15 * random.random()

    def _initialize_guidance(self, guidance_law_str: str) -> None:
        self._i_fixed_aim_angle = None
        self._i_fixed_aim_time = None
        if not guidance_law_str:
            raise ValueError('Guidance law may not be empty')
        if guidance_law_str.find('OPN') == 0:
            self.law = GuidanceLawType.opn
        elif guidance_law_str.find('APN') == 0:
            self.law = GuidanceLawType.apn
            if len(guidance_law_str) > 3:
                if len(guidance_law_str) != 7:
                    raise ValueError('The APN string must have length 3 or 7: ' + guidance_law_str)
                self._i_fixed_aim_angle = int(guidance_law_str[3:5])
                self._i_fixed_aim_time = int(guidance_law_str[5:7])
        else:
            raise ValueError('Guidance law is not supported: ' + guidance_law_str)


class HunterMissile:
    """A missile suitable for simulation in Hunter.

    The missile can have up to three stages.

    By convention variables starting with i_ or _i_ are initialized once and never updated.
    """
    # FIXME: lbm vs. lbf vs. lbs etc. is not correctly applied in naming and comments
    __slots__ = ('_i_shooter_callsign', '_i_locked_callsign', 'unique_identity', '_status',
                 '_life_time', '_chaff_time', '_chaff_number', '_flare_time', '_flare_number',
                 '_i_stage_duration',
                 '_orientation', '_position', '_current_speed_fps',
                 '_current_thrust_lbf', '_i_stage_thrust_lbf', '_i_stage_fuel_lbfs',
                 '_current_weight_lbs',
                 '_i_max_g_sea_level', '_current_g',
                 '_i_vector_thrust',
                 '_i_cd_base', '_i_cd_plume', '_i_cross_section_sqft',
                 '_i_min_speed_for_guiding_mach', '_max_mach_reached',
                 'telemetry', '_last_timestamp', '_i_timestamp',
                 'attacker_last_seen',
                 '_i_ammo_index', '_i_missile_is_ir', '_i_missile_radar_on',
                 '_i_counter_measure_resistance',
                 '_i_self_destruct_time_sec',
                 '_guidance',
                 '_i_ground_launched')

    def __init__(self, parent_callsign: str, locked_callsign: str, unique_identity: int,
                 launched_orientation: g.Orientation, launched_position: g.Position,
                 launched_speed_ms: float,
                 stage_duration: list[float], stage_thrust: list[float],
                 weight_total_launch_lbs: float, weight_fuel_launch_lbs: float,
                 max_g_sea_level: float, vector_thrust: bool,
                 cd_base: float,  # "drag-coeff"
                 cd_plume: float,  # "exhaust-plume-parasitic-drag-factor"
                 cross_section_sqft: float,
                 min_speed_for_guiding_mach: float,
                 ammo_index: int,
                 missile_is_ir: bool,
                 missile_radar_on: bool,  # only True if active radar homing
                 counter_measure_resistance: float,    # chaff-resistance or flare-resistance
                 self_destruct_time_sec: int,
                 navigation: str,
                 proportionality_constant: float | None,
                 ground_launched: bool) -> None:
        self._i_shooter_callsign = parent_callsign
        self._i_locked_callsign = locked_callsign
        self.unique_identity = unique_identity
        self._status = MissileState.active
        self._life_time = 0  # time in seconds since launch
        self._chaff_time:float = 0.
        self._chaff_number: float = 0.  # the chaff random number last seen
        self._flare_time: float = 0.
        self._flare_number: float = 0.  # the flare random number last seen

        self._orientation = launched_orientation
        self._position = launched_position
        self._current_speed_fps = g.metres_to_feet(launched_speed_ms)

        self._current_thrust_lbf = 0.
        self._i_stage_duration = stage_duration
        self._i_stage_thrust_lbf = stage_thrust

        self._current_weight_lbs = weight_total_launch_lbs
        self._initialize_fuel_consumption_per_stage(weight_fuel_launch_lbs)

        self._i_max_g_sea_level = max_g_sea_level
        self._current_g = 1.

        self._i_vector_thrust = vector_thrust

        self._i_cd_base = cd_base
        self._i_cd_plume = cd_plume
        self._i_cross_section_sqft = cross_section_sqft

        self._i_min_speed_for_guiding_mach = min_speed_for_guiding_mach
        self._max_mach_reached = 0.

        self.telemetry = list()  # List of MissileTelemetry objects
        self._last_timestamp = None  # set whenever run_increment(..)
        self._i_timestamp = time.time()
        self.attacker_last_seen = self._i_timestamp

        self._i_ammo_index = ammo_index
        self._i_missile_is_ir = missile_is_ir
        self._i_missile_radar_on = missile_radar_on
        self._i_counter_measure_resistance = counter_measure_resistance
        self._i_self_destruct_time_sec = self_destruct_time_sec

        self._guidance = Guidance(navigation, proportionality_constant)
        self_i_ground_launched = ground_launched

    @property
    def _mass(self) -> float:
        return self._current_weight_lbs / SLUG2LB

    @property
    def current_position(self) -> g.Position:
        return self._position

    @property
    def callsign(self) -> str:
        """The callsign of the locked on target for the missile (not the shooter)."""
        return self._i_locked_callsign

    @property
    def timestamp(self) -> float:
        """The timestamp when the missile was run first time."""
        return self._i_timestamp

    @classmethod
    def create_igla_9k38(cls, parent_callsign: str, locked_callsign: str,
                         unique_identity: int,
                         launched_orientation: g.Orientation,
                         launched_position: g.Position, launched_speed_ms: float,
                         ground_launched: bool) -> 'HunterMissile':
        """Simulates a man-pad IR-missile, which can also be used on a helicopter (e.g. the Igla-V),
        on naval ships for self-defence (e.g. the Igla-M and Stinger) as well on vehicles like the
        https://en.wikipedia.org/wiki/Bradley_Fighting_Vehicle#M6_Linebacker.

        https://en.wikipedia.org/wiki/9K32_Strela-2: 2M variant max fire range 4200 m, 500 m/s max speed
        https://en.wikipedia.org/wiki/9K38_Igla: S variant max fire range 6000 m, 570 m/s max speed
        https://en.wikipedia.org/wiki/FIM-43_Redeye: max fire range 4500 m, 580 max speed
        https://en.wikipedia.org/wiki/FIM-92_Stinger: max fire range 4800 m, 750 m/s max speed, start range 0.16 km

        Parameters according to wikipedia Igla article (SA-16 variant to be close to Stinger) as well as
        some inspiration from OPFOR Mig-21 (R-3S):
        https://github.com/l0k1/MiG-21bis/blob/master/MiG-21-set-common.xml

        See also https://en.wikipedia.org/wiki/K-13_(missile)
        """
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['R-60']  # because it has the smallest warhead
        instance = HunterMissile(parent_callsign, locked_callsign,
                                 unique_identity,
                                 launched_orientation, launched_position, launched_speed_ms,
                                 [.5, 4., 0.],  # pure guess - a bit more than the R-3S
                                 [200., 800., 0],  # experimental testing - less than R-3S
                                 39., 12.,  # 39 - 24- 2.6
                                 28.,  # from Mig-21 R-3S
                                 False,
                                 0.35,  # a bit less than R-3S (0.55) because less finns etc.
                                 CD_PLUME_DEFAULT, math.pow(g.metres_to_feet(0.077), 2) * math.pi,
                                 0.4,  # guess lower than 0.8 from R-3S because rolling
                                 warhead_data[0],
                                 True, False,
                                 0.92,
                                 30,  # experimental testing based on distance (5000m plus some slack)
                                 'OPN',
                                 None, # OPN since it is heat
                                 ground_launched)
        return instance

    @classmethod
    def create_buk_m2_9m317(cls, parent_callsign: str, locked_callsign: str,
                            unique_identity: int,
                            launched_orientation: g.Orientation,
                            launched_position: g.Position, launched_speed_ms: float,
                            ground_launched: bool) -> 'HunterMissile':
        """Parameters according to OPFOR BUK-M2

        See also https://en.wikipedia.org/wiki/Buk_missile_system
        """
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['9M317']
        instance = HunterMissile(parent_callsign, locked_callsign,
                                 unique_identity,
                                 launched_orientation, launched_position, launched_speed_ms,
                                 [15., 0., 0.],  # stage-1-duration-sec
                                 [11000., 0., 0],  # thrust-lbf-stage-#
                                 1576., 900.,
                                 24., False,
                                 0.10,  # OPRF has 0.2 - but it does not fly long enough
                                 CD_PLUME_DEFAULT, 1.312336,
                                 0.5,
                                 warhead_data[0],
                                 False, True,
                                 0.92,
                                 60,
                                 'APN0506',
                                 3.0,
                                 ground_launched)
        return instance

    @classmethod
    def create_sa_3_5v27(cls, parent_callsign: str, locked_callsign: str,
                         unique_identity: int,
                         launched_orientation: g.Orientation,
                         launched_position: g.Position, launched_speed_ms: float,
                         ground_launched: bool) -> 'HunterMissile':
        """Parameters according to OPFOR SA-3

        See also https://en.wikipedia.org/wiki/S-125_Neva/Pechora
        """
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['5V27']
        instance = HunterMissile(parent_callsign, locked_callsign,
                                 unique_identity,
                                 launched_orientation, launched_position, launched_speed_ms,
                                 [6., 7., 0.],  # OPRF has 6/13, but it flies too fast at self-destruct
                                 [29500., 4400., 0],  # thrust-lbf-stage-#
                                 2098., 1000.,
                                 22., False,
                                 0.15,  # OPRF has 0.1, but it seems to fly too fast at self-destruct
                                 CD_PLUME_DEFAULT, 1.24,
                                 0.4,
                                 warhead_data[0],
                                 False, True,
                                 0.88,
                                 35,
                                 'OPN',
                                 3.0,
                                 ground_launched)
        return instance

    @classmethod
    def create_kn_06(cls, parent_callsign: str, locked_callsign: str,
                     unique_identity: int,
                     launched_orientation: g.Orientation,
                     launched_position: g.Position, launched_speed_ms: float,
                     ground_launched: bool) -> 'HunterMissile':
        """Parameters according to OPFOR S-300 (Frigate seems to be off)

        See also https://en.wikipedia.org/wiki/KN-06
        """
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['KN-06']
        instance = HunterMissile(parent_callsign, locked_callsign,
                                 unique_identity,
                                 launched_orientation, launched_position, launched_speed_ms,
                                 [.5, 10., 0.],  # stage-1-duration-sec
                                 [13500., 55000., 0],  # thrust-lbf-stage-
                                 3920., 2100.,
                                 22., False,
                                 0.15,  # using 0.15 from Frigate instead of 0.1 from S-300
                                 CD_PLUME_DEFAULT, 2.11349,
                                 0.25,
                                 warhead_data[0],
                                 False, True,
                                 0.96,
                                 190,
                                 'APN3003',
                                 3.0,
                                 ground_launched)
        return instance

    def _initialize_fuel_consumption_per_stage(self, weight_fuel_launch_lbs: float) -> None:
        self._i_stage_fuel_lbfs = [0., 0., 0.]  # lbm/s
        impulse = [0., 0., 0.]
        impulse_total = 0.
        for i in range(3):
            if self._i_stage_thrust_lbf[i] > 0:
                assert self._i_stage_duration[i] > 0, f'In stage {i} there is thrust but no duration'
            impulse[i] = self._i_stage_thrust_lbf[i] * self._i_stage_duration[i]  # lbf*s
            impulse_total += impulse[i]
        fuel_per_impulse = weight_fuel_launch_lbs / impulse_total  # lbm/(lbf*s)
        for i in range(3):
            if impulse[i] > 0.:
                self._i_stage_fuel_lbfs[i] = fuel_per_impulse * impulse[i] / self._i_stage_duration[i]

    def run_increment(self, now: float, attacker_pos_hist: mat.AttackerPositionHistory | None,
                      visibility_state: MissileState,
                      msg_idx_callback) -> tuple[bool, bool, list[ep.EmesaryMessageAndBridge], list[m.MissileEvent]]:
        """Advance one time increment at a time - e.g. 0.5 seconds.

        The callback function should be mp_targets.py.MPTarget.next_emesary_msg_idx(self),
        which returns an increasing int.

        The first bool in return is True, if this missile should be deleted
        The second bool in return is True, if the missile has hit
        FIXME: need to have field_of_view to check whether attacker can be seen - not only the DEM check
        """
        rho, speed_sound_fps = _calc_rho_speed_of_sound(self._position.alt_ft)
        if attacker_pos_hist:
            if self._status not in [MissileState.hit_target, MissileState.overshot,
                                    MissileState.bit_on_counter_measures, MissileState.tracking_given_up]:
                self._status = visibility_state
        else:
            self._status = MissileState.tracking_given_up

        emesary_messages = list()
        messages: list[m.MissileEvent] = list()
        # The first increment is used as reaction time
        if self._last_timestamp is None:
            self._life_time = 0.
            self._last_timestamp = now
            self._safe_telemetry_data(speed_sound_fps, 0)
            return False, False, emesary_messages, messages

        delta_time = now - self._last_timestamp
        self._life_time += delta_time
        self._last_timestamp = now

        current_mach = self._current_speed_fps / speed_sound_fps
        if current_mach > self._max_mach_reached:
            self._max_mach_reached = current_mach

        deletion_reason = ''
        # is missile from previous increment in a state, where we should delete it?
        # FIXME: https://gitlab.com/vanosten/hunter/-/issues/145
        if self._status is MissileState.hit_target:
            logger.info('%s delete missile[%i] targeting %s because hit target in last increment',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'hit target'
        elif self._status is MissileState.overshot:
            logger.info('%s has self-destroyed missile[%i] targeting %s because overshot',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'overshot'
        elif self._status is MissileState.bit_on_counter_measures:
            logger.info('%s has self-destroyed missile[%i] targeting %s due to counter-measures',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'counter measures'
        elif self._status is MissileState.tracking_given_up:
            logger.info('%s has self-destroyed missile[%i] targeting %s because given up tracking',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'given up tracking'

        # should missile be self-destroyed due to lack of speed?
        elif current_mach < self._max_mach_reached and current_mach < self._i_min_speed_for_guiding_mach:
            logger.info('%s has self-destroyed missile[%i] targeting %s due to too low speed',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'too low speed'
        # should missile be self-destroyed based on time passed?
        elif self._life_time >= self._i_self_destruct_time_sec:
            logger.info('%s has self-destroyed missile[%i] targeting %s due to life time',
                        self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)
            self._status = MissileState.deleted
            deletion_reason = 'life_time'

        # Get rid of missiles we already know we will delete
        if self._status is MissileState.deleted:
            encoded_msg = ep.process_outgoing_armament_in_flight_deleted(now, msg_idx_callback(),
                                                                         self._i_ammo_index,
                                                                         self.unique_identity)
            logger.info('%s sending missile[%i] in flight deleted for %s', self._i_shooter_callsign,
                        self.unique_identity, self._i_locked_callsign)
            logger.info(str(self.telemetry[-1]))
            emesary_messages.append(encoded_msg)
            self._safe_telemetry_data(speed_sound_fps, 0, deletion_reason)
            messages.append(m.MissileEvent(self.unique_identity, self._i_shooter_callsign, self._i_locked_callsign,
                                           str(self.telemetry[-1])))
            return True, False, emesary_messages, messages

        # we are now left with MissileState.active | MissileState.tracking_lost
        # before we calculate new speed, orientation etc. we check, whether
        # the last calculated (future) position, speed, orientation etc. matches with
        # the now known position of the attacker
        hit = False
        has_manoeuvre_speed = current_mach >= self._i_min_speed_for_guiding_mach
        distance_to_attacker = g.calc_distance_pos_3d(self._position,
                                                      attacker_pos_hist.latest_position)
        # does the missile bite on chaff / flare? If yes then status is set to MissileState.bit_on_counter_measures
        if self._status is MissileState.active:
            lap = attacker_pos_hist.latest_attacker_position
            if self._i_missile_is_ir:
                self._check_for_flares(lap)
            else:
                self._check_for_chaff(lap)

        # then check for possible hit - as long as it is active - and we have a real position
        if attacker_pos_hist and self._status is MissileState.active:
            flown_dist_m = g.feet_to_metres(self._current_speed_fps * delta_time)
            if distance_to_attacker <= flown_dist_m:
                # now check whether the flight path of the missile from ._position with .hdg and .pitch
                # calculated at end of last iteration is close to the path between ._position and the
                # actual (not guessed) attacker position.
                # Using the angle between these two path we can approximate the distance at attacker position
                # as if it was a parallel line.
                # If that distance is less than a tolerance (allowed_dist), then we are close enough.
                allowed_dist = flown_dist_m * math.sin(math.radians(10.))  # ca. 17.3 metres per 100 metres
                bearing_to_attacker = g.calc_bearing_pos(self._position,
                                                         attacker_pos_hist.latest_position)
                abs_delta_bearing = math.fabs(g.calc_delta_bearing(self._orientation.hdg, bearing_to_attacker))
                horizontal_dist = g.calc_distance_pos(self._position, attacker_pos_hist.latest_position)
                dist_h_path = horizontal_dist * math.sin(math.radians(abs_delta_bearing))
                pitch_to_attacker = g.calc_altitude_angle(self._position,
                                                          attacker_pos_hist.latest_position)
                abs_delta_pitch = math.fabs(self._orientation.pitch - pitch_to_attacker)
                vertical_dist = math.fabs(self._position.alt_m - attacker_pos_hist.latest_position.alt_m)
                dist_v_path = vertical_dist * math.sin(math.radians(abs_delta_pitch))
                average_deviation = math.sqrt(math.pow(dist_h_path, 2) +
                                              math.pow(dist_v_path, 2))
                hit_accuracy = 'flown_dist_m: {:.1f}; allowed_dist: {:.1f}; '.format(flown_dist_m,
                                                                                     allowed_dist)
                hit_accuracy += 'horizontal_dist: {:.1f}; dist_h_path: {:.1f}; '.format(horizontal_dist,
                                                                                        dist_h_path)
                hit_accuracy += 'vertical_dist: {:.1f}; dist_v_path: {:.1f}'.format(vertical_dist,
                                                                                    dist_v_path)
                print(hit_accuracy)
                if average_deviation <= allowed_dist:
                    logger.info('Missile[%i] from %s has hit %s', self.unique_identity,
                                self._i_shooter_callsign, self._i_locked_callsign)
                    encoded_msg = ep.process_outgoing_armament_hit_notification(now, msg_idx_callback(),
                                                                                self._i_ammo_index,
                                                                                1,
                                                                                self._i_locked_callsign,
                                                                                False)
                    logger.info('%s sending missile[%i] hit armament notification on %s',
                                self._i_shooter_callsign, self.unique_identity, self._i_locked_callsign)

                    logger.info(str(self.telemetry[-1]))
                    emesary_messages.append(encoded_msg)
                    hit = True
                    self._status = MissileState.hit_target
                else:  # missile has overshot the target
                    logger.info('Missile[%i] from %s has overshot %s', self.unique_identity,
                                self._i_shooter_callsign, self._i_locked_callsign)
                    self._status = MissileState.overshot

        # now prepare the future orientation, position, speed etc.
        if self._status is MissileState.active:
            self._calc_current_guidance(now, rho, attacker_pos_hist, delta_time, has_manoeuvre_speed)
        else:  # no guidance
            self._calc_current_guidance(now, rho, attacker_pos_hist, delta_time, False)

        self._calc_current_thrust()
        self._calc_current_speed(current_mach, rho, delta_time)
        self._calc_current_weight(delta_time)

        flown_dist_m = g.feet_to_metres(self._current_speed_fps * delta_time)
        self._position = g.calc_destination_pos_3d(self._position, flown_dist_m,
                                                   self._orientation.hdg, self._orientation.pitch)

        # send in-flight notification no matter the status as long as we did not hit the target
        if self._status is not MissileState.hit_target:
            locked_on_callsign = "" if self._status is MissileState.tracking_lost else self._i_locked_callsign
            thrust_on = self._current_thrust_lbf > 0
            speed = g.feet_to_metres(self._current_speed_fps)
            encoded_msg = ep.process_outgoing_armament_in_flight_notification(now, msg_idx_callback(),
                                                                              self._i_ammo_index,
                                                                              self.unique_identity,
                                                                              self._position,
                                                                              self._orientation.hdg,
                                                                              self._orientation.pitch,
                                                                              speed,
                                                                              self._i_missile_radar_on,
                                                                              thrust_on,
                                                                              locked_on_callsign,
                                                                              False)
            logger.debug('%s sending missile[%i] in flight notification for %s',
                          self._i_shooter_callsign,
                          self.unique_identity, self._i_locked_callsign)
            emesary_messages.append(encoded_msg)

        self._safe_telemetry_data(speed_sound_fps, flown_dist_m)
        return False, hit, emesary_messages, messages

    def _check_for_flares(self, last_attacker_position: m.AttackerPosition) -> None:
        # A bit aligned with missile-code.nas check_for_flare
        if self._life_time - self._flare_time > 1.:
            if math.fabs(last_attacker_position.flares_on) > 0.001:
                if self._flare_number != last_attacker_position.flares_on:
                    self._flare_time = self._life_time
                    self._flare_number = last_attacker_position.flares_on
                    if random.random() > self._i_counter_measure_resistance:
                        self._status = MissileState.bit_on_counter_measures

    def _check_for_chaff(self, last_attacker_position: m.AttackerPosition) -> None:
        # A bit aligned with missile-code.nas check_for_chaff
        if self._life_time - self._chaff_time > 1.:
            if math.fabs(last_attacker_position.chaff_on) > 0.001:
                if self._chaff_number != last_attacker_position.chaff_on:
                    self._chaff_time = self._life_time
                    self._chaff_number = last_attacker_position.chaff_on
                    if random.random() > self._i_counter_measure_resistance:
                        self._status = MissileState.bit_on_counter_measures

    def _safe_telemetry_data(self, speed_sound_fps: float, flown_distance: float, deletion_reason: str = "") -> None:
        max_mach = self._current_speed_fps / speed_sound_fps
        max_g_force = self._current_g
        total_distance = flown_distance
        if self.telemetry:
            max_mach = max(max_mach, self.telemetry[-1].max_mach)
            max_g_force = max(max_g_force, self.telemetry[-1].max_g_force)
            total_distance += self.telemetry[-1].total_distance
        current_telemetry = MissileTelemetry(self._life_time,
                                             self._current_speed_fps,
                                             self._current_speed_fps / speed_sound_fps,
                                             max_mach,
                                             self._current_thrust_lbf,
                                             self._current_weight_lbs,
                                             self._current_g,
                                             max_g_force,
                                             g.Orientation(self._orientation.hdg, self._orientation.pitch),
                                             g.Position(self._position.lon, self._position.lat,
                                                        self._position.alt_m),
                                             self._status,
                                             total_distance,
                                             deletion_reason)
        self.telemetry.append(current_telemetry)

    def _calc_current_thrust(self) -> None:
        """Based on current life_time calculate the thrust depending on stages"""
        if self._life_time > (self._i_stage_duration[0] + self._i_stage_duration[1] + self._i_stage_duration[2]):
            self._current_thrust_lbf = 0.
        elif self._life_time > self._i_stage_duration[0] + self._i_stage_duration[1]:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[2]
        elif self._life_time > self._i_stage_duration[0]:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[1]
        else:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[0]

    def _calc_current_guidance(self, now: float, rho: float,
                               attacker_pos_hist: mat.AttackerPositionHistory | None,
                               delta_time: float,
                               has_manoeuvre_speed: bool) -> None:
        """Based on target position and limitations calculate heading, pitch etc."""
        usable = False  # is the guidance usable?
        asked_hdg = 0.
        asked_pitch = 0.
        use_direct_steering = False
        if attacker_pos_hist and has_manoeuvre_speed:
            if use_direct_steering:
                asked_hdg = g.calc_bearing_pos(self._position, attacker_pos_hist.guess_next_position(now, delta_time))
                asked_pitch = g.calc_altitude_angle(self._position,
                                                    attacker_pos_hist.guess_next_position(now, delta_time))
                usable = True
            else:
                speed_down_fps = -1 * math.sin(math.radians(self._orientation.pitch)) * self._current_speed_fps
                speed_horizontal_fps = math.cos(math.radians(self._orientation.pitch)) * self._current_speed_fps
                delta_hdg, delta_pitch, usable = self._guidance.run_guidance(self._life_time, self._position,
                                                                             self._orientation,
                                                                             attacker_pos_hist.latest_attacker_position,
                                                                             speed_down_fps, speed_horizontal_fps)
                asked_hdg = self._orientation.hdg + delta_hdg
                asked_pitch = self._orientation.pitch + delta_pitch
        if not usable:
            asked_hdg = self._orientation.hdg
            asked_pitch = self._orientation.pitch

        # the following calculations are done no matter what - we want the new _current_g etc.
        current_max_allowed_g = _calc_max_g_at_rho(rho, self._i_max_g_sea_level,
                                                   self._i_vector_thrust, self._current_thrust_lbf)
        limited_hdg, limited_pitch = self._calc_g_limited_directional_change(current_max_allowed_g,
                                                                             asked_hdg,
                                                                             asked_pitch,
                                                                             delta_time)
        self._orientation.hdg = limited_hdg
        self._orientation.pitch = limited_pitch

    def _calc_g_limited_directional_change(self, max_allowed_g: float,
                                           asked_hdg: float, asked_pitch,
                                           delta_time: float) -> tuple[float, float]:
        delta_hdg = g.calc_delta_bearing(self._orientation.hdg, asked_hdg)
        delta_pitch = g.calc_delta_bearing(self._orientation.pitch, asked_pitch)
        delta_orientation = math.sqrt(math.pow(delta_hdg, 2) + math.pow(delta_pitch, 2))
        self._current_g = _calc_angular_speed_g(delta_orientation, self._current_speed_fps, delta_time)
        limitation_factor = 1.0
        if self._current_g > max_allowed_g:
            limitation_factor = _approximate_max_delta_orientation(delta_orientation, self._current_speed_fps,
                                                                   delta_time, max_allowed_g)
            self._current_g = _calc_angular_speed_g(delta_orientation * limitation_factor,
                                                    self._current_speed_fps, delta_time)
        return asked_hdg * limitation_factor, asked_pitch * limitation_factor

    def _calc_current_speed(self, current_mach: float, rho: float, delta_time: float) -> None:
        # speed change due to thrust / weight ratio acceleration
        delta_speed_thrust = (self._current_thrust_lbf / self._mass) * delta_time

        # speed change due to drag
        current_cd = _calc_drag_coefficient(current_mach,
                                            self._current_thrust_lbf,
                                            self._i_cd_base, self._i_cd_plume)
        q = 0.5 * rho * self._current_speed_fps * self._current_speed_fps
        drag_acc = (current_cd * q * self._i_cross_section_sqft) / self._mass
        delta_speed_drag = drag_acc * delta_time

        # speed change due to energy bleed
        delta_speed_bleed = _calc_speed_loss_from_energy_bleed(delta_time,
                                                               self._i_vector_thrust,
                                                               self._current_g,
                                                               self._current_thrust_lbf,
                                                               self._position.alt_ft)

        # speed change due to gravity
        delta_speed_gravity = G_FPS * delta_time * math.sin(math.radians(self._orientation.pitch))
        self._current_speed_fps += delta_speed_thrust - delta_speed_drag + delta_speed_bleed \
            - delta_speed_gravity

    def _calc_current_weight(self, delta_time: float) -> None:
        """Weight is reduced by consuming fuel."""
        if self._life_time > (self._i_stage_duration[0] + self._i_stage_duration[1]):
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[2] * delta_time
        elif self._life_time > self._i_stage_duration[0]:
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[1] * delta_time
        else:
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[0] * delta_time


# Below is only for testing - normally this is in mp_targets.py
EMESARY_MSG_IDX = 1


def test_next_emesary_msg_idx() -> int:
    global EMESARY_MSG_IDX
    EMESARY_MSG_IDX += 1
    return EMESARY_MSG_IDX


class TestMissile(unittest.TestCase):
    """This class only tests whether the code can run. Parameters are fine-tuned else-where (not disclosed)"""

    def test_angular_force(self):
        g_force = _calc_angular_speed_g(45., 1., 0.5)
        limiting_factor = _approximate_max_delta_orientation(45., 1., 0.5, g_force * 0.8)
        self.assertLess(limiting_factor, 1.0, 'If max allowed is less than current then factor is below 1')

    def test_guidance_object(self):
        with self.assertRaises(ValueError):
            _ = Guidance('', None)
        with self.assertRaises(ValueError):
            _ = Guidance('xPN', None)
        with self.assertRaises(ValueError):
            _ = Guidance('APNxxy', None)
        guidance = Guidance('OPN', None)
        self.assertTrue(guidance.law is GuidanceLawType.opn)
        self.assertIsNone(guidance._i_fixed_aim_angle)
        guidance = Guidance('APN0506', None)
        self.assertTrue(guidance.law is GuidanceLawType.apn)
        self.assertEqual(guidance._i_fixed_aim_angle, 5)
        self.assertEqual(guidance._i_fixed_aim_time, 6)
        with self.assertRaises(ValueError):
            _ = Guidance('APN050i', None)

    def test_missile_initializable(self):
        shooter_callsign = 'SHOOTER'
        attacker_callsign = 'ATTACK'
        shooter_pos = g.Position(0., 0., 0.)
        try:
            _ = HunterMissile.create_igla_9k38(shooter_callsign, attacker_callsign,
                                               1,
                                               g.Orientation(0., 0.),
                                               shooter_pos,
                                               0., False)
            _ = HunterMissile.create_buk_m2_9m317(shooter_callsign, attacker_callsign,
                                                  1,
                                                  g.Orientation(0., 0.),
                                                  shooter_pos,
                                                  0., True)
            _ = HunterMissile.create_kn_06(shooter_callsign, attacker_callsign,
                                           1,
                                           g.Orientation(0., 0.),
                                           shooter_pos,
                                           0., True)
        except:
            self.fail("initializing missiles raised ExceptionType unexpectedly!")

    def test_hunter_missile_hit_static_target(self):
        """Make sure that an Igla can hit a static target up/down and left/right."""
        import hunter.emesary_notifications as en
        shooter_callsign = 'SHOOTER'
        attacker_callsign = 'ATTACK'
        hdg_missile_initial = 90.  # East
        shooter_pos = g.Position(0., 0., 2000.)
        configurations = [(1000., 45.),  # down to 1000, turn South
                          (1000, 135.),  # down to 1000, turn North
                          (3000, 45.),  # up to 3000, turn South
                          (3000, 135.)]
        for config_tuple in configurations:
            missile = HunterMissile.create_igla_9k38(shooter_callsign, attacker_callsign,
                                                     3,
                                                     g.Orientation(hdg_missile_initial, 0.),
                                                     shooter_pos,
                                                     0., False)
            delta_time = 0.2
            lon_lat = g.calc_destination_pos(shooter_pos, 2500, config_tuple[1])
            a_position = g.Position(lon_lat[0], lon_lat[1], config_tuple[0])
            attacker_pos_hist = mat.AttackerPositionHistory(10)
            now = time.time()
            while True:
                now += delta_time
                attacker_pos = m.AttackerPosition(attacker_callsign, a_position,
                                                  0., 0., None,
                                                  True, 0., 0.,
                                                  now, None)
                attacker_pos_hist.add_position(attacker_pos, now)
                delete_missile, hit, emesary, _ = missile.run_increment(now, attacker_pos_hist, MissileState.active,
                                                                        test_next_emesary_msg_idx)
                if delete_missile:
                    print('Igla missile needs to be deleted')
                    break
                elif hit:
                    print('Igla missile has hit attacker')
                    for eb in emesary:
                        print('encoded_message:', eb.encoded_message, ', for bridge: ', eb.oprf_bridge)
                        my_dict = en.process_incoming_message(eb.encoded_message)
                        for key, notification in my_dict.items():
                            print(key, str(notification))
                    break
            self.assertTrue(hit, 'Missile has hit the target')
            print('Igla missile metrics: ', str(missile.telemetry[-1]))

    def test_hunter_missile_basic_performance_parameters(self):
        """Make sure that a missile flying almost horizontal (a bit up) stays within basic performance parameters."""
        import hunter.mp_targets as mpt
        shooter_callsign = 'SHOOTER'
        attacker_callsign = 'ATTACK'
        hdg_missile_initial = 90.  # East
        shooter_pos = g.Position(0., 0., 2.)
        missiles_to_test = dict()
        missile = HunterMissile.create_igla_9k38(shooter_callsign, attacker_callsign,
                                                 1,
                                                 g.Orientation(hdg_missile_initial, 0.),
                                                 shooter_pos,
                                                 0., False)
        igla_stuff = [missile, 1., 2., 4000.]  # min_ok_mach, max_ok_mach, min_distance_covered
        missiles_to_test['igla'] = igla_stuff

        sam = mpt.SAM_VARIANTS[mpt.MPTarget.BUK_M2]
        missile = HunterMissile.create_buk_m2_9m317(shooter_callsign, attacker_callsign,
                                                    2,
                                                    g.Orientation(hdg_missile_initial, sam.rail_pitch_deg),
                                                    shooter_pos,
                                                    0., True)
        buk_stuff = [missile, 3., 4., 30000.]  # min_ok_mach, max_ok_mach, min_distance_covered
        missiles_to_test[mpt.MPTarget.BUK_M2] = buk_stuff

        sam = mpt.SAM_VARIANTS[mpt.MPTarget.SA_3]
        missile = HunterMissile.create_sa_3_5v27(shooter_callsign, attacker_callsign,
                                                 3,
                                                 g.Orientation(hdg_missile_initial, sam.rail_pitch_deg),
                                                 shooter_pos,
                                                 0., True)
        sa_3_stuff = [missile, 3., 4., 25000.]  # min_ok_mach, max_ok_mach, min_distance_covered
        missiles_to_test[mpt.MPTarget.SA_3] = sa_3_stuff

        sam = mpt.SAM_VARIANTS[mpt.MPTarget.S_300]
        missile = HunterMissile.create_kn_06(shooter_callsign, attacker_callsign,
                                             4,
                                             g.Orientation(hdg_missile_initial, sam.rail_pitch_deg),
                                             shooter_pos,
                                             0., True)
        kn_06_stuff = [missile, 5., 6., 80000.]
        missiles_to_test[mpt.MPTarget.S_300] = kn_06_stuff
        for name, stuff in missiles_to_test.items():
            now = time.time()
            observation_time = 240
            delta_time = 0.2
            number_iterations = int(observation_time / delta_time)
            lon_lat = g.calc_destination_pos(shooter_pos, stuff[3] * 1.5, hdg_missile_initial)
            a_position = g.Position(lon_lat[0], lon_lat[1], 1000.)
            attacker_pos_hist = mat.AttackerPositionHistory(10)
            for i in range(number_iterations):
                now += delta_time
                attacker_pos = m.AttackerPosition(attacker_callsign, a_position,
                                                  0., 0., None,
                                                  True, 0., 0.,
                                                  now, None)
                attacker_pos_hist.add_position(attacker_pos, now)
                delete_missile, hit, _, _ = stuff[0].run_increment(now, attacker_pos_hist, MissileState.active,
                                                                   test_next_emesary_msg_idx)
                if delete_missile:
                    print('Missile', name, 'needs to be deleted')
                    break
                elif hit:
                    print('Missile', name, 'has hit attacker')
                    break
            print('Missile', name, 'metrics: ', str(stuff[0].telemetry[-1]))
            self.assertLessEqual(stuff[0]._life_time, stuff[0]._i_self_destruct_time_sec + 1,
                                 'life_time may not be larger than self_destruct_time')
            self.assertGreater(stuff[0].telemetry[-1].max_mach, stuff[1], 'Minimal mach must be achieved')
            self.assertLess(stuff[0].telemetry[-1].max_mach, stuff[2], 'Mach must be below max value')
            self.assertGreater(stuff[0].telemetry[-1].total_distance, stuff[3], 'Minimal distance must be covered')
