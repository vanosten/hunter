# SPDX-FileCopyrightText: (C) 2020 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Utility functions and constants shared between controller and worker."""
import abc
from collections import deque
from dataclasses import dataclass
import datetime
from enum import IntEnum, unique
import hashlib
import os
import os.path as osp
import time
from typing import Any, List, Optional


MP_SERVER_HOST_DEFAULT = 'mpserver.opredflag.com'
PORT_TELNET_BASIS = 10000  # basic port for Telnet networking
PORT_HTTPD_BASIS = 9000  # basic port
PORT_MP_BASIS = 5000  # basic port for multiplayer networking

# Reserved positions in controller -> 5000 and 5001 can be used for local FG instance in parallel
PORT_MP_BASIS_CLIENT_CTRL = 5000  # basic port for multiplayer networking
INDEX_CONTROLLER = 2
INDEX_AWACS = INDEX_CONTROLLER + 1
INDEX_TANKER = INDEX_AWACS + 1
INDEX_CARRIER = INDEX_TANKER + 1
INDEX_DYNAMIC_START = INDEX_CARRIER + 1
# Client ports in Worker are significantly higher to allow Controller's MP targets on same machine
PORT_MP_BASIS_CLIENT_WORKER = 5150

# Update frequencies
MP_TIMESTAMP_IGNORE_MAXDIFF = 10  # seconds (as specified in FGMS packets)

WORKER_POLLING_REGISTRATION_REQUEST = 10
WORKER_CTRL_POLLING = 10

MAX_DELTA_SHOOTABLE = 5  # seconds since last time MP data for this aircraft (attacker) was processed
POS_UPDATE_FREQ_ATTACKER = 1
POS_UPDATE_FREQ_FG_TARGET = 2  # must be much lower than ..._UPDATES_TO_DS
POS_UPDATE_FREQ_MOVING_TARGET = 5  # every x seconds an update for moving mp_targets, trip mp_targets
POS_UPDATE_FREQ_STATIC_TARGET = 5
POS_UPDATE_FREQ_ZOMBIE_TIME = 15  # max time we consider OK that something is not attached to network
SEND_POSITION_UPDATES_TO_DS = 5  # every x seconds send an update for the position and health to datastore

# mp properties hardcoded instead of efgms.FGMS_prop_code_by_name for performance and to get rid of dependency
# See http://wiki.flightgear.org/Multiplayer_protocol#Properties_Packet
MP_PROP_CHAT = 10002  # '/sim/multiplay/chat'
MP_PROP_LIVERY = 1101  # sim/model/livery/file
MP_PROP_TANKER = 1300

MP_PROP_SURFACE_POS_RUDDER = 103
MP_PROP_SURFACE_POS_FLAP = 104

MP_PROP_GEAR_0_POSITION_NORM = 201
MP_PROP_GEAR_1_POSITION_NORM = 211
MP_PROP_GEAR_2_POSITION_NORM = 221
MP_PROP_ENGINE_0_N1 = 300
MP_PROP_ENGINE_0_N2 = 301
MP_PROP_ENGINE_0_RPM = 302
MP_PROP_ENGINE_1_N2 = 311
# MP_PROP_ENGINE_1_RPM = 302
MP_PROP_ROTOR_MAIN = 800
MP_PROP_ROTOR_TAIL = 801
MP_PROP_GENERIC_BOOL_BASE = 11000  # sim/multiplay/generic/bool[0]
MP_PROP_GENERIC_STRING_BASE = 10100  # sim/multiplay/generic/string[0]
MP_PROP_GENERIC_FLOAT_BASE = 10200  # sim/multiplay/generic/float[0]
MP_PROP_GENERIC_INT_BASE = 10300  # sim/multiplay/generic/int[0]
MP_PROP_OPRF_SMOKE = MP_PROP_GENERIC_INT_BASE  # /sim/multiplay/generic/int[0]
MP_PROP_OPRF_RADAR = MP_PROP_GENERIC_INT_BASE + 2  # int[2] value 1 means silent, 0 means active (yes, so it is)
# the radar lock is the first 4 characters of an MD5(callsign) - see e.g. method create_radar_hash_for_callsign(...)
MP_PROP_OPRF_RADAR_LOCK = MP_PROP_GENERIC_STRING_BASE + 6
MP_PROP_OPRF_SELECT = MP_PROP_GENERIC_INT_BASE + 17  # sim/multiplay/generic/int[17] - for depot, groundtarget, hunter

# rotors/main/blade[3]/flap-deg - if value != 0 and value is different from previous one, then we have a new flare
MP_PROP_OPRF_FLARES = 823  # rotors/main/blade[3]/flap-deg
MP_PROP_OPRF_CHAFFS = 813  # rotors/main/blade[3]/position-deg

MP_PROP_OPFOR_IFF_HASH = MP_PROP_GENERIC_STRING_BASE + 4
MP_PROP_OPFOR_DATALINK = MP_PROP_GENERIC_STRING_BASE + 7  # not the channel, but for transmitting the data

MP_PROP_EMESARY_BRIDGE_BASE = 12000

MP_PROP_FGUK_CRASHED = MP_PROP_GENERIC_INT_BASE + 7
MP_PROP_FGUK_BEACON = MP_PROP_GENERIC_INT_BASE + 1
MP_PROP_FGUK_STROBE = MP_PROP_GENERIC_INT_BASE + 3


# added altitude angle to measured altitude angle to compensate for earth curvature and other disturbing stuff,
# such that a very low flying target cannot be detected very far out
# 0.06 degrees is ca. 1 metres per 1000 m distance, 1 degree is ca. 17 metres.
# Earth curvature is 2 metres at 5 km and 8 metres at 10 km.
EXTRA_ALTITUDE = 0.5  # degrees (above ground)


def create_radar_hash_for_callsign(callsign: str) -> str:
    hashed = hashlib.md5(callsign.encode()).hexdigest()
    return hashed[:4]


def get_mp_server_address() -> str | None:
    """Reads the environment variable based on a name.
    If it is not set, then exit.
    """
    my_mp_server = os.getenv('HUNTER_MP_SERVER')
    if my_mp_server is None or len(my_mp_server) <= 1:  # it is set to "x" in the Dockerfile
        return MP_SERVER_HOST_DEFAULT
    if 'flightgear.org' in my_mp_server:
        return None
    return my_mp_server


def get_hunter_directory() -> str:
    """Determines the absolute path of the hunter root directory."""
    my_file = osp.realpath(__file__)
    my_dir = osp.split(my_file)[0]  # now we are in the hunter/hunter directory
    my_dir = osp.split(my_dir)[0]  # now we are in the root hunter directory
    return my_dir


SCENARIO_RESOURCES_DIR = 'scenario_resources'


def get_scenario_resources_dir() -> str:
    return osp.join(get_hunter_directory(), SCENARIO_RESOURCES_DIR)


def date_time_now() -> str:
    """Date and time as of now formatted as a string incl. seconds."""
    today = datetime.datetime.now()
    return today.strftime("%Y-%m-%d_%H%M%S")


@unique
class ShipShootingType(IntEnum):
    passive = 0
    only_ciws = 1
    ciws_or_missiles = 2
    only_missiles = 3
    # future: ciws_and_missiles = 4


def map_ship_shooting_display(shooting_type: int) -> str:
    if shooting_type == 1:
        return 'Shooting with CIWS'
    elif shooting_type == 2:
        return 'Shoots either with CIWS or missiles, but not both'
    elif shooting_type == 3:
        return 'Shoots only missiles'
    else:
        return 'Passive'


@unique
class SAMShootingType(IntEnum):
    passive = 0
    replace_all_sams = 1  # with Shilkas
    replace_not_simulated_sams_shilka = 2
    replace_not_simulated_sams_sa_3 = 3
    only_simulated = 4


def map_sam_shooting_display(shooting_type: int) -> str:
    if shooting_type == 1:
        return 'SAMs replaced with Shilkas'
    elif shooting_type == 2:
        return 'SAMs replaced with Shilkas, but only if SAM type not available for shooting in Hunter'
    elif shooting_type == 3:
        return 'SAMs replaced with SA-3, but only if SAM type not available for shooting in Hunter'
    elif shooting_type == 4:
        return 'All in Hunter simulated SAMs are shooting, others faking'
    else:
        return 'Passive'


@dataclass(frozen=True)
class Hostility:
    heli_shoot: bool
    drone_shoot: bool
    ship_shooting_type: ShipShootingType
    shilka_shoot: bool
    sam_shooting_type: SAMShootingType

    @property
    def is_hostile(self) -> bool:
        return self.heli_shoot or self.drone_shoot or self.ship_shoot or self.shilka_shoot or self.sam_shoot

    @property
    def ship_shoot(self) -> bool:
        return self.ship_shooting_type is not ShipShootingType.passive

    @property
    def sam_shoot(self) -> bool:
        return self.sam_shooting_type is not SAMShootingType.passive


def parse_hostility(hostility: str) -> Hostility:
    using = 'Hostility must have exactly 5 ints separated by "_" (e.g. 0_1_4_2_0)'
    split = hostility.split('_')
    if len(split) != 5:
        raise ValueError(using)
    try:
        heli_shoot = int(split[0]) > 0
        drone_shoot = int(split[1]) > 0
        ship_int = int(split[2])
        if ship_int == 0:
            ship_shooting_type = ShipShootingType.passive
        elif ship_int == 1:
            ship_shooting_type = ShipShootingType.only_ciws
        elif ship_int == 2:
            ship_shooting_type = ShipShootingType.ciws_or_missiles
        else:
            ship_shooting_type = ShipShootingType.only_missiles
        shilka_shoot = int(split[3]) > 0
        sam_int = int(split[4])
        if sam_int == 0:
            sam_shooting_type = SAMShootingType.passive
        elif sam_int == 1:
            sam_shooting_type = SAMShootingType.replace_all_sams
        elif sam_int == 2:
            sam_shooting_type = SAMShootingType.replace_not_simulated_sams_shilka
        elif sam_int == 3:
            sam_shooting_type = SAMShootingType.replace_not_simulated_sams_sa_3
        else:
            sam_shooting_type = SAMShootingType.only_simulated
    except ValueError:
        raise ValueError(using)
    return Hostility(heli_shoot, drone_shoot, ship_shooting_type, shilka_shoot, sam_shooting_type)


def number_part_of_callsign(callsign: str) -> int:
    """Extracts the last two letters the callsign and converts them to a number."""
    number_part = callsign[-2:]
    return int(number_part)


def parse_callsigns_string(callsigns_string: str, check_number_part: bool = False) -> List[str]:
    if callsigns_string:
        callsigns = callsigns_string.split('#')
        for callsign in callsigns:
            if callsign is None or len(callsign) == 0 or len(callsign) > 7:
                raise ValueError('A callsign needs to be max 7 characters and otherwise match the -i argument')
            if check_number_part:
                try:
                    _ = number_part_of_callsign(callsign)
                except ValueError:
                    raise ValueError('A callsign demands the last 2 chars to be numeric (e.g. OPFOR03')
    else:
        callsigns = list()
    return callsigns


def create_prop_for_string(key: str, value: str) -> str:
    return '--prop:string:/{}={}'.format(key, value)


def create_prop_for_int(key: str, value: int) -> str:
    return '--prop:int:/{}={}'.format(key, str(value))


def create_prop_for_double(key: str, value: float) -> str:
    return '--prop:double:/{}={}'.format(key, str(value))


def create_prop_for_float(key: str, value: float) -> str:
    return '--prop:float:/{}={}'.format(key, str(value))


def create_prop_for_bool(key: str, value: bool) -> str:
    return '--prop:bool:/{}={}'.format(key, str(value).lower())


def create_session_id() -> str:
    """The session id exchanged between controller and workers to makes sure they are in the same session.
    Nanoseconds since epoch should be pretty unique. Str due to Azure id must be str."""
    return str(time.time_ns())


def create_utf_isostring_timestamp() -> str:
    """Create a timestamp for UTC time formatted as with ISO 8601 UTC standard."""
    return datetime.datetime.now(datetime.UTC).isoformat()


def parse_isostring_timestamp(value: Optional[str]) -> Optional[datetime.datetime]:
    """Parse a timestamp from a ISO 8601 string (assumed to be UTC without timezone)."""
    if value:
        return datetime.datetime.fromisoformat(value)
    return None


def read_dotenv() -> None:
    """Read the .env file through a wrapper to apply some defaults.

    Using override=True especially because LD_LIBRARY_PATH must be set to something enhanced.
    """
    from dotenv import load_dotenv
    load_dotenv(verbose=True, override=True)
    print_os_environ()


def print_os_environ() -> None:
    for k, v in os.environ.items():
        if 'AZ_COSMOS' in k:
            print(f'{k}=****')
        else:
            print(f'{k}={v}')


class ParticipantMessageInterface(metaclass=abc.ABCMeta):
    """Broker to handle messages (mostly as defined by messages.py).

    It uses a queueing mechanism, such that this can work in e.g. for multithreading or multiprocessing.
    The "client" here the single Controller interacting with "providers" - which here are
    FGMS Participants. These FGMS Participants use a specific implementation of this interface to send
    and receive messages to/from the Controller.
    NB: there is a 1:1 relationship. For each participant there is 1 implementation to interface with the Controller.

    See also https://realpython.com/python-interface/#using-abstract-method-declaration
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'post_message') and
                callable(subclass.post_message) and
                hasattr(subclass, 'send_message_out') and
                callable(subclass.send_message_out) and
                hasattr(subclass, 'receive_message') and
                callable(subclass.receive_message) and
                hasattr(subclass, 'fetch_incoming_message') and
                callable(subclass.fetch_incoming_message) or
                NotImplemented)

    @abc.abstractmethod
    def post_message(self, message) -> None:
        """Allow the Controller to post a message to a participant."""
        raise NotImplementedError

    @abc.abstractmethod
    def send_message_out(self, message) -> None:
        """Allow the participant to send something out to the Controller."""
        raise NotImplementedError

    @abc.abstractmethod
    def receive_message(self) -> Optional[Any]:
        """Allow the Controller to get messages/results back from the participant.

        If there are no more messages in the queue, then None is returned.
        The caller must repeat calling this method until None is returned.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def fetch_incoming_message(self) -> Optional[Any]:
        """Allow the participant to look for incoming messages from the Controller.

        If there are no more messages in the queue, then None is returned.
        The caller must repeat calling this method until None is returned.
        """
        raise NotImplementedError


MIN_DURATION_CHAT_MESSAGE = 1.6


class ChatMessageHandler:
    """Handles the queueing and sending of chat messages. Makes sure that a message is sent during a certain
    period, such that a single drop of a message does not hide the message."""

    def __init__(self) -> None:
        self.messages = deque()
        self.current_message = None  # the message that currently should be sent
        self.current_message_first_sent = 0

    def add_message(self, message: str) -> None:
        self.messages.append(message[0:128])  # make sure it does not get too long

    def get_chat_message(self, props: dict[int, Any]) -> None:
        """Add message to MP props if necessary."""
        now = time.time()
        if self.current_message:
            if now - self.current_message_first_sent > MIN_DURATION_CHAT_MESSAGE:
                self.current_message = None
                self.current_message_first_sent = 0

        if self.current_message is None and self.messages:
            self.current_message = self.messages.popleft()
            self.current_message_first_sent = now

        if self.current_message:
            props[MP_PROP_CHAT] = self.current_message
