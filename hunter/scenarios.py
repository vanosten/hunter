# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from enum import IntEnum, unique
import os.path as osp
import pickle
import random
from typing import Optional
import time

import networkx as nx
import shapely.geometry as shg

import hunter.mp_carrier as mpc
import hunter.mp_targets as mpt
import hunter.geometry as g
import hunter.logger_config as lc
import hunter.messages as m
import hunter.network_stuff as ns
import hunter.utils as u

logger = lc.get_logger()

# Standard file name endings for scenario helper files - should start with an '_'
# The file name gets scenario.ident + ending
FILE_PICKLE_DEM = '_dem.bz2'  # digital elevation model
FILE_SHIP_NETWORKS = '_ship_networks.pkl'


APT_DAT_AIRPORT_FILE = 'airports.pkl'  # situated in directory scenario_resources and created by airports_io.py


class AptDatAirport:
    """Simplified airport to hold the minimal data from FG apt-dat.

    According to the x-plane definition 1 = land airport, 16 = sea base, 17 = heliport
    """
    __slots__ = ('icao', 'name', 'lon', 'lat', 'kind')

    def __init__(self, icao: str, name: str, lon: float, lat: float, kind: int) -> None:
        self.icao = icao
        self.name = name
        self.lon = lon
        self.lat = lat
        self.kind = kind


class StaticTarget:
    """A container for the properties of a static target.
    Missile_priority determines whether it might be run as an FG instance of the same type.
    0 means never be run as MP target.
    Other priorities can only be 1 (highest), 2 and 3. If there are more static targets of a specific type (SAM)
    and FG instances are run in parallel, then Hunter picks the highest prioritized first to be run as FG instances --
    still randomly (e.g. if there can be 5 FG instances for SAMs, but there are 10 SAMs defined, then Hunter needs to
    be able to decide, which should be picked).
    """
    __slots__ = ('target_type', 'position', 'heading', 'missile_priority', '_fg_instance')

    def __init__(self, target_type: str, position: g.Position, heading: float, missile_priority: int = 0) -> None:
        self.target_type = target_type
        self.position = position
        self.heading = heading
        self.missile_priority = missile_priority if 0 <= missile_priority < 4 else 3
        self._fg_instance = False

    def mark_as_fg_instance(self) -> None:
        self._fg_instance = True

    def is_fg_instance(self) -> bool:
        return self._fg_instance


@unique
class AutomatType(IntEnum):
    """Needs to follow the numbering in automat-set.xml"""
    f_16 = 0
    mig_29 = 1
    f_15 = 2
    su_27 = 3
    ja_37 = 4
    su_34 = 5
    e_3 = 6
    a_50 = 7
    mig_23 = 8


class AutomatTarget:
    """A container to define an automat (shooting fighter jet on FG instance).

    The parameters match to a certain degree the capabilities of the automat (at beginning of automat-set.xml,
    as per 2020-05-23).
    Parameters mean the following:
    * plan: flight plan (must exist in sub-folder "Routes")
    * craft: type of aircraft (see AutomatType)
    * randomize_plan: if True then randomize the order of waypoints after the 3rd, but not the last
    * fire-first: whether the automat fires first (offensive) or only starts offensive if having been shot at
    * short_range_ammo_max: 0 if None, maximum of aircraft if True, randomized between 1 and max-1 (sidewinder)
    * long_range_ammo_max: ditto (AMRAAM)
    * cannon_ammo: 0 - 150 bullets
    * floor: minimum altitude (ft) if automate is engaged in combat - it will always try to fight above this altitude.
             NB: this can be different from the altitude used in the waypoints of the flight plan
    * floor_dive: Set floor-dive to something above highest mountain peaks.
                  Otherwise, it might hit terrain when doing a split-S (terrain radar will not help in due time).
    * start_speed: Ground speed in KT. If starting from start of runway (cf. plan) it must be set to 0
    * transponder: if transponder is set to False, it will make it tougher to find the automat in a dogfight
    * engage_range: within this distance the automat will engage in fighting
    * g_endurance_modern: set to True means that the pilot uses a modern g-suit (e.g. 1997 checkbox  in F-16)
    * pause_after_crash: time in minutes before coming back online. 25 seconds will always be added to this.
    * wait_before_online: time in minutes (after a crash it takes pause_after_crash+wait_before_online)
    * number_of_lives: unlimited if 0, otherwise after a number of crashes the Automat is not restarted any more
    * tacview: stores https://www.tacview.net/ data into $fghome/Export

    The following property available in the automat is set by the Controller at runtime:
    * forced-callsign
    * forced-port

    The following properties available in the automat are fixed:
    * callsign-override (true)
    * blufor (false)
    * bomber (false)
    * remember-aggression-minutes (10)
    * fuel-percent (100)
    * cruise-mach (0.8)
    * cruise-min-mach (0.6)
    * emergency (false)
    * ceiling (45000)
    * roll (30)
    * counter (2)
    * port-override (true)
    * server-oprf (true)
    """
    __slots__ = ('plan', 'craft', 'randomize_plan', 'fire_first',
                 'a9_ammo', 'a120_ammo', 'cannon_ammo',
                 'floor', 'floor_dive', 'start_speed', 'transponder', 'engage_range',
                 'g_endurance', 'pause_time_minutes', 'freeze_time_minutes', 'number_of_lives', 'tacview')

    def __init__(self, plan: str, craft: AutomatType, randomize_plan: bool, fire_first: bool,
                 short_range_ammo_max: Optional[bool], long_range_ammo_max: Optional[bool], cannon_ammo: int,
                 floor: int, floor_dive: int, start_speed: int, transponder: bool, engage_range: int,
                 g_endurance_modern: bool, pause_after_crash: int, wait_before_online: int,
                 number_of_lives: int = 0, tacview: bool = False) -> None:
        self.plan = plan
        self.craft = craft
        self.randomize_plan = randomize_plan
        self.fire_first = fire_first
        self.a9_ammo = 0  # short_range_ammo_max is None
        if short_range_ammo_max is True:
            self.a9_ammo = 2
        elif short_range_ammo_max is False:
            self.a9_ammo = 1
        self.a120_ammo = 0  # long_range_ammo_max is None
        max_a120 = 4 if self.craft in [AutomatType.f_16, AutomatType.mig_29] else 6
        if long_range_ammo_max is True:
            self.a120_ammo = max_a120
        elif long_range_ammo_max is False:
            self.a120_ammo = random.randint(1, max_a120 - 1)
        self.cannon_ammo = cannon_ammo if 0 < cannon_ammo < 151 else 150
        self.floor = floor
        self.floor_dive = floor_dive
        self.start_speed = start_speed if start_speed >= 0 else 0
        self.transponder = transponder
        self.engage_range = engage_range if 0 < engage_range < 100 else 50
        self.g_endurance = 30 if g_endurance_modern else 10
        self.pause_time_minutes = pause_after_crash if pause_after_crash >= 0 else 0
        self.freeze_time_minutes = wait_before_online if wait_before_online >= 0 else 0
        self.number_of_lives = number_of_lives
        self.tacview = tacview

    def create_properties_string(self, forced_callsign: str, iff_opfor: int, datalink_opfor: int) -> str:
        props_string = list()
        props_string.append(u.create_prop_for_int('automat/config/craft', self.craft.value))

        props_string.append(u.create_prop_for_string('automat/next-flight/plan', self.plan))
        props_string.append(u.create_prop_for_bool('automat/next-flight/randomize-plan', self.randomize_plan))
        props_string.append(u.create_prop_for_string('automat/next-flight/forced-callsign', forced_callsign))
        props_string.append(u.create_prop_for_bool('automat/next-flight/callsign-override', True))
        props_string.append(u.create_prop_for_bool('automat/next-flight/blufor', False))
        props_string.append(u.create_prop_for_int('automat/next-flight/a9-ammo', self.a9_ammo))
        props_string.append(u.create_prop_for_int('automat/next-flight/a120-ammo', self.a120_ammo))
        props_string.append(u.create_prop_for_int('automat/next-flight/cannon-ammo', self.cannon_ammo))
        props_string.append(u.create_prop_for_int('automat/next-flight/start-speed', self.start_speed))
        props_string.append(u.create_prop_for_int('automat/next-flight/fuel-percent', 100))
        props_string.append(u.create_prop_for_int('automat/next-flight/ceiling', 45000))
        props_string.append(u.create_prop_for_int('automat/next-flight/floor', self.floor))
        props_string.append(u.create_prop_for_int('automat/next-flight/floor-dive', self.floor_dive))
        props_string.append(u.create_prop_for_double('automat/next-flight/pause-time-minutes',
                                                     self.pause_time_minutes))
        props_string.append(u.create_prop_for_double('automat/next-flight/freeze-time-minutes',
                                                     self.freeze_time_minutes))
        props_string.append(u.create_prop_for_bool('automat/next-flight/server-oprf', True))
        props_string.append(u.create_prop_for_bool('automat/next-flight/port-override', True))
        # prop forced-port is done elsewhere on the fly

        props_string.append(u.create_prop_for_bool('automat/real-time/fire-first', self.fire_first))
        props_string.append(u.create_prop_for_int('automat/real-time/remember-aggression-minutes', 10))
        props_string.append(u.create_prop_for_bool('automat/real-time/emergency', False))
        props_string.append(u.create_prop_for_bool('automat/real-time/tacview', self.tacview))
        props_string.append(u.create_prop_for_float('automat/real-time/counter', 2.))
        props_string.append(u.create_prop_for_bool('automat/real-time/transponder', self.transponder))
        props_string.append(u.create_prop_for_bool('automat/real-time/bomber', False))
        props_string.append(u.create_prop_for_int('automat/real-time/engage-range', self.engage_range))
        props_string.append(u.create_prop_for_int('automat/real-time/channel-datalink', datalink_opfor))
        props_string.append(u.create_prop_for_int('automat/real-time/channel-iff', iff_opfor))
        return ' '.join(props_string)


class CarrierDefinition:
    """Defines the properties for one carrier.

    Description of parameters:
    * carrier_type: one of the (4) MP carriers - most often Vinson is the best choice due to size and having fleet
    * sail_area: a list of lon/lat confining the area where the carrier can sail (first != last node -> not closed)
                 The sail area should be a https://en.wikipedia.org/wiki/Convex_polygon and at least have one line
                 between 2 points, which is longer than 20 km. Remember that a carrier's turn radius is measured in
                 several kilometres and that it can steam at over 20 kts.
    * loiter_centre: a point around which the carrier could loiter if nothing to do - is also the start point and
                     often in the middle
    """
    __slots__ = ('carrier_type', 'sail_area', 'loiter_centre')

    def __init__(self, carrier_type: mpc.CarrierType, sail_area: list[tuple[float, float]],
                 loiter_centre: tuple[float, float]) -> None:
        self.carrier_type = carrier_type
        self.sail_area = sail_area
        self.loiter_centre = loiter_centre
        if len(self.sail_area) < 3:
            raise ValueError('The sail area for the carrier must at least have 3 nodes')


class TankerDefinition:
    """Defines the properties for one tanker (KC-137R) flying a pattern between point A and B at a certain level.

    Typically, the pattern network is at FL200 and the tanker flies an endless loop.

    You should make sure that the distance between point A and point B is long enough such that the tanker is not
    constantly turning. Never go below 10 nm. Remember that the turning radius of a tanker is significant.

    If one of the below callsigns is used, then the tanker automatically gets a TACAN (cf. fgdata/Nasal/tanker.nas):
    ESSO1 040X, ESSO2 041X, ESSO3 042X
    TEXACO1 050X, TEXACO2 051X, TEXACO3 052X
    MOBIL1 060X, MOBIL2 061X MB2, MOBIL3 062X

    Other callsigns (for references - collected once from JMav):
    'SIMBA', 'TARTAN', 'FAGIN',  # RAF
    'PEDRO', 'OILER',  # Canadian AF
    'TOTAL', 'MARCO',  # French AF
    'LUIGI', 'BREUS',  # Italian AF
    'MITY',  # Swedish AF
    'ARSENA',  # Turkish AF
    'SHELL', 'TEXACO', 'ESSO', 'MOBIL', 'QUID', 'CAFE',  # USAF
    'DOLLAR', 'EXXON', 'GOLD', 'FUELER', 'GASMAN', 'MOBILE', 'NITRO'
    """
    __slots__ = ('tanker_network', 'callsign')

    def __init__(self, lon_a: float, lat_a: float, lon_b: float, lat_b: float,
                 alt_m: float, callsign: str = 'TEXACO1') -> None:
        plane = mpt.tanker_kc_137r
        self.tanker_network = ns.create_tanker_track(lon_a, lat_a, lon_b, lat_b, alt_m,
                                                     plane.cruise_speed, plane.turn_rate)
        self.callsign = callsign


class AwacsDefinition:
    __slots__ = ('awacs_network', 'callsign')

    def __init__(self, lon_a: float, lat_a: float, lon_b: float, lat_b: float,
                 alt_m: float, callsign: str = 'SKYEYE') -> None:
        self.awacs_network = ns.create_awacs_track(lon_a, lat_a, lon_b, lat_b, alt_m)
        self.callsign = callsign


class ShipNetworks:
    __slots__ = ('offshore_networks', 'coastal_networks')

    def __init__(self) -> None:
        self.offshore_networks: Optional[list[nx.Graph]] = None
        self.coastal_networks: Optional[list[nx.Graph]] = None

    @staticmethod
    def read_ship_networks_from_file(path: str) -> 'ShipNetworks':
        try:
            with open(path, 'rb') as file_pickle:
                logger.info('Loading ship networks from %s', path)
                return pickle.load(file_pickle)
        except IOError:
            logger.info('No network could be found with path = %s', path)
        return ShipNetworks()  # just an empty one

    def save_ship_networks_to_file(self, path: str):
        try:
            with open(path, 'wb') as file_pickle:
                pickle.dump(self, file_pickle)
        except IOError:
            logger.info('Could not save ShipNetworks to file with path = %s', path)


class ShipNetworkParameters:
    """Parameters used when creating ship networks using hunter.scenario_tools_ship_network:

    * sail_area: A list of lon/lat confining the area where the carrier can sail (first != last node -> not closed).
                 The list should be counter-clock wise
    * nearshore_factor: The factor to apply to the scenario.lon_dem_grid_size to get nearshore buffer
                        around land/coast
    * min_offshore_circumference: The minimum circumference of a (sub-)network for offshore (in metres).
                                  Should be large such that frigates have somewhere to navigate.
    * min_coastal_circumference: The minimum circumference of a (sub-)network for coastal (in metres).
                                 Should be large such that coastal ships / speedboats have somewhere to navigate.
    """
    __slots__ = ('sail_area_offshore', 'sail_area_coastal', 'min_level', 'max_level', 'nearshore_factor',
                 'min_offshore_circumference', 'min_coastal_circumference')

    def __init__(self, sail_area_offshore: Optional[list[tuple[float, float]]] = None,
                 sail_area_coastal: Optional[list[tuple[float, float]]] = None,
                 min_level: int = 3, max_level: int = 7, nearshore_factor: int = 20,
                 min_offshore_circumference: int = 80000,  # box of 20 km
                 min_coastal_circumference: int = 20000  # box of 5 km
                 ) -> None:
        self.sail_area_offshore = sail_area_offshore
        self.sail_area_coastal = sail_area_coastal
        self.min_level = min_level
        self.max_level = max_level
        if self.min_level < 0:
            raise ValueError('min_level may not be smaller than 0')
        if self.min_level > self.max_level:
            raise ValueError('max_level may not be smaller than min_level')

        self.nearshore_factor = nearshore_factor
        self.min_offshore_circumference = min_offshore_circumference
        self.min_coastal_circumference = min_coastal_circumference


class ShipsDefinition:
    """Defines stuff for offshore ships (ships) and coastal ships (coasters) incl. parameters for network creation.

    """
    __slots__ = ('min_ships', 'initial_ships', 'min_coasters', 'initial_coasters',
                 'ship_networks', 'ship_network_parameters')
    OFFSHORE_SHIPS = [mpt.oprf_frigate, mpt.us_navy_lake_champlain, mpt.us_navy_normandy, mpt.us_navy_oliver_perry,
                      mpt.us_navy_san_antonio]
    COASTAL_SHIPS = [mpt.inflatable_speedboat]

    def __init__(self, min_offshore: int, initial_offshore: int,
                 min_coastal: int, initial_coastal: int,
                 ship_network_parameters: ShipNetworkParameters, path: str) -> None:
        self.min_ships: int = min_offshore
        self.initial_ships: int = initial_offshore

        self.min_coasters: int = min_coastal
        self.initial_coasters: int = initial_coastal

        self.ship_network_parameters = ship_network_parameters
        self.ship_networks: ShipNetworks = ShipNetworks.read_ship_networks_from_file(path)

    def validate(self) -> None:
        if self.initial_ships > 0 and (self.ship_networks.offshore_networks is None or
                                       len(self.ship_networks.offshore_networks)) == 0:
            raise ValueError('cannot have ships defined when there is no corresponding offshore network')
        if self.initial_coasters > 0 and (self.ship_networks.coastal_networks is None or
                                          len(self.ship_networks.coastal_networks)) == 0:
            raise ValueError('cannot have coasters defined when there is no corresponding coastal network')

    def next_moving_ship(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        yield_asset = False
        if self.initial_ships > 0:
            self.initial_ships -= 1
            yield_asset = True
        elif remaining_assets < self.min_ships:
            yield_asset = True
        if yield_asset:
            network = random.choice(self.ship_networks.offshore_networks)
            starting_point = random.choice(list(network.nodes))
            asset = random.choice(self.OFFSHORE_SHIPS)
            return m.AddMovingTarget(asset, network, starting_point)
        return None

    def next_moving_coaster(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        yield_asset = False
        if self.initial_coasters > 0:
            self.initial_coasters -= 1
            yield_asset = True
        elif remaining_assets < self.min_coasters:
            yield_asset = True
        if yield_asset:
            network = random.choice(self.ship_networks.coastal_networks)
            starting_point = random.choice(list(network.nodes))
            asset = random.choice(self.COASTAL_SHIPS)
            return m.AddMovingTarget(asset, network, starting_point)
        return None


class ScenarioContainer:
    """Container for all elements of a scenario.

    NB: all positions are lon first, lat second!

    Description of Parameters:
    * ident: the identifier used: must not contain blanks, must be lower_case and must correspond to the name
             of the scenario file (e.g. "north_norway" when the scenario file is "scenario_north_norway.py")
    * name: the human-readable name of the scenario (e.g. shown in the web-ui).
    * description: longer description.
    * south_west: the South West corner of the scenario area (mostly for mapping purposes -> should include all
                  airports from which attackers or targets start/land -> "war area").
                  Be aware that this coordinate as well as north-east must be correct, because otherwise the
                  Hunter controller (which is placed in the middle) might be placed outside the MP distance
                  (depending on MP server ca. 200 nm visibility) and therefore give timeout exception because it
                  cannot communicate with the targets.
    * north_east: the North East corner of the scenario area
    * icao: "typical" airport in the scenario - default for automats, but they might take off from another place.
    * polling_freq: time in seconds to test whether a new heli or ship should be added
    * mp_visibility_range: how far out Hunter (specifically the receiver) can see. By default, set to 300 nm.
                           (https://wiki.flightgear.org/Multiplayer_protocol#Visibility_range)
                           It is not just a good thing to set the range too large - because potentially it increases
                           the network traffic - and Hunter might work on MP-participants that have nothing to do
                           with Hunter - thereby increasing computation unnecessarily. However, e.g. when running
                           scenarios with long-range SAMs and/or over a large area (like Falklands),
                           then it increases the chance that some assets might not be seen (especially the carrier).
    * dem_south_west: the South West corner of the digital elevation model (DEM) grid. If no value is given, then the
                      value of south_west is used.
    * dem_north_east: The North East corner of the DEM grid.
    * lon_dem_grid_size: The size in degrees for the (DEM) grid in longitudinal direction.
                         The grid is used to calculate visibility (e.g. due to mountains, curvature of earth) and
                         other things, which need a DEM.
                         The more height changes there are in the landscape, the smaller the value should be chosen.
                         However, more accuracy comes at the cost of disk size and somewhat calculation time.
                         Additionally, the digital elevation model in FlightGear (from which this DEM is derived)
                         has not a very high accuracy by itself. Going below 50 metres will not result in much
                         improvement - and the DEM is not really supposed to be used for very accurate calculations.
                         In North Norway at EMMA 0.001 degrees equals to ca. 38 metres (longitudinal).
                         In Switzerland at LSZR 0.001 degrees equals to ca. 74 metres (longitudinal).
                         The size is also used for the underlying network-resolution for ships - the "blockiness"
                         of the shore if large values are chosen. Again, around 50 m should be just fine.
    * lon_dem_grid_size: Ditto in latitude direction.
                         In North Norway at EMMA 0.0005 degrees equals to ca. 57 metres.
                         In Switzerland at LSZR 0.0005 degrees equals to ca. 54 metres.
    * iff_opfor:         The IFF code used by mp_targets (or defenders).
                         OPFOR Viggen knows only 1-11, Tomcat in real life 0-33, but in OPRF 0-9999, Mirage apparently
                         the last digit cannot be > 7.
    * iff_attackers:     The IFF code used by attackers as well as Hunter stuff supporting the attackers -
                         e.g. a tanker or AWACS.
    * datalink_opfor:    The datalink code/channel used by mp_targets (or defenders).
                         The OPFOR F-14 seems to be the most limited one being restricted to 0-99.
    * datalink_attackers:The datalink code/channel used by attackers as well as Hunter stuff supporting attackers -
                         e.g. a tanker or AWACS.

    Most targets are added with add_* methods. See the related description of the functions for parameters.
    NB: targets can only be added as part of the scenario creation - not dynamically afterwards.
    And only once - i.e. you cannot add several automats by calling the method add_automats() several times, you need
    to have a final list of automats when calling the method.
    """

    def __init__(self, ident: str, name: str, description: str,
                 south_west: tuple[float, float], north_east: tuple[float, float],
                 icao: str,
                 polling_freq: int = 60,
                 mp_visibility_range: int = 300,
                 dem_south_west: Optional[tuple[float, float]] = None,
                 dem_north_east: Optional[tuple[float, float]] = None,
                 lon_dem_grid_size: float = 0.001,
                 lat_dem_grid_size: float = 0.0005,
                 iff_opfor: int = 1,
                 iff_attackers: int = 6,
                 datalink_opfor: int = 11,
                 datalink_attackers: int = 66) -> None:
        self.ident = ident
        self.name = name
        self.description = description
        self.static_targets = list()
        self.automats = None

        self.ships_definition: Optional[ShipsDefinition] = None

        self.carrier_definition: Optional[CarrierDefinition] = None
        self.tanker_definition: Optional[TankerDefinition] = None
        self.awacs_definition: Optional[AwacsDefinition] = None

        self.helis_min = 0
        self.helis_initial = 0
        self.helis_network = None
        self.heli_assets = list()

        self.drones_min = 0
        self.drones_initial = 0
        self.drones_network = None
        self.drones_assets = list()

        self.ga_min = 0
        self.ga_initial = 0
        self.ga_network = None
        self.ga_assets = list()

        self.trip_targets = None

        # towed
        self.towed_number = 0
        self.towing_asset = None
        self.towed_dist = 0
        self.towed_speed = 0
        self.towed_network = None

        self.south_west = south_west
        self.north_east = north_east
        self.icao = icao
        self.polling_freq = polling_freq  # number of seconds before next_moving_xxx() should be called again

        self.mp_visibility_range = mp_visibility_range

        self.dem_south_west = south_west if dem_south_west is None else dem_south_west
        self.dem_north_east = north_east if dem_north_east is None else dem_north_east
        self.lon_dem_grid_size = lon_dem_grid_size
        self.lat_dem_grid_size = lat_dem_grid_size

        self.iff_opfor = iff_opfor
        self.iff_attackers = iff_attackers
        self.datalink_opfor = datalink_opfor
        self.datalink_attackers = datalink_attackers

        self.scenario_started = 0  # time.time() when started

        # set after scenario have been loaded to be available for other stuff
        self.load_name: str = ''
        self.scenario_path: str = ''

    def validate(self) -> None:
        # check the outer border
        if self.south_west[0] >= self.north_east[0]:
            raise ValueError('The longitude of the South-West corner needs to be smaller than the North-East.')
        if self.south_west[1] >= self.north_east[1]:
            raise ValueError('The latitude of the South-West corner needs to be smaller than the North-East.')

        scenario_border = shg.box(self.south_west[0], self.south_west[1],
                                  self.north_east[0], self.north_east[1])

        # check the DEM
        if self.dem_south_west[0] >= self.dem_north_east[0]:
            raise ValueError('The lon of the DEM-South-West corner needs to be smaller than the DEM-North-East.')
        if self.dem_south_west[1] >= self.dem_north_east[1]:
            raise ValueError('The lat of the DEM-South-West corner needs to be smaller than the DEM-North-East.')

        if self.dem_south_west[0] < self.south_west[0]:
            raise ValueError('The lon of the DEM-South-West corner needs to be larger or equal to the South-West lon.')
        if self.dem_south_west[1] < self.south_west[1]:
            raise ValueError('The lat of the DEM-South-West corner needs to be larger or equal to the South-West lat.')

        if self.dem_north_east[0] > self.north_east[0]:
            raise ValueError('The lon of the DEM-North-East corner needs to be smaller or equal to the North-East lon.')
        if self.dem_north_east[1] > self.north_east[1]:
            raise ValueError('The lat of the DEM-North-East corner needs to be smaller or equal to the North-East lat.')

        if (self.dem_north_east[0] - self.dem_south_west[0]) < self.lon_dem_grid_size:
            raise ValueError('The DEM longitude grid size must be smaller than the diff between the corners.')
        if (self.dem_north_east[1] - self.dem_south_west[1]) < self.lat_dem_grid_size:
            raise ValueError('The DEM latitude grid size must be smaller than the diff between the corners.')

        # check carrier sail area
        if self.carrier_definition:
            sail_area = shg.Polygon(self.carrier_definition.sail_area)
            if not sail_area.is_valid or not sail_area.is_simple:
                raise ValueError('The carrier sail area is not a valid polygon.')
            loiter_centre = shg.Point(self.carrier_definition.loiter_centre)
            if not loiter_centre.within(sail_area):
                raise ValueError('The loiter centre needs to be within the sail area.')
            if not sail_area.within(scenario_border):
                raise ValueError('The carrier sail area must be within the scenario boundary.')

        # check ships stuff
        if self.ships_definition:
            self.ships_definition.validate()
            if self.ships_definition.ship_network_parameters.sail_area_offshore:
                sail_area = shg.Polygon(self.ships_definition.ship_network_parameters.sail_area_offshore)
                if not sail_area.is_valid or not sail_area.is_simple:
                    raise ValueError('The ships sail area for offshore is not a valid polygon.')
                if not sail_area.within(scenario_border):
                    raise ValueError('The ships sail area for offshore must be within the scenario boundary.')
            if self.ships_definition.ship_network_parameters.sail_area_coastal:
                sail_area = shg.Polygon(self.ships_definition.ship_network_parameters.sail_area_coastal)
                if not sail_area.is_valid or not sail_area.is_simple:
                    raise ValueError('The ships sail area for coastal is not a valid polygon.')
                if not sail_area.within(scenario_border):
                    raise ValueError('The ships sail area for coastal must be within the scenario boundary.')

        # check towed network
        if self.towed_network:
            if not ns.is_circular_directed_graph(self.towed_network):
                raise ValueError('The towed network must be a directed circular graph.')

    def adapt_scenario_to_hostile_environment(self) -> None:
        """If a scenario is run in a hostile environment, then we want the shot down assets not to be spawned again."""
        if self.ships_definition:
            self.ships_definition.min_ships = 0
            self.ships_definition.min_coasters = 0
        self.helis_min = 0
        self.drones_min = 0

    def build_path_and_file_name(self, scenario_file_type: str) -> str:
        """The path to the scenario files plus the name of the scenario plus the file type.
        scenario_file_type is e.g. FILE_PICKLE_DEM
        """
        return osp.join(self.scenario_path, self.load_name + scenario_file_type)

    @property
    def centre_lon_lat(self) -> list[float]:
        lon = self.north_east[0] - (self.north_east[0] - self.south_west[0]) / 2
        lat = self.north_east[1] - (self.north_east[1] - self.south_west[1]) / 2
        return [lon, lat]

    @property
    def centre_position(self) -> g.Position:
        lon_lat = self.centre_lon_lat
        return g.Position(lon_lat[0], lon_lat[1], -500)  # make sure alt is out of reach even in Death Sea region

    def add_static_targets(self, static_targets: list[StaticTarget]) -> None:
        """Add one or several static (not moving) targets to the scenario."""
        self.static_targets = static_targets

    def add_automats(self, automats: list[AutomatTarget]) -> None:
        """Add one or several automatically flying shooting fighter aircraft as a list of AutomatTargets."""
        self.automats = automats

    def add_helicopters(self, min_number: int, initial_number: int, network: nx.Graph,
                        assets: list[mpt.MPTargetAsset]) -> None:
        """Add a set of (non_shooting) helicopters, which navigate randomly from node to node in the given network.
        * min_number: how many helicopters shall there be at any time. If fewer helicopters are available,
                      then Hunter will re-spawn additional helicopters
        * initial_number: how many helicopter to start with. Helicopters are added at the beginning add one by one
                         according to parameter polling_freq in the scenario definition.
        * network: a closed non-directional graph of WayPoints.
        * assets: list of MPTargetAssets, which will be randomly assigned as actual targets
        """
        self.helis_min = min_number
        self.helis_initial = initial_number
        self.helis_network = network
        self.heli_assets = assets

    def add_drones(self, min_number: int, initial_number: int, network: nx.Graph,
                   assets: list[mpt.MPTargetAsset]) -> None:
        """Same as helicopters."""
        self.drones_min = min_number
        self.drones_initial = initial_number
        self.drones_network = network
        self.drones_assets = assets

    def add_general_aviation(self, min_number: int, initial_number: int, network: nx.Graph,
                             assets: list[mpt.MPTargetAsset]) -> None:
        """Same as helicopters."""
        self.ga_min = min_number
        self.ga_initial = initial_number
        self.ga_network = network
        for asset in assets:
            if asset.civilian is False:
                raise ValueError('Cannot add a non-civilian asset to general aviation')
        self.ga_assets = assets

    def add_ships(self, ships: ShipsDefinition) -> None:
        """Add offshore and coastal ships using a definition."""
        self.ships_definition = ships

    def add_carrier(self, carrier: CarrierDefinition) -> None:
        """Add one (there can only be one aircraft carrier)."""
        self.carrier_definition = carrier

    def add_tanker(self, tanker: TankerDefinition) -> None:
        """Add one (there can only be one) air-to-air tanker."""
        self.tanker_definition = tanker

    def add_awacs(self, awacs: AwacsDefinition) -> None:
        """Add one (there can only be one) AWACS."""
        self.awacs_definition = awacs

    def add_targets_with_trips(self, trip_targets: list[mpt.MPTargetTrips]) -> None:
        """Add targets which follow network paths in trips (random or routed based origin/destination as a list).
         Each target has its own network. There is a possibility to create convoys.
        """
        self.trip_targets = trip_targets

    def add_towed_targets(self, towed_number: int, towing_asset: Optional[mpt.MPTargetAsset], towed_dist: int,
                          towed_speed: int, towed_network: Optional[nx.DiGraph]) -> None:
        """Add one or more towed targets (a plane towing a target [e.g. delta or wind sack] at some distance).
        * towed_number: number of towed targets
        * towing_asset: list of MPTargets used to tow the towed targets (aka. tractor). Can be None if towed number is 0
        * towed_dist: the distance between the tractor and the towed targets
        * towed_speed: how fast the tractor and thereby the towed target move
        * towed_network: a closed directional graph of WayPoints. Can be None if no towed target requested
        """
        if towed_number > 0 and (towing_asset is None or towed_network is None):
            raise ValueError('Towing assets or towed network may not be empty if at least one towing is requested')
        self.towed_number = towed_number
        self.towing_asset = towing_asset
        self.towed_dist = towed_dist
        self.towed_speed = towed_speed
        self.towed_network = towed_network

    def stamp_scenario_as_started(self) -> None:
        """Sets the timestamp when the scenario was fully loaded and started.

        Used e.g. for delayed activations.
        """
        self.scenario_started = time.time()

    def next_moving_ship(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        if not self.ships_definition:
            return None
        return self.ships_definition.next_moving_ship(remaining_assets)

    def next_moving_coaster(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        if not self.ships_definition:
            return None
        return self.ships_definition.next_moving_coaster(remaining_assets)

    def next_moving_heli(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in the future."""
        yield_asset = False
        if self.helis_initial > 0:
            self.helis_initial -= 1
            yield_asset = True
        elif remaining_assets < self.helis_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.helis_network.nodes))
            asset = random.choice(self.heli_assets)
            return m.AddMovingTarget(asset, self.helis_network, starting_point)
        return None

    def next_moving_drone(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in the future."""
        yield_asset = False
        if self.drones_initial > 0:
            self.drones_initial -= 1
            yield_asset = True
        elif remaining_assets < self.drones_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.drones_network.nodes))
            asset = random.choice(self.drones_assets)
            return m.AddMovingTarget(asset, self.drones_network, starting_point)
        return None

    def next_moving_ga(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in the future."""
        yield_asset = False
        if self.ga_initial > 0:
            self.ga_initial -= 1
            yield_asset = True
        elif remaining_assets < self.ga_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.ga_network.nodes))
            asset = random.choice(self.ga_assets)
            return m.AddMovingTarget(asset, self.ga_network, starting_point)
        return None

    def next_trip_targets(self) -> Optional[list[mpt.MPTargetTrips]]:
        """Determine which trip targets' activation time has come, so they are run."""
        now = time.time()
        if self.trip_targets:
            activated_trip_targets = list()
            for target in reversed(self.trip_targets):
                if (now - self.scenario_started) > target.activation_delay:
                    activated_trip_targets.append(target)
                    self.trip_targets.remove(target)
            return activated_trip_targets
        return None


def load_scenario(scenario_ident: str, path: str) -> ScenarioContainer:
    """Raises ValueError if it does not exist or validation fails."""
    import importlib.util
    module_name = 'scenario_' + scenario_ident
    spec = importlib.util.spec_from_file_location(module_name, osp.join(path, module_name + '.py'))
    scenario_module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(scenario_module)
    loaded_sc = scenario_module.build_scenario(path)
    loaded_sc.load_name = scenario_ident
    loaded_sc.scenario_path = path
    loaded_sc.validate()
    return loaded_sc


def load_network(file_name: str, path: str) -> nx.Graph:
    full_path = osp.join(path, file_name)
    with open(full_path, 'rb') as file_pickle:
        network = pickle.load(file_pickle)
    logger.info("Loaded network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                         nx.number_of_edges(network)))
    return network


# Assets are randomly chosen. If they appear more often, then the probability gets higher

default_helis_list = [mpt.red_ka_50, mpt.red_mi_8, mpt.red_mi_24]
default_drones_list = [mpt.blue_mq_9]

default_ga_list = [mpt.ga_c172p, mpt.ga_dhc6, mpt.ga_ec130b4, mpt.ga_b737ng, mpt.ga_a320neo]
