# SPDX-FileCopyrightText: (C) 2020 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Targets using a real FlightGear instance to be represented over MP."""
import time
import multiprocessing as mp
from typing import Optional, override

import hunter.events as ev
import hunter.geometry as g
import hunter.logger_config as lc
import hunter.messages as m
from hunter.mp_targets import DEATH_TIME_UNKNOWN, DEATH_TIME_VEHICLE, MPTargetType
import hunter.fg_instance_utils as fgiu
from hunter.fg_httpd_io import FGHttpd
from hunter.fg_telnet_io import FlightGear
from hunter.messages import HealthType
import hunter.mqtt_io as mqi
import hunter.utils as u

logger = lc.get_logger()


IS_ALIVE_FREQ = 2  # seconds


class FGInstanceRunner(mp.Process, mqi.WorkerExitHandler):
    """Runs an FG instance in a separate process and controls the communication"""
    __slots__ = ('mqtt_client', 'index', 'mp_server_host', 'session_id', 'target_type', 'fg_httpd', 'fg_telnet',
                 'callsign', 'crash_as_carrier',
                 'cmd_args', 'destroyed_timestamp', 'death_time', 'last_heartbeat_sent',
                 'instance_process')

    def __init__(self, index: int, mp_server_host: str, session_id: str, callsign: str,
                 target_type: MPTargetType, crash_as_carrier: bool, cmd_args: list[str], death_time: int,
                 is_automat: bool) -> None:
        mp.Process.__init__(self, name=callsign)
        mqi.WorkerExitHandler.__init__(self)
        self.index = index
        self.mp_server_host = mp_server_host
        self.session_id = session_id
        self.target_type = target_type
        self.fg_httpd = None  # is faster for property get/set and more convenient
        self.fg_telnet = None  # provides possibility to run Nasal
        self.callsign = callsign  # to display in logging
        self.crash_as_carrier = crash_as_carrier
        self.cmd_args = cmd_args
        self.destroyed_timestamp = 0  # if > 0 then point in time when self.crashed()
        self.death_time = death_time  # how long it takes from being destroyed until being dead and get removed
        self.start_timestamp = time.time()
        self.mqtt_client: Optional[mqi.TargetMQTTClient] = None
        self.last_heartbeat_sent = 0
        self.instance_process: Optional[mp.Process] = None  # set in start_fg_instance_process, which is called in run
        self._is_automat = is_automat

    def _start_fg_instance_process(self, cmd_args: list[str], callsign: str) -> None:
        logger.info('Starting process for FGMP connected asset %s', callsign)
        self.instance_process = mp.Process(target=fgiu.run_instance, args=(cmd_args,))
        self.instance_process.start()
        time.sleep(fgiu.WAIT_FG_READY)  # wait for FG to have loaded and be ready to accept a telnet connection

    def run(self) -> None:
        self.mqtt_client = mqi.TargetMQTTClient(self, self.callsign, True)
        # noinspection PyBroadException
        try:
            self._start_fg_instance_process(self.cmd_args, self.callsign)
            self._initialise_fg_interfaces()

            # Automat makes it own connection to MP through Nasal - thing to do compared to MARunner
            # And we let automat decide on frame rate throttling
            if not self._is_automat:
                self._throttle_framerate(fgiu.FRAME_RATE_THROTTLE)
                self._initialise_multiplayer()

            while True:
                if self._current_health_handling(self._check_health()):
                    return
                if self._check_exit_worker_and_heartbeat():
                    return
        except Exception:
            # Handle if there is an exception in the run method, which cannot be handled."""
            logger.exception('Some error occurred in FGMPConnected %s. Removing the target.', self.callsign)
            try:
                self._tear_fg_down()
            except ConnectionRefusedError:
                logger.exception('Cannot take FG instance %s down - probably already down. Nothing else to do.',
                                  self.callsign)
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.error, 'unknown crash')
            self._clean_before_stop_of_run()
            return  # go out of run()

    def _current_health_handling(self, health: m.HealthType) -> bool:
        """Return True is the run loop should be exited."""
        raise NotImplementedError

    def _initialise_fg_interfaces(self) -> None:
        self._initialise_http_interface()
        self._initialise_telnet_interface()
        time.sleep(5.)

    def _initialise_http_interface(self) -> None:
        http_port = u.PORT_HTTPD_BASIS + self.index
        logger.info('Initializing FG interfaces for %s on HTTP port %i', self.callsign, http_port)
        self.fg_httpd = FGHttpd(http_port, fgiu.HOST_FGFS)

    def _initialise_telnet_interface(self) -> None:
        telnet_port = u.PORT_TELNET_BASIS + self.index
        logger.info('Initializing FG interfaces for %s on Telnet port %i', self.callsign, telnet_port)
        self.fg_telnet = FlightGear(fgiu.HOST_FGFS, telnet_port)
        time.sleep(10.0)  # give it a bit of time
        # Wait five seconds for simulator to settle down
        num_errors = 0
        while True:
            try:
                if self.fg_telnet['/sim/time/elapsed-sec'] > 5:
                    break
                logger.debug('Elapsed time %d', self.fg_telnet['/sim/time/elapsed-sec'])
            except IndexError:
                num_errors += 1
                time.sleep(num_errors)

    def _initialise_multiplayer(self) -> None:
        """Connect to MP and tell the world that we are ready to accept commands."""
        port_in = u.PORT_MP_BASIS_CLIENT_WORKER + self.index
        nasal = 'fgcommand("multiplayer-connect", props.Node.new({"servername": "' + self.mp_server_host
        nasal += '", "rxport": {}, "txport": {}'.format(port_in, u.PORT_MP_BASIS)
        nasal += '}));'
        logger.info('Nasal for multi_player %s: %s', self.callsign, nasal)
        self.fg_telnet.run_nasal(nasal)
        logger.info('Connected to MP on %s', self.mp_server_host)

    def _throttle_framerate(self, frame_rate: int):
        # not needed because not existing self.fg_httpd.set_single_property('/sim/gui/frame-rate-throttled', 1)
        self.fg_httpd.set_single_property('/sim/frame-rate-throttle-hz', frame_rate)
        logger.info('Throttled frame rate for %s to %i', self.callsign, frame_rate)

    def _tear_fg_down(self) -> None:
        logger.info('Tearing down a FGMPConnected resource. Callsign = %s', self.callsign)
        # noinspection PyBroadException
        try:
            self.fg_httpd.simulator_exit()
        except Exception:
            logger.exception('Unable to request exit for %s', self.callsign)
            if self.instance_process is not None:
                self.instance_process.kill()

    def _crashed(self) -> bool:
        if self.crash_as_carrier:
            return self.fg_httpd.sunk()
        return self.fg_httpd.crashed()

    def _check_health(self) -> HealthType:
        """Checks the health of this FG instance"""
        if self.exit_requested:  # no point in checking if we are exiting in a bit
            logger.info('**** exit requested - check health')
            return HealthType.fit_for_fight
        if not self._is_ready_for_network_queries():
            return HealthType.fit_for_fight
        return self._check_subclass_health()

    def _is_ready_for_network_queries(self) -> bool:
        return time.time() - self.start_timestamp > fgiu.WAIT_CHECK_HEALTH

    def _check_subclass_health(self) -> HealthType:
        raise NotImplementedError('Needs to be overridden in sub-class')

    def _check_exit_worker_and_heartbeat(self) -> bool:
        """Return True is the run loop should be exited."""
        now = time.time()
        if self.exit_requested and self._is_ready_for_network_queries():
            logger.info('worker exiting')
            self._tear_fg_down()
            self._clean_before_stop_of_run()
            return True
        if now - self.last_heartbeat_sent > mqi.HEARTBEAT_SENDER_FREQ:
            self.mqtt_client.send_heartbeat(self.session_id)
            self.last_heartbeat_sent = now
        time.sleep(IS_ALIVE_FREQ)
        return False

    def _clean_before_stop_of_run(self) -> None:
        if self.mqtt_client:
            self.mqtt_client.stop_and_disconnect()


MISSILE_RELOAD_TIME = 240  # seconds
MISSILE_RELOAD_SAM_TIME = 480  # seconds


class AutomatAsset(FGInstanceRunner):
    """Leto's automat"""
    __slots__ = 'number_of_lives_left'

    def __init__(self, index: int, mp_server_host: str, session_id: str, callsign: str,
                 icao: str, number_of_lives: int, props_string: str,
                 show_world: bool) -> None:
        FGInstanceRunner.__init__(self, index, mp_server_host, session_id,
                                  callsign, MPTargetType.plane, True,
                                  fgiu.make_command_line_automat(index, callsign, icao, props_string, show_world),
                                  DEATH_TIME_UNKNOWN, True)
        self.number_of_lives_left = 9999 if number_of_lives == 0 else number_of_lives

    @override
    def _check_subclass_health(self) -> HealthType:
        # noinspection PyBroadException
        try:
            if self.fg_httpd.get_float_property('/finish') < 0.9:  # cf. mig28.nas resetMP -> 1 if re-spawning
                return HealthType.fit_for_fight
            else:
                return HealthType.destroyed
        except Exception:
            logger.exception('Problem in check_health for automat - will continue and return True')
        return HealthType.fit_for_fight

    @override
    def _current_health_handling(self, health: m.HealthType) -> bool:
        if health is m.HealthType.destroyed:
            logger.info('Health of automat %s is DESTROYED based on is_alive', self.callsign)
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.damaged_destroyed)
            self.number_of_lives_left -= 1
            if self.number_of_lives_left <= 0:
                self._tear_fg_down()
                logger.info('Automat %s has used all its lives. Tearing down.', self.callsign)
                self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.damaged_dead)
                self._clean_before_stop_of_run()
                return True
            else:
                time.sleep(30)  # wait an extra amount of time until re-spawned
        return False


class MissileAsset(FGInstanceRunner):
    """A shooting OPRF admin asset"""
    __slots__ = ('aero', 'start_reloading', 'readiness_confirmed', 'reload_time', 'cmd_args')

    def __init__(self, index: int, mp_server_host: str, session_id: str,
                 callsign: str, target_type: MPTargetType,
                 aero: str, position: g.Position, heading: float, death_time: int, show_world: bool) -> None:
        FGInstanceRunner.__init__(self, index, mp_server_host, session_id,
                                  callsign, target_type, True,
                                  fgiu.make_command_line_sam(index,
                                                             position.lon, position.lat, position.alt_m,
                                                             heading,
                                                             aero, callsign, show_world, True),
                                  death_time, False)
        self.aero = aero  # type name (e.g. 's-300', whereas target_type is 'vehicle')
        self.start_reloading = 0
        self.readiness_confirmed = None  # first time good to go after startup checked?
        self.reload_time = MISSILE_RELOAD_TIME

    @override
    def _current_health_handling(self, health: m.HealthType) -> bool:
        if health is m.HealthType.dead:
            logger.info('Health of missile asset %s is DEAD based on check_health', self.callsign)
            self._tear_fg_down()
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.damage_dead)
            self._clean_before_stop_of_run()
            return True
        elif health is m.HealthType.destroyed:
            logger.info('Health of missile asset %s is DESTROYED based on check_health', self.callsign)
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.damaged_destroyed)
        return False

    def set_enemies(self) -> None:
        """Make sure not to shoot at OPFOR callsigns."""
        self.fg_httpd.set_single_property('/enemies/opfor-switch', '1')
        logger.info('Set enemies to non-OPFOR')

    @override
    def _check_subclass_health(self) -> HealthType:
        logger.info('@@ - calling _check_subclass_health')
        # noinspection PyBroadException
        try:
            now = time.time()
            self._process_other_state()
            if self.destroyed_timestamp > 0:
                if (now - self.destroyed_timestamp) > self.death_time:
                    logger.info('Missile asset of type %s has passed its death time', self.aero)
                    return HealthType.dead
            elif self._crashed():
                logger.info('Missile asset of type %s has crashed', self.aero)
                self.destroyed_timestamp = now
                return HealthType.destroyed
            else:
                if self.readiness_confirmed:
                    if self._is_out_of_missiles():
                        if self.start_reloading == 0:  # we just discovered it
                            self.start_reloading = time.time()
                            self._change_radar_active(False)
                            logger.info('%s out of missiles: reload in seconds = %i', self.callsign, self.reload_time)
                        elif time.time() > self.start_reloading + self.reload_time:
                            self._reload_missiles()
                            time.sleep(1)
                            self._change_radar_active(True)
                            self.start_reloading = 0
                            logger.info('%s reloaded missiles', self.callsign)
                else:
                    self._check_readiness()

        except Exception:
            logger.exception('Problem in check_health for missile asset - will continue and return True')
        return HealthType.fit_for_fight

    def _process_other_state(self) -> None:
        """Can be overridden in subclasses to do additional processing of state in the FGFS instance.

        Called in is_alive()"""
        pass

    def _reload_missiles(self) -> None:
        self.fg_telnet.run_nasal('fire_control.reload()')

    def _is_out_of_missiles(self) -> bool:
        remaining = self.fg_httpd.get_float_property('/sam/missiles')
        return remaining < 1.

    def _check_readiness(self) -> None:
        if self.readiness_confirmed is None:
            self.set_enemies()
            self._change_radar_active(False)
            self.readiness_confirmed = False
            return

        remaining = self.fg_httpd.get_float_property('/sam/timeleft')
        if remaining < 1.:
            self._change_radar_active(True)
            self.readiness_confirmed = True

    def _change_radar_active(self, is_active: bool) -> None:
        """Toggle the radar active/passive.
        Currently, logic is commented out, because in Nasal/fire-control.nas the variable radarOn gets
        set all the time in iteration and thereby overrides stuff.
        """
        # value = 0 if is_active else 1  # value 1 means silent, 0 means active (yes, so it is for OPRF)
        # self.fg_httpd.set_single_property('/sim/multiplay/generic/int[2]', value)
        pass


class SurfaceAirMissile(MissileAsset):
    """A static and shooting SAM utilizing the OPRF admin assets"""
    def __init__(self, index: int, mp_server_host: str, session_id: str, callsign: str, aero: str,
                 position: g.Position, heading: float, show_world: bool) -> None:
        super().__init__(index, mp_server_host, session_id, callsign,
                         MPTargetType.vehicle, aero, position, heading,
                         DEATH_TIME_VEHICLE, show_world)
        self.reload_time = MISSILE_RELOAD_SAM_TIME
