# SPDX-FileCopyrightText: (C) 2024, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from enum import IntEnum, unique
import json
from typing import Any, Optional

import avro.schema
import avro.io
import io

import hunter.messages as m
import hunter.utils as u

CALLSIGN_ALL = '__all__'

PROP_CALLSIGN = 'callsign'
PROP_HEALTH = 'health'  # the value, not the name
PROP_CLIENT_ID = 'client_id'  # used for MTTQ client ids, often the same as callsign

PROP_ID = 'id'
PROP_SESSION_ID = 'session_id'
PROP_TYPE = 'type'

TYPE_HEARTBEAT = 'heartbeat'
TYPE_WORKER_EXIT = 'worker_exit'
TYPE_TARGET_KILLED = 'target_killed'
TYPE_CHAT_SEND = 'chat_send'
TYPE_MISSILE_EVENT = 'missile_event'

TYPE_RECEIVED_FGMS_DATA = 'received_fgms_data'
PROP_FGMS_DATA = 'fgms_data'

PROP_MESSAGE = 'message'
PROP_KILLED_TYPE = 'killed_type'  # int

TYPE_HUNTER_SESSION = 'HunterSession'

# is a str due to pubsub attributes limitations. Must be in sync with SESSION_ID in web_ui/ongoing_session.py
PROP_SCENARIO_IDENT = 'scenario_ident'
PROP_SCENARIO_NAME = 'scenario_name'
PROP_SCENARIO_DESCRIPTION = 'scenario_description'
PROP_SCENARIO_ICAO = 'scenario_icao'
PROP_SCENARIO_WEST_LON = 'scenario_west_lon'
PROP_SCENARIO_SOUTH_LAT = 'scenario_south_lat'
PROP_SCENARIO_EAST_LON = 'scenario_east_lon'
PROP_SCENARIO_NORTH_LAT = 'scenario_north_lat'
PROP_STARTED_BY = 'started_by'
PROP_STARTED_DT = 'started_dt'  # timestamp
PROP_STOPPED_DT = 'stopped_dt'
PROP_MP_SERVER_HOST = 'mp_server_host'
PROP_IDENTIFIER = 'identifier'
PROP_GCI = 'gci'
PROP_HELI_SHOOT = 'heli_shoot'
PROP_DRONE_SHOOT = 'drone_shoot'
PROP_SHIP_SHOOT = 'ship_shoot'
PROP_SHILKA_SHOOT = 'shilka_shoot'
PROP_SAM_SHOOT = 'sam_shoot'
PROP_AWACS = 'awacs'
PROP_TANKER = 'tanker'
PROP_CARRIER = 'carrier'
PROP_OVERRIDDEN = 'overridden'
PROP_NO_LIST = 'no_list'

TYPE_DAMAGE_RESULT = 'damage_result'
PROP_SHOOTING = 'shooting'
PROP_DAMAGE_TIME = 'damage_time'
PROP_WEAPON_TYPE = 'weapon_type'
PROP_WEAPON_NAME = 'weapon_name'
PROP_ATTACKER = 'attacker'  # MP callsign
PROP_ATTACKER_KIND = 'attacker_kind'
PROP_TARGET = 'target'  # MP callsign
PROP_TARGET_KIND = 'target_kind'
PROP_CIVILIAN = 'is_civilian'
PROP_FG_TARGET = 'is_fg_target'  # bool

TYPE_ATTACKER_EVENT = 'attacker_event'
PROP_EVENT_TIME = 'event_time'
PROP_HAS_OPFOR_IFF = 'has_opfor_iff'
PROP_TRIGGER = 'trigger'

PROP_UNIQUE_ID = 'unique_id'
PROP_TELEMETRY_STRING = 'telemetry_string'

TYPE_POSITION_UPDATES_BATCH = 'position_updates_batch'
PROP_POSITIONS = 'positions'
PROP_KIND = 'kind'  # the type of vehicle, aircraft, building etc.
PROP_POSITION_LON = 'lon'
PROP_POSITION_LAT = 'lat'
PROP_POSITION_ALT_M = 'alt_m'
PROP_HEADING = 'heading'
PROP_PITCH = 'pitch'
PROP_SPEED = 'speed'


def create_heartbeat_json(session_id: str, client_id: str) -> str:
    payload: dict[str, Any] = dict()
    payload[PROP_TYPE] = TYPE_HEARTBEAT
    payload[PROP_SESSION_ID] = session_id
    payload[PROP_CLIENT_ID] = client_id
    return json.dumps(payload)

def create_worker_exit_json(session_id: str, callsign: str) -> str:
    payload: dict[str, Any] = dict()
    payload[PROP_TYPE] = TYPE_WORKER_EXIT
    payload[PROP_SESSION_ID] = session_id
    payload[PROP_CALLSIGN] = callsign
    return json.dumps(payload)

def extract_callsign_from_worker_exit_json(json_str: str) -> str:
    payload = json.loads(json_str)
    return payload[PROP_CALLSIGN]

SCHEMA_RECEIVED_FGMS_DATA = """
{
 "type": "record",
 "name": "%s",
 "fields": [
     {"name": "%s", "type": "string"},
     {"name": "%s",  "type": "bytes"}
 ]
}
""" % (TYPE_RECEIVED_FGMS_DATA, PROP_SESSION_ID, PROP_FGMS_DATA)
schema_received_fgms_data = avro.schema.parse(SCHEMA_RECEIVED_FGMS_DATA)


def create_received_fgms_avro(session_id: str, fgms_data) -> bytes:
    bytes_writer = io.BytesIO()
    datum_writer = avro.io.DatumWriter(schema_received_fgms_data)
    binary_encoder = avro.io.BinaryEncoder(bytes_writer)
    datum_writer.write({PROP_SESSION_ID: session_id, PROP_FGMS_DATA: fgms_data}, binary_encoder)
    return bytes_writer.getvalue()


def extract_from_received_fgms_avro(message: bytes) -> bytes:
    bytes_reader = io.BytesIO(message)
    binary_decoder = avro.io.BinaryDecoder(bytes_reader)
    datum_reader = avro.io.DatumReader(schema_received_fgms_data, schema_received_fgms_data)
    data = datum_reader.read(binary_decoder)
    return data[PROP_FGMS_DATA]


@unique
class KilledType(IntEnum):
    damaged_dead = 10  # is no more - target has been removed
    damaged_destroyed = 11  # target still is in MP, but totally damaged
    error = 20  # e.g. fg_instance crashed or permanent socket problem with MP


def map_killed_type_from_value(int_value: int) -> KilledType:
    for type_ in KilledType:
        if type_.value == int_value:
            return type_


KILLED_NOTES_MAX_LENGTH = 50


def create_target_killed_json(session_id: str, callsign: str, killed_type: KilledType, notes: Optional[str],
                              fg_instance: bool) -> str:
    payload: dict[str, Any] = dict()
    payload[PROP_TYPE] = TYPE_TARGET_KILLED
    payload[PROP_SESSION_ID] = session_id
    payload[PROP_CALLSIGN] = callsign
    payload[PROP_KILLED_TYPE] = killed_type.value
    payload[PROP_MESSAGE] = notes
    payload[PROP_FG_TARGET] = fg_instance
    return json.dumps(payload)


def extraxt_from_target_killed_json(json_str: str) -> tuple[str, KilledType, Optional[str], bool]:
    payload = json.loads(json_str)
    return payload[PROP_CALLSIGN], map_killed_type_from_value(payload[PROP_KILLED_TYPE]), payload[PROP_MESSAGE], payload[PROP_FG_TARGET]


def create_chat_send_json(session_id: str, callsign: str, message: str) -> str:
    payload: dict[str, Any] = dict()
    payload[PROP_TYPE] = TYPE_CHAT_SEND
    payload[PROP_SESSION_ID] = session_id
    payload[PROP_CALLSIGN] = callsign  # who should send the message
    payload[PROP_MESSAGE] = message
    return json.dumps(payload)


def extract_from_chat_send_json(json_str: str) -> tuple[str, str]:
    payload = json.loads(json_str)
    return payload[PROP_CALLSIGN], payload[PROP_MESSAGE]

def create_missile_event_json(session_id: str, missile_event: m.MissileEvent) -> str:
    missile_event_dict = {
        # no ID -> will be created automatically
        PROP_TYPE: TYPE_MISSILE_EVENT,
        PROP_SESSION_ID: session_id,
        PROP_EVENT_TIME: missile_event.event_time.isoformat(),
        PROP_UNIQUE_ID: missile_event.unique_id,
        PROP_ATTACKER: missile_event.shooter_callsign,
        PROP_TARGET: missile_event.target_callsign,
        PROP_TELEMETRY_STRING: missile_event.telemetry_string,
    }
    return json.dumps(missile_event_dict)

def extract_from_missile_event_json(json_str) -> dict[str, Any]:
    return json.loads(json_str)

def create_damage_result_json(session_id: str, damage_result: m.DamageResult) -> str:
    damage_item_dict = {
        # no ID -> will be created automatically
        PROP_TYPE: TYPE_DAMAGE_RESULT,
        PROP_SESSION_ID: session_id,
        PROP_DAMAGE_TIME: damage_result.damage_time.isoformat(),
        PROP_HEALTH: damage_result.health.value,
        PROP_WEAPON_TYPE: damage_result.weapon_type.value,
        PROP_WEAPON_NAME: damage_result.weapon_name,
        PROP_ATTACKER: damage_result.attacker,
        PROP_ATTACKER_KIND: damage_result.attacker_kind,
        PROP_TARGET: damage_result.target,
        PROP_TARGET_KIND: damage_result.target_kind,
        PROP_CIVILIAN: damage_result.civilian,
        PROP_FG_TARGET: damage_result.fg_target,
        PROP_SHOOTING: damage_result.shooting
    }
    return json.dumps(damage_item_dict)

def extract_from_damage_result_json(json_str) -> dict[str, Any]:
    return json.loads(json_str)

def damage_result_object_from_dict(damage_result_dict: dict[str, Any]) -> m.DamageResult:
    health_type = m.map_health_type(damage_result_dict[PROP_HEALTH], True)
    weapon_type = m.map_weapon_type(damage_result_dict[PROP_WEAPON_TYPE])
    damage = m.DamageResult(health_type, weapon_type, damage_result_dict[PROP_WEAPON_NAME],
                            damage_result_dict[PROP_ATTACKER], damage_result_dict[PROP_ATTACKER_KIND],
                            damage_result_dict[PROP_TARGET], damage_result_dict[PROP_TARGET_KIND], damage_result_dict[PROP_CIVILIAN],
                            damage_result_dict[PROP_FG_TARGET], damage_result_dict[PROP_SHOOTING])
    damage.damage_time = u.parse_isostring_timestamp(damage_result_dict[PROP_DAMAGE_TIME])
    return damage

def create_attacker_event_json(session_id: str, attacker_event: m.AttackerEvent) -> str:
    attacker_event_dict = {
        # no ID -> will be created automatically
        PROP_TYPE: TYPE_ATTACKER_EVENT,
        PROP_SESSION_ID: session_id,
        PROP_EVENT_TIME: attacker_event.event_time.isoformat(),
        PROP_CALLSIGN: attacker_event.callsign,
        PROP_KIND: attacker_event.fgfs_model,
        PROP_HAS_OPFOR_IFF: attacker_event.has_opfor_iff,
        PROP_TRIGGER: attacker_event.trigger.name,
    }
    return json.dumps(attacker_event_dict)

def extract_from_attacker_event_json(json_str) -> dict[str, Any]:
    return json.loads(json_str)

def attacker_event_object_from_dict(attacker_event_dict: dict[str, Any]) -> m.AttackerEvent:
    trigger = m.map_attacker_event_trigger(attacker_event_dict[PROP_TRIGGER])
    event = m.AttackerEvent(attacker_event_dict[PROP_CALLSIGN], attacker_event_dict[PROP_KIND],
                            attacker_event_dict[PROP_HAS_OPFOR_IFF], trigger)
    event.event_time = u.parse_isostring_timestamp(attacker_event_dict[PROP_EVENT_TIME])
    return event

def create_position_updates_batch_json(session_id: str, position_updates_batch: m.PositionUpdatesBatch) -> str:
    position_records: list[dict[str, Any]] = list()
    for pos_update in position_updates_batch.position_updates:
        record = {
            PROP_CALLSIGN: pos_update.callsign,
            PROP_KIND: pos_update.kind,
            PROP_HEALTH: pos_update.health.value,
            PROP_POSITION_LON: pos_update.position.lon,
            PROP_POSITION_LAT: pos_update.position.lat,
            PROP_POSITION_ALT_M: pos_update.position.alt_m,
            PROP_HEADING: pos_update.heading,
            PROP_PITCH: pos_update.pitch,
            PROP_SPEED: pos_update.speed,
        }
        position_records.append(record)
    item = {
        PROP_ID: TYPE_POSITION_UPDATES_BATCH + '_1',
        PROP_TYPE: TYPE_POSITION_UPDATES_BATCH,
        PROP_SESSION_ID: session_id,
        PROP_POSITIONS: position_records
    }
    return json.dumps(item)

def extract_from_position_updates_batch_json(json_str) -> dict[str, Any]:
    return json.loads(json_str)
