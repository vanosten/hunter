# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Interaction with FlightGear MultiplayerServer over the Multiplayer Protocol.

Cf. https://wiki.flightgear.org/Multiplayer_protocol
Cf. https://wiki.flightgear.org/FlightGear_Multiplayer_Server
Cf. https://sourceforge.net/p/flightgear/flightgear/ci/next/tree/src/MultiPlayer/multiplaymgr.cxx#l129

NB: most of the processing is relying on code in ATC-pie.

In property tree: look at /sim/multiplay/generic in sending aircraft for sent values
Property /sim/multiplay/last-xmit-packet-len contains the size (in bytes) of the last transmitted packet.
"""
from dataclasses import dataclass
from datetime import datetime, timezone
import math
import multiprocessing as mp
import socket
import time
from typing import Any, Optional, override, Union

import paho.mqtt.client as mqtt

import hunter.atc_pie.fgms as f
import hunter.damage as d
import hunter.emesary_notifications as en
import hunter.events as ev
import hunter.geometry as g
import hunter.logger_config as lc
import hunter.messages as m
import hunter.mp_targets as wa
import hunter.mp_carrier as mpc
import hunter.mqtt_io as mqi
import hunter.utils as u

logger = lc.get_logger()


FGMS_MAX_PACKET_SIZE = 2048  # the actual size limit is 1200 for MP, but limits should be power of 2


def create_header_packet(mp_target: wa.MPTarget, packet_type: int,
                         content_data: f.PacketData) -> f.PacketData:
    packet = f.PacketData()
    # Header first (32 bytes)
    packet.append_bytes(b'FGFS')  # Magic
    packet.append_bytes(bytes.fromhex('00 01 00 01'))  # Protocol version 1.1
    packet.pack_int(packet_type)  # Msg type: position message
    packet.pack_int(32 + len(content_data))  # Length of data
    packet.pack_int(mp_target.radar_range)  # in nm
    packet.append_bytes(bytes(4))  # ReplyPort: ignored
    packet.pack_padded_string(8, mp_target.callsign)
    # Append the data
    packet.append_packed(content_data)
    return packet


def create_position_message_static(mp_target: wa.MPTarget,
                                   properties: Optional[dict[int, Any]],
                                   main_start: datetime) -> f.PacketData:
    pos_tuple = g.WGS84_geodetic_to_cartesian_metres(mp_target.position,
                                                   g.metres_to_feet(mp_target.position.alt_m + mp_target.z_offset))
    ori_tuple = f.FG_orientation_XYZ(mp_target.position, mp_target.orientation.hdg,
                                     mp_target.orientation.pitch, mp_target.orientation.roll)
    vel_tuple = (0., 0., 0.)
    la_tuple = (0, 0, 0)
    return _create_position_message_packed(mp_target.aircraft_model, pos_tuple, ori_tuple, vel_tuple, la_tuple,
                                           properties, main_start)


def _create_position_message_mover(mp_target: Union[wa.MPTargetMoving, wa.MPTargetTrips, mpc.MPCarrier],
                                   properties: Optional[dict[int, Any]],
                                   speed: float,
                                   acceleration: float,
                                   main_start: datetime) -> f.PacketData:
    """NB: speed and acceleration is not affected by send_freq"""
    pos_tuple = g.WGS84_geodetic_to_cartesian_metres(mp_target.position,
                                                   g.metres_to_feet(mp_target.position.alt_m + mp_target.z_offset))
    ori_tuple = f.FG_orientation_XYZ(mp_target.position, mp_target.orientation.hdg,
                                     mp_target.orientation.pitch, mp_target.orientation.roll)
    x_vel = speed * math.cos(math.radians(mp_target.orientation.pitch))
    y_vel = 0.
    z_vel = speed * math.sin(math.radians(mp_target.orientation.pitch))
    vel_tuple = (x_vel, y_vel, z_vel)
    la_tuple = (acceleration, 0, 0)
    return _create_position_message_packed(mp_target.aircraft_model, pos_tuple, ori_tuple, vel_tuple, la_tuple,
                                           properties, main_start)


def _create_position_message_moving(mp_target: wa.MPTargetMoving,
                                    properties: Optional[dict[int, Any]],
                                    main_start: datetime) -> f.PacketData:
    return _create_position_message_mover(mp_target, properties, mp_target.cruise_speed, mp_target.current_acceleration,
                                          main_start)


def _create_position_message_trips(mp_target: wa.MPTargetTrips,
                                   properties: Optional[dict[int, Any]],
                                   main_start: datetime) -> f.PacketData:
    return _create_position_message_mover(mp_target, properties,
                                          mp_target.current_speed_step * mp_target.SPEED_PER_STEP,
                                          mp_target.current_acceleration, main_start)


def _create_position_message_carrier(mp_target: mpc.MPCarrier,
                                     properties: Optional[dict[int, Any]],
                                     main_start: datetime) -> f.PacketData:
    return _create_position_message_mover(mp_target, properties,
                                          mp_target.cruise_speed,
                                          mp_target.current_acceleration, main_start)


def _create_position_message_packed(model: str, position: tuple[float, float, float],
                                    orientation: tuple[float, float, float],
                                    velocity: tuple[float, float, float],
                                    linear_acceleration: tuple[float, float, float],
                                    properties: Optional[dict[int, Any]],
                                    main_start: datetime) -> f.PacketData:
    """
    pos_coords: EarthCoords
    pos_amsl should be geometric alt in feet
    """
    buf = f.PacketData()
    buf.pack_padded_string(96, model)  # Aircraft model
    buf.pack_double((datetime.now(timezone.utc) - main_start).total_seconds())  # Time
    buf.pack_double(.1)  # Lag # WARNING zero value can make some FG clients crash (see SF tickets 1927 and 1942)
    buf.pack_double(position[0])  # PosX
    buf.pack_double(position[1])  # PosY
    buf.pack_double(position[2])  # PosZ
    buf.pack_float(orientation[0])  # OriX
    buf.pack_float(orientation[1])  # OriY
    buf.pack_float(orientation[2])  # OriZ
    buf.pack_float(velocity[0])  # VelX
    buf.pack_float(velocity[1])  # VelY
    buf.pack_float(velocity[2])  # VelZ
    buf.pack_float(0)  # AV1
    buf.pack_float(0)  # AV2
    buf.pack_float(0)  # AV3
    buf.pack_float(linear_acceleration[0])  # LA1
    buf.pack_float(linear_acceleration[1])  # LA2
    buf.pack_float(linear_acceleration[2])  # LA3
    buf.pack_float(0)  # AA1
    buf.pack_float(0)  # AA2
    buf.pack_float(0)  # AA3
    buf.append_bytes(f.v2_magic_padding)
    # finished position data; now packing properties
    buf.pack_property(f.FGMS_v2_virtual_prop, f.v2_version_prop_value)
    if properties:
        sorted_keys = sorted(properties.keys())
        for prop_code in sorted_keys:
            prop_value = properties[prop_code]
            try:
                if prop_code == u.MP_PROP_OPFOR_DATALINK or (
                        f.EMESARYBRIDGE_BASE <= prop_code <= f.EMESARYBRIDGE_BASE + 29):
                    my_buf = f.PacketData()
                    right_value = len(prop_value)
                    my_buf.pack_int(prop_code << 16 | right_value)
                    my_buf.append_bytes(prop_value)
                    buf.append_packed(my_buf)
                else:
                    buf.pack_property(prop_code, prop_value)
            except ValueError as err:
                logger.exception('Error packing property: %s' % err)
    return buf


class SocketUDPError(Exception):
    """Indicates something went wrong when sending or receiving stuff through sockets"""
    def __init__(self, message: str) -> None:
        self.message = message


class FGMSMQTTClient(mqi.TargetMQTTClient):
    __slots__ = '_fgms_participant'

    def __init__(self, fgms_participant: 'FGMSParticipant') -> None:
        self._fgms_participant = fgms_participant
        super().__init__(self._fgms_participant, self._fgms_participant.callsign, False)
        self.message_callback_add(mqi.CHAT_SEND_TOPIC, self._callback_on_chat_send)

    @property
    def _mp_target(self) -> wa.MPTarget:
        return self._fgms_participant.mp_target

    def send_received_fgms_data(self, session_id: str, fgms_data) -> None:
        avro_payload = ev.create_received_fgms_avro(session_id, fgms_data)
        _ = self.publish(topic=mqi.RECEIVED_FGMS_DATA_TOPIC, payload=avro_payload,
                         qos=mqi.RECEIVED_FGMS_DATA_SENDER_QOS)

    def send_missile_event(self, session_id: str, missile_event: m.MissileEvent) -> None:
        json_payload = ev.create_missile_event_json(session_id, missile_event)
        _ = self.publish(topic=mqi.MISSILE_EVENT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.MISSILE_EVENT_SENDER_QOS)

    def send_damage_result(self, session_id: str, damage_result: m.DamageResult) -> None:
        json_payload = ev.create_damage_result_json(session_id, damage_result)
        _ = self.publish(topic=mqi.DAMAGE_RESULT_TOPIC, payload=mqi.encode_message_payload(json_payload),
                         qos=mqi.DAMAGE_RESULT_SENDER_QOS)

    def _callback_on_chat_send(self, client, userdata, message: mqtt.MQTTMessage):
        callsign, chat_message = ev.extract_from_chat_send_json(mqi.decode_message_payload(message.payload))
        if self._callsign == callsign:
            self._mp_target.chat_message_handler.add_message(chat_message)

    @override
    def _subscribe_to_topics_on_connect(self) -> None:
        super()._subscribe_to_topics_on_connect()
        self.subscribe(mqi.CHAT_SEND_TOPIC, options=mqtt.SubscribeOptions(qos=mqi.CHAT_SEND_RECEIVER_QOS))

class FGMSParticipant(mqi.WorkerExitHandler):
    """Sends and receives data for a specific target or the controller on a socket connection with FGMS.

    Heavily inspired by ATC-pie.ext.fgms.FGMShandshaker"""
    __slots__ = ('mqtt_client', 'broker', 'mp_target', 'receiver', 'mp_server_host', 'mp_server_port', 'mp_client_port',
                 'session_id', 'last_position_update', 'socket', 'error_cycles', 'last_heartbeat_sent')

    def __init__(self, mp_target: wa.MPTarget, mp_server_host: str, mp_server_port: int, mp_client_port: int,
                 session_id: str) -> None:
        super().__init__()
        self.mp_target: wa.MPTarget = mp_target
        self.mqtt_client: FGMSMQTTClient | None = None  # initialized in main_loop()
        self.broker = None  # is set when Participant is added to e.g. FGMSThreadedBroker
        self.receiver = True if self.mp_target.target_type is wa.MPTargetType.receiver else False
        self.mp_server_host: str = mp_server_host
        self.mp_server_port: int = mp_server_port
        self.mp_client_port: int = mp_client_port
        self.session_id: str = session_id
        self.last_position_update: float = 0

        # used when running
        self.socket: Optional[socket.socket] = None

        # error handling state
        self.error_cycles: int = 0  # number of error cycles in the run() method since last success
        self.last_heartbeat_sent: float = 0

    @property
    def callsign(self) -> str:
        return self.mp_target.callsign

    def _connect_to_socket(self) -> None:
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            logger.info('Sender %s connected to %s.', self.mp_target.callsign,
                         '{}:{}'.format(self.mp_server_host, self.mp_server_port))
            if self.mp_target.target_type is wa.MPTargetType.receiver:
                self.socket.bind(('', self.mp_client_port))
                self.socket.settimeout(wa.MP_RECEIVER_TIMEOUT)
                logger.info('Receiver %s connected to %s.', self.mp_target.callsign,
                             '{}:{}'.format(self.mp_server_host, self.mp_client_port))
        except OSError as e:
            error_text = 'Connection error for {}'.format(self.mp_target.callsign)
            logger.exception(error_text, exc_info=e)
            raise SocketUDPError(error_text)

# noinspection PyBroadException
    def main_loop(self):
        """Infinitely loops to send data to FGMS and receive data on socket channel."""
        self.mqtt_client = FGMSMQTTClient(self)
        main_start = datetime.now(timezone.utc)
        while True:
            try:
                loop_start_time = time.time()
                if self.socket is None:  # first try to re-establish a connection
                    self._connect_to_socket()
                if self.socket:  # if we have a connection
                    # check for messages from session manager:
                    prev_health = self.mp_target.health
                    attacker_positions = dict()  # we could get multiple - and want only the last per attacker
                    armament_in_flight_notifications = list()
                    while True:
                        message = self.broker.fetch_incoming_message()
                        if message is None:
                            break
                        logger.debug('%s got message from session: %s', self.mp_target.callsign, message)
                        if isinstance(message, m.HitNotification):
                            health, w_type, name = d.process_damage_notification(message.notification, self.mp_target)
                            if m.DamageResult.check_send_result(health):
                                damage_result = m.DamageResult(health, w_type, name,
                                                               message.attacker_callsign,
                                                               message.attacker_aircraft,
                                                               message.target,
                                                               self.mp_target.name,
                                                               self.mp_target.civilian,
                                                               False,
                                                               self.mp_target.is_really_shooting)
                                self.mqtt_client.send_damage_result(self.session_id, damage_result)
                        # there might be several AttackerPositions arrived since last time
                        # we just overwrite as long as we get new ones - but only if newer in case the
                        # messages arrive out of order
                        elif isinstance(message, m.AttackerPosition):
                            if message.callsign not in attacker_positions:
                                attacker_positions[message.callsign] = message
                            else:
                                if attacker_positions[message.callsign].mp_timestamp < message.mp_timestamp:
                                    attacker_positions[message.callsign] = message
                        elif isinstance(message, en.ArmamentInFlightNotification):
                            armament_in_flight_notifications.append(message)
                        elif isinstance(message, m.DatalinkData):
                            self.mp_target.datalink_data = message.encoded_data
                        elif isinstance(message, m.CarrierPositionRequest) or isinstance(
                                message, m.CarrierCall) or isinstance(message, m.CarrierCommand) and (
                                isinstance(self.mp_target, mpc.MPCarrier)):
                            self.mp_target.process_incoming_chat(message)
                    if self.mp_target.is_shooting:
                        msgs: list[m.MissileEvent] = self.mp_target.shooter.process_attacker_info(attacker_positions,
                                                                                 armament_in_flight_notifications)
                        for msg in msgs:
                            self.mqtt_client.send_missile_event(self.session_id, msg)
                    if self.mp_target.is_dispensing:
                        _ = self.mp_target.dispenser.process_attacker_info(attacker_positions,
                                                                           armament_in_flight_notifications)
                    # periodically send the position to the controller for information
                    if not self.receiver:
                        self._update_position_for_controller(prev_health is not self.mp_target.health)
                    # check health of target
                    if not self.mp_target.is_alive():
                        self._tear_down_with_notification()
                        return
                    # prepare the package
                    props = self.mp_target.prepare_mp_properties()  # also does position/orientation calculations

                    if self.mp_target.is_shooting:
                        self.mp_target.shooter.extend_props_with_shooting_stuff(props)
                    if self.mp_target.is_dispensing:
                        self.mp_target.dispenser.extend_props_with_dispenser_stuff(props)
                    # append
                    if isinstance(self.mp_target, wa.MPTargetMoving):
                        packet_position = _create_position_message_moving(self.mp_target, properties=props,
                                                                          main_start=main_start)
                    elif isinstance(self.mp_target, wa.MPTargetTrips):
                        packet_position = _create_position_message_trips(self.mp_target, properties=props,
                                                                         main_start=main_start)
                    elif isinstance(self.mp_target, mpc.MPCarrier):
                        packet_position = _create_position_message_carrier(self.mp_target, properties=props,
                                                                           main_start=main_start)
                    else:
                        packet_position = create_position_message_static(self.mp_target, properties=props,
                                                                         main_start=main_start)
                    packet_header = create_header_packet(self.mp_target, f.position_message_type_code,
                                                         packet_position)
                    # send to FGMS
                    try:
                        logger.debug('%s sending FGMS data', self.mp_target.callsign)
                        self.socket.sendto(packet_header.all_data(), (self.mp_server_host, self.mp_server_port))
                    except OSError as e:
                        error_text = '{} could not send FGMS packet to server.'.format(self.mp_target.callsign)
                        logger.exception(error_text, exc_info=e)
                        raise SocketUDPError(error_text)
                    if not self.receiver:
                        # we want the loop to be as close to send_freq as possible
                        remaining_time = self.mp_target.send_freq - (time.time() - loop_start_time)
                        if remaining_time > 0:
                            time.sleep(remaining_time)

                    else:  # receiver is the only one which receives from FGMS
                        # Error handling: cf. https://stackoverflow.com/questions/16745409 ...
                        # /what-does-pythons-socket-recv-return-for-non-blocking-sockets-if-no-data-is-r
                        start_time = time.time()
                        try:
                            passed_time = 0
                            while passed_time < self.mp_target.send_freq:
                                logger.debug('%s receiving FGMS data', self.mp_target.callsign)
                                received_packet = self.socket.recv(FGMS_MAX_PACKET_SIZE)
                                if received_packet is None:
                                    raise SocketUDPError('Receiver received None')
                                elif len(received_packet) == 0:  # indicates graceful close on sender side
                                    raise SocketUDPError('Receiver received zero length data - sender hung up')
                                else:
                                    self.mqtt_client.send_received_fgms_data(self.session_id, received_packet)
                                passed_time = time.time() - start_time
                        except socket.timeout:
                            raise SocketUDPError('Receiver raised timeout exception')
                        except socket.error as e:
                            error_text = 'Receiver got an unexpected exception'
                            logger.exception(error_text, exc_info=e)
                            raise SocketUDPError(error_text)
                self.error_cycles = 0  # it was a success, so reset
            except SocketUDPError as e:
                try:
                    my_type = 'receiver' if self.receiver else 'mp_target'
                    error_text = 'Exception in run() of {} - error cycle {}'.format(my_type, self.error_cycles)
                    logger.exception(error_text, exc_info=e)
                    # use exponential backoff
                    if self.error_cycles < wa.MP_RECEIVER_MAX_BACKOFF_CYCLES:
                        self.error_cycles += 1
                        backoff_seconds = math.pow(2, self.error_cycles)
                        logger.info('waiting %i seconds before retry', backoff_seconds)
                        time.sleep(backoff_seconds)
                        if self.socket:
                            try:
                                self.socket.close()
                            except IOError:
                                pass  # nothing to do
                            self.socket = None
                    else:
                        logger.error('Taking FGMS for %s down due to too long time in error state', my_type)
                        self._tear_down_with_notification('too long time in error state')  # most probably a retry will not recover
                        return
                except Exception as e:
                    logger.exception('FGMSSender has exception within run() SocketUDPError handling', exc_info=e)
                    self._tear_down_with_notification('exception within run() SocketUDPError handling')  # most probably a retry will not recover
                    return

            except Exception as e:
                logger.exception('FGMSSender has unknown exception within run()', exc_info=e)
                self._tear_down_with_notification('unknown exception within run()')  # most probably a retry will not recover
                return

            if self.exit_requested:
                logger.info('worker exiting: %s', self.callsign)
                self._tear_down()
                return
            now = time.time()
            if now - self.last_heartbeat_sent > mqi.HEARTBEAT_SENDER_FREQ:
                self.mqtt_client.send_heartbeat(self.session_id)
                self.last_heartbeat_sent = now

    def _update_position_for_controller(self, health_has_changed: bool) -> None:
        """Position update back to controller as a message, such that controller logic has position info etc.
        For static targets this is only relevant the first time or it the health has changed."""
        current_time = time.time()
        if isinstance(self.mp_target, wa.MPTargetMoving) or isinstance(self.mp_target, wa.MPTargetTrips) or \
                isinstance(self.mp_target, mpc.MPCarrier):
            if current_time - self.last_position_update > u.POS_UPDATE_FREQ_MOVING_TARGET:
                self.last_position_update = current_time
                speed = 0.
                if isinstance(self.mp_target, wa.MPTargetMoving) or isinstance(self.mp_target, mpc.MPCarrier):
                    speed = self.mp_target.cruise_speed
                elif isinstance(self.mp_target, wa.MPTargetTrips):
                    speed = self.mp_target.current_speed_step * wa.MPTargetTrips.SPEED_PER_STEP
                self.broker.send_message_out(m.PositionUpdate(self.mp_target.callsign, self.mp_target.name,
                                                              self.mp_target.health, self.mp_target.position,
                                                              self.mp_target.orientation.hdg,
                                                              self.mp_target.orientation.pitch,
                                                              speed))

        else:  # static target
            if current_time - self.last_position_update > u.POS_UPDATE_FREQ_STATIC_TARGET:
                first_time = self.last_position_update == 0
                self.last_position_update = current_time
                if health_has_changed or first_time:
                    self.broker.send_message_out(m.PositionUpdate(self.mp_target.callsign, self.mp_target.name,
                                                                  self.mp_target.health, self.mp_target.position,
                                                                  self.mp_target.orientation.hdg,
                                                                  self.mp_target.orientation.pitch,))

    def _tear_down_with_notification(self, error_situation: Optional[str] = None):
        if error_situation:
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.error, error_situation)
        else:
            self.mqtt_client.send_target_killed(self.session_id, ev.KilledType.damaged_dead)
        self._tear_down()

    def _tear_down(self) -> None:
        """Closes the socket and thereby makes sure that FGMS interaction closes OK.
        Remember that the statement in the run() method leading to this call must have a "return".
        Otherwise, the run() method does not stop and the FGMS continuous."""
        if self.socket:
            try:
                self.socket.close()
                logger.info('Torn down %s', self.mp_target.callsign)
            except OSError as e:
                logger.exception('Unable to properly shutdown socket for %s: %s', self.mp_target.callsign, e)
            self.socket = None
        self.mqtt_client.stop_and_disconnect()


class FGMSMultiProcessingParticipant(mp.Process):
    slots = ('participant', 'log_config')

    def __init__(self, participant: FGMSParticipant, broker: u.ParticipantMessageInterface,
                 log_config: lc.LogConfig) -> None:
        super().__init__(name=participant.mp_target.callsign)

        self.participant = participant
        self.participant.broker = broker
        self.log_config = log_config

    @override
    def run(self) -> None:
        lc.configure_logging(self.log_config)
        global logger
        logger = lc.get_logger()

        self.participant.main_loop()


class FGMSMultiProcessingBroker(u.ParticipantMessageInterface):
    slots = ('mp_participant', 'ctrl_conn', 'target_conn')

    def __init__(self, participant: FGMSParticipant, log_config: lc.LogConfig) -> None:
        self.ctrl_conn, self.target_conn = mp.Pipe(duplex=True)
        self.mp_participant = FGMSMultiProcessingParticipant(participant, self, log_config)
        self.mp_participant.start()

    def post_message(self, message) -> None:
        logger.debug('Controller posting a new message to a participant (mp-mode): %s', message)
        self.target_conn.send(message)

    def send_message_out(self, message) -> None:
        logger.debug('Participant sending a new message out to the Controller (mp-mode): %s', message)
        self.ctrl_conn.send(message)

    def receive_message(self) -> Optional[Any]:
        message = None
        while self.target_conn.poll():
            message = self.target_conn.recv()
            logger.debug('Controller is receiving a message from participant (mp-mode): %s', message)
            break  # we want at most one message at a time
        return message

    def fetch_incoming_message(self) -> Optional[Any]:
        message = None
        while self.ctrl_conn.poll():
            message = self.ctrl_conn.recv()
            logger.debug('Participant is receiving a message from the Controller (mp-mode): %s', message)
            break  # we want at most one message at a time
        return message


@dataclass(frozen=True)
class FGMSUnpackedData:
    callsign: str
    time_stamp: float
    position: g.Position
    fgfs_model: str
    encoded_notifications: dict[int, Any]
    radar_lock: Optional[str]
    radar_on: bool
    chaff_on: float
    flares_on: float
    iff_hash: Optional[str]
    chat: Optional[str]


def decode_fgms_data_packet(packet, mp_targets: dict[str, Any]) -> Optional[FGMSUnpackedData]:
    """Decodes an attackers FGMS data packet and pulls out information needed by Hunter.

    Directly copied and adapted from function decode_FGMS_position_message() in ATCPie.ext.fgms
    """
    buf = f.PacketData(packet)
    # Header
    got_magic = buf.pop_bytes(4)
    if got_magic != b'FGFS':
        raise ValueError('Bad magic byte sequence: %s' % got_magic)
    got_protocol_version = buf.pop_bytes(4)
    if got_protocol_version != bytes.fromhex('00 01 00 01'):
        raise ValueError('Bad protocol version: %s' % got_protocol_version)
    got_msg_id = buf.pop_bytes(4)
    if got_msg_id != bytes.fromhex('00 00 00 07'):
        raise ValueError('Bad message type: %s' % got_msg_id)
    _ = buf.unpack_int()  # MsgLen
    _ = buf.unpack_int()  # RequestedRangeNm
    _ = buf.unpack_int()  # ReplyPort
    got_callsign = buf.unpack_padded_string(8)

    # if the callsign is not interesting, then return early
    if got_callsign is None or len(got_callsign) == 0:
        return None
    if got_callsign in mp_targets:
        return None

    # Done header; now obligatory data...
    got_model = buf.unpack_padded_string(96)
    got_time = buf.unpack_double()
    _ = buf.unpack_double()  # lag
    got_pos_x = buf.unpack_double()
    got_pos_y = buf.unpack_double()
    got_pos_z = buf.unpack_double()
    current_pos = g.cartesian_metres_to_WGS84_geodetic(got_pos_x, got_pos_y, got_pos_z)
    _ = buf.pop_bytes(15 * 4)  # Ori, Vel, AV, LA, AA triplets
    _ = buf.pop_bytes(4)  # pad INFO: packet is a v2 packet if got_padding == v2_magic_padding

    fgfs_model = wa.extract_fgfs_model_base_name(got_model)

    # Done obligatory data; now property data...
    got_emesary_notifications = dict()  # int, bytes value

    last_prop_ok = -1
    pairs = ['---']  # FIXME RICK
    errors = list()  # FIXME RICK
    is_protocol_version_2 = False

    radar_lock = None
    iff_hash = None
    radar_on = False

    flares_on = 0.
    chaff_on = 0.

    chat_msg = None

    while len(buf) >= 4:
        try:
            prop_code, prop_value, prop_type = buf.unpack_property(is_protocol_version_2)
            if prop_type == f.FgmsType.V1_String and not (
                    f.EMESARYBRIDGE_BASE <= prop_code <= f.EMESARYBRIDGE_BASE + 29):
                if not isinstance(prop_value, str):
                    prop_value = prop_value.decode(encoding=f.fgms_string_encoding)
            if got_callsign == 'MP_029X':  # FIXME RICK
                pairs.append(str(prop_code) + '->' + str(prop_value))
            if prop_code == f.FGMS_v2_virtual_prop:
                if prop_value >= f.v2_version_prop_value:
                    is_protocol_version_2 = True
            elif prop_code == u.MP_PROP_CHAT:
                chat_msg = prop_value
            elif prop_code == u.MP_PROP_OPRF_RADAR_LOCK:
                radar_lock = prop_value
            elif prop_code == u.MP_PROP_OPFOR_IFF_HASH:
                iff_hash = prop_value
            elif prop_code == u.MP_PROP_OPRF_RADAR:
                radar_on = True if prop_value == 0 else False
            elif prop_code == u.MP_PROP_OPRF_FLARES:
                flares_on = prop_value
            elif prop_code == u.MP_PROP_OPRF_CHAFFS:
                chaff_on = prop_value
            if prop_code == -1:  # boolean array
                # prop_value is a dict of prop_code / bool
                pass
            elif f.EMESARYBRIDGE_BASE <= prop_code <= f.EMESARYBRIDGE_BASE + 29:
                got_emesary_notifications[prop_code] = prop_value
                # print('Emesary bridge base: ', prop_code, 'value: ', prop_value)

            last_prop_ok = prop_code
        except ValueError as err:
            if last_prop_ok < 0:
                aux = 'none read before it'
            else:
                aux = 'last OK had code %d' % last_prop_ok
            if got_callsign == 'MP_029X':  # FIXME RICK
                errors.append(str(err))
            # print('Problem reading property from %s (%s): %s' % (got_callsign, aux, err))
    # if got_callsign == 'MP_029X':  # FIXME RICK
    #    print('\n'.join(pairs))
    #    print('current_pos', current_pos)
    #    print('x, y, z', got_pos_x, got_pos_y, got_pos_z)
    #    print('\n'.join(errors))

    return FGMSUnpackedData(got_callsign, got_time, current_pos, fgfs_model,
                            got_emesary_notifications,
                            radar_lock, radar_on, chaff_on, flares_on, iff_hash, chat_msg)
