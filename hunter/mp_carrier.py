# SPDX-FileCopyrightText: (C) 2024 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
import math
from enum import Enum
import time
from typing import Annotated, Any, override

import hunter.geometry as g
import hunter.iff_datalink as ifd
import hunter.logger_config as lc
import hunter.messages as m
import hunter.mp_targets as mpt
import hunter.utils as u

logger = lc.get_logger()

Knots = Annotated[float, 'nautical mile per hour']
Secs = Annotated[float, 'time seconds']
Metres = Annotated[float, 'length in metres']
MetresSec = Annotated[float, 'metres per second']
Degs = Annotated[float, 'degrees (360 for a circle)']

ENCODED_BOOL_BYTE_FALSE = 'A'
ENCODED_BOOL_BYTE_TRUE = 'B'
ENCODED_BOOL_DOUBLE_FALSE = 'AAAAAAAAAA'
ENCODED_BOOL_DOUBLE_TRUE = 'AAAABAAAAA'
MP_ANNOUNCE_INTERVAL = 10.0  # from MPCarrier/commander.nas
SEND_TIME = 0.6  # from fgdata/Nasal/mp_broadcast.nas


class CarrierType(Enum):
    clemenceau = '026Y'
    eisenhower = '030Y'
    nimitz = '029Y'
    vinson = '029X'


def make_tacan_callsign(carrier_type: CarrierType) -> str:
    return 'MP_' + str(carrier_type.value)


class MPCarrier(mpt.MPTarget):
    """ An MP Carrier driven by MP properties only without an FG instance.
    Some of the variables etc. are aligned with naming in the FG MP carrier files.

    The sail area is an enclosed list of lon/lat tuples, where the carrier can move.
    Given that a carrier can sail 20 or more miles per hour, the area should be big.

    In vinson-set.xml -> common.xml keybindings ->
    "CarrierControl" is mapped to Aircraft/MPCarrier/Systems/commander.nas
    CarrierControl.toggleAIControl() -> -> MP_CONTROL must be true such that key commands can work
    Left: CarrierControl.incRudder(-0.5) -> TGT_SPEED_KN (not rudder)
    PgDown: CarrierControl.incSpeed(-0.5) -> TGT_HEADING_DEGS

    See also: https://wiki.hoggitworld.com/view/Carrier_Air_Operations

    """
    SPEED_LOITER: Knots = 5  # guess
    SPEED_OPS_MIN: Knots = 5  # guess for minimal speed ahead during launch/recovery to keep the boat's rudder effective
    SPEED_OPS_MAX: Knots = 30  # according to Wikipedia and in FG MP Carriers
    SPEED_NAVIGATE: Knots = 15  # a bit fast, but not too much
    SPEED_EVADE: Knots = 20  # want to be a bit fast to get in a good position soon
    SPEED_TURNING: Knots = 25  # do it fast such that it does not take so much time

    WARNING_FLIGHT_OPS_TIME: Secs = 400  # seconds before evading manoeuvre is initiated (distance depends on speed)
    MIN_FLIGHT_OPS_TIME: Secs = 600  # minimum secs of free sailing to even initiate flight operations (incl. turn time)
    EVADE_DIST: Metres = 5000  # distance from intersecting sail area when evading manoeuvre is initiated
    LOITER_DIST: Metres = 10000

    TURNING_RADIUS_FT = 12000  # default from FG MP Carrier

    SAILING_CHECKS = 60

    def __init__(self, carrier_type: CarrierType,
                 sail_area: list[tuple[float, float]], loiter_centre: tuple[float, float]) -> None:
        self.carrier_type = carrier_type
        name = 'org.flightgear.fgaddon.trunk.{}'.format(carrier_type.name)
        model = 'Aircraft/MPCarrier/Models/mp-{}.xml'.format(carrier_type.name)
        super().__init__(mpt.MPTargetType.ship, name,
                         model,
                         mpt.MAX_HITPOINTS_NOT_VULNERABLE, mpt.FULL_DAMAGE_DIST_CARRIER)
        self.sail_area = sail_area  # List of lon_lat tuples. Last tuple != first tuple
        self.loiter_centre = loiter_centre  # where the carrier starts from and returns to if nothing happens
        self.tgt_heading: Degs = 180.  # current heading as per self.orientation
        self.tgt_speed: Knots = self.SPEED_LOITER
        self.cruise_speed: MetresSec = g.knots_to_ms(self.tgt_speed)  # m/s
        self.current_acceleration = 0.  # m/s2 - name must be this due to fgms_io.py._create_position_message_mover(...)
        self.turn_radius = self.TURNING_RADIUS_FT  # ft
        self.position = g.Position(loiter_centre[0], loiter_centre[1], 0.)
        self.orientation = g.Orientation(self.tgt_heading)
        self.next_position = self.position
        self.callsign = make_tacan_callsign(carrier_type)

        self.tgt_wind_over_deck_speed: Knots = 25.  # set by CarrierCommand
        self.tgt_navigation_course: Degs = 10.  # from captain command

        self.course_command = m.CarrierCourseType.loiter  # the command the ship is following

        self.last_sailing_check: Secs = 0

        self.avg_wind_deg: Degs = 90.  # default, can be overridden by CarrierCommand
        self.avg_wind_speed: Knots = 5.  # default, can be overridden by CarrierCommand

        # encoding of state transmitted
        # variable names are from commander.nas
        self.c_deck_elev = False  # "controls/elevators";
        self.c_deck_lights = True  # "controls/lighting/deck-lights";
        self.c_flood_lights = False  # "controls/lighting/flood-lights-red-norm";
        # not used due to self.course_command: self.c_turn_to_launch_hdg - "controls/turn-to-launch-hdg";
        # not used due to self.course_command: self.c_turn_to_recvry_hdg - "controls/turn-to-recovery-hdg";
        # not used due to self.course_command: self.c_turn_to_base_co - "controls/turn-to-base-course";
        self.c_wave_off_lights = False  # "controls/flols/wave-off-lights";
        self.last_announce = None
        self.last_send = None
        self.message_id = None

    @override  # overrides method in mpt.MPTarget
    def prepare_mp_properties(self) -> dict[int, Any]:
        """
        vinson-set.xml:
            <multiplay>
            <chat_display>1</chat_display>
            <generic>
            <string n="2" alias="/sim/messages/approach"/>
            <float n="3" alias="/instrumentation/radar/target-brg"/>
            <float n="4" alias="/instrumentation/radar/target-lat"/>
            <float n="5" alias="/instrumentation/radar/target-lon"/>
            <float n="6" alias="/instrumentation/radar/target-alt"/>
            <int n="0" alias="/ai/submodels/launch-master"/>
            </generic>
            </multiplay>

        common.xml:
            <multiplay>
            <generic>
            <string n="0" type="string"></string><!-- MP events -->
            <float n="0" alias="controls/tgt-heading-degs"/>
            <float n="1" alias="controls/tgt-speed-kts"/>
            <float n="2" alias="controls/turn-radius-ft"/>
            </generic>
            </multiplay>
        """
        # we are not doing on purpose, because we want to have full control - the carrier has its own stuff
        # and is not OPRF "compliant"
        # props = super().prepare_mp_properties()
        now = time.time()
        if (now - self.last_sailing_check) > self.SAILING_CHECKS:
            self.last_sailing_check = now
            self._check_position_and_course()

        self.update_position_variables()  # needs to be called explicitly if we do not call super().prepare_...
        props = dict()
        # Carrier stuff MPCarrier.nas, common.xml, vinson-set.xml
        props[u.MP_PROP_SURFACE_POS_RUDDER] = self.tgt_heading
        props[u.MP_PROP_SURFACE_POS_FLAP] = g.ms_to_knots(self.tgt_speed)
        props[u.MP_PROP_GENERIC_FLOAT_BASE] = self.orientation.hdg  # <float n="0" alias="controls/tgt-heading-degs"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 1] = g.ms_to_knots(self.cruise_speed)  # alias="controls/tgt-speed-kts"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 2] = self.turn_radius  # <float n="2" alias="controls/turn-radius-ft"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 3] = 0  # <float n="3" alias="/instrumentation/radar/target-brg"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 4] = 0  # <float n="4" alias="/instrumentation/radar/target-lat"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 5] = 0  # <float n="5" alias="/instrumentation/radar/target-lon"/>
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 6] = 0  # <float n="6" alias="/instrumentation/radar/target-alt"/>
        props[u.MP_PROP_GENERIC_STRING_BASE] = self._prepare_mp_broadcast_message()
        props[u.MP_PROP_GENERIC_STRING_BASE + 2] = ''  # <string n="2" alias="/sim/messages/approach"/>
        props[u.MP_PROP_GENERIC_INT_BASE] = 0  # <int n="0" alias="/ai/submodels/launch-master"/>

        # More stuff found when looking through vinson-set.xml and comparing with what MP carrier FG instance emits
        props[1006] = 1  # controls/armament/station[0]/jettison-all

        # More stuff only found in what MP carrier FG instance emits
        props[1200] = ''  # environment/wildfire/data
        props[1201] = 0  # environment/contrail
        props[1400] = ''  # scenery/events
        props[11990] = 1  # sim/multiplay/mp-clock-mode

        # more generic stuff
        self.chat_message_handler.get_chat_message(props)
        if self.iff_code:
            props[u.MP_PROP_OPFOR_IFF_HASH] = ifd.calculate_iff_hash(self.callsign, self.iff_code)

        return props

    @override  # overrides method in mpt.MPTarget
    def update_position_variables(self) -> None:
        """Calculating acceleration and turning etc.

        When turning then not self.tgt_speed but SPEED_TURNING is used - otherwise turning can take a lot of time,
        e.g. if the speed is slow due to a lot of wind for e.g. RECOVERY. No idea whether this is realistic.

        From https://www.quora.com/During-landing-operations-do-aircraft-carriers-increase-their-speed-in-order-to-keep-
        a-slower-relative-speed-to-landing-aircraft:
        A nuclear carrier needs ca. 1-1.5 min to accelerate from 10 to 20 kts and ca. 3 min from 10 to 30 kts.
        A conventional needs ca. 2.5 to 5 min to accelerate from 10 to 20 kts. and ca. 12.5 min from 10 to 30 kts
        if all 8 boilers are online. Otherwise, much longer.
        """
        self.position = self.next_position

        # check whether we are probably turning to override the tgt_speed - we will recalculate later
        if math.fabs(g.calc_delta_bearing(self.orientation.hdg, self.tgt_heading)) < 5:
            overridden_tgt_speed = self.tgt_speed
        else:
            overridden_tgt_speed = self.SPEED_TURNING

        # speed control - we assume 100 seconds for 10 kn difference -> 0.1 kn per second
        accel_per_freq = g.knots_to_ms(10 / 100 * self.send_freq)
        if math.fabs(self.cruise_speed - g.knots_to_ms(overridden_tgt_speed)) < 2 * accel_per_freq:
            # handle small residual as if it does not exist
            self.cruise_speed = g.knots_to_ms(overridden_tgt_speed)
            self.current_acceleration = 0.
        elif self.cruise_speed > g.knots_to_ms(overridden_tgt_speed):
            self.current_acceleration = -1 * accel_per_freq / self.send_freq
            self.cruise_speed -= accel_per_freq
        else:
            self.current_acceleration = accel_per_freq / self.send_freq
            self.cruise_speed += accel_per_freq

        # bearing control
        # at full speed (30 kn) it would take the carrier ca. 4 seconds per degree at a radius of
        # 12000 ft (= 2 nm).
        deg_per_freq = 360/(2 * g.feet_to_metres(self.turn_radius) * math.pi / (self.cruise_speed * self.send_freq))
        if math.fabs(g.calc_delta_bearing(self.orientation.hdg, self.tgt_heading)) < 2 * deg_per_freq:
            self.orientation.hdg = self.tgt_heading
        elif g.calc_delta_bearing(self.orientation.hdg, self.tgt_heading) > 0:
            self.orientation.hdg = g.normalize_degrees(self.orientation.hdg + deg_per_freq)
        else:
            self.orientation.hdg = g.normalize_degrees(self.orientation.hdg - deg_per_freq)

        # calc new position
        vertical_dist = self.cruise_speed * self.send_freq
        lon_lat = g.calc_destination(self.position.lon, self.position.lat, vertical_dist,
                                     self.orientation.hdg)
        self.next_position = g.Position(lon_lat[0], lon_lat[1], 0.)

    def _prepare_mp_broadcast_message(self) -> str:
        """Create the mp_broadcast message to be sent for carrier control states.
        Cf. MPCarrier/Systems/mp-network.nas ca. line 140 mp_network_init = func (active_participant=0):
        -> Each message is prepended with a letter starting from B through H

        Send sequence etc. are from MPCarrier/Systems/commander.nas ca. line 111 send_events()
        and MPCarrier/systems/mp-network.nas ca. line 26 ff send message wrappers
        """
        do_send = False
        now = time.time()
        message = ''
        if self.last_announce is None or (now >= self.last_announce + MP_ANNOUNCE_INTERVAL):
            self.last_announce = now
            self.last_send = now
            self.message_id = 1
            do_send = True
        elif self.message_id > 7:
            do_send = False
        elif now >= self.last_send + SEND_TIME:
            self.last_send = now
            self.message_id += 1
            do_send = self.message_id <= 7

        if do_send:
            if self.message_id == 1:
                message = chr(65 + self.message_id) + self.encode_bool_as_byte(self.c_deck_elev)
            elif self.message_id == 2:
                message = chr(65 + self.message_id) + self.encode_bool_as_byte(self.c_deck_lights)
            elif self.message_id == 3:
                message = chr(65 + self.message_id) + self.encode_bool_as_double(self.c_flood_lights)
            elif self.message_id == 4:
                state = True if self.course_command is m.CarrierCourseType.launch else False
                message = chr(65 + self.message_id) + self.encode_bool_as_double(state)
            elif self.message_id == 5:
                state = True if self.course_command is m.CarrierCourseType.recovery else False
                message = chr(65 + self.message_id) + self.encode_bool_as_double(state)
            elif self.message_id == 6:
                state = True if self.course_command not in [m.CarrierCourseType.launch,
                                                            m.CarrierCourseType.recovery] else False
                message = chr(65 + self.message_id) + self.encode_bool_as_double(state)
            elif self.message_id == 7:
                message = chr(65 + self.message_id) + self.encode_bool_as_double(self.c_wave_off_lights)
        return message

    @staticmethod
    def encode_bool_as_byte(state: bool) -> str:
        """Directly from observations instead of implementing Binary.encodeByte()."""
        return ENCODED_BOOL_BYTE_TRUE if state else ENCODED_BOOL_BYTE_FALSE

    @staticmethod
    def encode_bool_as_double(state: bool) -> str:
        """Directly from observations instead of implementing Binary.encodeDouble()."""
        return ENCODED_BOOL_DOUBLE_TRUE if state else ENCODED_BOOL_DOUBLE_FALSE

    def process_incoming_chat(self, message) -> None:
        if isinstance(message, m.CarrierCommand):
            if message.course_type:
                if message.course_type is m.CarrierCourseType.navigate:
                    self.tgt_navigation_course = g.normalize_degrees(message.course_deg)
                elif message.course_type in [m.CarrierCourseType.launch, m.CarrierCourseType.recovery]:
                    self.tgt_wind_over_deck_speed = message.deck_wind_speed_kn
                self._change_course_command(message.course_type)
            elif message.deck_lights:
                self._set_deck_lights(message.deck_lights)
            elif message.flood_lights:
                self._set_flood_lights(message.flood_lights)
            elif message.weather_wind_hdg:
                self.avg_wind_deg = g.normalize_degrees(message.weather_wind_hdg)
                self.avg_wind_speed = message.weather_wind_kn
                self._change_course_command(self.course_command, True)
        elif isinstance(message, m.CarrierCall):
            if message.call_type is m.CarrierCallType.inbound:
                msg = ("{}, mother's visibility is your visibility. Wind from {:.0f} at {:.0f}. Expected BRC is {:.0f}."
                       " Report a see me at 10.")
                self.chat_message_handler.add_message(msg.format(message.callsign, self.avg_wind_deg,
                                                                 self.avg_wind_speed, self.tgt_heading))
            elif message.call_type is m.CarrierCallType.see_you_at_10:
                msg = '{}, Tower, Roger. Current BRC is {:.0f} deg.'.format(message.callsign, self.orientation.hdg)
                if math.fabs(self.orientation.hdg - self.tgt_heading) > 2:
                    msg += ' Turning - target BRC is {:.0f} deg.'.format(self.tgt_heading)
                self.chat_message_handler.add_message(msg)
        elif isinstance(message, m.CarrierPositionRequest):
            msg = "{}, Tower, my pos: lon = {:.4f}, lat = {:.4f}."
            msg += " Fly {:.0f} deg for {:.0f} nm. Wind from {:.0f} deg at {:.0f} kn."
            bearing = int(g.calc_bearing_pos(message.caller_pos, self.position))
            dist = int(g.metres_to_nm(g.calc_distance_pos(message.caller_pos, self.position)))
            self.chat_message_handler.add_message(msg.format(message.callsign, self.position.lon,
                                                             self.position.lat, bearing, dist,
                                                             self.avg_wind_deg, self.avg_wind_speed))

    def _create_chat_msg_wind_changed(self) -> str:
        return f'Roger, getting wind from {self.avg_wind_deg:.0f} deg at {self.avg_wind_speed:.0f} kn.'

    def _set_deck_lights(self, state: bool) -> None:
        logger.info('Setting deck lights to on: %s', state)
        self.c_deck_lights = state
        on_off_state = 'ON' if state else 'OFF'
        self.chat_message_handler.add_message(f'Deck lights are set to {on_off_state}')

    def _set_flood_lights(self, state: bool) -> None:
        logger.info('Setting red flood lights to on: %s', state)
        self.c_flood_lights = state
        on_off_state = 'ON' if state else 'OFF'
        self.chat_message_handler.add_message(f'Flood lights are set to {on_off_state}')

    def _change_course_command_flight_ops(self, new_course_command: m.CarrierCourseType, wind_changed: bool):
        if new_course_command is not self.course_command and \
                self.course_command not in [m.CarrierCourseType.recovery, m.CarrierCourseType.launch]:
            # make sure that there is enough time left to do flight operations if we are going into flight ops
            # if we are already in flight ops and just changing between recovery and launch, then only little
            # course corrections etc. -> continue
            # using launch params as approximation
            lack_of_time = self._check_violating_sail_area(self.MIN_FLIGHT_OPS_TIME, self.tgt_speed,
                                                           self.tgt_heading)
            if lack_of_time:
                msg = 'Overriding request for {} flight ops due to running out of time in sail area.'
                msg += ' Keeping course type {}.'
                self.chat_message_handler.add_message(msg.format(new_course_command.name, self.course_command.name))
                return

        self.course_command = new_course_command
        if self.course_command is m.CarrierCourseType.recovery:
            asked_speed, asked_course = self._calc_recovery_tgt_speed_course()
        else:  # launch
            asked_speed, asked_course = self._calc_launch_tgt_speed_course()
        self.tgt_speed = g.ms_to_knots(asked_speed)
        self.tgt_heading = asked_course

        # finally check whether flight ops should be warned
        if self._check_violating_sail_area(self.WARNING_FLIGHT_OPS_TIME, asked_speed, asked_course):
            msg = f'{self.WARNING_FLIGHT_OPS_TIME} seconds or less left for {self.course_command.name}.'
        else:
            msg = 'Course type used is {}: tgt speed {:.1f} and tgt bearing {:.0f}'
            msg = msg.format(self.course_command.name, self.tgt_speed, self.tgt_heading)
        if wind_changed:
            msg = self._create_chat_msg_wind_changed() + ' ' + msg
        self.chat_message_handler.add_message(msg)

    def _change_course_command_other(self, new_course_command: m.CarrierCourseType) -> None:
        if new_course_command is self.course_command:
            return  # nothing to do, we do not care about potential wind changes
        self.course_command = new_course_command
        if self.course_command is m.CarrierCourseType.loiter:
            asked_speed = self.SPEED_LOITER
            asked_course = g.calc_bearing(self.position.lon, self.position.lat,
                                          self.loiter_centre[0], self.loiter_centre[1])
        elif self.course_command is m.CarrierCourseType.evade:
            asked_speed = self.SPEED_EVADE
            asked_course = g.calc_bearing(self.position.lon, self.position.lat,
                                          self.loiter_centre[0], self.loiter_centre[1])
        else:  # m.CarrierCourseType.navigate:
            asked_speed = self.SPEED_NAVIGATE
            asked_course = self.tgt_navigation_course
        self.tgt_speed = asked_speed
        self.tgt_heading = asked_course
        msg = 'Course type used is {}: tgt speed {:.1f} and tgt bearing {:.0f}'
        self.chat_message_handler.add_message(msg.format(self.course_command.name, self.tgt_speed,
                                                         self.tgt_heading))

    def _change_course_command(self, new_course_command: m.CarrierCourseType, wind_changed: bool = False) -> None:
        """Makes sure that the carrier goes where the captain wants - within the sail area limits.
        In base course there are two possibilities: either loiter around the centre point in a rectangle of ca.
        4 nm size and slow speed - or travel back to the loiter point.
        For launch and recovery the course is based on generating a good relative headwind, which for the angled
        recovery deck is different from the sail direction.
        """
        logger.info('Carrier initiating potential course type change from %s to %s',
                    self.course_command.name, new_course_command.name)
        if new_course_command in [m.CarrierCourseType.launch, m.CarrierCourseType.recovery]:
            self._change_course_command_flight_ops(new_course_command, wind_changed)
        elif wind_changed:  # no need to change course if not in operations, just announce that wind has changed
            self.chat_message_handler.add_message(self._create_chat_msg_wind_changed())
        else:
            self._change_course_command_other(new_course_command)

    def _check_violating_sail_area(self, sail_time: float, tgt_speed: Knots, tgt_course: Degs) -> bool:
        """Checks whether carrier would cross sail area boundary with tested for tgt speed and tgt course."""
        transformer = g.make_utm_transformer(self.position.lon, self.position.lat)
        dist = g.knots_to_ms(tgt_speed) * sail_time + self.EVADE_DIST
        dest_point = g.calc_destination(self.position.lon, self.position.lat, dist, tgt_course)
        line = g.local_line_from_lon_lat(self.position.lon, self.position.lat,
                                         dest_point[0], dest_point[1], transformer)
        for idx in range(0, len(self.sail_area)):
            idx_prev = len(self.sail_area) - 1 if idx == 0 else idx - 1
            cross_line = g.local_line_from_lon_lat(self.sail_area[idx_prev][0], self.sail_area[idx_prev][1],
                                                   self.sail_area[idx][0], self.sail_area[idx][1],
                                                   transformer)
            if line.intersects(cross_line):
                return True
        return False

    def _check_position_and_course(self) -> None:
        proposed_course_command = None

        # if we are evading and within x miles from loiter point -> change to loiter because no need to steam fast
        if self.course_command is m.CarrierCourseType.evade:
            dist = g.calc_distance(self.position.lon, self.position.lat, self.loiter_centre[0], self.loiter_centre[1])
            if dist < self.LOITER_DIST:  # just using the same distance
                proposed_course_command = m.CarrierCourseType.loiter
        else:
            # make sure we are not exiting the sail area -> evade
            current_tgt_speed = self.tgt_speed
            current_tgt_deg = self.tgt_heading
            if self._check_violating_sail_area(0., current_tgt_speed, current_tgt_deg):
                proposed_course_command = m.CarrierCourseType.evade

        if proposed_course_command and proposed_course_command is not self.course_command:
            proposed_course_command = self.course_command
            self._change_course_command(proposed_course_command)

    def _limit_operational_speeds(self, requested_speed: Knots) -> MetresSec:
        limited_speed = requested_speed
        if requested_speed < self.SPEED_OPS_MIN:
            limited_speed = self.SPEED_OPS_MIN
        elif requested_speed > self.SPEED_OPS_MAX:
            limited_speed = self.SPEED_OPS_MAX
        return g.knots_to_ms(limited_speed)

    def _calc_launch_tgt_speed_course(self) -> tuple[MetresSec, Degs]:
        if self.avg_wind_speed < 0.1 * self.tgt_wind_over_deck_speed:  # no need to change the course for so little
            calculated_course = self.tgt_heading
            calculated_speed = self._limit_operational_speeds(self.tgt_wind_over_deck_speed)
        else:  # take wind from front
            calculated_course = self.avg_wind_deg
            calculated_speed = self._limit_operational_speeds(self.tgt_wind_over_deck_speed - self.avg_wind_speed)
        return calculated_speed, calculated_course

    def _calc_recovery_tgt_speed_course(self) -> tuple[MetresSec, Degs]:
        """Recovery speed depends on deck angle, wind speed and wind direction.
        If wind is faster than tgt_wind_over_deck, then carrier should stay almost still and have direction =
        wind direction + deck angle
        If wind is very small, then carrier should sail into wind direction.
        In between the difference between tgt_wind_over_deck and the wind speed will tell, how much angle to give
        in addition to the wind direction and how fast the carrier should go

        From https://www.quora.com/During-landing-operations-do-aircraft-carriers-increase-their-speed-in-order-to-keep-
        a-slower-relative-speed-to-landing-aircraft:

        Approximately 30 knots of wind over the deck is common for flight operations. If there is much less than 30 kts
        of natural wind, the carrier will likely speed up to gain more wind. If there is no natural wind, the carrier
        will speed up to 25-30 knots through the water to make all its own wind over the deck. This accomplishes a
        couple of things:
        * Slower relative approach speed making it easier to land aboard.
        * Slower trap speed, reducing wear and tear on the cables, arresting gear, and the aircraft.

        The highest desired speed is normally at the beginning of the cycle, when the heaviest planes are taking off.
        In my era that was normally fully-loaded A-6 Intruders needing 27 knots of wind, 10 degrees to port.
        Once they were gone the Boss would tell us to slow, typically to ~25 knots wind over deck.

        The speed of the carrier is entirely secondary to the relative speed of the wind over the deck (for the
        purposes of recovering aircraft). If there is a nice, 25-knot wind, you might see the carrier barely making
        steerage speed, with a light, trickling wake. The wind will be in that sweet spot, coming right down the angled
        deck (9° offset from the ship’s axis on the Nimitz-class boats). That reduces the burble (that wind that is
        upset from the island and the rest of the ship). When the winds are light, in order to find a suitable
        wind-over-deck (WOD), they’ll pull the rods and set whatever speed the Air Boss calls for. When there is no
        wind, you’ll see a massive wake, and know that the winds are axial and the burble is going to be nasty.
        """
        deck_angle = 8. if self.carrier_type is CarrierType.clemenceau else 9.

        diff_wind = self.tgt_wind_over_deck_speed - self.avg_wind_speed
        if diff_wind <= 0:  # wind is faster than asked for deck wind
            calculated_speed = self._limit_operational_speeds(0.)
            calculated_course = g.normalize_degrees(self.avg_wind_deg + deck_angle)
        else:
            calculated_speed = self._limit_operational_speeds(diff_wind)
            angle = deck_angle * self.avg_wind_speed / (g.ms_to_knots(calculated_speed) + self.avg_wind_speed)
            calculated_course = g.normalize_degrees(self.avg_wind_deg + angle)

        return calculated_speed, calculated_course
