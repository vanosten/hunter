# SPDX-FileCopyrightText: (C) 2023 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
import argparse
import math
import random
import time
from typing import Tuple

import networkx as nx
from networkx.algorithms.shortest_paths.weighted import dijkstra_path
from networkx.algorithms.shortest_paths.astar import astar_path
from networkx.exception import NetworkXNoPath

import hunter.geometry as g
import hunter.logger_config as lc

logger = lc.get_logger()


# To recognize a DiGraph as a circuit
GRAPH_CIRCUIT = 'circuit'
GRAPH_YES = 'yes'


# networkx edge attribute for the weight in Dijkstra shortest path
EDGE_ATTR_COST = 'cost'
MIN_NUMBER_NODES_NETWORKS = 5


POSSIBLE_NODE_COMBINATIONS_2_RECTANGLES = [  # sequence from tuple out of function _envelop_from_graph()
    (0, 1, 0, 1),
    (0, 1, 0, 3),
    (0, 1, 2, 1),
    (0, 1, 2, 3),
    (0, 3, 0, 1),
    (0, 3, 0, 3),
    (0, 3, 2, 1),
    (0, 3, 2, 3),
    (2, 1, 0, 1),
    (2, 1, 0, 3),
    (2, 1, 2, 1),
    (2, 1, 2, 3),
    (2, 3, 0, 1),
    (2, 3, 0, 3),
    (2, 3, 2, 1),
    (2, 3, 2, 3),
]


def _envelope_from_graph_points(sub_graph_nodes, buffer: float) -> Tuple[float, float, float, float]:
    """The smallest rectangle along lon/lat axis, which contains all nodes of a sub-graph."""
    smallest_lon = 999
    largest_lon = -999
    smallest_lat = 999
    largest_lat = -999
    for sub_node in sub_graph_nodes:
        smallest_lon = min(sub_node.lon, smallest_lon)
        largest_lon = max(sub_node.lon, largest_lon)
        smallest_lat = min(sub_node.lat, smallest_lat)
        largest_lat = max(sub_node.lat, largest_lat)
    return smallest_lon - buffer, smallest_lat - buffer, largest_lon + buffer, largest_lat + buffer


def _has_points_in_envelope(envelope: Tuple[float, float, float, float], graph_nodes) -> bool:
    for node in graph_nodes:
        if envelope[0] <= node.lon <= envelope[2] and envelope[1] <= node.lat <= envelope[3]:
            return True
    return False


def _closest_node_to_point(sub_graph_nodes, other_lon: float, other_lat: float):
    closest_dist = 999999
    closest_sub_node = None
    for sub_node in sub_graph_nodes:
        dist = g.calc_distance(sub_node.lon, sub_node.lat, other_lon, other_lat)
        if dist < closest_dist:
            closest_dist = dist
            closest_sub_node = sub_node
    return closest_sub_node


def info_about_network(network: nx.Graph) -> str:
    return 'Nodes {}, edges {}'.format(len(network.nodes), len(network.edges))


def remove_dangling_nodes(network: nx.Graph) -> int:
    """Remove the remaining dangling iteratively (if we remove one edge - then next edge behind might be dangling)."""
    total_removed = 0
    found_dangling = 99
    while found_dangling > 0:
        found_dangling = 0
        for dangling in list(network.nodes):
            neighbors = list(nx.neighbors(network, dangling))
            if len(neighbors) <= 1:
                found_dangling += 1
                total_removed += 1
                network.remove_node(dangling)
    return total_removed


def connect_disjoint_subgraphs(network: nx.Graph, acceptable_link_distance: int) -> None:
    """Connect disjoint sub-graphs if any included in network using envelop as proxy.
    Using while-loop instead of for-loop because when connecting the number of sub-graphs changes
    https://networkx.org/documentation/stable/reference/algorithms/component.html
    https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.components.connected_components.html#networkx.algorithms.components.connected_components
    """
    max_acceptable_link_distance = acceptable_link_distance
    loops = 0
    while nx.number_connected_components(network) > 1:
        sub_graphs = [network.subgraph(c) for c in nx.connected_components(network)]
        # make sure that we do not just expand the first all the time - might make a too big envelope
        chosen_index = random.randint(0, len(sub_graphs) - 1)
        current_graph = sub_graphs[chosen_index]
        closest_dist = 999999
        closest_combination = None
        closest_other = None
        current_envelope = _envelope_from_graph_points(list(current_graph.nodes), 0)
        for i in range(len(sub_graphs)):
            if i == chosen_index:  # may not test against self
                continue
            other_graph = sub_graphs[i]
            other_envelope = _envelope_from_graph_points(list(other_graph.nodes), 0)
            for combination in POSSIBLE_NODE_COMBINATIONS_2_RECTANGLES:
                dist = g.calc_distance(current_envelope[combination[0]], current_envelope[combination[1]],
                                       other_envelope[combination[2]], other_envelope[combination[3]])
                if dist < closest_dist:
                    closest_dist = dist
                    closest_combination = (current_envelope[combination[0]], current_envelope[combination[1]],
                                           other_envelope[combination[2]], other_envelope[combination[3]])
                    closest_other = i
                    # if there are many nodes it could take a very long time
                    if closest_dist < max_acceptable_link_distance:
                        break
            if closest_dist < max_acceptable_link_distance:
                break
        if closest_dist < max_acceptable_link_distance:
            sub_node_1 = _closest_node_to_point(list(current_graph.nodes), closest_combination[0],
                                                closest_combination[1])
            other_graph = sub_graphs[closest_other]
            sub_node_2 = _closest_node_to_point(list(other_graph.nodes), closest_combination[2], closest_combination[3])
            network.add_edge(sub_node_1, sub_node_2)
            logger.info('Reduced to %i sub-graphs with distance %d', nx.number_connected_components(network),
                         closest_dist)
            loops = 0
        else:
            loops += 1
            if loops > nx.number_connected_components(network):
                max_acceptable_link_distance *= 1.5
                logger.info('Matching is getting difficult - increasing link dist to %d', max_acceptable_link_distance)
                loops = 0


def connect_disjoint_subgraphs_by_node(network: nx.Graph, acceptable_link_distance: int) -> None:
    """Connect disjoint sub-graphs if any included in network by adjacent nodes.
    Using while-loop instead of for-loop because when connecting the number of sub-graphs changes.
    """
    while nx.number_connected_components(network) > 1:
        sub_graphs = [network.subgraph(c) for c in nx.connected_components(network)]
        chosen_index = 0
        largest_number = 0
        # use the largest subgraph
        for index, subgraph in enumerate(sub_graphs):
            if len(subgraph.nodes) > largest_number:
                largest_number = len(subgraph.nodes)
                chosen_index = index
        current_subgraph = sub_graphs[chosen_index]
        closest_dist = 999999
        closest_current = None
        closest_other = None
        current_envelope = _envelope_from_graph_points(list(current_subgraph.nodes), 0.1)
        for other_index, other_subgraph in enumerate(sub_graphs):
            if other_index == chosen_index:  # may not test against self
                continue
            if _has_points_in_envelope(current_envelope, list(other_subgraph.nodes)):
                for current_node in list(current_subgraph.nodes):
                    for other_node in list(other_subgraph.nodes):
                        dist = g.calc_distance(current_node.lon, current_node.lat, other_node.lon, other_node.lat)
                        if dist < closest_dist:
                            closest_dist = dist
                            closest_current = current_node
                            closest_other = other_node
                if closest_dist < acceptable_link_distance:
                    break
        if closest_dist < acceptable_link_distance:
            network.add_edge(closest_current, closest_other)
            logger.info('Reduced to %i sub-graphs with distance %d', nx.number_connected_components(network),
                         closest_dist)
        else:
            logger.info('Stopping at %i sub-graphs', nx.number_connected_components(network))
            break
        # Now remove everything not connected to the largest network
        for other_index, other_subgraph in enumerate(sub_graphs):
            if other_index == chosen_index:  # may not test against self
                continue
            for other_node in list(other_subgraph.nodes):
                network.remove_node(other_node)


def create_argument_parser_default(description: str, include_boundary: bool = True,
                                   include_file: bool = True, include_output: bool = True,
                                   include_scenario: bool = True) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description)
    if include_file:
        parser.add_argument("-f", "--file", dest="filename",
                            help="read parameters from fILE (e.g. params.py) - only needed if osm2city is used "
                            "e.g. for reading from database or using fgelev",
                            type=str, required=True)
    if include_output:
        parser.add_argument("-o", "--output", dest="output",
                            help="write network to file (e.g. foo_network.pkl)",
                            type=str, required=True)
    if include_boundary:
        parser.add_argument("-b", "--boundary", dest="boundary",
                            help="set the boundary as WEST_SOUTH_EAST_NORTH like *9_47.0_11_48.5 (. as decimal)",
                            type=str, required=True)
    if include_scenario:
        parser.add_argument('-s', dest='scenario', type=str,
                            help='the scenario to use',
                            required=True)
    parser.add_argument('-d', dest='scenario_path', type=str,
                        help='the path to the directory where the scenarios live',
                        required=True)
    return parser


def create_circle_points(lon_center: float, lat_center: float, radius: float, alt_m: int,
                         number_of_points: int) -> nx.DiGraph:
    """Creates a number of WayPoints clock-wise around a point starting in North."""
    graph = nx.DiGraph()
    graph.graph[GRAPH_CIRCUIT] = GRAPH_YES
    prev_node = None
    first_node = None
    for i in range(number_of_points):
        bearing = i * 360/number_of_points  # now we look from the centre to the point
        point_lon, point_lat = g.calc_destination(lon_center, lat_center, radius, bearing)
        node = g.WayPoint(i, point_lon, point_lat, 0, alt_m)
        node.is_point_of_interest = True
        graph.add_node(node)
        if prev_node is None:
            prev_node = node
            first_node = node
        else:
            graph.add_edge(prev_node, node)
            prev_node = node
    graph.add_edge(prev_node, first_node)  # close the network
    return graph


def _calculate_radius(speed_ms: float, turn_rate: float) -> float:
    """Calculates a proxy for a heavy plane.
    See also https://aviation.stackexchange.com/questions/8012/what-does-it-take-to-turn-a-747-around-180-degrees
    for calculations of turn radius etc.
    """
    time_full_circle = 360 / turn_rate
    travel_distance = time_full_circle * speed_ms
    return travel_distance / 2 / 3.1416


def create_tanker_track(lon_a: float, lat_a: float, lon_b: float, lat_b: float,
                        alt_m: float, speed_ms: float, turn_rate: float) -> nx.DiGraph:
    """Create a track along which a tanker flies. The tanker always flies against the clock.

    See also https://aviation.stackexchange.com/questions/8012/what-does-it-take-to-turn-a-747-around-180-degrees
    for calculations of turn radius etc.
    """
    radius_m = _calculate_radius(speed_ms, turn_rate)

    # create the graph
    graph = nx.DiGraph()
    graph.graph[GRAPH_CIRCUIT] = GRAPH_YES
    # -- point A
    wp_1 = g.WayPoint(1, lon_a, lat_a, alt_m=alt_m)
    # -- point before B where turning is announced
    bearing = g.calc_bearing(lon_b, lat_b, lon_a, lat_a)
    away = radius_m + 2000
    distance = math.sqrt(away * away + radius_m * radius_m)
    angle = math.atan(radius_m / away)
    bearing = g.normalize_degrees(bearing - angle)
    lon_lat = g.calc_destination(lon_b, lat_b, distance, bearing)
    wp_2 = g.WayPoint(2, lon_lat[0], lon_lat[1], alt_m=alt_m)
    wp_2.is_point_of_interest = True
    # -- point before B where turning starts
    bearing = g.calc_bearing(lon_b, lat_b, lon_a, lat_a)
    away = radius_m
    distance = math.sqrt(away * away + radius_m * radius_m)
    angle = math.atan(radius_m / away)  # would be 45
    bearing = g.normalize_degrees(bearing - angle)
    lon_lat = g.calc_destination(lon_b, lat_b, distance, bearing)
    wp_3 = g.WayPoint(3, lon_lat[0], lon_lat[1], alt_m=alt_m)
    # -- point B
    wp_4 = g.WayPoint(4, lon_b, lat_b, alt_m=alt_m)
    # -- point before A where turning is announced
    bearing = g.calc_bearing(lon_a, lat_a, lon_b, lat_b)
    away = radius_m + 2000
    distance = math.sqrt(away * away + radius_m * radius_m)
    angle = math.atan(radius_m / away)
    bearing = g.normalize_degrees(bearing - angle)
    lon_lat = g.calc_destination(lon_a, lat_a, distance, bearing)
    wp_5 = g.WayPoint(5, lon_lat[0], lon_lat[1], alt_m=alt_m)
    wp_5.is_point_of_interest = True
    # -- point before A where turning starts
    bearing = g.calc_bearing(lon_a, lat_a, lon_b, lat_b)
    away = radius_m
    distance = math.sqrt(away * away + radius_m * radius_m)
    angle = math.atan(radius_m / away)
    bearing = g.normalize_degrees(bearing - angle)
    lon_lat = g.calc_destination(lon_a, lat_a, distance, bearing)
    wp_6 = g.WayPoint(6, lon_lat[0], lon_lat[1], alt_m=alt_m)

    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6])
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)

    return graph


def create_awacs_track(lon_a: float, lat_a: float, lon_b: float, lat_b: float, alt_m: float) -> nx.DiGraph:
    """Create a track along which an AWACS flies. The AWACS always flies against the clock.

    See also https://aviation.stackexchange.com/questions/8012/what-does-it-take-to-turn-a-747-around-180-degrees
    for calculations of turn radius etc.
    """
    half_way = g.calc_distance(lon_a, lat_a, lon_b, lat_b) / 2.

    # create the graph
    graph = nx.DiGraph()
    graph.graph[GRAPH_CIRCUIT] = GRAPH_YES
    wp_1 = g.WayPoint(1, lon_a, lat_a, alt_m=alt_m)  # Point A
    bearing = g.calc_bearing(lon_b, lat_b, lon_a, lat_a)
    bearing = g.normalize_degrees(bearing - 45)
    lon_lat = g.calc_destination(lon_b, lat_b, half_way * math.sqrt(2), bearing)
    wp_2 = g.WayPoint(2, lon_lat[0], lon_lat[1], alt_m=alt_m)  # before point B where turn is initiated
    wp_3 = g.WayPoint(3, lon_b, lat_b, alt_m=alt_m)  # Point B
    bearing = g.calc_bearing(lon_a, lat_a, lon_b, lat_b)
    bearing = g.normalize_degrees(bearing - 45)
    lon_lat = g.calc_destination(lon_a, lat_a, half_way * math.sqrt(2), bearing)
    wp_4 = g.WayPoint(4, lon_lat[0], lon_lat[1], alt_m=alt_m)  # before point A where turn is initiated

    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4])
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)

    return graph


def _find_random_point_of_interest(network: nx.Graph) -> g.WayPoint:
    points_of_interest = list()
    for node in list(network.nodes):
        if node.is_point_of_interest:
            points_of_interest.append(node)
    return random.choice(points_of_interest)


def is_circular_directed_graph(network: nx.Graph) -> bool:
    return isinstance(network, nx.DiGraph) and (
            GRAPH_CIRCUIT in network.graph and network.graph[GRAPH_CIRCUIT] == GRAPH_YES)


def a_star_heuristic(first_wp: g.WayPoint, second_wp: g.WayPoint) -> float:
    """Heuristics for the A* shortest path algorithm.
    Factor has been tested with several networks and seems to be fine. Also, it does not
    really matter whether the shortest path is found or not - just any path is good."""
    return g.calc_distance_wp_wp(first_wp, second_wp) * 1.5


def find_next_shortest_path(network: nx.Graph, start_wp: g.WayPoint, callsign: str,
                            use_astar: bool = True) -> list[g.WayPoint]:
    """Finds a random destination and returns a list of nodes on the shortest patch to this destination.
    Need >= 3 nodes to have start_node, next_node and node after as minimum requirement for the moving
    algorithm to work.
    """
    starting_wp = start_wp
    count_no_path_exception = 0

    waypoints_on_path = list()

    if is_circular_directed_graph(network):
        # we have a circuit and can just loop for a long time
        is_before = True
        for node in list(network.nodes):
            if node == start_wp:
                is_before = False
            elif is_before is False:
                waypoints_on_path.append(node)
        for i in range(1000):
            for node in list(network.nodes):
                waypoints_on_path.append(node)
        return waypoints_on_path
    # else
    while True:
        now = time.time()
        try:
            destination_wp = _find_random_point_of_interest(network)
            # maybe the start_wp is not good - let us look for another one
            # use a high threshold - because we really want to start from the chosen wp
            # but then again we do not want an endless loop
            if count_no_path_exception > 50:
                logger.info('There seems to be a problem with the start wp for %s - let us try another one',
                            callsign)
                starting_wp = _find_random_point_of_interest(network)
                count_no_path_exception = 0
            if use_astar:
                nodes_list = astar_path(network, starting_wp, destination_wp, a_star_heuristic, EDGE_ATTR_COST)
            else:
                nodes_list = dijkstra_path(network, starting_wp, destination_wp, EDGE_ATTR_COST)
            if len(nodes_list) >= MIN_NUMBER_NODES_NETWORKS - 2:
                diff_time = time.time() - now
                logger.info('Found a new shortest path with %i nodes for %s in %.4f seconds',
                            len(nodes_list), callsign, diff_time)
                break
        except NetworkXNoPath:
            logger.info('Cannot find a shortest path for %s - will try another destination',
                        callsign)
            count_no_path_exception += 1
    for wp in nodes_list:
        waypoints_on_path.append(wp)

    # remove the starting point - we do not need it as the start_wp is already known
    waypoints_on_path.pop(0)

    return waypoints_on_path


if __name__ == '__main__':
    """Make some statistics to test whether Dijkstra or A* is faster.
    
    Results (Feb 2024):
    Swiss helis:
        Dijk: time: 17.07694218158722  - # waypoints: 1802.45
        A*: time: 0.666373348236084  - # waypoints: 1685.5
    Falklands helis:
        Dijk: time: 6.003207552433014  - # waypoints: 2121.9
        A*: time: 0.18236138820648193  - # waypoints: 1867.45
    North Norway helis:
        Dijk: time: 0.27038506269454954  - # waypoints: 2180.65
        A*: time: 0.030731165409088136  - # waypoints: 2214.5
    North Norway offshore ships:
        Dijk: time: 0.00037459135055541994  - # waypoints: 21.9
        A*: time: 0.000159454345703125  - # waypoints: 16.45
    North Norway coastal ships:
        Dijk: time: 0.0007234454154968262  - # waypoints: 34.1
        A*: time: 0.0006375312805175781  - # waypoints: 41.85
    Cyclades helis:
        Dijk: time: 0.04847985506057739  - # waypoints: 815.4
        A*: time: 0.017059135437011718  - # waypoints: 671.3
    Cyclades offshore ships:
        Dijk: time: 8.465051651000977e-05  - # waypoints: 7.2
        A*: time: 8.280277252197266e-05  - # waypoints: 9.75
    Cyclades coastal ships:
        Dijk: time: 3.894567489624023e-05  - # waypoints: 3.25
        A*: time: 4.83393669128418e-05  - # waypoints: 4.6
    """
    import hunter.scenarios as sc
    scenario = sc.load_scenario('swiss', '/home/vanosten/develop_hunter/hunter-scenarios')
    my_network = scenario.helis_network
    # sd = scenario.ships_definition
    # my_network = random.choice(scenario.ships_definition.ship_networks.offshore_networks)
    total_time_dijk = 0
    total_time_astar = 0
    total_waypoints_dijk = 0
    total_waypoints_astar = 0
    iterations = 20
    for my_i in range(iterations):
        print(my_i)
        my_start_wp = random.choice(list(my_network.nodes))

        my_now = time.time()
        wps = find_next_shortest_path(my_network, my_start_wp, 'HELLO' + str(my_i))
        my_diff_time = time.time() - my_now
        total_time_astar += my_diff_time
        total_waypoints_astar += len(wps)

        my_now = time.time()
        wps = find_next_shortest_path(my_network, my_start_wp, 'HELLO' + str(my_i), False)
        my_diff_time = time.time() - my_now
        total_time_dijk += my_diff_time
        total_waypoints_dijk += len(wps)

    print('Dijk: time:', total_time_dijk/iterations, ' - # waypoints:', total_waypoints_dijk/iterations)
    print('A*: time:', total_time_astar/iterations, ' - # waypoints:', total_waypoints_astar/iterations)