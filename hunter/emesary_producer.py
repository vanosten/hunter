# SPDX-FileCopyrightText: (C) 2023 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Convenience objects and methods to produce Emesary messages to be sent out."""
from dataclasses import dataclass
from typing import Any, Dict

import hunter.emesary as e
import hunter.emesary_notifications as en
import hunter.geometry as g
import hunter.logger_config as lc
import hunter.utils as u

logger = lc.get_logger()

@dataclass(frozen=True)
class EmesaryMessageAndBridge:
    encoded_message: bytes
    oprf_bridge: int
    now: float


class EmesaryMessageContainer:
    __slots__ = 'message_dict'

    def __init__(self) -> None:
        self.message_dict = dict()  # int for bridge, list of EmesaryMessageAndBridge

    def add_message(self, message_and_bridge: EmesaryMessageAndBridge) -> None:
        if message_and_bridge.oprf_bridge not in self.message_dict:
            self.message_dict[message_and_bridge.oprf_bridge] = list()
        self.message_dict[message_and_bridge.oprf_bridge].append(message_and_bridge)

    def reset(self, now: float) -> None:
        """Should be called for each new iteration in shooter."""
        hit_bridge_encoded_messages = list()
        if en.OPRF_BRIDGE_CHANNEL_HIT in self.message_dict:
            for message_and_bridge in self.message_dict[en.OPRF_BRIDGE_CHANNEL_HIT]:
                if now - message_and_bridge.now <= 1.:
                    hit_bridge_encoded_messages.append(message_and_bridge)

        self.message_dict.clear()
        if hit_bridge_encoded_messages:
            self.message_dict[en.OPRF_BRIDGE_CHANNEL_HIT] = hit_bridge_encoded_messages

    def add_messages_to_props(self, props: Dict[int, Any]) -> None:
        for bridge, encoded_messages in self.message_dict.items():
            if encoded_messages:
                the_message = None
                for index, message in enumerate(encoded_messages):
                    if index == 0:
                        the_message = message.encoded_message
                    else:
                        the_message += message.encoded_message
                props[u.MP_PROP_EMESARY_BRIDGE_BASE + bridge] = the_message


def process_outgoing_armament_hit_notification(now: float, index: int, shell_id: int, number_of_hits: int,
                                               remote_callsign: str, is_cannon: bool) -> EmesaryMessageAndBridge:
    """Number of hits is for bullets. Else it is the distance of the missile explosion to the plane."""
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(index, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ArmamentNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    secondary_kind = shell_id * -1 - 1 if is_cannon else 21 + shell_id
    notification = en.ArmamentNotification(en.IDENT_MP_BRIDGE, en.KIND_COLLISION, secondary_kind)
    logger.info('Hit notification: %s', str(notification))
    notification.distance = number_of_hits
    notification.remote_callsign = remote_callsign
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return EmesaryMessageAndBridge(encoded_notification, en.OPRF_BRIDGE_CHANNEL_HIT, now)


def process_outgoing_armament_in_flight_lost(now: float, index: int, shell_id: int, unique_identity: int,
                                             position: g.Position, heading: float, pitch: float,
                                             speed_ms: float) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message when the missile is lost.
    E.g. lost lock / line of sight, locked on flares/chaff, too slow, ...

    Based on F16/Nasal/missile_code.nas ca. line 2537
    ... me.free or me.lostLOS or me.tooLowSpeed or me.flareLock or me.chaffLock ...
    """
    return process_outgoing_armament_in_flight_notification(now, index, shell_id, unique_identity,
                                                            position, heading, pitch,
                                                            speed_ms,
                                                            False, False, "", False)


def process_outgoing_armament_in_flight_deleted(now: float, index: int, shell_id: int, unique_identity: int
                                                ) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message when the missile needs to be deleted.

    Based on F16/Nasal/missile_code.nas ca. line 907
    ... me.free or me.lostLOS or me.tooLowSpeed or me.flareLock or me.chaffLock ...
    """
    position = g.Position(0., 0., 0.)
    return process_outgoing_armament_in_flight_notification(now, index, shell_id, unique_identity,
                                                            position, 0., 0., 0.,
                                                            False, False, "", True)


def process_outgoing_armament_in_flight_notification(now: float, index: int, shell_id: int, unique_identity: int,
                                                     position: g.Position, heading: float, pitch: float,
                                                     speed_ms: float,  # metres per second
                                                     radar_on: bool, thrust_on: bool,
                                                     remote_callsign: str,
                                                     is_deleted: bool) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message.
    Based on F16/Nasal/missile_code.nas->notifyInFlight() - ca. line 3976.

    On line ca. 4046 if a missile explodes -> speed = 0, remote_callsign = '', is_deleted=False
    """
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(index, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ArmamentInFlightNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    secondary_kind = 21 + shell_id
    collision_kind = en.KIND_DELETED if is_deleted else en.KIND_MOVED
    unique_identity = unique_identity
    notification = en.ArmamentInFlightNotification(en.IDENT_MP_BRIDGE, unique_identity, collision_kind, secondary_kind)
    notification.position = position
    notification.heading = heading
    notification.pitch = pitch
    notification.u_fps = g.metres_to_feet(speed_ms)
    notification.set_oprf_flags(radar_on, thrust_on, False)  # missile shooter does not have semi-active radar
    # print('shell id=', shell_id, 'id=', unique_identity, 'lon =', position.lon, 'lat=', position.lat,
    # 'heading=', heading, 'pitch=', pitch, 'alt=', position.alt_m, 'speed=', speed_ms, 'flags=', notification.flags)
    notification.is_distinct = 0 if is_deleted else 1
    notification.remote_callsign = remote_callsign
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return EmesaryMessageAndBridge(encoded_notification, en.OPRF_BRIDGE_CHANNEL_INFLIGHT, now)
