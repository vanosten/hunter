# SPDX-FileCopyrightText: (C) 2019 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""The message objects exchanged between the different process instances.

Typing is only done for standard library - not for other stuff in war-room in order to reduce module dependencies.
"""
import copy
import datetime
from enum import IntEnum, unique
from typing import Optional

import hunter.geometry as g
import hunter.emesary_notifications as en
import hunter.logger_config as lc

logger = lc.get_logger()


@unique
class HealthType(IntEnum):
    """Type of damage an MPTarget has over time.
    The values below 10 are returned to attacker for statistics."""
    missed = -1  # not used for health, but returned if attacker did not hit
    fit_for_fight = 0
    hit = 1
    broken = 2  # so much damage that no movement and self-defense is possible
    destroyed = 3  # it is burning, falling from the sky etc.
    dead = 4  # should be removed from MP


def is_capable_of_fighting_health(health: HealthType) -> bool:
    return health in [HealthType.fit_for_fight, HealthType.hit]


def map_health_type(value: int, is_damage: bool) -> HealthType:
    for type_ in HealthType:
        if type_.value == value:
            return type_
    logger.warning('Possible programming error: could not map value = %i to HealthType (damage = %s)',
                   value, is_damage)
    if is_damage:
        return HealthType.missed
    else:
        return HealthType.fit_for_fight


@unique
class WeaponType(IntEnum):
    cannon = 0
    rocket = 1
    unguided_ground = 10
    guided_ground = 11
    cluster_ground = 12
    # unguided_air = 20 - not used yet (maybe never, because rockets and bullets are only unguided used in air)
    guided_air = 21
    unknown = 30


def map_weapon_type(value: int) -> WeaponType:
    for type_ in WeaponType:
        if type_.value == value:
            return type_
    logger.warning('Possible programming error: could not map value = %i to WeaponType', value)
    return WeaponType.unknown


class ExitSession:
    """Signal between CLI and Controller to exit the session."""
    def __str__(self):
        return 'Exit session'


class DamageResult:
    """Received by Controller from target to indicate the result of shooting at the target"""
    __slots__ = ('damage_time', 'health', 'weapon_type', 'weapon_name',
                 'attacker', 'attacker_kind', 'target', 'target_kind',
                 'civilian', 'fg_target', 'shooting')

    def __init__(self, health: HealthType, weapon_type: WeaponType, weapon_name: str,
                 attacker: str, attacker_kind: str,
                 target: str, target_kind: str,
                 civilian: bool = False, fg_target: bool = False, shooting: bool = False) -> None:
        self.damage_time = datetime.datetime.now(datetime.UTC)
        self.health = health
        self.weapon_type = weapon_type
        self.weapon_name = weapon_name
        self.attacker = attacker  # callsign of the attacker
        self.attacker_kind = attacker_kind
        self.target = target  # callsign of the target
        self.target_kind = target_kind  # basically a type
        self.civilian = civilian
        self.fg_target = fg_target  # mp_target is False, else fg_target
        self.shooting = shooting  # really shooting, not just simulating
        if not self.check_send_result(health):
            raise ValueError('DamageResult needs to be hit, broken or destroyed. {}'.format(self))

    def copy_with_health(self, new_health: HealthType) -> 'DamageResult':
        new_damage = copy.deepcopy(self)
        new_damage.health = new_health
        return new_damage

    @staticmethod
    def check_send_result(health_result: HealthType) -> bool:
        """Checks whether the damage result should be sent."""
        return health_result in (HealthType.hit, HealthType.broken, HealthType.destroyed)

    def _stringify_civilian(self) -> str:
        civilian_str = 'military'
        if self.civilian:
            civilian_str = 'civilian'
        return civilian_str

    def __str__(self):
        target_str = 'FG target' if self.fg_target else 'MP target'
        return 'Attacker {} reduced health of {} {}/{} to {} at {}: {}'.format(self.attacker,
                                                                               self._stringify_civilian(),
                                                                               self.target, self.target_kind,
                                                                               self.health.name,
                                                                               self.damage_time.strftime(
                                                                                   '%d %b %Y %H:%M:%S +0000'),
                                                                               target_str)


class HitNotification:
    """The ArmamentNotification from the attacker to the target before damage calculation through Emesary.
    The result will be sent as a DamageResult message.
    """
    __slots__ = ('notification', 'attacker_callsign', 'attacker_aircraft', 'target')

    def __init__(self, notification: en.ArmamentNotification, attacker_callsign: str, attacker_aircraft: str,
                 target: str) -> None:
        self.notification = notification
        self.attacker_callsign = attacker_callsign
        self.attacker_aircraft = attacker_aircraft
        self.target = target  # callsign of the target

    def __str__(self):
        return 'Hit notification from attacker {} using {} to target {}: {}'.format(self.attacker_callsign,
                                                                                    self.attacker_aircraft,
                                                                                    self.target, self.notification)


class AddMovingTarget:
    __slots__ = ('asset', 'path', 'start_node')

    def __init__(self, asset, path, start_node) -> None:
        self.asset = asset
        self.path = path  # nx.Graph
        self.start_node = start_node

    def __str__(self):
        return 'Add moving target {}'.format(self.asset.name)


class AddFGFSAircraft:
    __slots__ = ('aircraft', 'airport', 'target_alt_ft')

    def __init__(self, aircraft, airport, target_alt_ft: int) -> None:
        self.aircraft = aircraft  # wa.Aircraft
        self.airport = airport  # app.Airport
        self.target_alt_ft = target_alt_ft

    def __str__(self):
        return 'Add FGFS aircraft {} departing at {} with target altitude in ft {}'.format(self.aircraft.aero,
                                                                                           self.airport.icao,
                                                                                           self.target_alt_ft)


class AddFGFSCarrier:
    __slots__ = ('carrier',)

    def __init__(self, carrier) -> None:
        self.carrier = carrier  # wa.Carrier

    def __str__(self):
        return 'Add FGFS carrier {}'.format(self.carrier.aero)


class AttackerPosition:
    """The position of an attacker as read directly from the receiver - to be sent to a target shooter."""
    __slots__ = ('callsign', 'position', 'heading', 'pitch', 'radar_lock', 'radar_on', 'chaff_on', 'flares_on',
                 'mp_timestamp', 'timestamp', 'iff_hash')

    def __init__(self, callsign: str, position, heading: float, pitch: float,
                 radar_lock: Optional[str], radar_on: bool, chaff_on: float, flares_on: float,
                 mp_timestamp: float, iff_hash: Optional[str]) -> None:
        self.callsign = callsign
        self.position = position
        self.heading = heading
        self.pitch = pitch
        self.radar_lock = radar_lock
        self.radar_on: float = radar_on
        self.chaff_on: float = chaff_on
        self.flares_on = flares_on
        # the original timestamp from MP - might be different from time.time() - incl. due to lag
        # therefore only suitable for relative calculations - diffs
        self.mp_timestamp = mp_timestamp
        self.timestamp = None  # set when added to tracker
        self.iff_hash = iff_hash


@unique
class AttackerEventTrigger(IntEnum):
    register = 0
    plane_model_changed = 1
    iff_changed = 2
    zombie = 3
    unknown = 10


def map_attacker_event_trigger(name: str) -> AttackerEventTrigger:
    for trigger_ in AttackerEventTrigger:
        if trigger_.name == name:
            return trigger_
    logger.warning('Possible programming error: could not map value = %s to AttackerEventTrigger', name)
    return AttackerEventTrigger.unknown


class AttackerEvent:
    __slots__ = ('event_time', 'callsign', 'fgfs_model', 'has_opfor_iff', 'trigger')

    def __init__(self, callsign: str, fgfs_model: str, has_opfor_iff: bool, trigger: AttackerEventTrigger):
        self.event_time = datetime.datetime.now(datetime.UTC)
        self.callsign = callsign
        self.fgfs_model = fgfs_model
        self.has_opfor_iff = has_opfor_iff
        self.trigger = trigger


class MissileEvent:
    __slots__ = ('event_time', 'unique_id', 'shooter_callsign', 'target_callsign', 'telemetry_string')

    def __init__(self, unique_id: int, shooter_callsign: str, target_callsign: str, telemetry_string: str):
        self.event_time = datetime.datetime.now(datetime.UTC)
        self.unique_id = unique_id
        self.shooter_callsign = shooter_callsign
        self.target_callsign = target_callsign
        self.telemetry_string = telemetry_string


class PositionUpdate:
    """Objects send their position once in a while, such that controller can display or assign targets."""
    __slots__ = ('callsign', 'kind', 'position', 'heading', 'pitch', 'speed', 'health')

    def __init__(self, callsign: str, kind: str, health: HealthType, position,  # g.Position
                 heading: float = -.01, pitch: float = -0.1, speed: float = -.01) -> None:
        self.callsign = callsign
        self.kind = kind
        self.health = health
        self.position = position
        self.heading = heading  # true heading
        self.pitch = pitch  # 0 is horizontal, negative is down, never exceeds 90 degs
        self.speed = speed  # not necessarily airspeed. If < 0 then not known or not relevant

    @property
    def unknown_speed(self) -> bool:
        return self.speed < 0.

    @property
    def unknown_heading(self) -> bool:
        return self.heading < 0.

    @property
    def unknown_pitch(self) -> bool:
        return self.pitch < 0.

    def __str__(self):
        return '{} \'s health is {}: has position: {} with heading: {} and speed: {}'.format(self.callsign, self.health,
                                                                                             self.position,
                                                                                             self.heading, self.speed)


class PositionUpdatesBatch:
    """A list of PositionUpdate messages to be processed in one batch."""
    __slots__ = 'position_updates'

    def __init__(self) -> None:
        self.position_updates = list()

    def add_position_update(self, position_update: PositionUpdate) -> None:
        self.position_updates.append(position_update)

    def __str__(self):
        return 'Position updates for {} items'.format(len(self.position_updates))


@unique
class GCIRequestType(IntEnum):
    plane = 0  # fixed-wing and not drone
    helicopter = 1
    drone = 2
    offshore_ship = 3
    coastal_ship = 4
    sam = 5  # fixed or movable
    vehicle = 6  # not SAM
    static = 7  # building etc.
    clear = 99  # clear assignments
    unknown = 999  # request is not understood / not available


def map_command_to_type(command: str) -> GCIRequestType:
    if command == 'g_p':
        return GCIRequestType.plane
    elif command == 'g_h':
        return GCIRequestType.helicopter
    elif command == 'g_d':
        return GCIRequestType.drone
    elif command == 'g_os':
        return GCIRequestType.offshore_ship
    elif command == 'g_cs':
        return GCIRequestType.coastal_ship
    elif command == 'g_s':
        return GCIRequestType.sam
    elif command == 'g_v':
        return GCIRequestType.vehicle
    elif command == 'g_b':
        return GCIRequestType.static
    elif command == 'g_c':
        return GCIRequestType.clear
    return GCIRequestType.unknown


MESSAGE_ID = 0


def _get_unique_message_id() -> int:
    global MESSAGE_ID
    MESSAGE_ID += 1
    return MESSAGE_ID


class GCIResponse:
    __slots__ = ('req_callsign', 'tgt_callsign', 'request_type', 'message_id', '_bearing', '_range_m', '_altitude_ft',
                 '_aspect', '_vector', '_eta_s', '_lon', '_lat', '_is_update')

    def __init__(self, request_type: GCIRequestType, req_callsign: str, tgt_callsign: str,
                 is_update: bool) -> None:
        self.req_callsign = req_callsign
        self.tgt_callsign = tgt_callsign
        self.request_type = request_type
        self.message_id = _get_unique_message_id()
        self._bearing = 0
        self._range_m = 0
        self._altitude_ft = 0
        self._aspect = 0
        self._vector = 0
        self._eta_s = 0  # seconds
        self._lon = 0
        self._lat = 0
        self._is_update = is_update

    def set_altitude_m(self, altitude_m) -> None:
        """Feet is used per default. Change to m for metric aircraft is done in aircraft"""
        self._altitude_ft = int(g.metres_to_feet(altitude_m))

    def set_bearing(self, magnetic_bearing: float) -> None:
        self._bearing = int(magnetic_bearing)

    def set_range(self, range_m: float) -> None:
        self._range_m = int(range_m)

    def set_aspect(self, aspect: float) -> None:
        self._aspect = int(aspect)

    def _get_aspect(self) -> str:
        if self._aspect < 60:
            return 'drag'
        elif 60 <= self._aspect < 120:
            return 'beam'
        elif 120 <= self._aspect < 150:
            return 'flank'
        return 'hot'

    def set_vector(self, magnetic_vector: float) -> None:
        self._vector = int(magnetic_vector)

    def set_eta_s(self, eta_s: float) -> None:
        self._eta_s = int(eta_s)

    def set_lon_lat(self, lon: float, lat: float) -> None:
        self._lon = lon
        self._lat = lat

    def __str__(self):
        if self._is_update:
            response = '{}, update for your assigned {}: '.format(self.req_callsign, self.tgt_callsign)
        else:
            response = '{}, assigned {} to you: '.format(self.req_callsign, self.tgt_callsign)
        if self.request_type is GCIRequestType.static:
            response += 'lon={:.5f}, lat={:.5f}, alt_ft={}'.format(self._lon, self._lat, int(self._altitude_ft))
        elif self.request_type in [GCIRequestType.plane, GCIRequestType.helicopter, GCIRequestType.drone]:
            angels = int(self._altitude_ft/1000)
            angels_str = 'below 1 thousand' if angels < 1 else f'{angels} thousand'
            response += 'BRAA {}/{}, {}, {} # '.format(int(self._bearing),
                                                       int(g.metres_to_nm(self._range_m)),
                                                       angels_str,
                                                       self._get_aspect())
            response += 'Vector={} for ca. {} secs'.format(int(self._vector), self._eta_s)
        else:
            response += '{} deg, {} nm'.format(int(self._bearing),
                                               int(g.metres_to_nm(self._range_m)))
        return response


class DatalinkData:
    __slots__ = 'encoded_data'

    def __init__(self, encoded_data: bytes) -> None:
        self.encoded_data = encoded_data


class CarrierPositionRequest:
    __slots__ = ('callsign', 'caller_pos')

    def __init__(self, callsign: str, caller_pos) -> None:  # g.Position
        self.callsign = callsign
        self.caller_pos = caller_pos


class CarrierCallType(IntEnum):
    inbound = 0
    see_you_at_10 = 1


class CarrierCall:
    __slots__ = ('callsign', 'call_type')

    def __init__(self, callsign: str, call_type: CarrierCallType) -> None:
        self.callsign = callsign
        self.call_type = call_type

    def __str__(self):
        return 'CarrierCall of type {} issued by {}'.format(self.call_type.name, self.callsign)


class CarrierCourseType(IntEnum):
    """Situated here instead of in mp_carrier.py due to import dependencies."""
    loiter = 1  # loitering around because nothing to do - navigating towards or near the loiter point
    navigate = 2  # the carrier is commanded into a specific direction
    recovery = 3  # same as for FG AI carrier -> aircraft landing
    launch = 4  # same as for FG AI carrier -> aircraft starting

    # evade cannot be chosen as a command
    evade = 5  # the carrier is at the border of the sail area and needs to get free


class CarrierCommand:
    __slots__ = ('course_type', 'course_deg', 'deck_lights', 'flood_lights', 'weather_wind_hdg', 'weather_wind_kn',
                 'deck_wind_speed_kn')

    def __init__(self) -> None:
        self.course_type = None
        self.course_deg = None
        self.deck_lights = None
        self.flood_lights = None
        self.weather_wind_hdg = None
        self.weather_wind_kn = None
        self.deck_wind_speed_kn = None

    @classmethod
    def command_loiter(cls) -> 'CarrierCommand':
        command = CarrierCommand()
        command.course_type = CarrierCourseType.loiter
        command.deck_wind_speed_kn = 0
        return command

    @classmethod
    def command_launch(cls, deck_wind_speed: int) -> 'CarrierCommand':
        command = CarrierCommand()
        command.course_type = CarrierCourseType.launch
        command.deck_wind_speed_kn = deck_wind_speed
        return command

    @classmethod
    def command_recovery(cls, deck_wind_speed: int) -> 'CarrierCommand':
        command = CarrierCommand()
        command.course_type = CarrierCourseType.recovery
        command.deck_wind_speed_kn = deck_wind_speed
        return command

    @classmethod
    def command_navigate(cls, course: int) -> 'CarrierCommand':
        command = CarrierCommand()
        command.course_type = CarrierCourseType.navigate
        command.course_deg = course
        return command

    @classmethod
    def command_deck_lights(cls, deck_lights: bool) -> 'CarrierCommand':
        command = CarrierCommand()
        command.deck_lights = deck_lights
        return command

    @classmethod
    def command_flood_lights(cls, flood_lights: bool) -> 'CarrierCommand':
        command = CarrierCommand()
        command.flood_lights = flood_lights
        return command

    @classmethod
    def command_wind(cls, heading: int, speed_kt: int) -> 'CarrierCommand':
        command = CarrierCommand()
        command.weather_wind_hdg = heading
        command.weather_wind_kn = speed_kt
        return command

    def __str__(self):
        if self.course_type:
            return 'CarrierCommand for course_type is {}'.format(self.course_type.name)
        elif self.deck_lights:
            return 'CarrierCommand for deck_light is {}'.format(self.deck_lights)
        elif self.flood_lights:
            return 'CarrierCommand for flood_light is {}'.format(self.flood_lights)
        elif self.weather_wind_hdg:
            return 'CarrierCommand for weather/wind: hdg is {}, knots is {}'.format(self.weather_wind_hdg,
                                                                                    self.weather_wind_kn)
