# SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
"""Handles all pages within an ongoing event."""
from typing import Any, List, Optional

from flask import Blueprint, render_template, request

import hunter.azure_io as aio
import hunter.messages as m
import hunter.mp_targets as mpt
import hunter.scenarios as s

bp_ongoing_session = Blueprint('ongoing_session', __name__, url_prefix='/ongoing_session', template_folder='templates')

TITLE_SESSIONS_LIST = 'List of available Sessions'
TITLE_SESSION_DETAILS = 'Session Details'
TITLE_DAMAGES = 'Damage and Hits'
TITLE_ATTACKER_EVENTS = 'Attacker Events'
TITLE_POSITIONS_LIST = 'List of Attacker and Target Positions'
TITLE_POSITIONS_MAP = 'Map of Attacker and Target Positions'
TEXT_INVALID_SESSION = '''
                       Somehow you managed to choose an invalid session. 
                       Choose a different session: <a href="/ongoing_session/sessions_list">
                       ''' + TITLE_SESSIONS_LIST + '</a>'
SESSION_ID = "session_id"

db_client = aio.initialize_container_proxy()


def airports_as_list_in_list(airports: Optional[List[s.AptDatAirport]]) -> List[List[Any]]:
    """Transforms a list of airports to a list of lists directly usable in leaflet for markers"""
    if not airports:
        return list()

    leaflet_list = list()
    for apt in airports:
        if apt.kind == 16:
            kind = 'Sea base'
        elif apt.kind == 17:
            kind = 'Heliport'
        else:
            kind = 'Land airport'
        text = '<b>{}</b><br>{}<br>({})'.format(apt.icao, apt.name, kind)
        apt_data = [text, apt.lon, apt.lat]
        leaflet_list.append(apt_data)
    return leaflet_list


def positions_as_list_in_list(positions: Optional[List[m.PositionUpdate]],
                              hunter_session: aio.HunterSession) -> List[List[Any]]:
    """Cf. mp_targets.py amongst others from ca. line 135 for names/kinds"""
    if not positions:
        return list()
    leaflet_list = list()
    for position in positions:
        if position.kind == 'sack':
            continue
        my_name = position.kind
        if position.callsign.startswith(hunter_session.identifier) is False:
            if position.callsign.startswith('MP_0'):
                colour = 'navy'
            else:
                colour = 'royalblue'  # will also be used for awacs and tankers
        elif mpt.MPTarget.is_sam_or_shilka(position.kind) or position.kind in [mpt.MPTarget.TRUCK,
                                                                               mpt.MPTarget.M1_TANK,
                                                                               mpt.MPTarget.MLRS_ROCKET_LAUNCHER,
                                                                               mpt.MPTarget.BRADLEY_TANK,
                                                                               mpt.MPTarget.BUFFALO_MINE,
                                                                               mpt.MPTarget.STRYKER_TANK]:
            colour = 'magenta'
        elif position.kind in [mpt.oprf_frigate.name, mpt.us_navy_lake_champlain.name, mpt.us_navy_normandy.name,
                               mpt.us_navy_oliver_perry.name, mpt.us_navy_san_antonio.name]:
            colour = 'saddlebrown'
        elif position.kind in [mpt.inflatable_speedboat.name]:
            colour = 'orange'
        elif position.kind in ['containers', 'bridge', 'light_hangar', 'double_shelter', 'radar_station', 'bunker',
                               'gasometer', 'checkpoint', 'power_plant', 'oilrig',
                               'depot', 'warehouse', 'hard_shelter', 'hq', 'contarget',
                               'hill_target', 'cliff_target', 'water_target']:
            colour = 'red'
        else:
            colour = 'purple'
        text = '<b>{}</b><br>{}<br>{}<br>{:.5f}, {:.5f}<br>at {:d} ft ({:d} m)'.format(position.callsign, my_name,
                                                                                       position.health.name,
                                                                                       position.position.lon,
                                                                                       position.position.lat,
                                                                                       int(position.position.alt_ft),
                                                                                       int(position.position.alt_m))
        positions_data = [text, position.position.lon, position.position.lat, colour]
        leaflet_list.append(positions_data)
    return leaflet_list


def session_id_from_request(my_request) -> str:
    """Gets the session_id parameter out of the request.
    Returns 0 if parameter is not found.
    """
    return my_request.args.get(SESSION_ID, default='0', type=str)


@bp_ongoing_session.route('sessions_list')
def sessions_list():
    the_sessions = aio.query_available_sessions(db_client)
    return render_template('ongoing_session/sessions_list.html', title=TITLE_SESSIONS_LIST, sessions=the_sessions)


@bp_ongoing_session.route('current_session')
def session():
    hunter_session = aio.query_session(db_client, session_id_from_request(request), True)
    airports = list()
    if hunter_session:
        airports = airports_as_list_in_list(hunter_session.airports)
    return render_template('ongoing_session/current_session.html', title=TITLE_SESSION_DETAILS, leaflet=True,
                           hunter_session=hunter_session, airports=airports,
                           text_invalid_session=TEXT_INVALID_SESSION)


@bp_ongoing_session.route('damages')
def damages():
    hunter_session = aio.query_session(db_client, session_id_from_request(request))
    the_damages = list()
    if hunter_session:
        the_damages = aio.query_damage_results(db_client, hunter_session.session_id)
    return render_template('ongoing_session/damages.html', title=TITLE_DAMAGES, damages=the_damages,
                           hunter_session=hunter_session,
                           text_invalid_session=TEXT_INVALID_SESSION)


@bp_ongoing_session.route('attacker_events')
def attacker_events():
    hunter_session = aio.query_session(db_client, session_id_from_request(request))
    the_events = list()
    if hunter_session:
        the_events = aio.query_attacker_event_results(db_client, hunter_session.session_id)
    return render_template('ongoing_session/attacker_events.html', title=TITLE_ATTACKER_EVENTS,
                           attacker_events=the_events,
                           hunter_session=hunter_session,
                           text_invalid_session=TEXT_INVALID_SESSION)


@bp_ongoing_session.route('positions_list')
def positions_list():
    hunter_session = aio.query_session(db_client, session_id_from_request(request))
    the_positions = list()
    if hunter_session:
        the_positions = aio.query_current_session_positions(db_client, hunter_session.session_id)
    return render_template('ongoing_session/positions_list.html', title=TITLE_POSITIONS_LIST, positions=the_positions,
                           hunter_session=hunter_session,
                           text_invalid_session=TEXT_INVALID_SESSION)


@bp_ongoing_session.route('positions_map')
def positions_map():
    hunter_session = aio.query_session(db_client, session_id_from_request(request))
    positions = list()
    if hunter_session:
        the_positions = aio.query_current_session_positions(db_client, hunter_session.session_id)
        positions = positions_as_list_in_list(the_positions, hunter_session)
    return render_template('ongoing_session/positions_map.html', title=TITLE_POSITIONS_MAP, leaflet=True,
                           positions=positions,
                           hunter_session=hunter_session,
                           text_invalid_session=TEXT_INVALID_SESSION)
