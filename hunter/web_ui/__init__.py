# SPDX-FileCopyrightText: (C) 2020 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from flask import Flask, render_template


# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)

# imports to other modules need to be done AFTER app has been defined
# otherwise app will not be available in the module and the import will fail

import hunter.logger_config as lc
lc.configure_logging(lc.LogConfig('INFO', False), 'web-ui')

import hunter.web_ui.tools as t
import hunter.web_ui.ongoing_session as ose
app.register_blueprint(t.bp_tools)
app.register_blueprint(ose.bp_ongoing_session)

TITLE_HOME = 'Home'


@app.route('/')
def root():

    return render_template('index.html', title=TITLE_HOME)
