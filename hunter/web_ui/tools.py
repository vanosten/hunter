# SPDX-FileCopyrightText: (C) 2023 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
from typing import Any, Dict, List, Tuple

from flask import Blueprint, render_template, request
from metar import Metar
import requests
import xmltodict

import hunter.geometry as g
import hunter.logger_config as lc

logger = lc.get_logger()


bp_tools = Blueprint('tools', __name__, url_prefix='/tools', template_folder='templates')


INHG_TO_MBAR = 33.86398
HPA_PER_METRE = 12.017

METAR_RESPONSE_TIMEOUT_GET = 4.

TITLE_WEATHER = 'Weather tools'


class MetarContainer:
    """This is a direct object mapping from https://aviationweather.gov/dataserver/fields?datatype=metar.
    The information returned contains more data than the raw metar, e.g. lon/lat, elevation.

    For displaying safely data https://github.com/python-metar/python-metar is used.
    """
    __slots__ = ('station_id', 'raw_text', 'position', 'metar_obj')

    def __init__(self, station_id: str, raw_text: str, lon: float, lat: float, elevation_m: float) -> None:
        self.station_id = station_id
        self.raw_text = raw_text
        self.position = g.Position(lon, lat, elevation_m)
        self.metar_obj = Metar.Metar(raw_text, strict=False)

    @property
    def html_tooltip(self) -> str:
        lines = self.metar_obj.string().split('\n')
        return '\n'.join(lines)

    @classmethod
    def metar_from_response_dict(cls, metar_dict: Dict[str, Any]) -> 'MetarContainer':
        station_id = metar_dict['station_id']
        raw_text = metar_dict['raw_text']
        lon = float(metar_dict['longitude'])
        lat = float(metar_dict['latitude'])
        elevation_m = metar_dict['elevation_m']
        return MetarContainer(station_id, raw_text, lon, lat, elevation_m)


# https://aviationweather.gov/data/api/
# https://aviationweather.gov/api/data/dataserver?dataSource=metars&requesttype=retrieve&format=xml&hoursBeforeNow=3&mostRecentForEachStation=constraint&stationString=KORD,KLAX,KMIA
def fetch_metars(icaos: List[str]) -> List[MetarContainer]:
    final_list = list()
    for icao in icaos:
        final_list.append(icao.strip().upper())
    payload = {'dataSource': 'metars',
               'requesttype': 'retrieve',
               'format': 'xml',
               'hoursBeforeNow': 3,
               'mostRecentForEachStation': 'constraint',
               'stationString': ','.join(final_list)}
    r = requests.get('https://aviationweather.gov/api/data/dataserver', params=payload,
                     timeout=METAR_RESPONSE_TIMEOUT_GET)
    status_code = r.status_code
    logger.info('HTTP return code %i for metar fetch %s', status_code, r.request.url)
    if status_code != 200:
        raise Exception('Something was not ideal - got status code %i', status_code)

    metar_containers = list()
    dict_data = xmltodict.parse(r.content)
    if 'METAR' in dict_data['response']['data']:
        metar_element = dict_data['response']['data']['METAR']
        if isinstance(metar_element, list):
            for metar_dict in metar_element:
                metar_containers.append(MetarContainer.metar_from_response_dict(metar_dict))
        else:
            metar_containers.append(MetarContainer.metar_from_response_dict(metar_element))
    return metar_containers


def interpolate_qnh(metar_containers: List[MetarContainer], lon: float, lat: float) -> float:
    """Interpolate the seal level pressure (QNH) for several stations with a linear distance interpolation.

    The value used in meta.Metar is hPa / mbar by default. Therefore, the result is also in hPa / mbar
    """
    values = list()
    min_distance = 99999999
    for container in metar_containers:
        distance = g.calc_distance(lon, lat, container.position.lon, container.position.lat)
        if container.metar_obj.press:  # only if it is known
            min_distance = min(min_distance, distance)
            values.append([container.metar_obj.press.value(), distance])
    total_shares = 0.
    total_qnh = 0.
    for value in values:
        value[1] = min_distance / value[1]
        total_shares += value[1]
    for value in values:
        total_qnh += value[0] * value[1] / total_shares
    return total_qnh


# https://aviation.stackexchange.com/questions/34267/how-can-i-calculate-qnh-from-qfe
# https://www.metpod.co.uk/calculators/pressure/
# using the approximate formula here without temperature
def calc_qfe(qnh: float, is_mbar: bool, elevation: int, is_metres: bool) -> Tuple[float, float]:
    qnh_mbar = qnh if is_mbar else qnh * INHG_TO_MBAR
    elevation_m = elevation if is_metres else elevation / g.METRES_TO_FEET
    qfe_mbar = qnh_mbar - elevation_m / HPA_PER_METRE
    qfe_hg = qfe_mbar / INHG_TO_MBAR
    return qfe_mbar, qfe_hg


@bp_tools.route('weather_qnh', methods=['GET', 'POST'])
def weather_qnh():
    if request.method == 'GET':
        return render_template('tools/weather.html', title=TITLE_WEATHER,
                               qnh_error=None, qfe_error=None,
                               qnh_result=None, metar_containers=None,
                               qfe_result=None)
    # else we are handling the POST
    icao_input = request.form['icao'].strip()
    lon_input_raw = request.form['lon']
    lat_input_raw = request.form['lat']
    metar_containers = None
    error_text = None
    qnh_result = None
    icao_list = list()
    if len(icao_input) < 4:
        error_text = 'There needs to be at least one 4 letter ICAO code.'
    else:
        icao_list = icao_input.split(',')
        for icao in icao_list:
            if len(icao.strip()) != 4:
                error_text = "At least one submitted ICAO code's length is not 4."
                break
            if not icao.strip().isalpha():
                error_text = "At least one submitted ICAO code does contain other characters than alphabetic."
                break

    if error_text is None:
        try:
            metar_containers = fetch_metars(icao_list)
            if lon_input_raw and lat_input_raw:
                qnh_mbar = interpolate_qnh(metar_containers, float(lon_input_raw), float(lat_input_raw))
                qnh_result = '{:0.1f} mbar ({:0.2f} inHg)'.format(qnh_mbar, qnh_mbar / INHG_TO_MBAR)
        except Exception as e:  # lots of different stuff can happen
            logger.exception(e)
            error_text = 'Something went wrong when fetching METARs. Retry or give up.'

    return render_template('tools/weather.html', title=TITLE_WEATHER,
                           qnh_error=error_text, qfe_error=None,
                           qnh_result=qnh_result, metar_containers=metar_containers,
                           qfe_result=None)


@bp_tools.route('weather_qfe', methods=['GET', 'POST'])
def weather_qfe():
    if request.method == 'GET':
        return render_template('tools/weather.html', title=TITLE_WEATHER,
                               qnh_error=None, qfe_error=None,
                               qnh_result=None, metar_containers=None,
                               qfe_result=None)
    # else we are handling the POST
    qnh = float(request.form['qnh'])
    is_mbar = request.form['p_unit'] == 'mbar'
    elevation = int(request.form['elevation'])
    is_metres = request.form['a_unit'] == 'm'
    qfe_mbar, qfe_hg = calc_qfe(qnh, is_mbar, elevation, is_metres)
    qfe_result = '{:0.1f} mbar ({:0.2f} inHg)'.format(qfe_mbar, qfe_hg)
    if is_mbar is False:
        qfe_result = '{:0.2f} inHg ({:0.1f} mbar)'.format(qfe_hg, qfe_mbar)

    return render_template('tools/weather.html', title=TITLE_WEATHER,
                           qnh_error=None, qfe_error=None,
                           qnh_result=None, metar_containers=None,
                           qfe_result=qfe_result)
