# needed for osm2city stuff, but not officially imported
descartes==1.1.0
shapely==2.0.4
networkx==3.3
requests==2.32.3
pyproj==3.6.1
scipy==1.13.1
cgal==5.6.1.post202406030950
matplotlib==3.9.0
