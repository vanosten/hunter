# Using LTS version of Ubuntu because:
#    * Closest to development environment
#    * Different challenges with Alpine and not using apt-get
#    * Official https://hub.docker.com/_/pytho lacks e.g. libgl1-mesa-dev (for PyQt)
FROM ubuntu:noble

# See also https://github.com/opencontainers/image-spec/blob/main/annotations.md
LABEL maintainer="rick@vanosten.net"
LABEL org.opencontainers.image.authors="rick@vanosten.net"
LABEL org.opencontainers.image.title="Hunter Controller"
LABEL org.opencontainers.image.description="Hunter is a military combat scenario generator for FlightGear placing different types of static and moving targets into Multiplayer. "
LABEL org.opencontainers.image.documentation="https://vanosten.gitlab.io/hunter/installation_server.html"
LABEL org.opencontainers.image.url="https://hub.docker.com/r/vanosten/hunter_container"
LABEL org.opencontainers.image.source="https://gitlab.com/vanosten/hunter"
LABEL org.opencontainers.image.vendor="Private - see maintainer"
LABEL org.opencontainers.image.licenses="GPL-2.0"
LABEL org.opencontainers.image.base.digest="ubuntu:noble"

# Not necessary, but good practice. The container listens over UPD for messages of a
# FlightGear multiplayer server (https://wiki.flightgear.org/FlightGear_Multiplayer_Server)
# using https://wiki.flightgear.org/Multiplayer_protocol.
EXPOSE 5001-5110:5001-5110/udp

# Allow statements and log messages to immediately appear in the native logs
# Will immediately be dumped to the stream instead of being buffered. This is useful for 
# receiving timely log messages and avoiding situations where the application crashes 
# without emitting a relevant message due to the message being "stuck" in a buffer.
ENV PYTHONUNBUFFERED=1

ENV PYTHONPATH=/app

ENV HUNTER_MP_SERVER=x

# See https://dev.to/setevoy/docker-configure-tzdata-and-timezone-during-build-20bk
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install Python and other operating system libs and stuff if Alpine
# libgl1-mesa-dev needed at runtime as solution for 
# ImportError: libGL.so.1: cannot open shared object file: No such file or directory
#
# PyQt5 cannot be installed via pip when building image on macOS, so we can install it via apt-get
# It doesn't need to be removed from the requirements - pip will skip the install since it already sees PyQt5
RUN apt-get update
RUN apt-get install -y libglib2.0-0 python3 python3-dev python3-pip python3-venv proj-bin proj-data

# Install necessary dependencies for NanoMQ
RUN apt-get install -y wget
RUN wget https://www.emqx.com/en/downloads/nanomq/0.23.1/nanomq-0.23.1-linux-amd64.deb
RUN apt install ./nanomq-0.23.1-linux-amd64.deb

# Since we installed PyQt5 with apt-get, we need to fix PYTHONPATH
# We changed it earlier, which removed dist-packages
ENV PYTHONPATH=$PYTHONPATH:/lib/python3/dist-packages

WORKDIR /app

# Adding requirements file plus includes to current directory
# just this file first to cache the pip install step when code changes
COPY hunter/requirements_controller.txt .
COPY hunter/requirements_requests.txt .
COPY hunter/requirements_env.txt .
COPY hunter/requirements_azure.txt .
COPY hunter/requirements_geometry.txt .
COPY hunter/requirements_messaging.txt .

# VENV is a bit tricky in Docker - see https://potiuk.com/to-virtualenv-or-not-to-virtualenv-for-docker-this-is-the-question-6f980d753b46
RUN python3 -m venv /app/venv
RUN /app/venv/bin/python3 -m pip install -r requirements_controller.txt
ENV PATH=/app/venv/bin:${PATH}
ENV VIRTUAL_ENV=/app/venv

# Hunter controller files
COPY hunter/start_controller.py .
COPY hunter/hunter hunter
COPY hunter/scenario_resources scenario_resources

# run from working directory, and separate args in the json syntax
# ENTRYPOINT ["/app/venv/bin/python3", "start_controller.py"]
COPY hunter/docker_start.sh /app/docker_start.sh
COPY hunter/nanomq.conf /app/nanomq.conf
RUN chmod +x /app/docker_start.sh

# Use the script as the entry point
ENTRYPOINT ["/app/docker_start.sh"]
