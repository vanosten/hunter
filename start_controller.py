# SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later
import argparse
import logging
import multiprocessing as mp
import signal
import time


# Global variable such that it can be used in exit handler
global sm_parent_conn


def exit_gracefully(signum, frame) -> None:
    """Makes sure that the child processes in main get a chance to exit gracefully and kill their child processes."""
    global sm_parent_conn
    logging.info('Controller is trying to exit gracefully')
    import hunter.messages as m
    sm_parent_conn.send(m.ExitSession())


if __name__ == '__main__':
    global sm_parent_conn
    signal.signal(signal.SIGINT, exit_gracefully)  # e.g. CTRL-C on Linux
    signal.signal(signal.SIGTERM, exit_gracefully)

    my_parser = argparse.ArgumentParser(description='Hunter runs FlightGear military scenarios over MP - controller')
    my_parser.add_argument('-i', dest='identifier', type=str,
                           help='the identifier of this instance will be used as prefix in callsigns for targets',
                           default='x',
                           required=False)
    my_parser.add_argument("-l", dest="logging_level",
                           help="set logging level. Valid levels are DEBUG, INFO (default), WARNING",
                           required=False)
    my_parser.add_argument('-L', dest='log_to_file', action='store_true',
                           help='set this flag to log to file on top of console (might have performance impact)',
                           default=False, required=False)
    my_parser.add_argument('-c', dest='ctrl_callsign', type=str,
                           help='the callsign used for the controller in MP chat',
                           default='h_', required=False)
    my_parser.add_argument('-s', dest='scenario', type=str,
                           help='the scenario to run (e.g. "swiss" for scenario "scenario_swiss.py")',
                           required=True)
    my_parser.add_argument('-d', dest='scenario_path', type=str,
                           help='the path to the directory where the scenarios live',
                           required=True)
    my_parser.add_argument('-g', dest='gci', action='store_true',
                           help='GCI functionality',
                           default=False, required=False)
    my_parser.add_argument('-o', dest='hostility', type=str,
                           help='''Specify how hostile the environment is. Any number > 0 means that part is shooting.
                           A string separated by "_" -> #_#_#_#_#
                           1st position: helicopter (IR missiles)
                           2nd position: drones (IR missiles)
                           3rd position: ships
                               1 = all ships use only CIWS
                               2 = ca. 50% use only CIWS, the other ca. 50% use missiles [either/or, not both]
                               3 = all ships use only missiles
                               (not available yet: 4 = all ships use both CIWS and missiles)
                           4th position: Shilkas (artillery)
                           5th position: SAMs [only Shilka.IR, S-300, S-3 and BUK-M2 actually shoot missiles - others 
                           (e.g. the S-75) are not modelled and will just emit radar etc. to fool]: 
                               * 0 = nothing shoots;
                               * 1 = replace all SAMs with Shilkas;
                               * 2 = replace all non-shooting SAMs with Shilkas;
                               * 3 = replace all non-shooting SAMs with SA-3;
                               * 4 = all shooting SAMs shoot missiles, rest is fooling..
                           E.g. 0_0_1_1_0 means only ships (using CIWS) and Shilkas are shooting.
                           However, is there are FG-instances (SAMs, automats), then they will shoot no matter what.
                           If the environment is hostile, then ships, drones and helis do not respawn to keep a minimum 
                           number of targets of that category available. Automats respawn according to their 
                           configuration.
                           ''',
                           required=True)
    my_parser.add_argument('-x', dest='cloud', action='store_true',
                           help='''
                           Use cloud integration meaning that data is stored in cloud and can be 
                           accessed through the web application.
                           ''',
                           default=False, required=False)
    my_parser.add_argument('-y', dest='defenders', type=str,
                           help='# separated list (no space) of reserved callsigns for human defenders (act as OPFOR)',
                           default='', required=False)
    my_parser.add_argument('-f', dest='fg_instances', type=int,
                           help='''The maximum number of FG-instances to use in parallel with simulated targets. 
                           This is not available if you are running Hunter in a container - and you have to know 
                           what you are doing and/or ask the maintainer of Hunter. 
                           Spawning automats has priority over SAMs if running out of instances.
                           The default is 0.''',
                           default=0, required=False)
    my_parser.add_argument('-a', dest='admins', type=str,
                           help='''# separated list (no space) of callsigns for admins in this Hunter session.''',
                           default='', required=False)
    my_parser.add_argument('-t', dest='throttle_static', type=int,
                           help='''Throttle the creation of static targets to have less spike of RAM/CPU.
                           Default is 2 seconds between static targets. Larger values result in less spike of RAM/CPU,
                           but also longer time until all static targets are loaded.''',
                           default=2, required=False)
    my_parser.add_argument('-n', dest='nolist', action='store_true',
                           help='''
                           Do not show this session in the list over sessions in the web application.
                           ''',
                           default=False, required=False)

    args = my_parser.parse_args()

    # configure logging
    my_log_level = 'INFO'
    if args.logging_level:
        my_log_level = args.logging_level.upper()

    import hunter.logger_config as lc
    log_config = lc.LogConfig(my_log_level, args.log_to_file)
    lc.configure_logging(log_config, 'StartController')
    logger = lc.get_logger()
    logger.info('Done configuring logging')

    # now that the logger has been configured, we can start importing packages
    # because they in turn will call get_logger - so it needs to be ready
    import hunter.azure_io as aio
    import hunter.utils as u
    import hunter.controller as c

    if args.cloud:
        aio.check_minimal_azure_env_variables()

    if args.defenders:
        defenders = u.parse_callsigns_string(args.defenders, True)
    else:
        defenders = list()

    hostility = u.parse_hostility(args.hostility)  # check that max instances has correct format

    if args.admins:
        admins = u.parse_callsigns_string(args.admins)
    else:
        admins = list()

    sm_parent_conn, sm_child_conn = mp.Pipe()

    controller = c.Controller(sm_child_conn, args.ctrl_callsign,
                              args.identifier, args.gci, args.cloud, args.fg_instances,
                              args.scenario_path, args.scenario, hostility,
                              defenders, admins, args.throttle_static, args.nolist,
                              log_config)
    controller.daemon = False
    time.sleep(10)  # Just to give the controller a bit time to initialize and log messages - not really needed
    controller.start()
