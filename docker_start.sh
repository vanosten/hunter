#!/bin/bash

# Start NanoMQ in the background
nanomq start --conf /app/nanomq.conf &
echo "MQTT broker started"

# Just to give the broker a bit of time to start up, because Hunter needs it to be ready at the very beginning
# And to reduce probability of CPU and RAM spikes
sleep 5

# Start the Python controller process
/app/venv/bin/python3 /app/start_controller.py "$@"
echo "Hunter Controller started"
