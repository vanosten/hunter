# SPDX-FileCopyrightText: (C) 2020 - 2025, rick@vanosten.net
# SPDX-License-Identifier: GPL-2.0-or-later

if __name__ == '__main__':
    import hunter.logger_config as lc
    lc.configure_logging(lc.LogConfig('INFO', False), 'web-ui-local')
    logger = lc.get_logger()
    logger.info('Done configuring logging')

    import hunter.azure_io as aio
    aio.check_minimal_azure_env_variables()
    # This is used when running locally only. When deploying to Azure App
    # Service, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.

    from hunter.web_ui import app  # Need to do import late, otherwise env variable check is too late due to imports
    app.run(host='127.0.0.1', port=8080, debug=True)
