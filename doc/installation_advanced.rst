:orphan:

================================
More Server Installation Options
================================
------------------------------------------------------
For More Experienced Users Knowing What They Are Doing
------------------------------------------------------

..............................................
Option: Creating the Docker Image from Scratch
..............................................

NB: This is not needed in most cases, because a Docker image is already provided. You only need to do this if you do not trust the sources or want to have the latest and greatest versions available.

NB: It might be better to run an `official Docker install <https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository>`_ over using the Ubuntu snap. See e.g. `problem with permissions <https://stackoverflow.com/questions/75651585/docker-permissionerror-errno-13-permission-denied-command-returned-a-non>`_.

* As the working directory choose a directory, which has a local clone of Hunter in a directory called ``hunter``. Otherwise the directory should be empty. This working directory is called ``build context`` when building Docker containers.
* Make sure that the clones are up-to-date.
* Run ``ln -s hunter/Dockerfile Dockerfile`` in the working directory to make a symlink
* Run ``docker build -t hunter_controller:latest .``
* Run ``docker run -p 5001-5110:5001-5110/udp -v /home/vanosten/develop_hunter/hunter-scenarios:/hunter-scenarios hunter_controller -d /hunter-scenarios -s test -o 0_0_0_0_0`` for a local test before publishing
* Publish the image on a public registry:
    * Authenticate with Docker: ``docker login`` and provide your credentials.
    * Tag the image: ``docker tag hunter_controller vanosten/hunter_container:latest``
    * Push to the registry: ``docker push vanosten/hunter_container:latest``
    * For a release tag the image with a number instead of ``latest`` and then push again.


-------------------------------------------------------------
Installation for Developers: Running Hunter from Command Line
-------------------------------------------------------------

Have a look at the ``Dockerfile`` in the root directory. It shows how to install stuff in your local environment - amongst others to create a `Python virtual environment <https://docs.python.org/3/library/venv.html>`_. Alternatively, have a look at the Ansible `playbook <https://gitlab.com/vanosten/hunter-scenarios/-/blob/master/scenario_north_norway.py?ref_type=heads>`_ for the `Hunter-VM <https://gitlab.com/vanosten/hunter-vm>`_.

.................
Main Repositories
.................

There are two repositories to take into account:

* The Hunter program code: https://gitlab.com/vanosten/hunter
* Scenario files for Hunter: https://gitlab.com/vanosten/hunter-scenarios

........
osm2city
........

Not required at runtime. In order to create routes etc. for scenarios, you need to have `osm2city <https://gitlab.com/osm2city/osm2city>`_ using the ``ws30`` branch.

..........
FlightGear
..........

There are no runtime dependencies.

.....................
Environment Variables
.....................

Hunter reads a ``.env`` file from the project root directory. Alternatively, add environment variable to your shell environment. A ``.env`` file could have the following content:

.. code-block:: bash

    FG_INSTALL=${HOME}/bin/flightgear/dnc-managed/install
    FG_ROOT=/home/vanosten/bin/flightgear/dnc-managed/install/flightgear/fgdata
    FG_BIN=/home/vanosten/bin/flightgear/dnc-managed/install/flightgear/bin
    FG_TERRASYNC=/home/vanosten/.fgfs/TerraSync
    FG_AIRCRAFT=/home/vanosten/bin/flightgear/customaircraft:/home/vanosten/.fgfs/Aircraft/org.flightgear.fgaddon.trunk:/home/vanosten/bin/flightgear/customaircraft/oprf_admin_assets
    LD_LIBRARY_PATH=${FG_INSTALL}/simgear/lib:${FG_INSTALL}/openscenegraph/lib:${FG_INSTALL}/openrti/lib:${FG_INSTALL}/plib/lib:${LD_LIBRARY_PATH}

    # osm2city stuff - also needs LD_LIBARY_PATH and FG_ROOT like set above
    O2C_PATH_TO_FG_ELEV=${FG_INSTALL}/flightgear/bin/fgelev
    O2C_OVERPASS_API=https://overpass-api.de/api/interpreter


    # Hunter mpserver.opredflag.com
    HUNTER_MP_SERVER=mpserver.opredflag.com

    # Testing
    HUNTER_SCENARIOS_PATH=${HOME}/develop_hunter/hunter-scenarios
    HUNTER_SCENARIO_NAME=test

    AZ_COSMOS_ACCOUNT_URI=https://aaaaaaaaa.documents.azure.com:443/
    AZ_COSMOS_ACCOUNT_KEY=aaaaaaaaa==

``O2C_*`` is only needed to generate networks etc. for scenarios with ``osm2city``. ``AZ_*`` is needed for sending statistics to the cloud (Azure). If you want to do so, then contact the maintainer of Hunter. ``FG_*`` is needed if you run FG instances as slaves to Hunter on the same computer.


-----------
Azure Stuff
-----------

The Hunter web-ui, the database and an optional VM run on `Microsoft Azure <https://azure.microsoft.com/>`_. The repo `Hunter-VM <https://gitlab.com/vanosten/hunter-vm>`_ contains the details.
