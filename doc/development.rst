:orphan:

===========
Development
===========


-----------------
Program Structure
-----------------
* Multi-processing over multi-threading has been chosen for several reasons, amongst others:

  * Possibility to combine pure MP assets with running / controlling FG-instances using the same processing model
  * Most processing is CPU bound and/or it is easier to understand sockets per process vs. per thread
  * Inter-process communication is more explicit using multi-processing - and therefore easier to understand
  * No hassle with Python's GIL
  * If a process (apart from the controller) crashes, then only one target is lost
  * A not verified believe that sockets either work better or can only work for several incoming and outgoing MP ports if done in separate processes per MP object.


MPSessionManager manages the bi-directional communication between:

* the user interface and the session
* the session and targets

MPSessionManager is a sub-process of the main thread. Targets are sub-processes of the session. All communication is done with multiprocessing.Pipe channels.

Each session manager has one special MPTarget of type ``receiver``, which is not visible to attackers. The receiver registers with the MP server with the solve purpose of listening to all MP chat message. If a chat message contains shooting info, then it is sent to the session manager, which then re-directs for final interpretation to the specific target's damage processing.


-----------
Other Notes
-----------
* The targets do not have any properties telling whether they can be hit by e.g. air munition vs. ground ammunition or cannon. The attacking planes' logic is trusted. A target can be made invulnerable (damage returns always Health.missed) or the max_hp can just be set to a very high number (e.g. 1'0000'000 requiring over 100'000 hits by a cannon before getting broken, but still returning Health.hit)
* Towing targets: Using the towing plane's movement properties with a delay in the towed target did not work. The delay increases a bit over time due to processing time (maybe 0.01 seconds each time), which is enough to make it flickery (the towing plane is also not exact due to processing maybe every 1.01 seconds when it is assumed to be 1 second - plus the time it takes for the pipe between the processes to forward the message). Those two things together make it look wrong. Therefore, the towed plane just uses the same network, start point and start after a delay - using the tractor's (instead of its own) flight model parameters (cruise speed, acceleration, turn rate etc.). The pre-requisite for this is that the network is a directed (and closed loop) graph.
* Network objects: nodes are always geometry.WayPoints. There is no special object for edges, but there are can be two attributes, which are used in moving targets using trips: ``cost`` for pre-calculated weights for Dijkstra shortest path algorithms as well as ``max_speed`` in m/s. Networkx Dijkstra assumes 1 if the cost attribute is not present. If the max_speed attribute is not present or 0, then there are no speed limits.
* The generated road/rail networks are not directly using osm2city code for several reasons: First because a network should not be constrained to a tile and second because osm2city removes/simplifies some info (e.g. highway/rail types) and stuff (e.g. tunnels) and finally because it is in local coordinates and would complicate the logic in roads.py. Instead Hunter has its own logic, duplicates the logic to add additional nodes for elevations and then gets the elevation simply by probing directly on top of a osm2city scenery (incl. bridges) - apart from tunnels, where it interpolates start and stop for inbetween nodes (in the tunnel) - because tunnels are needed.

--------------------------
Updating the Documentation
--------------------------

Follow the following for the end-user part:

* Create the html files for local testing:
    * Change directory to ``hunter\doc``
    * Issue ``sphinx-build -b html . build``
    * Change directory up ``cd ..``
* Deployment is done manually by pushing the ``*pages`` job in GitLab CI/CD to GitLab Pages if it is pushed to main.

------------------------------
Updating tags etc. for release
------------------------------

First update the changelog and commit.

Then update the following tags:

* Gitlab: go to Repository - Tags: Choose new tag and just fill out the number part. It will use the latest commit by default.
* While building a Docker image tag it not only with ``latest`` but also a version number.


---------------
Web application
---------------

Miscellaneous points:

* Map displays are using `Leafletjs <https://leafletjs.com/>`_ (version 1.9.4). The `Leafletjs providers javascript <https://github.com/leaflet-extras/leaflet-providers>`_ is version is commit `49b2501 <https://github.com/leaflet-extras/leaflet-providers/commit/49b250162aa1c9373aee5ed1ce841cf80394ec5a>`_.
* The coordinate controls are derived from `Leaflet-Coordinate_controls <https://github.com/zimmicz/Leaflet-Coordinates-Control>`_ version 0d21a1c using an `MIT License <https://github.com/zimmicz/Leaflet-Coordinates-Control/blob/master/LICENSE.md>`_. The only change from the original is that lon and lat have been switched around.
* How to deploy the web-app is described in the ``hunter-vm`` repo.

------------
GitLab CI/CD
------------

To install a local runner on the development workstation:

* Install manually from a deb-package cf. https://docs.gitlab.com/runner/install/linux-manually.html. The `official repository <https://docs.gitlab.com/runner/install/linux-repository.html>`_ does not support e.g. Ubuntu/Lunar as of summer 2023.
* In the `Hunter GitLab repo <https://gitlab.com/vanosten/hunter/-/settings/ci_cd>`_ go to ``Settings/CI CD`` and expand section ``Runners``. Press the ``New Project runner`` button and use the token etc. from there to register the installed local runner. Use tag ``linux_local``.


--------------------
Components in Hunter
--------------------

Hunter is handled as a mono-repo, but contains several components as follows:

==================== ========================== ================================================================= =================================================================
Component            Directory                  Execution environment                                             Remarks
==================== ========================== ================================================================= =================================================================
Documentation        ``/doc``                   `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_  Statically built with Sphinx
                                                at https://vanosten.gitlab.io/hunter
Web-ui               ``/hunter/web_ui``         Flask web app (running on Azure App Service)                      Can also be run locally by means of ``start_web_ui.py``
Hunter Controller    ``/hunter``                A container image ("Docker") runtime environment                  The main multi-player application as the core of the project.
                                                (preferably on Linux)                                             Can also be run locally by means of ``start_controller.py``.
Hunter Worker        ``/hunter/fg_*.py``        Local Python 3.1x as a sub-process of the Controller.
Scenario tools       ``/hunter/scenario_tools`` Local Python 3.1x with a main method per module
Database             ``/hunter/azure_io.py``    Azure Cosmos DB NoSQL integration
==================== ========================== ================================================================= =================================================================

---------------------------
Architecture Considerations
---------------------------

Hunter has from the beginning been designed as an event based system. While Python inter-process communication is convenient for messaging (fast, built in using Python types/classes, no additional service running):

* observability is poor
* pub/sub needs to be hard coded with logic (incl. filtering) in the broker
* monolithic: (A) no possibility to use different programming languages - or Python processes not spawned from the Controller; (B) constrained to 1 computer (only vertical scalability)

Therefore, event based messaging is taken out and the different (Python) processes like Controller, mp_targets, fg_targets, Azure Cosmos DB integration etc. will now pub/sub with a broker and "live on their own" in processes - i.e. not anymore be directly connected with the Controller. This essentially getting closer to the `High-Level Architecture (HLA) standard for distributed simulation <https://en.wikipedia.org/wiki/High_Level_Architecture>`_, where the `run-time infrastructure (RTI) <https://en.wikipedia.org/wiki/Run-time_infrastructure_(simulation)>`_ takes over - amongst others - the messaging.

In FG the use of HLA has been `discussed and experimented with <https://wiki.flightgear.org/High-Level_Architecture>`_ using the open source software `OpenRTI <https://sourceforge.net/p/openrti/wiki/Home/>`_ and associated `SimKit <https://sourceforge.net/projects/sim-kit/>`_ created and maintained by a FG core developer. Therefore, HLA is an obvious candidate for the architecture of Hunter. There are an `extensive HLA tutorial as well as other resources <https://pitchtechnologies.com/HLATUTORIAL/>`_ available from Pitch Technologies having their own commercial solution.

An alternative is `distributed Interactive Simulation (DIS) <https://en.wikipedia.org/wiki/Distributed_Interactive_Simulation>`_, which in the `DIS-tutorial <https://github.com/open-dis/dis-tutorial>`_ also contains a `good comparison between HLA, DIS and others <https://github.com/open-dis/dis-tutorial/blob/master/DoDModelingAndSimulationStandards.md>`_. With `Open DIS <http://open-dis.org/>`_ there is also well-maintained open source implementation with many language bindings (incl. C++ and Python).

However, both DIS and HLA come with some complexity and learning-curve, which are due to a lot of capabilities, which Hunter in its current state does not need. Therefore, a more simplified approach is taken:

* Interaction between different MP instances (simulated in Hunter, real pilots) is done through the standard FG MP (including Emesary). This of course necessitates the use of MP even if you are a single player using Hunter (same as has been the case for Hunter all the time).
* Interaction between the different Hunter services incl. the Controller is over a pub/sub protocol using an external broker - especially because the number of participants (e.g. mp_targets) cannot be known.

In order to allow many client programming languages (incl. Python and C++) to be used as well as cloud native brokers (e.g. Azure Event Grid) and OSS brokers, the `MQTT <https://mqtt.org/>`_ protocol could be used.

Choice of MQTT broker (see also a `more complete list <https://mqtt.org/software/>`_):

======================== ==================== ==================== ==================== ==================== ====================
Criteria                 RabbitMQ             Mosquitto            NanoMQ               NATS                 RocketMQ
======================== ==================== ==================== ==================== ==================== ====================
Part of CNCF             yes                  no                   no                   yes                  yes
Available in Ubuntu 24.x yes                  yes                  (as deb-package)     no                   no
Remarks                  Using plugin         native               native               needs Jetstream      Needs JDK /
                                                                                                             and extension
======================== ==================== ==================== ==================== ==================== ====================

Alternatively some of the popular OSS messaging platforms could be used. `Kafka vs. Pulsar vs. RabbitMQ: Performance, Architecture, and Features Compared <https://www.confluent.io/kafka-vs-pulsar/?utm_medium=sem&utm_source=google&utm_campaign=ch.sem_br.nonbrand_tp.prs_tgt.kafka_mt.xct_rgn.emea_lng.eng_dv.all_con.kafka-pulsar&utm_term=kafka%20vs%20pulsar&creative=&device=c&placement=&gad_source=1&gclid=CjwKCAjwqre1BhAqEiwA7g9QhlEL7h40a-ETbcNNgIIcelwgy1thqwABxw6Om91Hp2ex6qCMz8dyixoC6FsQAvD_BwE>`_ provides a good overview. Given that there in Hunter only will be maybe 100s of messages per second, high throughput is not very important - operational (e.g. number of components) simplicity is important. The `Redpanda <https://www.redpanda.com/>`_ Community Edition installed on a single node could be a good alternative to Kafka due to deployment simplicity and performance.

The article `MQTT vs asynchronous streaming tools <https://medium.com/@kenzahassanine1/mqtt-vs-asynchronous-streaming-tools-23345a867b55>`_ provides a good comparison - the Hunter use case is closer to IoT discrete messages without data processing etc. - i.e. MQTT over streaming.

.. https://snapcraft.io/mqttx, https://nanomq.io/docs/en/latest/quick-start/quick-start.html
   https://github.com/eclipse/paho.mqtt.python, https://eclipse.dev/paho/files/paho.mqtt.python/html/index.html
   $ nanomq start

